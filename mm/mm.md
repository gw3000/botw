# Zelda – Majora's Mask

## Kaptilel 1: DER WEG IN DIE FREIHEIT

Es war einmal in einem fernen Land… 

Nach einem langen Ritt durch die Wälder Hyrules, bist du auf dem Rücken von Epona ein bisschen 
eingenickt. Nichts ahnend, reitest du immer weiter, bis du schließlich gewaltsam vom Pferd geworfen 
wirst. Es sind zwei Feen, die Epona so erschreckt haben, dass sie wild aufgesprungen ist.  Aus weiterer 
Entfernung nähert sich jetzt eine mysteriöse Kreatur mit einer farbigen Maske. Es scheint fast so, als 
wäre das Geschöpf das Horror-Kid aus den Verlorenen Wäldern. Langsam kommt es auf dich und Epona 
zu, dreht dich auf die andere Seite und durchsucht deine Taschen nach etwas Wertvollem. Schnell wird 
es fündig und nimmt die Okarina der Zeit an sich. Von den Tönen die es nun spielt, wirst du langsam 
wach und realisierst erst jetzt, dass das deine Okarina ist. 

Als das Horror-Kid bemerkt, dass du wach bist, versteckt es schnell die Okarina hinter seinem Rücken. 
Ohne viel zu reden, greifst du nach dem Dieb und willst ihm dein Instrument wieder abnehmen. Doch 
das Horror-Kid weicht blitzschnell aus und springt auf dein Pferd, welches nun wild davon springt. Mit 
einem großen Sprung, kannst du dich noch rechtzeitig am Fuss des Diebes festhalten. 

Nach einer wilden Tour kreuz und quer durch den Wald, wirst du in einer Kurve auf den Boden geworfen. 
Der Dieb kann nun durch einen Baumstumpf flüchten, während du dich noch versuchst wieder auf die 
Beine zu kriegen. Du solltest nun deine Beine unter die Arme nehmen und den Dieb verfolgen. Dein Weg 
führt dich durch den Baumstumpf und direkt auf eine Lichtung mit unterschiedlich großen 
Holzelementen. Springe bis ganz nach oben, wo du nun wieder in einen dunklen Gang kommst. Dein 
Wille treibt dich zu Höchstleistungen. Doch vor einem Abgrund kannst du plötzlich nicht mehr bremsen 
und fällst in das tiefe Loch. Doch zum Glück landest du weich - und zwar auf einer Deku-Blume. Nicht 
lange wirst du allein gelassen. Denn gleich springen Scheinwerfer an und im grellen Licht erkennst du 
den Pferde- und Okarina-Dieb. 

Mit Freude erzählt dir das Horror-Kid, dass es dir die Bürde mit Epona abgenommen hätte, da das Pferd 
einfach nicht gehorchen wollte. Langsam steigt die Wut in dir, doch das Horror-Kid hat mehr mit dir vor. 
Plötzlich beginnt seine Maske zu leuchten. Ein seltsamer Traum setzt ein, wo du von lauter Laubkerlen 
umzingelt bist und dann auch noch von einem riesigen Laubkerl verfolgt wirst. 

Als du aufwachst, traust du deinem Spiegelbild auf der Wasseroberfläche vor dir nicht. Dein Gesicht, dein 
Körper sieht nun aus wie der Körper von einem Deku. Mit einem schaden freudigen Lachen, verschwindet 
das Horror-Kid langsam hinter dem großen Tor. Als du versuchst ihm nachzulaufen, wirst du von einer 
seiner Feen zurück gestoßen. Schnell schließt sich das Tor wieder und die zwei Feengeschwister werden 
von einander getrennt. 

Von der kleinen Fee wirst du nun beschimpft und beschuldigt, das alles nur wegen dir geschehen sei. Als 
Dank öffnest du ihr die große Tür und folgst dem Korridor. Er bringt dich zu einem neuen Raum, wo du 
eine Deku-Blume vorfindest. Wenn du jetzt den "A"-Knopf gedrückt hältst, vergräbst du dich in der 
Blume und wirst beim Loslassen des Knopfes wieder an die Oberfläche geschleudert. Doch nicht nur das: 
Jetzt kannst du mit Hilfe von zwei rotierenden Blüten für kurze Zeit durch die Lüfte schweben. 

Gleite also auf die andere Seite, wo du gleich die Tür öffnest. Ein großer und dunkler Raum öffnet sich 
dir. Vor dir siehst du verschiedene Plattformen mit Deku-Blumen. So musst du gleich von Blume zu 
Blume fliegen, um zum Ende des Raumes zu gelangen. Auf deinem Weg von Insel zu Insel findest du in 
einer Truhe deine erste Packung Deku-Nüsse. Du kannst sie mit Hilfe des "B"-Knopfes abwerfen, wenn 
du in der Luft schwebst. Aber auch auf dem Boden betäuben die Nüsse Gegner für einen kurzen Moment. 
Der Angriff kann also besser durchgeführt werden. 

Mach dich nun auf zum Ende des Raumes, wo dich Taya kurz anspricht. Denn mit dem "Z"-Knopf kannst 
du alles anvisieren, wo Taya ist. Du erhältst so einige wichtige Informationen (abgesehen von diesem 
Beispiel mit dem Baum). 

Dein Weg führt dich weiter durch einen ganz verdrehten Korridor. Weiter vorne gelangst du nun so zu 
sagen in den Bauch der Uhr von Unruh-Stadt. Sobald du über die Brücken nach oben gehst und gerade 
die große Tür aufschieben willst, wirst du von einer mysteriösen Person angesprochen. 

Es ist der Maskenhändler aus Hyrule, für den du auch schon Geschäfte abgeschlossen hast. Er schliesst 
mit dir nun einen Deal ab: Du sollst ihm die Okarina beschaffen, die dir der Kobold gestohlen hat und er 
würde dich damit zurückverwandeln. Doch im Gegenzug verlangt der Maskenhändler von dir die Maske 
des Horror-Kids. Auch diese hat es sich illegal angeeignet. Das ganze Geschäft ist sehr heikel, da der 
Maskenhändler bereits in drei Tagen wieder aus Unruh-Stadt abreisen will. Auch ein Maskenhändler hat 
Verpflichtungen. So musst du ihm seine Maske innerhalb von den drei Tagen beschaffen. Hast du das 
geregelt, so geh durch die große Tür nach draußen. Der erste Tag in Unruh-Stadt bricht an. Taya spricht 
dich auf die Große Fee in der Nähe des Nord-Tors an. Mach dich gleich auf den Weg dorthin...

## Kaptilel 2: DER WEG IN DIE FREIHEIT 

Willkommen in Unruh-Stadt. Insgesamt 72 Stunden verblieben dir junger Held, dem Maskenhändler 
seine Maske zurück zu bringen und das HorrorKid zu finden. Und schnell wirst du merken – 72 Stunden 
sind nicht sehr viel Zeit. In Links Zeit bedeuten 72 Stunden für dich nicht mehr als 72 Minuten in 
Echtzeit, daher spute dich. Link wird in kommenden Verlauf noch kleine Tricks erlernen, die Zeit weiter 
zu verlangsamen und vorzuspulen, doch auch diese halten diese nicht auf Dauer auf. Es ist sinnvoll, das 
Spiel mithilfe von „Start“ zu unterbrechen, solltest du mal nicht weiter kommen oder eine Denkpause 
benötigen. Los geht's!

Kaum aus dem Turm, gibt dir deine neue Feebegleiterin Taya bereits Rat, was du als Erstes tun solltest: 
Die große Fee kann dir vielleicht helfen, was dir auf deiner Suche nach HorrorKid helfen könnte. Bevor du 
aber in den Norden von Stadt Unruh aufbrichst, gehe folgend vor: Direkt in deiner Sicht geradeaus siehst 
du einen Arbeiter, der nach oben zu seinem Kameraden winkt, daneben befindet sich rechts ein 
Briefkasten und ein aufsteigender Treppenweg, der nach oben rechts in eine Seitengasse führt. Laufe zu 
diesem aufsteigenden Weg, ignoriere den Hund und du wirst bemerken, dass ein blau-haariger 
maskierter Junge von dir davon flieht. Folge ihm in die Seitengasse, indem du den Weg hoch läufst und 
rechts den neuen Abschnitt betrittst. 

Hier am Waschplatz der Stadt wird der Junge an dir vorbei flitzen und hinter einer entfernten Tür 
verschwinden. Im Augenblick ist er nicht für dich relevant, aber merke dir sein mysteriöses Verhalten. 
Gehe den Weg hinab und schaue nach links: Du entdeckst ein goldenes leuchtendes kleines Wesen, dass 
über der Wasseroberfläche schwimmt. Hierzu gehe einfach die hölzernen Unterstufen vorne neben dem 
Baum hinab – denn sobald du das Wasser betrittst, wird Dekulink springen und das kleine Wesen 
einsacken. Hast du es berührt, bittet es um deine Hilfe – es möchte zur großen Fee am Nordtor von 
Stadt Unruh gebracht werden. Nachdem du ihr Flehen erhört hast, kannst du den Waschplatz verlassen, 
indem du zurückgehst.*

Wieder in Süd-Unruh-Stadt, gehe den kleinen Treppenweg hinab und laufe stets weiter geradeaus, direkt 
links an dem Turm vorbei aus dem du gekommen bist. Laufe den Steg weiter hoch, an der Eulenstatue 
rechts vorbei weiter geradeaus. Hinter dem Turm führt ein weiterer Weg nach oben zu einem Durchgang, 
durch diesen du gehst. 

Nun befindest du dich in Nord-Unruh-Stadt. Gehe jetzt nach links, an dem schießenden Jungen vorbei 
und laufe den Weg hoch, der dich direkt zu einem Höhleneingang führt. Gehe hinein. Dort, laufe weiter 
die Passage nach vorne um dein kleines Feenwesen zu ihren Artgenossen zurück zu bringen. Sie 
vereinen sich kurzerhand zur Großen Fee. Sie verleiht dir die Gabe der Magie, mit der Dekulink nun in 
der Lage ist, mithilfe des B-Knopfs Seifenblasen zu erzeugen, die als Angriffsgeschoss gegen Feinde und 
andere Objekte benutzt werden können. Sie gibt dir Rat, nach einen Mann in einem Observatorium 
außerhalb Stadt Unruhs zu suchen. Er könnte dir mehr über den Verbleib des HorrorKids sagen. Merke 
dir schon einmal, später in deiner richtigen Gestalt wieder zu ihr zurück zu kommen. Da du nun alles 
hast, verlasse ihren Schrein.

Deine neu verliehene Fähigkeit wollen wir doch gleich mal testen. Kaum hast du die Feenquelle 
verlassen, begebe dich zum kleinen Jungen hier in Nord-Unruh-Stadt. Er versucht mit einem Blasrohr 
einen Violetten Ballon abzuschießen, dessen Bildnis du kennst. Nutze den B-Knopf, halte ihn gedrückt um 
eine große Blase zu erzeugen. Schieße sie anschließend in Richtung des Ballons. Kaum ist dieser 
zerplatzt, wird der Junge verwirrt um sich schauen. Spreche ihn an und er wird sich dir als Mitglied der 
Bombergang vorstellen. Sie besitzen ein Versteck, von dem aus man ins Observatorium von Stadt-Unruh 
gelangt. Aber so einfach will er es dir nicht machen: Für den richtigen Code des Verstecks musst du 
natürlich den Aufnahmetest der Bomber bestehen. Stimme zu, den Test mitzumachen.

Die Bomberjungs bestehen aus 5 Mitgliedern; der Aufnahmetest besteht darin, sie alle in der Stadt zu 
finden und einzufangen – und das bis zum frühen Morgen des nächsten Tages! Solltest du bis dahin nicht 
so viel Zeit haben, warte besser zum nächsten Morgen. Diese Aufgabe ist leichter gesagt als getan. 
Kaum sind sie verschwunden, halte dich an folgende Beschreibung. Tipp: Du kannst die Kinder auch mit 
Blasen abschießen. Wurden sie getroffen, sind sie eine kurze Zeit bewusstlos.

# Bomberkind 1
Bleibe erst einmal in Nord-Unruh-Stadt. Du wirst vor dir auf der gegenüberliegenden Seite der 
Feenquelle eine Art Kinderrutsche entdecken. Gehe dahinter um den ersten Bomber schnell zu 
überraschen, indem du mit A in einer Drehung auf ihn zueilst. Ist er gefangen, bleiben nur noch 4. 

# Bomberkind 2
Auch dieses befindet sich in Nord-Unruh Stadt. Es befindet sich hinter einem Baum, der links unten vom 
Eingang zur Großen Fee befindet. Fange auch dieses Kind ein und es verbleiben nur noch 3. 

# Bomberkind 3
Nun gehe von Nord-Unruh-Stadt in das Seitentor, was sich neben der Rutsche bzw. rechts vom jenen Tor 
befindet, aus dem du vorhin gekommen bist. Du gelangst nach Ost-Unruh-Stadt. Der kleine Junge direkt 
vor dir, der eine kleine Gasse bewacht, kannst du hier getrost ignorieren. Er bewacht nur den Eingang 
des Verstecks. Gehe aber trotzdem zu ihm hin und schaue nach rechts – ein Treppenaufgang führt zum 
Dach eines Hauses hinauf. Gehe diesen Hoch und du entdeckst einen weiteren Bomberjungen. Er fliegt 
dir aber mit einem Huhn weg. Springe ihm trotzdem nach und verfolge hin bis du ihn hast.

# Bomberkind 4
Bleibe in Ost-Unruh Stadt. Hier suche die beiden tänzelnden Männer auf, die mit bunten Bällen werfen. 
Vom Mann mit roter Tracht nutze Links Rundumsicht und schau dich in deiner Gegend um: Hier erkennst 
du oben auf einem Strohdach einen weiteren Bomberjungen, der in der Nähe einer großen weißen Glocke 
auf und ab geht. Hierzu musst du nur unter das besagte Strohdach umher laufen, damit der Junge 
unruhig wird und von seinem gesicherten Posten zu dir hinab springt. Fange auch ihn ein.

# Bomberkind 5
Das letzte Kind befindet sich in West-Unruh-Stadt. Begebe dich von Ost-Unruh-Stadt neben dem 
Schatztruhen-Haus die große Treppe hinab, die dich nach Süd-Unruh-Stadt führt. Hier laufe weiter 
geradeaus zum Tor auf der anderen Seite, das rechts vom Aufgang zum Waschplatz sich befindet. Nun 
gelangst du nach West-Unruh-Stadt. Laufe hier einfach die Treppenstufen hoch um schließlich auf dem 
Platz den letzten Bomber anzutreffen. Fange ihn ein und du hast es geschafft. 

Sind alle Kinder eingefangen, befindest du dich wieder in Nord-Unruh-Stadt. Aufgrund schlechter 
Erfahrungen mit einem anderem Deku-Kerl wollen sie keine Dekus in ihre Gang aufnehmen – aber den 
Code verraten sie dir trotzdem. (Dieser Code ist in jedem Spiel anders, also solltest du nicht den hier 
verwendeten Code nehmen) . Merke dir, dass du als Mensch später einmal zu den Bombern noch einmal 
zurückkehren solltest, um ein Notizbuch von ihnen zu erhalten. Du wirst es im Verlauf des Spiels noch 
gut gebrauchen können. 

Ihr Versteck befindet sich in Ost-Unruh-Stadt und dahin geht’s wieder hin. Von Nord-Unruh-Stadt laufe 
wieder durch das Seitentor neben der Rutsche um nach Ost-Unruh-Stadt zu gelangen. Renne 
anschließend geradeaus zum Jungen, der den Eingang zum Versteck bewacht. Hier spreche ihn an und 
gebe den richtigen Code ein, damit er dir Platz macht. Betrete anschließend das Bomberversteck.

Düster und nicht gerade friedlich sieht es hier aus – laufe mit Dekulink weiter runter, bis du die Fackel 
erreichst. Dahinter befindet sich ein Abwasserkanal. Dekulink kann nicht schwimmen, aber für eine kurze 
Zeit auf dem Wasser springen. Von der Fackel aus schaue nach links, um einen weiteren Weg zu 
entdecken. Er führt zum Teil über Wasser, sodass du nur zu den einzelnen Inseln herüber springen 
musst. Hast du in der Ferne eine zweite Fackel erreicht, springe dahinter auf den Weg. Bewege dich nun 
nach links weiter aber Vorsicht: Schaue nach oben und du siehst, das etwas dort sich bewegt. Näherst du 
dich diesem, krabbelt etwas herunter – eine Riesenskulltula. Nehme sie mit Z ins Fadenkreuz, halte eine 
Seifenblase mit dem B-Knopf gedrückt und schieße sie ab, sobald die Spinne ihre Bauchseite zeigt. 
Besiegt, löst sie sich in Rauch auf.

Gehe nun weiter. Direkt hinter der Spinne befindet sich eine Abzweigung nach rechts, die du nimmst. 
Folgst du diesen Weg, entdeckst du schon bald einen dir bekannten Ballon. Näher dich diesem und 
schieße ihn ab. Der Weg zu einer Leiter dahinter wird frei. Klettere die Leiter hoch und laufe die Passage 
entlang. Die Musik ändert sich nun und du hast dein Ziel, das Observatorium, erreicht. Hier schaue dich 
erst mal um. Entdecke die Vogelscheuche im hinteren Teil des Raumes und spreche sie an. Lehne ab, mit 
ihr in den nächsten Morgen oder Nacht zu tanzen. Trotzdem nennt sie dir einen nützlichen Hinweis: Er 
kennt ein geheimnisvolles Lied, mit dessen Kraft sich die Zeit manipulieren lässt. Die genauen Noten 
kann er dir nicht nennen, noch hast du ein Instrument, mit dem du es spielen kannst. Merke dir 
trotzdem: Spielst du das Lied rückwärts, verlangsamst du die Zeit. Spielst du jede Note von ihr Doppelt, 
kannst du einen halben Tag sofort überspringen. Übrigens, mit der Vogelscheuche kannst du auch gleich 
die Zeit einen halben Tag verstreichen lassen. Du kannst die Vogelscheuche einmal hier bei der 
Sternwarte als auch im Gemischtwarenladen in West-Unruh-Stadt finden.

Lasse von der Vogelscheuche ab und gehe nun den bunten Treppenaufgang nach oben. Spreche den 
alten Mann vor dem Teleskop an und stimme zu, ein wenig durch dieses zu schauen. Lenke nun dein 
Blickfeld nach oben auf den Stadtturm – dort zoome heran um das HorrorKid zu finden. Es befindet sich 
also auf der Spitze des Stadtturms, dessen Eingang sich nur am Vorabend des Karnevals öffnet – das ist 
der Abend des Dritten Tages. Beobachte weiter, wie vom Mond eine Mondträne fällt. Anschließend wird 
dich der alte Mann bitten, doch einmal draußen nachzuschauen. Gehe in diesem Raum zur Türe und 
schau dich draußen um. Du findest tatsächlich einen leuchtenden Gegenstand, den Dekulink als 
Mondträne aufhebt. 

Mit dieser Mondträne in Besitz kannst du nun vom alten Mann ablassen. Bis zum Abend des dritten Tages 
musst du nun warten. Entweder kannst du nun die restliche Zeit frei nutzen um dir die Stadt und ihre 
Bewohner genauer anzusehen oder du tanzt mit der Vogelscheuche bis zum Abend des letzten Tages. 
Begebe dich dann zum Karnevalsturm von Stadt Unruh. Es ist derselbe Turm, aus dem du gekommen 
bist und in dessen Uhrwerk der Maskenhändler auf dich wartet. Aber der Weg zur Spitze de Turms ist 
verschlossen! Richtig, er öffnet sich erst Mitternacht des Letzten Tages. Dieser Eingang befindet sich 
oben auf einer Vorplatte des Turms über dem Eingang zum Uhrwerk. Normal kannst du diese Plattform 
nicht erreichen. Aber du hast die Mondträne und deswegen begebe dich rechts, vom Eingang des 
Uhrturms aus gesehen, zu einer gelben Dekublume. Sie befindet sich zwischen den beiden 
Seiteneingängen nach West-Unruh-Stadt. Ein Deku bewohnt diese. Setze die Mondträne auf einen C-
Knopf deiner Wahl, indem du auf START drückst und den Gegenstand mit dem C-Knopf auswählst. 
Spreche mit dem Deku und übergebe ihm die Mondträne. Er übergibt dir im Gegenzug die Urkunde. Mit 
dieser Blume gerüstet kannst du bequem die Plattform über den Eingang zum Uhrwerk des Turmes 
erreichen und die restliche Zeit totschlagen.

Kaum schlägt die Uhr Mitternacht, öffnet sich der Weg zur Turmspitze. Fliege mit der Dekublume zur 
Vorplatte. Nehme das Herzteil an dich und gehe durch die kleine dunkle Pforte des Turms. 

Oben angekommen, erfolgt ein kleiner Dialog deiner Fee Taya, ihrem Bruder Tael und dem HorrorKid. 
Dessen wahren Absichten werden nun deutlich – es möchte den Mond auf die Erde stürzen lassen. Das 
kannst du wohl schlecht zulassen... aber was tun? Sobald die 5 Minuten Restzeit laufen, schieße eine 
große Blase auf das HorrorKid, genauso wie du das bei den Ballons geübt hast. Wurde es getroffen, fällt 
es ihm deine Ocarina hinab: Die Ocarina der Zeit! Hebe sie auf, um dich an einen Moment deiner Abreise 
zu erinnern: Prinzessin Zelda übergab dir dieses Instrument und mit ihr lerntest du einst ein wichtiges 
Lied: Die Hymne der Zeit. 

Nachdem du sie in deiner Erinnerung erlernt hast, platziere die Ocarina auf einen der C-Knöpfe und 
spiele die Hymne der Zeit. Speichere hier und kehre zur Dämmerung des ersten Tages zurück. Du 
verlierst Gegenstände wie Rubine oder Dekunüsse, ein jedes Mal, wenn du die Zeit von nun an zurück 
spulst. Aber darum solltest du dir jetzt keine Sorgen machen. 

Hat der erste Tag von neuem begonnen, wird Taya dich an den Maskenhändler erinnern. Gehe also zum 
Uhrturm direkt vor dir und laufe gegen die Tür, um einzutreten. Spreche den Maskenhändler an... der dir 
gleich abkauft, dass du seine Maske gefunden hast. Erlerne das Lied der Befreiung und hol dir 
anschließend die entsprechende Abreibung. Seine Maske sollst du natürlich immer noch suchen. Zurück 
in deiner alten Gestalt, geht das doch zumindest viel besser. Du erhältst fortan die Deku-Schale: Mit ihr 
kannst du dich jederzeit in Dekulink verwandeln und seine besonderen Spezialfähigkeiten nutzen. Nun, 
wo du weißt, welch schrecklicher Fluch Majora’s Maske anhaftet, ist dir klar was zu tun ist: die Bewohner 
dieses Landes vor der Katastrophe zu bewahren und HorrorKid die Maske abzunehmen, bevor er noch 
weiter Unheil anrichtet.

* Die kleine Fee kann je nach Uhrzeit an zwei verschiedenen Positionen gefunden werden: Tagsüber 
befindet sie sich an allen drei Tagen am Waschplatz, wie es die Lösung vorgibt. Nachtsüber wechselt sie 
nach Ost-Unruh-Stadt. Hierzu nutze die Dekublume in der Nähe des Eingangs zum Schatztruhen-Haus, 
um an ihren erhöhten Standort zu gelangen. Du musst die Fee berühren, damit du sie eingesackt hast.

## Kaptilel 3: ERSTE SCHRITTE 

Hast du den Turm verlassen, erinnert dich Taya an die Worte ihres Bruders. Von vier Gegenden hat er 
gesprochen, doch was meinte er genau? Sie gibt dir Rat, zunächst die Sümpfe aufzusuchen.

Bevor du dies tust, erledige erst ein paar Dinge vorab, die du auf deiner Reise gut gebrauchen kannst. 
Erinnerst du dich an die große Fee? Gehe zurück zum Waschplatz und schnappe dir die kleine goldene 
Fee. Bringe sie zurück zur Großen Fee in Nord-Unruh-Stadt. Spreche mit ihr und sie überlässt dir ihre 
Feen-Maske: Mit ihr kannst du andere kleine verirrte Feenwesen auflesen, die du im Laufe des Spiels 
noch finden wirst.

Nun begebe dich nach West-Unruh-Stadt: Es wird Zeit, das du lernst, wie man Geld hortet. Jedes Mal 
wenn du mithilfe der Hymne der Zeit zur Dämmerung des ersten Tages zurückkehrst, wirst du unter 
anderem all deine gesammelten Rubine verlieren. Zwar kannst du diesen Prozess nicht aufhalten, aber 
du kannst deine Rubine an einem Ort lassen, wo sie selbst vor Zeitreisen sicher bleiben. Gehe in West-
Unruh-Stadt zum Mann, der sein Geschäft außer Haus betreibt – immer zu erreichen, sobald du die 
Treppen hoch oder runter gehst. Spreche mit ihm. Er ist der Bankier von Stadt Unruh. Deponier hier 
deine Rubine. Sammelst du 200 Rubine an, wird er dir eine größere Geldbörse übergeben. Aktuell kannst 
du am Anfang 99 Rubine bei dir führen.

Noch ist nicht alles Wichtige erledigt. Es fehlt das Bomber-Notizbuch. Hierzu hast du zwei Optionen. Wir 
bestehen den Bomber Aufnahmetest nochmal und fangen die Kinder diesmal als Mensch. Spreche dafür 
das Bomberkind als Mensch an, sobald du den Ballon als Dekulink zerschossen hast. Hast du den Test 
bestanden, überreichen sie dir das Bomber Notizbuch: 

Es zeigt dir an, welchen Personen im Spiel geholfen werden kann, insofern du mit ihnen geredet hast; 
wann sie dir Bitten, Aufgaben oder Treffpunkte genannt haben und wann du sie wahrnehmen musst und 
schließlich, ob du ihnen geholfen hast. Im Verlauf des Spiels wirst du viele nützliche Masken und 
Gegenstände erwerben können – das Bomber-Notizbuch hilft dir dabei, entsprechende Ereignisse und 
Notizen zu vermerken.

Alternativ kannst du auch direkt den Code beim Bomber-Versteck eingeben. Kommst du aus dem 
Versteck zurück, wird sich der Junge über dein Wissen des Codes wundern und dir das Bomber-Notizbuch 
auch so schenken. 

Gehe nun nach West-Unruh Stadt. Suche hierbei den Bomben-Laden auf. Du kannst die entsprechende 
Tür mit dem daneben eingravierten Bombensymbol erkennen. Im Laden spreche den Ladenbesitzer an 
und kaufe die Bombentasche für 50 Rubine ab. Damit kannst du vorerst 20 Bomben mit dir führen. 
Praktisch. 

In Nord-Unruh-Stadt wird dir nicht nur der violette Ballon des Bomberjungen aufgefallen sein. Daneben 
hängt noch ein roter Ballon – zusammen mit einem Mann! Dieser komische Kauz namens Tingle will dir 
Karten verkaufen. Wenn du also interessiert bist, kannst die Karte von Stadt-Unruh für 5 Rubine kaufen. 
Wenn das kein Schnäppchen ist. Tipp: Bei Tingle ist es ratsam, sich die entsprechende Karte vor Ort zu 
kaufen – sie ist dort wesentlich billiger.

Zu guter Letzt will ich dich noch an den Hinweis der Vogelscheuche erinnern: Mithilfe der Hymne der Zeit 
kannst du die Zeit noch auf zwei anderen Weisen manipulieren: Spielst du die Hymne der Zeit 
Rückwärts, verlangsamst du die Zeit erheblich. (Ballade des Kronos). Spielst du dagegen jede Note 
doppelt, kannst du zum nächsten Tag oder Nacht springen. (Thema der Zeit im Wind). 

## Kaptilel 4: DIE WEITEN VON TERMINA 

Begeben wir uns auf die Reise. Direkt vom Uhrturm laufe geradeaus auf das Ausgangstor, die von einer 
Stadtwache bewacht wird. Sie wird dich zu anfangs noch aufhalten, aber sobald du mit ihr gesprochen 
hast, lässt sie dich passieren. Draußen weht dir endlich ein neuer Wind entgegen: Die Ebenen von 
Termina steht dir offen. Der Weg zu den Sümpfen ist hierbei nicht zu übersehen, er führt dich direkt 
geradeaus. Laufe ihn hinab zur Baumrinde, durch sie hindurch und weiter. Am nächsten Baum bleibt 
Taya stehen und eine kurze Rückblende erfolgt. Anschließend setze deinen Weg weiter fort, am Baum 
vorbei und weiter, bis du in den nächsten Abschnitt erreichst.

Auch hier laufe den vorgegebenen Pfad entlang. Weiche den Grünen oder Roten Wabblern aus – sie sind 
leicht zu bekämpfen und dienen als gute Quelle für Herzen oder Magieflaschen. Dennoch tauchen nach 
kurzer Zeit neue von ihnen auf. Dies kann Segen oder Fluch zugleich sein. Folgst du weiter dem Weg, ist 
der rote Ballon von Tingle in der Luft nicht weiter zu übersehen. Schieße ihn erneut ab, wenn du wieder 
eine Karte kaufen möchtest. Die Karte für den Dämmerwald kostet dich nur schlappe 20 Rubine. Passiere 
Tingle und setze deine Reise fort, bis du den nächsten Spielabschnitt erreichst. 

Willkommen in den Sümpfen des Vergessens. Begebe dich hier erst mal zum Haus. Es ist nicht zu 
übersehen; klettere die Leiter hoch und gehe durch die Tür. Spreche mit dem Besitzer des Sumpf-
Infocenters. Er weist dich in seinen aktuellen Foto-Wettbewerb ein, in der es eine kostenlose Bootsfahrt 
durch den Sumpf zu gewinnen gibt. Eine solche Bootsfahrt wirst du auch brauchen. Leider ist niemand 
am Schalter Gegenüber.

Verlasse wieder das Haus. Verwandele dich nun als Dekulink und halte dich direkt rechts. Spreche mit 
dem Deku. Er wird der künftig Wundererbsen verkaufen, also merke ihn dir. Springe neben ihm die 
Plattform herunter aufs Wasser, sodass du Halt auf einem Seerosenblatt findest. Springe nun weiter, von 
Seerosenblatt zu Seerosenblatt um neue Gebiete des Sumpfs zu erreichen. Wie zum Beispiel den Steg, 
der dich zum Abschnitt des Magieladens führen wird. 

Nehme diesen Weg und pass auf die Dekurahna auf: Bissige garstige und gefräßige Pflanzen, die Link 
ans Fleisch wollen. Weiche ihren Bissen aus und erschlage sie zweimal an ihrem Stiel, damit sie besiegt 
werden. Auch andere Arten von Dekurahnas werden dir künftig auf dem Weg begegnen; sie hinterlassen 
dir meist Dekunüsse, manchmal auch Dekustäbe. Dahinter zeichnet sich bereits der Magieladen „Zur 
alten Schachtel“ ab. Begebe dich dorthin, klettere die Leiter hoch und gehe hinein. Am ersten Tag kannst 
du hier mit Kotake sprechen, die ihre Schwester Koume vermisst. Sie ging in die nahegelegenen Wälder, 
um Pilze zu suchen. Aber sie ist schon spät dran ... Ist bereits der zweite Tag angebrochen, hat sich 
Kotake bereits auf die Suche in die Wälder gemacht.

Der Eingang zu den Wäldern der Mysterien befindet sich direkt hinterm Haus in der hinteren Felswand. 
Gehe hinein und ein Affe wird wild auf sich aufmerksam machen. Spreche ihn an. Er bittet dich ihm zu 
folgen. Was bleibt dir anders übrig? Nur ist dies nicht ganz so einfach: Der Affe ist nicht nur 
außerordentlich schnell, nein, gemeine Gegner stellen sich dir in den Weg, um dich an deinem Vorhaben 
zu hindern. Lass dich nicht abschrecken und folge dieser Reihenfolge: 

Tag 1: links, rechts, rechts, geradeaus, links, links
Tag 2: rechts, links, geradeaus, links, links, rechts
Tag 3: geradeaus, links, rechts, rechts, geradeaus, rechts

Sein Weg hat dich zu Koume geführt, die am Boden liegt. Spreche sie an, damit sie dir von ihrem Leid 
erzählt. Nun spreche mit Kotake, die schockiert dir eine Flasche mit rotem Elixier überreicht. Dieses 
Gebräu soll ihre Schwester wieder stärken. Begebe dich erneut in den Wald, folge erneut dem Affen. 
Spreche Koume ein zweites Mal an und überreiche ihr den rettenden Saft. Prompt gewinnt sie an neuer 
Stärke und überlässt dir die leere Flasche. Ein sehr nützlicher Gegenstand, wie du später feststellen 
wirst.

Jetzt wo Koume wieder im Bootsgeschäft tätig ist, kannst du zum Sumpf-Infocenter zurück gehen. 
Verlässt du die Wälder, sprechen dich die Affen an. Sie bitten dich um Hilfe, weil ihr Bruder aus 
irgendeinem Grund im Palast der Dekus gefangen gehalten wird. Verwundert setzt du deine Reise fort. 
Im Sumpf-Infocenter kannst du nun deine Freifahrt bei Koume einlösen. Tu dies und lasse dir die Foto-
Box schenken, von dieser der Besitzer des Ladens gesprochen hat. Mit diesem Gegenstand ist es dir 
Möglich, ein Foto von deiner Umgebung zu machen. Mit der Bootstour kannst du nun nicht nur durch die 
vergifteten Gebiete des Sumpfs reisen, sondern auch ein lästiger Oktopus, der den weiteren Weg zum 
Deku-Palast versperrt hat, wird durch das Boot beseitigt. Steige aus, sobald das Boot den Eingang des 
Palasts erreicht. Ein Affe erwartet dich schon. 

## Kaptilel 5: VON AFFEN UND ERBSEN 

Nehme deine Deku-Schale und verwandle dich in Dekulink. Folge dem Affen. Du gelangst nun in den 
Vorhof des Deku-Palasts. Als Dekulink gehe weiter geradeaus, über den Flossweg hinüber zum Tor des 
Palasts. Zwei Wächter halten dich auf, um dich über die Regeln des Palasts hinzuweisen. Folge ihren 
Anweisungen und gehe geradeaus hindurch. Im Thronsaal erblickst du eine aufgebrachte Dekumeute. 
Spreche den Deku-König an, um den Grund für den Aufstand zu erfahren. Der Affe soll die Deku-
Prinzessin geraubt haben und solle aus diesem Grund heute seine Bestrafung erhalten. Schieße ein Foto 
vom Deku-König, Dieses Foto solltest du bevor du zum Tempel gehst (besser: bevor du den ersten 
Tempelwächter besiegst) beim Sumpf-Infocenter abgeben, um den Preis zu erhalten. Dann bewege dich 
zum Käfig. Der Affe berichtet von einem Monster, dass die Prinzessin überfallen hat. Aber so genau hilft 
dir dies auch nicht weiter..

Verlasse den Thronsaal um mit dem Bruder des entführten Affen zu sprechen. Voller Sorge weist er dich 
darauf hin, dass es einen Hintereingang zum Käfig gibt. Er sei von dem äußeren Garten zu erreichen. 
Doch dieser sei zu weit oben – eine Wundererbse muss her. Dieser befindet sich im Palastgarten, 
genauer genommen unter dem Palastgarten. Hierzu gehe direkt vom Thronsaal ausgesehen nach links. 
Schnell wird dir in den Inneren Palastgärten klar, was du tun musst: Den Wachen ausweichen. Dies ist 
nicht so einfach. Weiche einfach ihrem Blickfeld aus, indem du vorsichtig an ihnen vorbei gehst, ohne 
dass sie dich erspähen. Solltest du aufgedeckt worden sein, spreche erneut mit den Torwachen. Nachts 
wird das Sichtfeld der Palastwachen durch weiß gepunktete Linien dargestellt.

Nach zwei kleinen Abschnitten findest du ein schwarzes Loch im Boden. Lass dich hinein fallen und 
spreche dort den futternden Mann an. Er verkauft dir Wundererbsen für 10 Rubine pro Stück. Die erste 
bekommst du Gratis. Vergiss nicht, in der kleinen Pfütze frisches Quellwasser abzufüllen. Hierzu benutze 
deine Flasche, und benutze sie, damit Link nach Wasser schnappt. Mit Quellwasser und Wundererbse 
kannst du diesen Ort wieder verlassen.

Da es hier außer Rubinen für dich nichts mehr Interessantes gibt, lass dich einfach von den Palastwachen 
schnappen. Du landest wieder vor dem Palasttor. Hier wende dich nun nach rechts, springe von Seerose 
zu Seerose. Hinten am Ufer angelangt, findest du hier zwei Dinge: Rechts ist vorne eine Dekurahna. Töte 
sie für eine Dekunuss, aber pass auf. Sie wächst schnell nach. Ideal, um seinen Vorrat an Dekunüssen 
aufzustocken. Links dagegen findest du ein weiches Erdbeet. Pflanze eine Wundererbse und gieße das 
Quellwasser, damit sie wächst und zur großen Blüte heranreift. Stell dich auf sie, damit sie dich nach 
oben zum äußeren Eingang der Palastgärten bringt. Hier laufe hinein, schnappe dir die Dekublume. Dein 
Ziel ist es, von einer Dekublume zur anderen zu springen, ohne hinab zu fallen. Andernfalls musst du den 
ganzen Weg zurücklaufen und von neu beginnen. Pass auf: Manche Dekublumen werden von feindlichen 
Dekus bewacht. Schieße diese mit einer Seifenblase ab oder Alternativ mit Dekunüssen. 

Hast du es endlich in den Käfig des Affen geschafft, spreche mit ihm als normaler Mensch. Er bittet dich 
seine Seile zu zerschneiden. Aber mit deinem Schwert kommst du nicht ran. Spreche ihn ein zweites Mal 
an. Er fragt dich nach einem Instrument. Verwandle dich als Dekulink und zücke deine Deku-Pfeifen. Mit 
diesen Blasinstrumenten überzeugst du den Affen garantiert, sodass er dir eine Melodie bei bringt: Die 
Sonate des Erwachens. Damit erhältst du Zugang zum Dämmerwald-Tempel, jenem Ort, wo die 
Prinzessin der Dekus von einem Monster gefangen gehalten wird. Anstelle des Affen wirst du diese nun 
befreien.

Erneut aus dem Palast geworfen, weißt du nun Bescheid: Nehme die Abkürzung zum Dämmerwald-
Tempel. Diese ist von deiner jetzigen Position rechts gesehen. Ein Schild und deine Dekublume weisen 
direkt auf den erhöhten Eingang im Felsen hin. Springe dorthin, nehme die Dekublume und erreiche den 
Felsdurchgang. 

Im nächsten Abschnitt landest du wieder in den Sümpfen des Vergessens. Du musst hierbei von 
Dekublume zu Dekublume springen. Aber Achtung: Rechts von dir treiben Drachenlibellen ihr Unwesen. 
Komme diesen also nicht lieber zu nahe. Andernfalls schieße schnell eine Seifenblase nach ihnen, um sie 
abzuwehren. Springe zunächst vom orangefarbigen Blumenbaum zu Blumenbaum. Beim Letzten 
angekommen, springe nach links, um auf eine Plattform des geschuppten grünen Felsens zu gelangen. 
Sofort wird dich eine Eule ansprechen. Sie macht dich auf ihre Eulenstatuen aufmerksam, die dir bereits 
in Stadt Unruh aufgefallen ist. 

Nachdem die Eule dich verlassen hat, schaue dir den grauen Felsen an. Lerne die dort eingravierte 
Melodie: Das Lied der Schwingen. Solltest du zukünftig eine Eulenstatue finden, schlage sie mit dem 
Schwert. Fortan kannst du jedes Mal, sobald du diese Melodie spielst zu allen Eulenstatuen der Spielwelt 
teleportieren, die du schon einmal berührt hast. Nutze nun die Dekublume direkt daneben. Fliege den 
kleinen Abschnitt zur anderen Plattform seitens des Wasserfalls und gehe dort durchs Tor.

Du erreichst nun den Dämmerwald. Nicht gerade freundlich sieht es hier aus. Siehst du Geradeaus, 
erkennst du in der Ferne ein Podest. Genau da müssen wir hin. Hierzu schaue nach rechts und springe 
über das Seerosenblatt zu einer kleinen Insel, auf der ein Weg hinauf führt. Aber der Weg ist bewacht: 
Ein Hiploop bedroht all Jene, die diesen Weg beschreiten wollen. Locke ihn am Besten an, sodass er nach 
unten kommt. Anschließend schlage auf ihn ein oder nutze den freigemachten Weg. Da es aber zwei 
Hiploops sind, die den Weg bewachen, ist es ratsam, diese jeweils mit zwei Seifenblasen abzutöten. 
Danach stellen sich dir nur ein paar Laubkerle in den Weg, bis du das Podest erreicht hast.

Nachdem dich Taya auf das Deku-Symbol aufmerksam gemacht hat, verwandle dich in Link zurück und 
schlage die Eulenstatue. Du kannst die Eulenstatue nutzen, um hier abzuspeichern. Möchtest du das tun 
und mit dem Spiel aufhören, ist hier deine Gelegenheit. 

Andernfalls nehme die Deku-Schale zur Hand, stelle dich auf das Deku-Symbol und nutze die Deku-
Pfeifen. Spiele die Sonate des Erwachens damit der Tempel des Dämmerwalds aus dem Sumpf erhebt. 
Nehme deine Flasche und zerschlage rechts von dir die Töpfe, um dir dort die rosa Fee einzufangen – 
solltest du im Kampf alle Herzen verlieren, wird sie dich wiederbeleben. Anschließend nutze die 
Dekublume direkt vor dir.

Bevor du zum Tempel aufbrichst, kannst du dir noch schnell drei Truhen ansehen, die in diesem Gelände 
herumstehen. Die erste befindet sich direkt in deiner Nähe. Dort befindet sich nur ein lausiger blauer 
Rubin (5 Rubine). Die beiden Truhen in der hinteren nord-östlichen Ecke enthalten einen orangen Rubin 
(20 Rubine) und ein Herzteil. Nun kannst du zum  Dämmerwald-Tempel hinüber fliegen. Trete ein.

## Kaptilel 6: DER DÄMMERWALD TEMPEL

Du erinnerst dich noch, was einst die Große Fee zu dir sagte? Gut, dann nutze als Mensch die Feen-
Maske damit eine verirrte rosa Fee (1) zu dir findet. Sie ist eine von 15 verirrten Feen, die in diesem 
Tempel hausen. Hast du dies getan, dann nutze als Dekulink die Dekublume links von dir, um mit ihr in 
einer Rechtskurve um den Ast auf die rechte Seite des Raumes zu gelangen und auf einem Podest mit 
einer Truhe zu landen. In der Truhe findest du eine weitere rosa Fee (2). 

Nun fliege mithilfe der Dekublume weiter nach hinten in den Raum auf ein anderes, weiteres Podest, 
dass sich von der Truhe ausgesehen links befindet. Um nun zu der hinteren Tür, flankiert von zwei 
Fackeln, zu gelangen, schaue erst einmal nach oben. Zwei Riesenskulltulas hängen oben an der Decke. 
Pass also auf deinem Flug auf, dass du nicht gegen sie fliegst – ansonsten stürzt du unsanft hinab. Am 
Besten du navigierst Dekulink genau in die Mitte des Podests, damit die Spinnen dich nicht bekommen. 
Solltest du trotzdem auf dem Boden landen, kannst du am Eingang des Raumes eine Leiter nach oben 
finden, um zur Ausgangsposition zu gelangen. Aber achte hier unten auf den Schwarzen Spuk – kleine 
schwarze Schatten, die dich aus dunklen Ecken angreifen. 

Hast du es geschafft, gehe durch die Tür. Taya äußert sich gleich über den Gestank – dies ist der 
zentrale Sumpf-Raum des Tempels. Schneide links neben dir die Dekurahna für einen Dekustab ab und 
wiederhole den Vorgang ein paar Mal, da der Gegner sich wieder regeneriert. Mit nun ein paar 
Dekustäben ausgerüstet, laufe den Baumstamm weiter nach unten. Besiege die bissige Dekurahna für 
eine verirrte Fee (3). Da die Tür hier verschossen ist, springe als Dekulink auf das Blütenblatt der 
Sumpfpflanze vor dir. Nutze die weiteren zwei Blütenblätter, um auf die andere Seite des Raumes zu 
gelangen. Bevor du hier durch die Tür tapst, springe über das Wasser zum kleinen Eckufer zu deiner 
rechten. In den Töpfen findest du nicht nur ein paar nützliche Items, sondern eine verirrte Fee (4). 
Springe anschließend zurück und trete durch die Tür. 

Ein weiterer vergifteter Raum mit 4 Sumpf-Blütenblättern erwartet dich. Zu deiner linken Seite siehst du 
eine große Schatztruhe. Hierzu springe über das Wasser zur Deku-Blume inmitten des Raumes und lass 
dich mit ihr in Richtung der Truhe katapultieren. Du findest einen kleinen Schlüssel. Suche erneut die 
Dekublume auf und fliege nun zum erhöhten Podest, auf der zwei Fackeln stehen. Hier gehe durch die 
Tür. Jetzt wird’s eng. Kaum bist du hindurch getreten, so versperrt sich die Tür hinter dir. 

Und vor dir... lauern drei Schnapper, fiese Schildkröten, die du bereits in den Wäldern der Mysterien 
kennen gelernt hast. Hierzu verschwinde in einer Dekublume, warte bis einer der Schnapper genau über 
deine Blume gleitet und schieße dann hervor – der Treffer auf der Bauchseite wirkt tödlich. Verfahre so 
mit dem Rest deiner Feinde. Anschließend entriegelt sich die Türe und eine große Kiste erscheint: Die 
Labyrinth-Karte ist dein!

Mit dieser Karte lässt es sich schon leichter eine Übersicht des Tempels verschaffen. Kehre nun in den 
zentralen Sumpf-Raum des Tempels zurück, in der vorhin Taya über den Gestank gewettert hat. Da du 
nun einen kleinen Schlüssel hast, kannst du nun hier die andere Tür öffnen, die dir bisher versperrt blieb. 

Nun wird es kniffelig: Ein grauer Stein mit dem Zeichen der Majora Maske steht inmitten mehrerer 
Durchgänge, die dich über das Wasser bringen. Links entdeckst du eine versperrte Tür. Daneben ist ein 
erloschener Fackelständer, den du dir merken solltest. Genau unter dem Stein, unter dem Holzweg 
befindet sich in einer Seifenblase eine weitere verirrte Fee (5). Hierzu lege erst einmal eine Bombe vor 
den Stein. Die Blase der Fee zerplatzt. Nutze die Feen-Maske, damit die verirrte Fee zu dir findet. Dann 
schiebe den grauen Stein nach vorne. Laufe dann nach Rechts, den Weg entlang weiter bis du eine 
Fackel erreichst. Töte die Riesenskulltula für eine verirrte Fee (6). Dann laufe erst einmal weiter, bis du 
wieder vor dem grauen Block stehst. Schiebe ihn nach vorne, sodass er die Wege nach links und rechts 
nicht mehr behindert. Dann gehe zurück zur Fackel. Nehme nun einen Dekustab, entzünde ihn am Feuer 
und renne wieder zurück, am Block nach rechts vorbei und entzünde die tote Fackel. Die verriegelte Tür 
öffnet sich dir. Gehe hinein.

Dahinter erwartet dich ein weiterer Kampf: Erneut verschließt sich die Tür hinter dir und zwei 
Drachenlibellen richten ihre Aufmerksamkeit sofort auf dich. Erfasse sie jeweils mit der Zielerfassung und 
schieße als Deku eine Seifenblase auf sie. Sind die Libellen besiegt, entriegelt sich die Türe und eine 
weitere große Kiste erscheint: Der Labyrinth-Kompass! 

Gut gerüstet kehre in den Vorraum zurück. Als Link nehme erneut einen Dekustab zur Hand, entzünde 
sie an der Fackel und renne damit nach links, laufe die Treppe hoch und bringe dort eine andere tote 
Fackel zum Leben. Packe hier deinen brennenden Stab vorerst weg und benutze Links Rundumsicht, um 
unten auf zwei braune Bienenstöcke aufmerksam zu werden, die über den umzäunten Wasserstegen 
unten hängen. Schieße als Dekulink den rechten Bienenstock ab, die in der Nähe des grauen 
Schiebeblocks hängt. Nutze dann die Feen-Maske um die verirrte Fee (7) zu dir zu locken. Dann nehme 
einen Dekustab, entzünde ihn und springe über die Plattformen zum entfernten Tor, das von einem 
Spinnennetz versperrt wird. Mit dem Stab wird dieses lodernd vergehen und du kannst hindurch gehen. 
Töte hier die beiden Dekurahnas für Dekustäbe. Anschließend eile die Treppe hinauf.

Willkommen im ersten Stock! Doch die Freude währt nicht lange, denn Taya spürt hier oben sehr viel 
Böses. Mutig wie du bist, gehst du mit Schwert und Schild gewappnet weiter voran... Tatsächlich ist es 
hier dunkel, denn schwarzer Spuk fühlt sich hier sehr heimisch. Töte diese Biester alle; sie kommen dir 
auf den Weg entgegen und befinden sich im nachkommenden Raum überall dort, wo Fackelständer sind. 
Sind alle getötet, erscheint eine kleine Holzkiste. Eine weitere verirrte Fee (8) hat zu dir gefunden. Nun 
nehme einen Dekustab parat und entzünde in diesem Raum alle drei toten Fackeln, die an den Wänden 
stehen. Anschließend öffnet sich dir die entriegelte Tür.

Der kommende Raum sieht schwieriger aus, als er ist. Einziges nervige Problem werden die 
Drachenfliegen sein – hier ist es aber sinnvoll, diese nicht zu bekämpfen, sondern Schutz bei den 
Dekublumen zu finden. Fliege also von Dekublume zu Dekublume zum hinteren Teil des Raumes und 
laufe anschließend die Treppenstufen hinab.

Dies hier dürfte dir bekannt vor kommen... richtig, es ist der zentrale Sumpf-Raum. Nur befindest du 
dich nun oben, deswegen mache gleich von diesem Vorteil Gebrauch. Laufe geradeaus und stelle dich auf 
den Schalter: Leitern erscheinen. Nun kannst du prima ohne Umwege jeweils vom ersten Stock zum 
Erdgeschoss klettern und umgekehrt, solltest du mal versehentlich nach unten fallen. Anschließend gehe 
hinter dem Schalter durch die Tür. 

Auch dieser Raum kommt dir bekannt vor. Begib dich nach rechts und verschwinde dort durch die 
nächste Tür. Diese verriegelt sich wieder - erneut kommt es zu einem Kampf. Du trittst gegen den Mini-
Wächter des Tempels an: Einem Dinofol. Wechsle hier zum Menschen und nehme deinen Schild 
vorsichtshalber mit R zur Hand, während du deinen Gegner in die Zielerfassung nimmst. Ziel ist es, ihn 
mit deinem Schwert zu schlagen, ohne dass seine Klinge dich trifft. Hast du ihn getroffen, weiche zurück, 
denn der Dinofol verbreitet in Wut seinen Feueratem. Vier Treffer mit deinem Schwert sind nötig um 
diesen Gegner in seine Schranken zu weisen. Aus der großen Kiste entnimmt Link nun den Schatz dieses 
Labyrinths: Den Heroenbogen. Endlich! Jetzt kannst du auch als Mensch Objekte zielgenau abschießen, 
ohne auf die Seifenblasen eines Dekus angewiesen zu sein.

Mit dieser tollen Waffe geht's zurück in den Vorraum. Ist dir schon das goldene Auge auf der gegenüber 
liegenden Seite aufgefallen? Na dann wird’s Zeit für einen Test. Spanne deinen Heroenbogen mit einem 
Pfeil und schieße ihn ins Auge. Dies bewirkt, dass die Plattform der Dekublume im Wasser nun rauf und 
runter fährt. Fliege also mithilfe der Dekublume zu dieser Plattform und springe von dort auf die andere 
Seite. Bevor du hier durch die Tür gehst, schaue noch zuvor nach links. Ein Bienenstock ruht dort. 
Schieße ihn ab und nutze deine Feen-Maske für eine weitere verirrte Fee (9). Nun betrete den nächsten 
Raum.

Noch ein Kampf! Was ist denn nun schon wieder... ein zorniger Gecko stellt sich dir in den Weg. Ein 
weiterer Mini-Wächter? Wie dem auch sei, attackiere Gecko drei mal mit deinem Schwert. Außer sich vor 
Zorn, ruft er nun seinen Gehilfen herbei... niemand anders als einen Schnapper. Gemeinsam wollen sie 
dich zur Strecke bringen – aber du bist klüger. Suche als Dekulink eine Dekublume auf und verschwinde 
in ihr. Sobald der Schnapper über deine Blume gleitet, springst du hervor und lehrst seinem Reittier das 
Fürchten. Gecko flieht.. und zwar eilig hoch zur Decke. Nehme als Link deinen Heroenbogen zur Hand 
und schieße den Frosch ab. Wurde er getroffen, findet Gecko zu seinem Gefährten zurück. Es lohnt sich 
hierbei auf Gecko ununterbrochen zu schießen, wenn er auf den Wänden herumturnt. Pfeile überlässt er 
dir jedes Mal, wurde er von seinem Reittier getrennt. Verfahre drei Mal auf diese Weise und Gecko ist 
besiegt... er verwandelt sich in einen harmlosen kleinen Frosch. Seltsam oder? Merke dir den blauen 
Frosch für den späteren Spielverlauf und suche die blaue Truhe auf – der Master-Schlüssel ist nun dein. 
Mit ihm kannst du das Tor des Endgegners dieses Tempels aufschließen. 

Kehre nun in den zentralen Sumpf-Raum zurück. Bleibe direkt vor dem Schalter stehen, den du zuvor 
runter gedrückt hast. Nehme deinen Bogen und schieße deinen Pfeil auf die Blume im Wasser – genauer 
gesagt auf den seltsamen Fackelkasten in der Mitte. Du musst natürlich den Pfeil so abschießen, dass er 
genau durchs Feuer der Fackel vor dir gleitet. Ist der Fackelkorb der Blume aktiviert, wird das Wasser 
entgiftet und sie verwandelt sich in eine drehende Plattform, die nach oben steigt. Nutze dies: Laufe 
nach rechts, dann nach links. Schnappe dir hier die verirrte Fee (10) in der Seifenblase. Von hier kannst 
du einen Schalter und eine tote Fackel entdecken. Merk es dir und begebe dich nun zur Dekublume in 
deiner Nähe, um so auf diesen Schalter zu landen. Aktivierst du ihn, erscheint eine Truhe. Darin findest 
du eine verirrte Fee (11). 

Begebe dich nun per Dekublume auf die rotierende Blumenplattform. Erinnere dich an die tote Fackel 
und stelle dich an den Fackelkorb – dein Ziel ist es, mit einem Pfeil diese tote Fackel zu erwischen. Dies 
ist bei der Bewegung gar nicht so einfach, daher verzweifle nicht, wenn es nicht sofort klappt. Gelingt es 
dir, entriegelt sich die obere nördliche Tür hinter der Dekublume. Gehe dort hinein. 

Im Vorraum des Endgegners gibt es noch allerhand zu tun: Zunächst benutze deinen Heroenbogen und 
schieße einen Pfeil in die linke Flammensäule, damit die Blase der verirrten Fee (12) zerspringt. Mit der 
Feen-Maske sammelst du sie ein. Optional kannst du schon einmal versuchen, die Drachenlibellen mit 
deinen Pfeilen abzuschießen. Klettere dann die Leiter herunter. Nutze die Dekublume um dich zunächst 
nach links in die Einbuchtung der Wand hinein gleiten zu lassen. Sammle die in der Blase gefangene 
verirrte Fee (13) ein. 

Fliege anschließend zur nächst höheren Einbuchtung. Merke dir hier in der Nähe befindlichen 
Kristallschalter und fliege wieder zur nächst höheren Einbuchtung nach oben. Sammle eine weitere 
verwirrte Fee (14) ein. Kehre nun zur Dekublume unten an der Leiter zurück. Hier geht’s nun in die 
andere Richtung; springe anschließend von Dekublume zur Dekublume weiter nach oben. Schnappe dir 
die in der Blase gefangene verirrte Fee (15). Alle verirrten Feen sind dein. 

Von hier aus spanne deinen Bogen – schieße den Kristallschalter in der Ferne ab. Das Feuer auf den 
Feuersäulen verschwindet. Lass dir nun nicht viel Zeit sondern nutze die Dekublume, um schließlich auf 
der ersten erloschenen Feuersäule zu landen. Dort springe schnell nach hinten zur überdachten 
Dekublume, suche in ihr Schutz und springe ab, um zur zweiten Feuersäule zu segeln. Nutze ihre 
Dekublume, um auf das Dach der anderen Plattform vor dir zu fliegen. Hast du dies geschafft, ohne dass 
sich das Feuer zuvor materialisiert hat, kannst du die Dekublume hier oben nutzen, um bequem zur Tür 
des Endgegners zu segeln. Schnappe dir das ein oder andere Herz aus den Töpfen, dann mach dich 
gefasst auf das Schlimmste. Trete ein..

Laufe weiter nach vorne, um dem großen Wächter des Labyrinths entgegen zu treten: Der wilde 
Dschungelkrieger Odolwa springt direkt zu dir hinab. Und er scheint dich nicht besonders zu mögen... 

Um ihn zu besiegen reicht dein Schwert und einige Bomben, die du im Raum verteilst. Fürchte seine 
Größe nicht sondern dresche auf ihn ein. Vor Schmerz hält er sogar oft inne, sodass du noch ordentlich 
weiter austeilen kannst. Fängt er an zu tanzen, entsendet er seine Motten und Käfer – letzteres sind 
lästige Biester, die sich aber mit einigen Hieben vernichten lassen um hin und wieder neue Energie in 
Form von Herzen zu spenden. Dreht er mit seinem Schwert herum, weiche ihm einfach aus oder suche 
Schutz in der Dekublume. Da die Käfer oder Odolwas gezauberter Feuerkreis deinen Deku trotz Blume 
verletzen, ist es ratsam, in Menschenform zu bleiben.

Ist Odolwa besiegt, nehme den Herzcontainer an dich: Es erweitert deine Energieleiste um ein weiteres 
Herz. Betrete anschließend das blaue Licht und nehme die Maske, Odolwas Vermächtnis, entgegen. Es 
erfolgt eine kurze Zwischeneinblende einer fernen Welt. Im Kreis der Götter kannst du nun den Gesang 
des Himmels lernen, sobald Taya dir die Melodie vorgibt. 

## Kaptilel 7: RETTUNG DER DEKU-PRINZESSIN 

Im Sumpf bessert sich dank deiner Taten die Lage – das Wasser wird entgiftet und der Dämmerwald-
Tempel erhebt sich nun zu seiner ursprünglichen Größe. Für dich geht es zurück... in einen dunklen 
nassen Raum. Nachdem Taya dich dazu überredet, auch die Geister der anderen drei Gegenden zu 
befreien, kannst du die Deku-Prinzessin befreien. Zerschlage einfach das Blättergeflecht. Nach einem 
kurzen Dialog kannst du die holde Dekulady in deiner Flasche einfangen. 

Bevor du aber zum Deku-Palast zurück kehrst, musst du noch etwas kleines erledigen. Richtig, du hast 
15 verirrte rosa Feen eingefangen. Fliege dazu am Besten mit dem Lied der Schwingen zur Eulenstatue 
im Dämmerwald. Von dort schaue nach links – du entdeckst eine Höhle. Springe vom Podest ab und 
schwimme zum Ufer vor dem Höhleneingang. Da die Höhle zu weit oben im Felsen liegt, musst du die 
Deku-Blumen in deiner Nähe benutzen. Gleite also bequem zur Höhle.

In der Feenquelle vereinen sich deine verirrten Feen zur Großen Fee der Kraft. Zum Dank verleiht sie dir 
die Wirbelattacke: Hältst du den B-Knopf gedrückt, kannst du mit deinem Schwert einen kräftigen 
Rundum-Schlag ausführen. Jetzt beeile dich, der Deku-Palast wartet. Begebe dich zum Thronsaal und 
lasse die Prinzessin direkt vor ihrem Vater frei. Nachdem der Affe befreit und alles wieder gut ist, kannst 
du dir eine Belohnung von ihrem Butler abholen. 

Verlasse den Deku-Palast und wende dich direkt nach rechts. Springe über die Seerosenblätter entlang, 
bis du eine Höhle entdeckst. Darin erwartet dich tatsächlich der Deku-Butler. Folge ihm... das ist leichter 
als getan. Er führt dich durch ein Labyrinth, durch das du ihm schnell folgen solltest, ehe sich die Türen 
für dich verschließen. Für diese harte Herausforderung gibt es schließlich die Maske der Düfte verliehen. 
Mit ihr kannst du im Wald der Mysterien auf Zutatensuche gehen, die dir helfen werden, das blaue Elixier 
bei Kotake einzukaufen. [Du kannst das Labyrinth auch zu einem späteren Zeitpunkt meistern, wenn du 
im Besitz der Hasenmaske bist. Mit ihr kannst du schneller rennen.]

Falls du das Labyrinth nicht auf eigene Faust schaffst, folge meiner Wegbeschreibung: Nachdem der Weg 
direkt eine Rechtsbiegung macht, halte dich an der Gabelung nach Links und direkt bei der Nächsten 
noch einmal Links. Laufe den Weg hier entlang, dann geht es hinauf, bei der nächsten Gabelung eile 
nach links (zum blauen Feuer), anschließend direkt nach Rechts. Folge dem Weg übers Wasser, am 
goldenen Feuer vorbei und springe über die Holzplattformen. 

Jetzt biege bei der nächsten feurig grünen Weggabelung nach rechts ab, dann bei der nächsten 
Weggabelung nach links, springe über die Floße zu Wasser. Weiter geht’s, wähle dann zwischen den Drei 
Eingängen den Eingang ganz links, um zu einem kleinen Labyrinth mit unsichtbaren Feuerwänden zu 
gelangen, die Link ausweichen muss, sobald er sich ihnen nähert. 

Eile dahinter die Treppe hoch, dann eile nach rechts (sehe geradeaus das pinke Feuer) und wende dich 
direkt nach links um die Treppen hinauf zu gehen. Hier angekommen halte dich direkt wieder links, am 
violetten Feuer vorbei nach rechts, schieße dann mit einen Pfeil den Kristallschalter ab um über die 
Holzplattformen zu springen, dahinter biege direkt nach links ab (hellgrünes Feuer) und folge den Rest 
des Weges. 

Da die Gewässer nun entgiftet sind, kannst du bei Koume ein neues Spiel ausprobieren – du machst eine 
lustige Bootsfahrt und musst Koumes Zielscheiben treffen, die an ihrem Besen befestigt sind. Triffst du 
insgesamt 19 von ihren Zielscheiben, erhältst du ein weiteres Herzteil. Aber Achtung: Verwundest du die 
alte Hexe 10 Mal, bläst sie das Spiel ab. Verlasse anschließend das Sumpf-Center und springe links ab, 
um dort hinten am Uferrand die Eulenstatue zu aktivieren. 

Jetzt, wo du alles hast, deponiere deine Rubine in West-Unruh-Stadt. Aktiviere anschließend die 
Eulenstatue, die seitlich des Uhrturms in Süd-Unruh-Stadt vor dem Eingang zu West-Unruh-Stadt steht. 
Wenn du jetzt nichts mehr erledigen möchtest, kannst du die Hymne der Zeit spielen, um zur 
Dämmerung des ersten Tages zurück kehren. Aber Vorsicht: All die Ereignisse, die im Sumpf geschehen 
sind, werden rückgängig gemacht. Sie haben logischerweise nicht stattgefunden. Der gefundene 
Heroenbogen, die Fotobox und die Masken bleiben trotzdem dein. Wenn du dir sicher bist, nichts 
vergessen zu haben, spiele die Hymne und kehre zum ersten Tag zurück. 

Ergänzung: Solltest du das Foto noch nicht abgegeben haben, kannst du nochmal zum den Sumpf 
zurückkehren und ein Bild vom Dekukönig machen. Zeige dem Betreiber vom Sumpf-Info-Center dein 
geschossenes Foto. Das Bild des Dekukönigs wird ihn so faszinieren, dass er dir den Hauptgewinn 
überreicht: Ein Herzteil. Tipp: Als Alternative zum Dekukönig kannst du auch ein Bild von Tingle machen.

## Kaptilel 8: NÜTZLICHE MASKEN 

Falls du etwas Zeit hast, wollen wir uns die Hasenohren holen. Mit ihr kannst du dich schneller bewegen, 
also macht es Sinn, sie uns schon früh zu holen. Dazu eile in Stadt Unruh nachts an den Waschplatz und 
sprich mit dem Musikanten. Er übergibt dir die Bremer Maske, Drückst du B, spielt sie eine Melodie, die 
Tierbabys dazu angeregt, dir zu folgen und zu wachsen.

Warte nun bis zum dritten Tag, dann verlasse Stadt Unruh durch den Süd-Ausgang, sodass du zu den 
Ebenen von Termina kommst. Hier eile nach Süd-Westen. Weiche dort dem Takuri Geier aus, damit er 
dir nichts klaut. Ansonsten musst du den verlorenen Gegenstand beim Kuriositäten-Händler in Unruh-
Stadt abkaufen oder die Zeit zurückspulen. [Der Vogel wird mit einem roten Punkt auf der Karte 
dargestellt.] 

Folge dem Weg zur Romani Ranch. Eile im nächsten Abschnitt einfach den Weg weiter geradeaus, sodass 
du zu einem weiteren Spielabschnitt gelangst: eine große Weidenfläche streckt sich dir entgegen. Zu den 
Romani-Schwestern kommen wir in einem späteren Teil der Lösung; eile also zügig zur kleinen Tür, die 
sich genau auf der anderen Seite der Weide befindet, sprich ganz im Westen der Karte.

Dahinter befindet sich die kleine Hühnerfarm. Grog, der Besitzer der Farm ist unglücklich, da er bis zum 
Aufprall des Mondes nicht erleben kann, wie seine Küken zu kräftigen Hähnen heranwachsen. Hilf ihm 
dabei: Setze die Bremer Maske auf und sammle die Küken ein, die dir folgen werden, sobald du in ihre 
Nähe kommst. Hast du alle eingesammelt, wachsen sie. Sprich wieder mit Grog, der dir aus Freude die 
Hasenohren überreicht. 

Jetzt können wir auch gleich einen Dieb stellen: Stelle dich um Mitternacht des ersten Tages in Nord-
Unruh-Stadt hin und beobachte, wie eine alte Dame von einem Dieb angegriffen wird. Jage dem Dieb die 
Beute ab, indem du ihn einfach angreifst. 

Nachdem du die Beute zurückerobert hast, dankt dir die alte Dame mit einer weiteren Maske: Mit Ka-
Bumm Maske kannst du wie eine Bombe explodieren, was dir aber auch jeweils ein halbes Herz abzieht. 
Gehe anschließend zum Bomben-Laden in West-Unruh-Stadt und schau nach – die Große Bombentasche 
ist dort für 90 Rubine zu haben. Das ist ein günstiger Preis als das Angebot im Kuriositäten-Händler, der 
auch seinen Laden in West-Unruh-Stadt hat. Solltest du den Dieb nicht fangen, ist die größere 
Bombentasche dort in der Nacht des Letzten Tages für 100 Rubine zu holen.

Noch Herzteile gefällig? Gehe zwischen 10-12 Uhr des ersten Tages zur Residenz des Bürgermeisters in 
Ost-Unruh-Stadt und sprich dort mit einer korpulenten Frau, Madame Aroma. Sie vermisst ihren Sohn, 
Kafei und übergibt dir Kafeis Maske, mit der du Hinweise nach ihm suchen sollst. In Ost-Unruh-Stadt 
steht auch der Gasthof zum Eintopf. Dieser wird von Anju verwaltet. Sprich mit ihr, indem du Kafeis 
Maske aufsetzt. Sie wird dich bitten, um 23:30 in der Küche zu erscheinen. Tu dies und sie übergibt dir 
einen Brief, den du auch gleich in einen der Briefkästen einwirfst. Schau dir im Gasthof nach Mitternacht 
noch die mysteriöse Hand aus dem Klo an, die nach Papier verlangt. Du kannst ihr beispielsweise die 
Deku-Urkunde von Stadt-Unruh geben, damit die Hand dir ein Herzteil überlässt.

Am Zweiten Tag finde dich um 15:00 am Waschplatz ein. Während der Postbote die Glocke läutet und 
Kafei den Brief abholt, schleiche dich zur hinteren Tür und warte, bis Kafei zurückkehrt. Er erzählt dir 
sein Dilemma und überreicht dir seinen Glücksbringer. Bringe diesen zu Anju. Du wirst dem Liebespaar 
ab diesem Punkt im Moment nicht weiterhelfen können, aber das brauchst du erst einmal auch nicht, weil 
wir uns zunächst zwei andere Masken holen wollen.

Kehre um 13:00 des dritten Tages zurück zu Kafei und sprich mit dem Kuriositäten-Händler. Er 
überreicht dir die Fuchs-Maske und die Eilpost an Mama. Bringe die Post nach 18:00 zum Postboten in 
West-Unruh-Stadt, damit er die Post zu Madame Aroma überbringen kann. Ist das geschehen, sprich den 
Postboten nochmal an, damit er dir die Mütze des Postboten überlässt und endlich fliehen kann. Damit 
hast du Zugang zu den Briefkästen. Probiere es aus und du erhältst ein Herzteil. 

[Hinweis: Du kannst auch alternativ nach Mitternacht des letzten Tages die Post selbst abgeben, indem 
du sie Madame Aroma in der Milchbar übergibst. Sie schenkt dir dafür eine neue Flasche mit  Chateau 
Romani. Mit diesem Getränk kannst du solange unbegrenzt Magie wirken, bis du zur Dämmerung des 
ersten Tages zurückreist. Wir lösen diesen Alternativ-Weg in unserer Lösung zu einem späteren 
Zeitpunkt des Spiels.]

In West-Unruh-Stadt gibt es noch den Dojo des Schwertmeisters – da solltest du mal hin. Versuche 30 
Punkte zu erreichen, denn dafür gibt es ein Herzteil. Ein weiteres Herzteil kannst du bekommen, wenn du 
jeden Tag in Nord-Unruh-Stadt das Deku-Hüpf-Minispiel gewinnst: Direkt neben dem Eingang zur Großen 
Fee befindet sich ein Loch hinter einem Gitter, dass du per Dekubume erreichen kannst. Knacke jeden 
Tag den Rekord, sodass der Besitzer dir verzweifelt beim dritten Mal ein Herzteil schenkt. Dann solltest 
du in Nord-Unruh-Stadt ein wenig auf der Rutsche klettern, um den Baum mit einem weiteren einsamen 
Herzteil zu erreichen. 

Da du im Besitz der Fuchs-Maske bist, solltest du dir die sich bewegenden Grasbüschel ansehen. Es 
handelt sich dabei um eine Ansammlung an Grasbüscheln, von denen du gewöhnlich mit paar 
Schwerthieben ein paar Rubine, Herzen und Munition findest. Die Grasbüschel finden sich überall im Spiel 
– zum Beispiel auch in Nord-Unruh-Stadt oder auf der Milchstraße, die zur Romani Ranch führt. Setze die 
Maske auf und hebe eines der Grasbüschel auf, damit der Fuchs Kreaton erscheint. Er stellt dir einige 
Fragen (schau hierzu in unseren speziellen Guide). Beantwortest du sie alle richtig, gibt es ein Herzteil. 

Du könntest dir noch einen größeren Köcher für deine Pfeile verdienen. Dazu gehe von den Ebenen 
Terminas in Richtung Sümpfe bzw. von den Sümpfen des Vergessens in Richtung Ebene von Termina. 
Auf dem Pfad, der beide Abschnitte verbindet, nehme den Weg nach rechts und du kommst schließlich 
zur Sumpf-Schießbude. Erziele 2000 Punkte – der Schlüssel zur hohen Punktzahl sind die Vögel, für die 
es jeweils 60 Punkte gibt. Zufrieden gibt der Besitzer dir den Großen Köcher. Mit ihm kannst du nun 40 
Pfeile mit dir führen. Spiel das Spiel nochmal und überschreite jetzt die 2180 Punkte Marke. Als 
Belohnung wartet ein Herzteil auf dich.

Am zweiten Tag solltest du dich ins Observatorium aufmachen. Schau mit dem Fernrohr aus dem 
Observatorium auf Unruh Stadt und behalte dabei besonders die linke Seite im Auge. Es wird Morgens 
gegen 9 Uhr oder Nachts gegen 3 Uhr ein Deku Händler auftauchen, der zu seinem Geheimversteck 
fliegt. Es befindet sich nahe der Bäume neben dem Observatorium. Gehe als Dekulink dorthin und 
springe ins Erdloch. Sprich den Deku Händler an und er möchte dir für 150 Rubine ein Herzteil 
verkaufen. Schlage das Angebot aus und er wird es dir nur noch für 100 Rubine verkaufen wollen. Willige 
ein und du freust dich über ein neues Herzteil. 

In der Ebene von Termina, solltest du dir das hohe Gras anschauen, das sich zwischen dem Ausgang von 
Süd-Unruh-Stadt und in Nähe des Eingangs zur Ranch befindet. Nach kurzem Suchen verschwindest du 
in einem Erdloch. Innen findest du eine einsame Killeranas. Erledige das Biest, indem du die untere 
Spitze mit deinem Schwert bearbeitest. Als Belohnung winkt dir ein Herzteil in einer Truhe.

## Kaptilel 9: FROSTIGER EMPFANG 

Du startest wieder vor dem Turm in Unruh-Stadt und gehe sicher, dass du ein paar Pfeile und Bomben 
bei dir trägst. Laufe nach Nord-Unruh Stadt und spreche dort mit dem Wächter, auf dass er dich 
durchlassen soll. Außerhalb der Stadtmauern gehe einfach weiter geradeaus. Die leichten Schneemassen 
am Ende des Vorsprungs lassen dich ahnen, dass es in den Bergen sehr kalt werden wird. Erreichst du 
den schneeüberzogenen Vorsprung, gehe nach unten, an dem feuerspeienden Dodongo vorbei auf die 
andere Wandseite. Laufe rechts den steilen Weg hoch und richte deinen Blick gen Norden zu den Bergen. 
Laufe hier rechts den steilen Pfad weiter hoch, sodass Taya zu einem Eiszapfen hochfliegt. Mit zwei 
Pfeilen kannst du den Eiszapfen herunterholen, sodass der Pass freigelegt wird. Laufe weiter und das 
Spiel wechselt in einen neuen Abschnitt.

Hier oben setzt du mutig deinen Weg fort. Kämpfe gegen vereinzelte blaue Arachnos, bis du auf halben 
Weg auf ein paar große Schneekugeln stößt, die den Weg versperren. Eine Bombe löst das Problem. 
Weiter geht's: Klettere die Stufen hinauf und renne weiter bis zum Ende des Abschnitts. 

Du gelangst zur Bergsiedlung. Eisiger Wind kommt dir entgegen... offensichtlich hat sich der Frühling 
verspätet. Laufe an dem Haus vorbei, denn der darin wohnende Schmied kann dir zur Zeit nicht sehr viel 
weiterhelfen, da der ewige Frost seinen Ofen eingefroren hat. Vor dir liegt wenige Meter weiter ein 
kleiner See mit paar Eisschollen. Merke dir diese Stelle. Schau erst mal nach links um hinter der kleinen 
Brücke eine verschlossene Eulen-Statue zu entdecken. Aktiviere sie mit dem Schwert. Daneben weiter 
links steht ein frierender Gorone. Spreche mit ihm. Gehe dann zurück zum See und wende dich jetzt 
nach rechts. Folge dieser Richtung, bis Link in den nächsten Spielabschnitt läuft.

In der Ferne kannst du jetzt Tingles Ballon in den Lüften erkennen. Weiche dem Schnee-Wolfos aus und 
spreche mit Tingle, indem du ihn mit einem Pfeil aus der Luft holst. Kaufe bei ihm die Karte „Pic 
Hibernia“ für 20 Rubine. Nun setze deinen Weg weiter fort, über die Brücke und merke dir hier das Loch, 
welches allerdings durch Eis fest verschlossen ist. Ignoriere den Schnee-Wolfo und passiere die zweite 
Brücke, um zum Goronen-Dorf zu gelangen.

Auch das Dorf der Goronen wird von einem festen Winter heimgesucht. Direkt bei deiner Ankunft kannst 
du den frierenden Goronen nicht verfehlen. Spreche mit dem armen Kerl, der bei diesen kalten 
Temperaturen Wache halten muss. Er öffnet dir das Tor mit einem einfachen Goronen-Stampfer. Hierzu 
springe einfach nach rechts die Anhöhe hinab. Der Eingang liegt nun direkt hinter dir. Da du aber im 
Moment nichts im Goronen-Schrein machen kannst, umrunde das Haus ein wenig, um in der Ferne die 
weise Eule zu erspähen. Eile zu ihr und spreche mit ihr. Sie wird dich loben und bitten, das Schicksal der 
Goronen in die Hand zu nehmen. Anschließend fordert sie dich auf, ihr zu folgen. 

Schau wo ihre Federn fallen und laufe ihr nach. Beeile dich, denn die Federn verschwinden nach einiger 
Zeit. Hast du den Abgrund überwunden, wird die Eule gut zu dir sprechen. Dann betrete die kleine Höhle. 
Innen findest du in einer großen Truhe das Auge der Wahrheit. 

Ocarina of Time Fans werden sich gut an diesen wertvollen Gegenstand erinnern: Mit dem Auge der 
Wahrheit kannst du unter Einsatz von magischer Energie unsichtbare Objekte sehen – wie zum Beispiel 
die unsichtbaren Blöcke, auf die du soeben gesprungen bist. 

Probiere es gleich mal aus und du wirst dahinter eine Truhe entdecken, die von einer Riesen-Skulltula 
bewacht wird. Darin sind 20 Rubine versteckt. Einen Purpurnen Rubin (50 Rubine Wert) findest du links 
von dir unter dem großen braunen Stein, der mit einer Bombe weg gesprengt werden muss. Um deinen 
Bomben- oder Magievorrat wieder aufzufüllen, kannst du die Grasbüschel abschneiden. Da hier nichts 
mehr ist, kannst du diesen Ort wieder verlassen. 

## Kaptilel 10: DARMANI, DER GORONEN-GEIST

Wieder draußen vor der Höhle, wo du nun das Auge der Wahrheit gefunden hast, machst du jetzt erneut 
vom eben diesem Gebrauch. Springe über die Blöcke zurück zum Goronen-Dorf. Kaum angekommen, 
wirst du jemanden entdecken: Einen Goronen-Geist. Verwundert über deine magischen Fähigkeiten bittet 
er dich, ihm zu folgen. Tu dies und laufe ihm nach, indem du das Magische Auge aktiviert lässt (oder 
wenn du dicht bei ihm bist, folge einfach seinem Schatten). Er führt dich zurück zur Bergsiedlung zum 
kleinen See. Springe über die zwei Eisschollen auf die andere Seite. 

Durch das magische Auge kannst du jetzt eine Leiter auf der Wand erkennen. Klettere diese hoch. 
Kommst du an einer Abzweigung, gehe erst nach rechts, dann nach links und zum Schluss wieder nach 
rechts. Oben angekommen, folge dem Geist in die Höhle. Es ist der ehrenwerte Ruheplatz des 
verstorbenen Goronen. Spreche mit dem Geist. 

Er stellt sich dir als Darmani den III. vor und erzählt dir von seinem Unglück. Komme seiner Bitte nach, 
seine Seele zu erlösen: Spiele das Lied der Befreiung und Darmani findet seinen Frieden. Zurück bleiben 
seine unsterblichen Sorgen als ein Abbild einer Maske: Die Goronen-Maske! Mit ihr kannst du in den 
Körper eines Goronen schlüpfen, was du auch gleich tust. Spürst du die enorme Kraft?

Darmani äußert seinen letzten Wunsch, seinem Volk zu helfen. Bevor du das tust, lese zuvor die 
Inschriften seines Grabs, um zu lernen, welche neue Fähigkeiten dir als Gorone zur Verfügung stehen. 
Anschließend gehe einmal um den Grabstein herum, um die Kraft eines Goronen gleich unter Beweis zu 
stellen. Ziehe an den Stein zu dir und gebe nun die darunter liegende heiße Quelle frei. 

Das heiße Wasser ... bringt dich auch gleich auf eine Idee. 
Schöpfe eine Flasche Wasser davon und mache dich auf den Weg.

## Kaptilel 11: GORONEN SCHLAFLIEDER

Um die Goronen vom kalten Frost zu retten, kann eine Flasche mit heißem Wasser aus der Quelle, die du 
bei Darmanis Grabmal freigelegt hast, helfen. Eile damit zur Schnee-Ebene zu dem Punkt, wo wir Tingle 
getroffen haben. Gehe die erste Brücke in Richtung Goronen-Dorf hoch. Erinnerst du dich an das 
zugefrorene Loch? Gehe dorthin und gieße das heiße Wasser darüber. Lass das Eis auftauchen und 
springe ins Loch. Unten findest du eine weitere heiße Quelle vor. 

Bevor du heißes Wasser holst, gehe zunächst zum Goronen-Dorf und öffne das Tor, indem du einfach 
eine Stampfattacke vor dem Eingang ausführst. Im Goronen-Schrein angekommen, ist das Babygeschrei 
nicht zu überhören. Die Goronen scheinen es kaum auszuhalten. Gehe nach links den Weg entlang nach 
oben, um dort den kleinen Eingang zu betreten, der mit einem roten Läufer versehen ist. Innen findest 
du die Ursache des Baby-Geschreis. Spreche mit dem Goronenbaby, dass seinen Daddy vermisst. Merke 
dir das und verlasse den Goronen-Schrein. 

Gehe nun zurück zur heißen Quelle, die wir vorhin freigelegt haben und schöpfe eine Flasche mit heißen 
Quellwasser. Direkt vom Loch aus nach rechts und springe die Anhöhe hinunter. Schaue nach rechts. 
Taya wird zu einer Schneekugel fliegen, von der sie überzeugt ist, dass diese größer ist als die Anderen.

Lege eine Bombe davor und sehe das Ergebnis: Ein eingefrorener Gorone! Helfe ihm mit dem heißen 
Wasser aus dem Eis. Es ist der Goronenälteste. Spreche als Gorone mit ihm und erzähle ihm, dass sein 
Sohn verzweifelt nach seinem Vater schreit. Doch der Älteste will trotz seiner schwachen Verfassung 
nicht nach Hause gehen und bittet dich, seinen Sohn in den Schlaf zu wiegen. Leider fällt dem Ältesten 
bei der Kälte nur der Anfang an. Zücke als Gorone deine Ocarina (die hier Trommeln bilden) und erlerne 
das Intro des Goronischen Schlummerlieds. 

Kehre jetzt zum schreienden Goronenbaby zurück. Hole deine Trommeln hervor und spiele das Intro, 
sodass das Baby das Lied ergänzt. Du erlernst nun das vollständige Goronische Schlummerlied. Sobald 
das Baby eingeschlafen ist und die anderen Goronen ebenso, kannst du nun dich zum Pic Hibernia 
aufmachen. 

Bevor du aber gehst, werden dir im Raum des Babys die brennenden Fackeln auffallen, die zuvor noch 
erloschen waren. Verwandle dich in Link und entzünde die Fackeln im Vorraum. Wenn du schnell genug 
warst, beginnt sich oben der Kronleuchter zu drehen. Kehre zum Baby zurück und beziehe vor dem Baby 
die Startposition. Roll dich ein und rolle nach vorne. Mit genügend Anlauf saust du zügig über die Rampe 
und zerstörst die Ton-Fackeln. In einer dieser Fackelkrüge ist eine leckere goronische Notfallration. Hebe 
sie hoch und kehre zur Bergsiedlung zurück. Werfe das feinste Dodongo-Filet zu dem frierenden 
Goronen, der dank dir neue Kraft schöpft. Zum Dank seiner Rettung überreicht er dir Don Geros Maske. 

Du erreichst den Weg zu Pic Hibernia, wenn du von der Eulenstatue aus nach links oben gehst. Im 
kommenden Abschnitt wirst du dein Können als Goronenroller unter Beweis stellen müssen. Dazu 
brauchst du ein wenig magische Energie. Rolle dich mit etwas Abstand vor der Rampe ein, um dann mit 
Vollgas über die Rampe zu brettern. Rolle immer weiter, über die zweite Rampe hinweg und vorbei an 
den Bäumen, sodass du den finalen Abschnitt erreichst.

Kaum hast du Pic Hibernia erreicht, weht dir ein eisiger Wind entgegen. Das muss wohl die Stelle sein, 
an dem Darmani sein Leben verloren hat. Du wirst auch nicht viel weiter kommen, denn der eisige Wind 
hält dich zurück. Aktiviere zunächst die Eulen-Statue zu deiner rechten. 

Gehe nun weiter, bis Taya unruhig wird. Nutze das Auge der Wahrheit: Ein riesiger Gorone wacht über 
den Pass und lässt niemanden hindurch. Spiele jetzt das Goronische Schlummerlied. In einer kurzen 
Sequenz wird der riesige Gorone seinen Schlaf finden und zur Seite rollen. Der Weg ist frei! Überquere 
die Brücke und weiche den Schneefelsen aus oder zerschlage sie für eine Magieflasche. Den steilen Hang 
überwindest du leicht mit einer Goronenrolle. Oben angekommen, rolle nach links, um ein Stück den 
Aufgang hinauf zu rollen. Wird der Weg zu schmal, breche die Rolle ab und laufe den Rest, bis du den 
Eingang des Pic Hibernia Tempels erreichst.

## Kaptilel 12: DER PIC-HIBERNIA TEMPEL

Nachdem die einleitende Sequenz vorüber ist, erledige erst mal die weißen kleinen Spuks. Zerschlage die 
Eiszapfen und schiebe als Gorone den großen Stein mit dem Maskensymbol nach vorne, um einen 
Durchgang zu schaffen. Erledige dahinter den Schneewolfo, indem du wartest bis er ausholt und du 
zustichst. Da die rote Tür vorne vereist und die linke weiße Tür verschlossen ist, laufe nach rechts und 
öffne die blaue Tür. 

Dahinter gelangst du zu einer Halle mit einer zerteilten Brücke. Gehe hier erst mal nach unten, indem du 
links die Leiter an der Kante nach unten nimmst. Unten entdeckst du gleich eine Fee im Inneren eines 
Pfeilers. Laufe als Gorone über die Glut. Dann schieße die Feenblase mit eine Pfeil ab und nutze die 
Feenmaske, um die Fee (1) zu dir zu locken. Schaust du nun, das Glutbecken hinterm Rücken, nach 
oben, entdeckst du eine weitere Fee (2) in einer Blase. Schieße auch sie ab und lock sie zu dir. 

Gehe nun wieder zurück zur Leiter und nach oben. Versuche als Gorone mit einer schnellen Goronenrolle 
über den Abgrund zu kommen, ohne von dem eisigen Atem deiner Feinde erwischt zu werden. 

Bist du auf der anderen Seite, federt dich eine Kiste ab. Gehe hier geradeaus, indem du als Link über den 
kleinen Abgrund springst und die Treppe hochgehst. Du kommst zu einem kleinen Raum mit einer 
Krabbel-Ratte. Ziehe die Feenmaske an um die kleine Fee (3) rechts von dir anzulocken. Nehme aus der 
großen Truhe die Karte. Gehe wieder nach unten in die Halle mit der Brücke. Da es hier nichts mehr gibt, 
springe über den kleinen Abgrund und gehe rechts durch die blaue Tür. 

Du erreichst jetzt die große Haupthalle dieses Labyrinths. Von hier aus gehen wir nach rechts und 
nehmen die Treppen in den Keller. Geh einmal im halben Raum herum, weiche den brennenden Schädeln 
aus und aktiviere als Gorone den verrosteten Schalter. Es erscheint eine Truhe mit einer Fee (4). 
[Anmerkung: Hier unten gibt es einen Goronen-Schalter. Im Moment ist er unwichtig, da der Schalter 
sich im Boden befindet. Er bedient den großen Hauptsockel, der sich in der Haupthalle befindet. Solltest 
du später einmal dessen Höhe ändern, kannst du mit diesem Schalter den Sockel zurück in seine 
Ausgangsposition befördern.]

Gehe zurück in die Haupthalle und suche die gelbe Tür dir gegenüber auf. Es ist die Einzige, die nicht 
vereist ist. Ziehe im kommenden Raum die Blöcke links aus der Wand heraus. Dahinter ist eine Truhe mit 
einem kleinen Schlüssel. Ziehe nun die Blöcke weiter nach hinten in die dafür eingelassene Vertiefung. Es 
erscheint eine Truhe, die du erst später öffnen kannst.

Mit dem kleinen Schlüssel geht es zurück in die Eingangshalle, wo wir den Schneewolfo getroffen haben. 
Schließe hier die verschlossene weiße Tür auf. Sprenge hinten die rissige Wand mithilfe einer Bombe und 
nehme die Treppen. (Du kannst bereits in diesem Raum aus der großen Truhe den Kompass 
herausnehmen; unsere Bilderlösung holt sie sich ein wenig später.)

Im folgenden Raum macht dich Taya auf die Eiszapfen aufmerksam. Schieße diese ab, um unten das Eis 
brechen zu lassen. Springe über die freigelegten Steine zur Schneekugel und zerstöre diese mit deiner 
Goronenfaust. Nehme aus der Truhe einen kleinen Schlüssel. Nehme anschließend das Auge der 
Wahrheit zur Hand und schwenke deinen Blick durch den Raum. Du erkennst eine Nische in der 
gegenüberliegenden Wand. Hole dir aus der Kiste eine verirrte Fee (5) heraus. Schließe anschließend 
unten die Tür auf. 

Im kommenden Raum erledigst du unten die weißen Spuks. Aktiviere dann das Auge der Wahrheit und 
schau gen Decke: Schieße die Blase ab und lasse die Fee (6) zu dir kommen. Dann kletterst du rechts 
oben auf den gelben Sockel, der leicht aus dem Boden schaut. Roll dich ein und stampfe ihn nach unten. 
Es erscheinen zwei neue Sockel: einer schiebt sich direkt über den Ausgang; der andere unten auf der 
Schneefläche. Spring nun auf die andere Seite. Stampfe hier den grünen Sockel in den Boden, um einen 
anderen grünen Sockel auszufahren. Jetzt musst du dich beeilen: Stampf jetzt den gelben Sockel hinter 
dir wieder ein, sodass der andere Sockel, welcher den Ausgang versperrt, ebenso runter fährt. Nun laufe 
schnell zum grünen Sockel und springe zum Ausgang. 

Du gelangst wieder in die Haupthalle, allerdings ein Stockwerk höher. Vor dir sind zwei Brücken, die sich 
überkreuzen. Als Gorone rollst du zuerst auf die andere Seite. Von dort aus rollst du dich rechts über den 
Schneehang zur anderen Seite. Rolle nun wieder über die Brücke. Gehe nun durch die Tür. 

Hinter der Tür erwartet dich der Mini-Boss: Der Eiszauberer Zaurob!
Zaurob mag es dich zu verwirren indem er ständig von Ecke zu Ecke kreuz und quer teleportiert. 
Materialisiert er sich, schleudert er seinen Eisatem gegen dich. Zu musst ihn also in der Zeitspanne, in 
der er wirklich da ist und bevor er seine Attacke ausführt (er dreht sich in diesem Fall), mit deinem 
Schwert treffen. Nach einer Weile wird er Trugbilder erschaffen. Lass dich nicht täuschen und folge Taya, 
denn sie fliegt immer nur zum richtigen Zaurob. Verfolgt ihn und bleibe in Bewegung, so wird er nach 
kurzer Zeit besiegt sein. Es erscheint eine große Kiste. Die Feuerpfeile sind Dein!

Mit diesen Feuerpfeilen kann man endlich so einiges anstellen, was man vorher nicht gekonnt hatte. 
Dazu gehe nun zurück in die Haupthalle und rolle dich geradeaus auf die andere Seite der Brücke. Du 
kennst diesen Raum, denn hier haben wir die Karte gefunden. Schieße mit dem Feuerpfeil das 
Eismonster tot, anschließend aktiviere das Goldenen Auge, um dich mit der Plattform nach oben 
transportieren lassen. Benutze dein Auge der Wahrheit und du siehst hier kleine Treppenstufen. Nehme 
sie, um eine Kiste mit einer verirrten Fee (7) zu erreichen, die in einer Wandnische auf dich wartet.

Nimm wieder einen Feuerpfeil und schieße oben das Goldene Auge ab, damit du mit der gelben Plattform 
wieder nach oben gelangst. Gehe wieder durch die Tür zurück zur Haupthalle und schau dich um. 
Schieße mit einem Feuerpfeil das Eis weg, um eine Treppe auf dieser Ebene freizulegen. Springe dann 
den Abgrund hinab nach unten und schmelze mit den Feuerpfeilen das Eis vor der grünen Tür weg. 

Dahinter nehme deine Feuerpfeile zur Hand um links die Eismonster einzuheizen. Zum Dank erscheint 
hinten eine Truhe mit einer Fee (8). Entzünde danach die Fackeln und die Tür entriegelt sich. Dahinter 
stampfst du den gelben Sockel ein, damit ein großer Sockel in der Haupthalle hochfährt. 

Gehe wieder zurück in die Haupthalle. Da der große Sockel uns den Weg versperrt, gehst du wieder in 
den Vorraum und nimmst dort die grüne Tür. Schmelze das Eis vor der roten Tür, um zur Eingangshalle 
zurück zu kehren. Hier eile durch die weiße Tür. Da du nun die Feuerpfeile hast, schmelze die Eisblöcke, 
welche einen Schneewolfo, einen grauen Block und einen Schalter freilegen. Falls du es noch nicht getan 
hast, kannst du aus der großen Truhe den Kompass herausnehmen. 

Nun schiebe den grauen Steinblock so, um auf den Vorsprung zu klettern. Stell dich anschließend als 
Gorone auf den freigelegten Schalter, damit eine kleine Truhe erscheint. Darin befindet sich eine kleine 
Fee (9). Gehe nach hinten: Neben den Treppenstufen ist rechts oben eine Kiste. Schiebe den grauen 
Block dorthin, dann klettere hinauf und wirf eine Bombe. Ziehe die Feen-Maske auf, um die die verirrte 
Fee (10) zu dir zu rufen. 

Schiebe dann den Block zum anderen Vorsprung mit der gelben Tür. Hinter dieser kannst du eine andere 
Truhe mit einer Fee (11) öffnen, die du vorher nicht erreichen konntest. Gehe jetzt in den Raum mit der 
langen Brücke und heize die Eisatmer ein. Es erscheint eine kleine Truhe mit einem kleinen Schlüssel, 
den du natürlich mitnimmst.

Eile von dort weiter die Treppen hoch, sodass du wieder zum Raum gelangst, wo wir die Karte gefunden 
haben. Von dort aus geht es weiter nach oben zur Haupthalle. Die Säule in der Mitte stört den bisherigen 
Wegverlauf, also wird es Zeit, sie ein wenig zu kürzen. Schlage zunächst nur eine Eisplatte vor dir weg. 
Dann geht es nach rechts, über den Schneehang auf die rechte Seite. Laufe hier die Treppen hoch. 
Beseitige auch hier die beiden Eisplatten in der Säule. Gehe wieder die Treppe hinab und zerschlage nun 
unten die anderen, verbleibende Eisplatte. Eile erneut die Treppe hoch, dann gehe über die Platte nach 
rechts und öffne die Tür. Erledige die Schneemonster mit Feuerpfeilen, dann nehme das Auge der 
Wahrheit zur Hand und suche die Wände für eine Fee (12) ab, die sich hier in einer Blase in einer 
Wandnische versteckt.

Schieße das Eis von dem Treppenaufstieg ab und gehe nach oben. Besiege hier die beiden Dinofols, denn 
sie tragen jeweils eine verirrte Fee (13 und 14) bei sich. Gehe jetzt durch die Tür. Wieder in der 
Haupthalle, siehst du, dass der Sockel ein Stück weit unter dir ist. Springe hinab zum Sockel. 

Nimm nun das Auge der Wahrheit zur Hand und suche die Wände ab. In der nordwestlichen Ecke dieses 
Raums befindet sich eine versteckte Wandnische, in der sich eine kleine Truhe befindet. Mit Anlauf, 
Hasenmaske und einem sauberen Sprung kannst du zu dieser Nische herabspringen und die letzte 
verirrte Fee (15) einsammeln. [Dieses Unterfangen ist im späteren Verlauf des Spiels wesentlicher 
einfacherer, wenn man den Fanghaken besitzt. Dazu muss Link nur die Vogelscheuchen-Polka spielen, 
um sich dann an diese heran zu ziehen.] Verwandele dich anschließend in Deku-Link, tauche in die 
Dekublume ab und fliege zur Treppe, die sich nördlich in diesem Raum befindet. 

Jetzt wird es Zeit für den Endgegner: Oben boxt du die Schneekugeln weg und nimmst dahinter die 
Treppe nach oben. Rolle über die Rampe, sodass du die Tür zum Endgegner erreichst. Nur... der Master-
Schlüssel fehlt. Was nun? Ganz in deiner Nähe befindet sich eine Dekublume. Tauche als Deku-Link dort 
hinab und fliege ein Stockwerk tiefer, nach Osten. Gehe hier durch die Tür. 

Dahinter wartet der Mini-Boss Zaurob zum zweiten Mal auf dich. Diesmal sind mehrere Teleporter im 
Raum verteilt und Zaurob ist stärker geworden. Besiege ihn trotzdem. Anschließend öffnet sich eine 
zweite Tür, hinter der eine blaue Truhe mit dem Master-Schlüssel auf dich wartet.

Eile hier einfach aus der Tür, sodass sie sich hinter dir verriegelt. Nun musst du wieder nach oben; boxe 
die Schneekugeln weg und nehme ein weiteres Mal die Treppe nach oben. Hier rolle einfach nur über die 
Rampe und öffne das große Schloss. Der stählerner Alptraum Goth ist bereit, mit dir schnaufend um die 
Wette zu rennen – sobald du ihn einmal mit einem Feuerpfeil auftaust. 

Strategie: Prinzipiell musst du nur eines tun - verwandele dich in einen Goronen und rolle ihm hinterher. 
Sammle auf dem Weg die Magieflaschen ein, die sich in den grünen Töpfen befinden. Goth wird 
versuchen, dir Bomben hinter her zu werfen, Steine hinterher zu werfen und dich mit Energiekugeln vom 
Leibe zu halten. Bleibe an ihm dran und schade ihm mit der Goronenrolle. Desto mehr Funken bei ihm 
sprühen, desto mehr Schaden hat Goth genommen.

Insgesamt musst du ihn 30 Mal mit deinen Stacheln erwischen: Nach 10 Treffern wirft er mit Stalagtiten 
nach dir; nach 20 Treffern schmeißt er sogar Bomben. Dann ist er besiegt und zerstört sich selbst. 
Nehme den Herzcontainer an dich und trete anschließend ins blaue Licht, um Goths Vermächtnis 
anzunehmen. Es folgt eine neue Sequenz im Reich des Himmels. 

Anschließend kehrt der Frühling ein... endlich!

## Kaptilel 13: DAS GORONEN-RENNEN

Kehr noch mal zum Pic Hibernia zurück, da du die 15 Feen abgeben möchtest. Der Eingang befindet sich 
auf der anderen Seite des Tempels. Wenn du also die dünne Brücke mit den Fahnen überstanden hast, 
laufe nach rechts und suche die Wand nach einer Höhle ab. Darin findest du die Feenquelle und die 
zugehörige große Fee, die dank dir wieder zusammengesetzt wurde. Sie vermacht dir das Geschenk der 
doppelten Magieleiste.

Eile nun zurück zum Goronen-Dorf und schau dich dort draußen um. Im Süden ist dir bestimmt der 
einsame Deku-Händler aufgefallen. Hast du bereits die Große Bomben-Tasche und 200 Rubine bei dir, 
kannst du diese gegen die Riesenbombentasche tauschen. In ihr finden nun satte 40 Bomben Platz. 
[Hinweis: Diese Tasche kannst du immer bekommen, unabhängig vom Frühling.]

Wenn du Lust hast, kannst du nun am Goronen-Rennen teilnehmen. Als Gewinn beim Rennen winkt dir 
eine neue Flasche mit Goldstaub. Leider kannst du damit in diesem Durchgang wenig anfangen, außer 
die Flasche behalten. Aber halb so schlimm.

Du hast die Möglichkeit, die Zeit zurückzuspulen und Goth erneut zu besiegen, denn ein Teleport am 
Anfang des Tempels wird dich gleich zum Endgegner führen. Dieses Portal findest du in allen Tempeln 
und sie funktionieren sobald, wenn der Endgegner bereits einmal von dir bezwungen wurde. Erledige 
Goth nun ein zweites Mal, bringe erneut Frühling über die Berge und gib im Verlauf des ersten Tages 
dein Schwert beim Bergschmied ab, der dir dieses für 100 Rubine zum Elfenschwert verbessert. Das 
Elfenschwert sieht zwar klasse aus, hält jedoch nicht besonders lange. Nach 100 Streichen ist die Klinge 
abgestumpft und sie übersteht auch keine Zeitreisen. Aber... wenn du Goldstaub hast, so meint der 
Schmied, wäre das natürlich etwas anderes. Genau das wollen wir uns nun verdienen.

Zunächst gilt es, den Zugang für das Goronen-Rennen frei zu schalten. Dafür holst du dir ein Pulverfass 
im Goronen-Dorf. Der Laden befindet sich vom Eingang des Dorfs aus links, indem man ein Stück die 
Wandseite dort abfährt und die Höhle unten besucht. 

Die Rennbahn ist dagegen direkt links oben, wenn man von der Bergschmiede aus in den Abschnitt der 
Ebene um Tingle kommt. Wenn du also die Bomben-Prüfung fürs Pulverfass ablegst, musst du am 
Anfang die Bombe nach oben werfen, aus dem Goronen-Dorf gehen, im kommenden Abschnitt alle 
Brücken entlang gehen und schließlich links abbiegen, um die Bombe die letzten Hürden nach oben zu 
werfen. Dort positionierst du sie am Stein und sprichst dann mit dem Goronen Baby. 

Anschließend gehst du zurück zum Pulverfass Shop und sprichst mit dem Goronen, damit du die offizielle 
Genehmigung erhältst, Pulverfässer zu transportieren und zu benutzen. Du bekommst gleich ein Gratis-
Pulverfass geschenkt. 

Nimm nun am Goronen-Rennen teil und gewinne eine Flasche mit Goldstaub. Bringe diesen zum 
Bergschmied und gib dein Elfenschwert ab. Es dauert einen Tag, bis er daraus eine Schmirgelklinge 
macht. Sie ist stärker als deine normale Klinge und übersteht auch Zeitreisen. Und das tolle dabei ist, die 
leere Flasche kannst du gleich auch behalten. 

## Kaptilel 14: AUF DER ROMANI RANCH

Hast du einmal die Pulverfass-Prüfung abgelegt, kannst du dir nun jederzeit ein Pulverfass kaufen, egal 
ob bei den Goronen oder in West-Unruh-Stadt im Bombenshop. Die Romani Ranch liegt im Süd-Westen 
der großen Hochebene von Termina. Wenn du also von der Stadt aus in Richtung Sümpfe aufbrichst, 
brauchst du außerhalb der Tore nur ein kleines Stücks weiter nach rechts geradeaus laufen. 

Beginne mit einem neuen Durchgang, hol dir ein Pulverfass und begib dich am ersten Tag in Richtung der 
Ranch. Achte dabei wieder auf den diebischen Vogel, der in der Karte mit einen roten Punkt markiert ist. 
Lass dir nichts stehlen und betrete den neuen Abschnitt. 

Laufe geradeaus und schieße Tingle auf den Boden. Kaufe die Karte „Romani Ranch“ für 20 Rubine. 
Aktiviere hier die Eulenstatue auf dem Weg. Nun kommst du zu einem von Felsen versperrten 
Durchgang. Leg ein Pulverfass ab und warte, bis es ordentlich kracht [- oder
schieße einfach einen Pfeil aufs Fass].

Dahinter kommst du zur Romani Ranch – und dein Pferd Epona wartet auch schon hier auf dich. Es steht 
auf der Farm, die sich direkt vor dir befindet. Gleich kommst du schon ins Gespräch mit Romani, dem 
brünetten Mädchen. Sie erzählt dir die Geschichte von unbekannten Wesen, die diese Nacht noch 
kommen sollen, um die Kühe zu entführen. Und sie sucht einen Assistenten... 

Beweise dein Können als Schütze und als gelernter Reiter, indem du die 10 Ballons, die sie auf der Farm 
platziert hast, innerhalb von 2 Minuten abschießt. Anschließend lehrt Romani dich Eponas Lied: Mit dieser 
Melodie bist du künftig in der Lage, auf der Ebene von Termina dein Pferd zu dir zu rufen. Versprich ihr, 
um 2:00 nachts des ersten Tages bei ihr an der Scheune zu sein. Wir haben noch ein wenig Zeit, darum 
verlassen wir zunächst die Farm. 

Gehe wieder zu dem Punkt, wo du vorhin Tingle die Karte abgeluchst hast und nimm den Weg nach Süd-
Osten. Hier triffst du auf die mürrischen Gorman-Brüder, die sich zunächst über Epona lustig machen. 
Gegen 10 Rubine fordern sie dich zu einem Rennen heraus. Nimm an und schlage die beiden. Tja, wer 
hat hier den Schleppergaul? Ganz so undankbar wollen die Brüder nicht sein und schenken dir im 
Gegenzug Garos Maske. 

Nun warten wir es ab, bis es Nacht wird. Falls du es noch nicht getan hast, spiele die Ballade des Kronos 
um die Zeit zu verlangsamen. Zwar wird dein Einsatz auf der Farm nun ein klein wenig länger dauern; im 
Gegenzug sind die Geister langsamer und du kannst sie besser von der Farm abhalten. Zieh die 
Hasenohren an und triff dich mit Romani wie Versprochen vor der Scheune. Kurze Zeit später erscheinen 
auch die unbekannten Wesen. Du kannst ihre Positionen an der Karte erkennen. 

Eine gute Möglichkeit ist es, sich auf die Kiste zu stellen, welche vor der Farm zu finden ist. Dadurch 
kann man denn alle Richtungen gut im Auge behalten. Achte einerseits auf die Front vor dir. Dann 
schleicht sich noch hinter Romanis Haus ein Geist heran sowie einer von der anderen Seite. Falls man 
Pfeilmangel hat, nutzt man entweder die Büsche hinter dem Stall oder die Kiste. Alternativ geben auch 
die Geister Pfeile. Bis 5 Uhr früh geht das Spektakel, dann ist es vorbei. Romani ist dir so dankbar, dass 
sie dir für deine Hilfe eine neue Flasche mit Milch schenkt. 

Du kannst den beiden Schwestern am selbigen Tag nochmal aushelfen: Sprich um 18:00 des zweiten 
Tages mit Romanis Schwester Cremia, um ihr zu helfen, die Milchlieferung zur Stadt zu beschützen. 
Räuber wollen das Geschäft der Romani Schwestern kaputt machen und die Milchladung zerstören. Nimm 
deinen Bogen heraus und halte die beiden Räuber ab, indem du sie mit Pfeilen bespickst. Als Lohn für 
deine Verdienste schenkt dir Cremia die Romani-Maske. Die Romani-Maske ist die Eintrittskarte in die 
exklusive Milchbar in Ost-Unruh-Stadt. Da solltest du später einmal hin. 

Wenn du noch Zeit hast, kannst du dir zwei Masken aneignen. Zunächst wollen wir uns das blaue Elixier 
holen. Dazu geht es in die Wälder der Mysterien. Ziehe deine Schweine-Maske an, da du mit ihr besser 
riechen kannst und du wirst bald fündig. Nimm eine leere Flasche und fange einen magischen Pilz ein. 
Bringe diesen zum Magieladen und gib ihn bei Kotake ab. Verlasse ihren Laden und gehe wieder hinein. 
Weil du ihr die wichtige Zutat gebracht hast, brauchst du für dein erstes blaues Elixier nicht zu bezahlen. 

Mit diesem Elixier (du kannst auch alternativ ein Rotes nehmen, welches du
von der Hexe für die Rettung ihrer Schwester bekommst) reitest du mit Epona von den Ebenen Terminas 
nach Osten. Du kommst zu einer felsigen Gegend. Nach zwei Zäunen ist auf der rechten Seite einen 
Steinkreis zu sehen. Nimm das Auge der Wahrheit zur Hand und schau nach – da sitzt ein völlig 
erschöpfter Soldat. Gib ihm dein Elixier und er wird dir im Gegenzug die Fels-Maske schenken. Mit ihr 
nehmen dich viele Leute nicht mehr wahr.

Nun wollen wir uns eine weitere Maske holen: Von Stadt-Unruh geht es nach Nord-Westen. Laufe hier 
nach Mitternacht den Vorsprung entlang und du entdeckst auf einem Felsen einen Steinkreis. Spring (am 
besten mit den Hasenohren) von der Mauer hinab, um zum Felsen zu gelangen. Hier sprich mit dem 
geisterhaften Kamaro. Spiele das Lied der Befreiung, damit seine Seele erlöst und du Kamaros Maske 
erhältst. Mit dieser Maske geht es nun nachts zu den Zwillingen in Stadt-Unruh. Da sie mit ihrer 
aktuellen Tanzchoreografie nicht zufrieden sind, solltest du sie eines besseren belehren. Setze Kamaros 
Maske auf, visiere eine der beiden Schwestern an und tanze ihnen vor. Zum Dank schenken dir die 
Zwillinge ein Herzteil. 

## Kaptilel 15: DIE SCHÄDELBUCHT

Starte mit einem neuen Durchgang und eile nach West-Unruh-Stadt. Nachdem der Wächter dich 
passieren lässt, rufst du in den Ebenen von Termina Epona zu dir. Gemeinsam eilt ihr nach Westen in 
Richtung Küste. Springe mit ihr über die zwei Zäune, sodass du den Strand erreichst. Das große Meer 
erstreckt sich vor deinen Augen und in einiger Entfernung entdeckst du eine Forschungsstation. Steige 
von Epona ab und springe darauf ins kühle Nass. An einer Stelle kreisen Möwen verdächtig auf und ab 
und du findest einen schwerverletzten Zora. Hilf ihm, das Ufer zu erreichen, wo er zusammenbricht. Er 
stellt sich dir als Mikau vor, Gitarrist in einer Zora Band. Höre seine letzten Worte …

Mikau erzählt dir seine Tragödie – Gerudo-Piraten haben der Sängerin seiner Band ihre Zora Eier 
gestohlen. Spiele das Lied der Befreiung, damit die Seele dieses Zoras seinen Frieden findet. Zurück 
bleiben die Zora-Schuppen, die du an dich nimmst. Mikaus Geist erscheint mit der Bitte  sein Tun zu 
vollenden und die Eier zu retten. Er hinterlässt ein Grab, an dem du die Zora Techniken erlernen kannst. 

Nun steige wieder ins Wasser und schwimme zur Forschungsstation. Dort kannst du zunächst die Eulen-
Statue aktivieren. Dann zücke deinen Heroen-bogen und schieße einen Pfeil auf Tingles roten Ballon. 
Kaufe von dem grünen Kauz die Karte der Schädelbucht für läppische 20 Rubine. 

Gehe anschließend ins Forschungslabor und sprich mit dem alten Mann. Er berichtet dir von der 
seltsamen Temperaturveränderung. Zora Eier reagieren sehr empfindlich auf Temperaturschwankungen 
und können daher nicht in diesem Wasser überleben. Er hat aber ein Aquarium, in dem die Eier jedoch 
überleben und schlüpfen können. Merke dir dies. Wieder draußen, gehe wieder an den Strand und eile 
nach Süden. Passiere dort die Höhle, die dich zu einem weiteren Abschnitt der Küste bringt. Eile von hier 
aus nach Osten den Fluss entlang, an den grünen Wabblern vorbei bis zum Wasserfall. Davor ist ein 
Becken, an dessen Grund ein Raubschleim sein Unwesen treibt. Erledige diesen für ein Herzteil. 

Nun eile zum Meer, setze deine Zora-Maske auf und schwimme zum Eingang des Fischmauls, die dich zu 
der Höhle der Zoras führen wird. Die Bandmitglieder, die dich nun alle für Mikau halten, sind allesamt 
beschäftigt. Verlasse die Höhle, indem du den Südöstlichen Ausgang nimmst. Dort steht jene Zora Dame, 
die Mikau so sehr beschäftigt hat. Vor Trauer hat sie ihre Stimme verloren und würdigt dich nur eines 
stummen Blickes. Da du ihr im Moment nicht helfen kannst, schau dir hier die Eulen-Statue an und 
aktiviere sie. 

## Kaptilel 16: VON EIERN UND PIRATEN

Zeit, um die Zora Eier zu retten und zur Piraten-Festung aufzubrechen. Ziel ist es, die Piraten-Festung zu 
infiltrieren, ohne dass die Gerudos etwas von deiner Anwesenheit bemerken, andernfalls musst du wieder 
am Anfang der Festung beginnen. Für diesen Durchlauf gibt es zwei Wege: Entweder du schleichst dich 
gekonnt an den Wächterinnen vorbei und suchst nach den Eiern oder du nutzt die Fels-Maske. Mit ihr bist 
du für die Gerudos unsichtbar und kannst einfach an ihnen vorbeilaufen.

Der Zugang zur Piraten-Festung befindet in der anderen Bucht, wo wir am Anfang waren. Nördlich von 
dem Forschungslabor befindet sich Unterwasser entlang der Mauer Holzplatten, auf denen Totenköpfe 
aufgemalt wurden. Schwimme gegen die zweite von links und du entdeckst einen geheimen Durchgang, 
der dich in die Festung bringen wird. Auf der anderen Seite angekommen, tauche an dem Wachboot 
vorbei oder wenn du die Fels-Maske dabei hast, fahre einfach ein Stück mit. 

Auf dem gegenüberliegenden Ufer gibt es weiter links einen gelben Sockel. Du weißt, was du zu tun hast. 
Verwandele dich in einen Goronen und führe eine Stampfattacke aus. Ein Abwasserkanal wird geöffnet. 
Wieder als Zora tauchst du ins Wasser ab und folgst dem neuen Durchgang. Er entpuppt sich als ein 
kleines Labyrinth aus Abzäunungen. Zerschlage die Holzplatten und du entdeckst dahinter einen grauen 
Block. Ziehe ihn zu dir. 

Ist das getan, nimm den Weg nun nach links. Schwimme zunächst nach oben aufs Gitter, sodass du dort 
eine Einbuchtung nach unten entdeckst. Nimm dir aus der kleinen Truhe 20 Rubine heraus. Kehre wieder 
zurück und schiebe den anderen grauen Block ganz nach vorne. Hast du das getan, biege rechts ab und 
folge dem Gang, bis du die Wasserströmung erreichst. Lass dich von ihr nach oben tragen. Lass dich hier 
oben nicht von den Seitenströmen erfassen, sondern schwimme konsequent zur Tür, die sich am Ende 
des Korridors befindet.

Im nächsten Raum, eile an den Stachelbomben vorbei nach hinten, um aus der kleinen Truhe weitere 20 
Rubine heraus zu nehmen. Laufe dann den Steg hoch, der dich zu einer verschlossenen Gefängniszelle 
führt, in dem ein einsames Herzteil liegt. Doch wie kommst du daran? Eile dazu die Treppe weiter nach 
oben. Rechts von dir siehst du einen Kristallschalter, den du dir vorerst merkst, dann folge dem Weg 
nach links. Du entdeckst eine Reihe von Fässern, die du als Gorone mit wenigen Faustschlägen 
zertrümmern kannst. Dahinter befindet sich der Schalter, der die Tür der Gefängniszelle für wenige 
Sekunden öffnet. Eile hinab und schnappe dir das Herzteil. 

Anschließend widmest du dich dem Kristallschalter, den du vorhin ausgelassen hast. Schieße ihn ab und 
springe als Zora von der Treppe herunter, sodass du rechts zügig durch den geöffneten 
Unterwasserkanal schwimmen kannst. Wieder tauchst du an den Stachelbomben vorbei, bis du am Ende 
eine Leiter erreichst. Klettere diese hoch.

Hier oben entdeckst du hinter einem Gitter einen Kristallschalter. Tritt auf den Bodenschalter, um für 
kurze Zeit dieses Gitter verschwinden zu lassen. Renne schnell zum Gulli, nimm den Heroenbogen zur 
Hand und schieße einen Pfeil in den Kristallschalter, sodass die Wasserfontäne unter dir dich ins nächste 
Stockwerk trägt. Oben angekommen, klettere eine weitere Leiter hoch. 

Du entdeckst oben ein Fernrohr, mit dem du auf den Innenhof der Festung blicken kannst. Jede Menge 
Wachen erwarten dich dort... mutig, wie du bist, wendest du dich ab und drehst dich um. Du erspähst im 
Raum dir gegenüber einige Stachelbomben. Lass sie mit einem Pfeil explodieren, damit sie die Sicht auf 
einen Kristallschalter freigeben. Ein weiterer Pfeil öffnet dir sogleich die Tür, die dich nach draußen 
bringen wird. Laufe einfach den folgenden Pfad entlang, der dich zur eigentlichen Piraten-Festung führt. 

Willkommen im Piraten-Nest. Ein Blick auf die Karte verrät dir die Position der einzelnen Wächterinnen; 
sie sind mit einem weißen Punkt markiert. Mit deinen Pfeilen kannst du eine Gerudo für einen kurzen 
Moment ausschalten, aber sie steht in Kürze wieder auf, also warte nicht zulange. Weil wir die Gerudo-
Piraten so gern haben, wollen wir uns gleich die Fels-Maske aufziehen. Zücke deine Foto-Kamera heraus 
und mache ein schönes Bild von einer Gerudo. Du wirst dieses Bild später brauchen. Nachdem du das 
getan hast, eile nun in die Mitte des Innenhofs, wo der Wachturm steht. Klettere hier heimlich die Leiter 
hoch und laufe die Hängebrücke entlang (auf der Brücke patrouilliert eine weitere Gerudo), an dessen 
Ende sich eine Tür befindet. 

Du beobachtest, wie eine Wespe durch ein kleines Gitterfenster schlüpfst. Wenige Sekunden später wirst 
du Zeuge einer Unterredung der Piraten, in der du erfährst, dass sich 4 Zora Eier in der Festung 
befinden. Taya bringt dich auf die Idee, das Wespennest herunter zu schießen. Tu dies und die Piraten im 
Raum unter dir werden panisch die Flucht ergreifen. Zurück bleibt eine große Schatztruhe, die wir uns 
bald holen wollen.

Dazu gehe wieder zurück vor die Tür. Springe rechts die Kante hinab und du gelangst zu einer weiteren 
Tür, die dich zum Hauptquartier von vorhin bringt. Gehe ruhig hinein und schnappe dir aus der großen 
Truhe den Fanghaken. Mit ihm kannst du dich an verschiedenen Dingen festhaken und hochziehen – sei 
es an dafür vorgesehenen Pfeilern oder Holzbrettern. Deinen neuen Gegenstand probieren wir gleich aus. 
Ziele auf das Holzbrett über dem Wasserbecken und lass dich hinaufziehen. Als Zora fängst du sodann 
das erste Ei mithilfe einer Flasche. 

Verlasse nun das Quartier und schau dich um. Drei weitere Eier müssen gerettet werden. Da du nur drei 
leere Flaschen hast, wirst du dich zwangsläufig einmal aus der Festung begeben müssen und die Eier 
beim Forschungslabor abgeben. Hier in der Festung gibt es mehrere Pfeiler, an denen du dich hochziehen 
kannst. Durch sie kannst du die anderen Räume der Festung erreichen; dabei spielt die Reihenfolge keine 
Rolle. 

Hier auf dem Innenhof, eile zunächst auf die Nordseite der Festung und zieh dich über zwei Pfeiler zum 
Eingang. Im Inneren musst du dich an einer Wächterin vorbei schleichen (mit der Fels-Maske geht das 
prima), bis du die nächste Kammer erreichst. 

Eine Gerudo-Piratin stellt sich dir in den Weg, die deine Maske durchschaut. Versuche ihr in die Seite zu 
stechen; holt sie zu einer größeren Attacke aus, ist Vorsicht geboten: Trifft sie dich in einem solchen 
Moment, verlierst du das Bewusstsein. Klug ist es, ihr vor diesem Angriff auszuweichen und anschließend 
die ungeschützte Seite anzugreifen. Nach kurzem Intermezzo zieht sich die Kriegerin zurück und der Weg 
ist frei. In der Kammer dahinter findest du ein Aquarium voller aggressiver Fische... mit deinem 
Fanghaken kannst du sie schnell zur Strecke bringen. Tauche dann hinab und hol dir aus der kleinen 
Truhe das zweite Zora Ei. 

Kehre zurück in den Innenhof und suche nun die Südseite der Festung auf. Ziehe dich über den ersten 
Pfeiler auf die obere Plattform und sieh dich um. Du entdeckst hinten in der Ecke eine weitere kleine 
Truhe mit 20 Rubinen. Anschließend eile zurück und zieh dich über zwei weitere Pfeiler nach oben, um 
hier wieder ins Innere der Festung zu gelangen. 

Hier darfst du erst einmal mit der Wächterin um die Fässer Slalom spielen. In der folgenden Kammer 
erwartet dich eine weitere Gerudo-Piratin, die dir das Leben schwer machen möchte. Verfahre mit ihr wie 
zuvor und auch sie wird sich bald zurückziehen. Dahinter bewacht eine einsame Killermuschel das dritte 
Zora Ei, welches du dir schnell aus dem Aquarium holst.

Da du über keine weitere leere Flasche verfügst, müssen wir hier vorerst aus der Festung eilen und die 3 
Eier zum Forschungslabor bringen. Der Ausgang der Festung befindet sich auf der Karte in der unten 
links. Beim Forschungslabor angekommen, weist dich der alte Mann darauf hin, die Eier rasch ins 
Aquarium zu bringen. Stell dich aufs Aquarium und öffne den Inhalt deiner Flaschen.

Zurück im Piraten-Nest hat noch niemand dein Treiben bemerkt. Da du nun über den Fanghaken 
verfügst, musst du nicht wieder durch den Keller. Schau dich auf der Karte oben, wo der gelbe Sockel 
steht, ein wenig um und du siehst einen Greifpunkt. Ziehe dich rüber und du gelangst schnell zum 
Innenhof. Ziehe dich oben auf der Karte den ersten Pfeiler hoch und drehe dich um. Auf der 
gegenüberliegenden Seite entdeckst du einen anderen Pfeiler, zu dem du dich herüber ziehst. Eile die 
Plattform entlang, die dich zum Eingang ganz oben rechts führt. Vergiss nicht die Fels-Maske aufzuziehen 
und hol dir aus der Truhe einen silbernen Rubin (100 Rubine wert). 

Nun nutze die Holzbalken oben an der Decke, um dich so zur Tür herüber zu ziehen. Dahinter wartet die 
dritte und letzte Gerudo-Piratin, die du rasch niederstreckst. Dahinter findest du das letzte Aquarium. 
Erledige die Killermuschel und schnappe dir das vierte Zora Ei. Jetzt, wo du alle Eier der Piraten 
entwendet hast, gibt es hier nichts mehr zu tun. Verlasse die Piraten-Festung und bring auch das letzte 
Ei zum Forschungslabor. 

## Kaptilel 17: DIE NADELFELSEN

Nachdem du die Eier der Piraten-Festung zum Forschungslabor gebracht hast, gilt es die letzten drei Eier 
zu suchen. Aus der geheimen Unterredung der Piraten weißt du, dass sie von Seeschlangen gefressen 
wurden. Am besten, du gehst der Sache selbst auf den Grund. Bevor du dich ins Meer stürzt, begib dich 
zum vom Forschungslabor aus zum Strand. Du entdeckst dort das Haus des Anglers im Südosten der 
Karte. Sprich mit ihm und gib ihm dein Foto, welches du von einer Gerudo-Piratin gemacht hast. Zum 
Dank schenkt er dir das Seepferdchen, welches dich um Hilfe anfleht. Es möchte zu den Nadelfelsen 
gebracht werden.

Der Zugang zu den Nadelfelsen befindet sich nordwestlich vom Forschungslabor. Siehst du auf der Karte 
die beiden Pfeiler? Schwimme dorthin, um einen neuen Abschnitt zu erreichen. Sogleich wirst du hier 
unten auf dem Grund des Bodens auf ein Schild aufmerksam. Du hast zwei Optionen: Du kannst zum 
einen den Schildern folgen, damit du sicher die Nadelfelsen erreichst – es handelt sich hierbei um ein 
kleines Labyrinth, in dem dich jeder falscher Richtungswechsel wieder zum Anfang führt. Andererseits 
hast du das Seepferdchen dabei. Befreie es aus deiner Flasche und es wird dir den Weg zeigen.

Solltest du nicht das Seepferdchen dabei haben, folge diesem Weg: Mit dem ersten Schild geht es nach 
rechts, das Zweite und Dritte zeigen dir jeweils nach links. Hier kommst du an einem Schwarm Fische 
vorbei, an denen du vorbei musst. Am nächsten Schild angekommen, schau dich um nach einem 
weiteren Schild in deiner Nähe. Laufe zu diesem; dann bewege dich in Richtung der Öffnung im Felsen. 

Hier bei den Nadelfelsen erstreckt sich nach unten eine ziemliche Kluft. Das Seepferdchen bittet dich, 
seinen Freund zu retten. Das kannst du gleich im Anschluss erledigen. Die Eier befinden sich jeweils in 
den Höhlen der Seeschlangen. Um daran zu kommen, musst du diese jeweils aus den Höhlen 
hervorlocken. Zwei Schläge mit deines magischen Schutzschildes genügen, um sie zu vertreiben. 

Nach und nach durchkämme die verschiedenen Höhlen und vertreibe die Seeschlangen aus ihren 
Löchern. Du findest unter anderem auch Truhen, in denen Rubine versteckt sind. Kaum sind alle 
Seeschlange fort, finden die beiden Seepferdchen zusammen. Zum Dank, dass sie wieder vereint sind, 
schenken sie dir ein Herzteil. Hast du die drei Eier gefunden und auch die letzte Truhe ihres Inhalts 
erleichtert, verlasse die Nadelfelsen und schwimme zurück zum Ausgangstor.

Nun geht es zum Forschungslabor. Sobald sich alle sieben Zora Eier im Aquarium befinden, beginnen 
diese zu schlüpfen. Ihre besondere Anordnung zeigen dir eine Melodie, welche du mit deiner Ocarina 
nachspielen solltest: Du erlernst die Bossa Nova der Kaskaden. Der alte Forscher neben dir rät, sie der 
traurigen Sängerin der Zora Band vorzuspielen. 

Kehre zurück zur Zora Höhle. Bevor du zu Lulu gehst, wollen wir uns schnell ein Herzteil holen [– denn 
wenn erst Lulu ihre Stimme wieder hat, sind die Bandmitglieder zur Probe]. Als Zora besuchst du 
zunächst dein Zimmer und ziehe dich als Link mit den Fanghaken nach oben. Lies das Tagebuch durch 
und merke dir die beiden Notenanfänge. Verlasse das Zimmer und gehe ein Zimmer weiter. Sprich mit 
Japas, dem Gitarristen. Zeig ihm dein Musikinstrument und jamme mit ihm eine Runde. Er wird die 
fehlenden Noten zu einem Lied ergänzen. Merke dir die Notenabfolge:

rechts, rechts, unten, A, A, unten, rechts, A, links, links, rechts, unten, unten, rechts, links, unten

Mit diesem neuen Lied begibst du dich zum Bandleader Evan. Als Zora wird er dich nur nach den Eiern 
fragen, die du aber schon abgegeben hast. Spielst du aber als Link das vollständige Lied vor, ist Evan so 
von dieser Melodie fasziniert, dass er dir ein Herzteil schenkt. 

Gehe nun nach draußen zu Lulu und spiele ihr die Bossa Nova der Kaskaden vor. Es folgt eine 
anschließende Sequenz, in dem ein Wächter des Schädelbuchtempels erweckt wird: Eine gigantische 
Schildkröte. Sie ist es, die dich dazu auserkoren hat, den Veränderungen des Schädelbuchttempels 
nachzugehen. 

Hast du alles dabei, wollen wir uns gleich auf den Weg machen: Ziehe dich mit dem Fanghaken auf den 
Rücken der Schildkröte, indem du einfach die Palmen anvisierst. Sofort wird sich die Schildkröte mit dir 
auf den Weg machen. Sie ist die einzige, die dich sicher durch den Wirbelsturm bringen kann – wie du 
feststellst, denn die Gerudo-Piraten haben bei dem Sturm offensichtlich kein Glück.

## Kaptilel 18: DER SCHÄDELBUCHT-TEMPEL

Willkommen auf der anderen Seite des Meeres – und in der Eingangshalle des Schädelbuchttempels. 
Nachdem du dich von der Schildkröte verabschiedet hast, solltest du dich hier umschauen. Abgesehen 
von paar Fässern befinden sich hier auch erloschene Fackeln. Entzünde sie mit Feuerpfeilen, damit eine 
kleine Truhe erscheint. Eine verirrte Fee (1) befindet sich darin. Gehe nun durch die Tür. 

Hinter der Tür erwartet dich jeder Alptraum eines Klempners. Der ganze Tempel besteht aus einer 
großen Maschine, dessen Vorgänge du nach und nach erforschen musst, bevor du die Eigenheiten des 
Tempels dir zu nutze machen kannst: Den Wasserfluss. Ein großes Wasserrad versorgt diesen Tempel 
mit Energie; um das Wasser entsprechend zu regulieren musst du die farbigen Wasserhähne der drei 
Leitungssysteme nutzen (gelb, rot, grün), die du in diesem Raum als auch in anderen Teilen des Tempels 
finden kannst. Zur Einführung gilt es den gelben Wasserhahn im Wasser zu drehen:

Springe zunächst ins Wasser und schau dir die Stelle unterhalb der Plattform des Eingangs an. Befreie 
die verirrte Fee (2) aus der Blase. Dann eile hier unter Wasser zu einem gelben Sockel. Drehe bzw. ziehe 
an dem Rad, damit ein Wasserstrahl aus einem gelben Rohr hochfährt. An der vom Eingang aus linke 
Seite kannst du die bewegende Plattform nutzen, um schnell Bekanntschaft mit einer Riesenskulltula zu 
machen. Erledige sie für eine verirrte Fee (3). An der vom Eingang aus rechte Seite nutze die andere sich 
bewegende Plattform, um oben auf die andere Seite vor dir zu springen. Erledige zwei Riesenskulltulas 
und springe (ohne Hasenohren) auf die geschaffene Wasserfontäne sobald sich der Wasserstrahl unten 
befindet. In entsprechender Höhe kannst du mithilfe der Fontäne anschließend die gegenüberliegende 
Plattform erreichen. 

Du gelangst in eine mächtige und für dich wichtige Halle: Dem großen Wasserrad. Wie Taya bereits 
richtig erkennt, stellt dieser Kessel die Energieversorgung sicher. Zunächst holst du dir zwei Feen. Eine 
verirrte Fee (4) befindet sich links von dir, in einem Fass am Rand des Beckens. Die andere Fee ist 
schwieriger zu erreichen, da sie sich ganz unten unter Wasser, unterhalb der Schraube in einem Tonkrug 
befindet. Hierzu springe als Zora runter in den Rührkessel und tauche zum Grund. 

Ein gezielter Schuss mit deinen Zoraflossen lässt den Krug zerbrechen. Tauche zum Rand des Beckens 
und nutze die Feen-Maske, um die verirrte Fee (5) anzulocken. Anschließend tauche wieder als Zora ins 
Gewässer. Du erkennst hier mehrere Kanäle: Es sind insgesamt 4 an der Zahl, wobei es jeweils zwei 
Eingänge und zwei Ausgänge sind. Dein Ziel ist es erst einmal jenen oberen Eingang zu erreichen, der 
sich auf der rechten Seite vom Eingang aus gesehen befindet und in dem ein rotes und ein grünes Rohr 
abfließen. 

Folge dem Verlauf, um eine neue Kammer zu erreichen. Klettere auf die kleine Plattform, nimm den 
Fanghaken heraus und ziehe dich zur großen Truhe: Die Labyrinth-Karte ist dein. Anschließend solltest 
du ein Blick in die Tonkrüge werfen, die sich auf der Plattform gegenüber befinden. Dort hält sich eine 
weitere verirrte Fee (6) versteckt. 

Von dort aus, wo du die Fee im Tonkrug gefunden hast, nimmst du rechts den nächsten Abwasserkanal, 
durch den ein rotes Rohr entlang führt. Pass hier auf die beiden Dexi-Hände am Eingang auf, die du mit 
deinen Zoraflossen oder deinem elektrischen Schutzschild erledigen kannst. 

Auf dem Weg zur nächsten Kammer wird dir eine neue Art einer Dekurahna entgegen kommen: Sie 
wachsen unterhalb großer Seerosenblätter und haben auch noch die nette Eigenschaft, sich zu lästigen 
Vierbeinern zu entwickeln. Erledige erst die im Wasser, klettere dann auf trockenen Boden und schieße 
mit einem Pfeil die letzte oben an der Decke. Nun erscheint eine kleine Truhe auf der anderen Seite. 
Springe auf eins der Seerosenblätter und nimm den Fanghaken heraus, um dich zur Truhe zu ziehen. 
Darin schlummert eine verirrte Fee (7). Anschließend gehe durch die Tür.

Im folgenden Raum solltest du an den Uferrand schauen. Dort steht im Wasser bei den beiden Dexi-
Händen ein Tonkrug. Zerlege ihn mit dem Fanghaken, dann ziehe die Feen-Maske auf, um die verirrte 
Fee (8) zu dir zu locken. Erledige mit dem Fanghaken die Dekurahnas, damit eine kleine Truhe im 
Wasser erscheint. Du kannst dich, wenn du über die Seerosen springst, mithilfe des Fanghaken zur 
großen Truhe ziehen und dir den Kompass raus nehmen (unsere Bilderlösung holt ihn sich später).

Springe ins Wasser (bitte nicht in den Wassergraben mit den Strömungen) und hole dir aus der kleinen 
Truhe einen kleinen Schlüssel. Anschließend lass dich von der Wasserströmung wieder zum großen 
Wasserrad treiben. 

Wieder im Strudel, versuche nun den unteren Abwasserkanal (oben-links auf der Karte) zu erreichen, 
das mit einem rotem Rohr gekennzeichnet ist. Du gelangst in einen Raum, in dem du nur dem roten 
Rohr entlang folgen musst. An einer Stelle hebt es sich aus dem Wasser; laufe auf dem Rohr nach oben, 
sodass du oben die verschlossene Tür erreichst. Die nächste Kammer enthält  einige Krüge... ja du ahnst 
sicherlich, dass hinter der nächsten Tür etwas Großes auf dich zu kommt. Im Wahrsten Sinne des Wortes 
- stärke dich rasch und stell dich den Gefahren: Hinter der nächsten Tür kannst du vorsichtig einen Blick 
an die Decke werfen... 

Das Warzenauge hat es auf dich abgesehen. Fans von A Link to the Past dürften diesen Gegner bereits 
kennen. Das Prinzip hat sich nicht geändert: Um das Warzenauge zu besiegen, muss die Schutzhülle in 
Form der roten Blasen fallen. Du kannst dich entscheiden, ob du per Fanghaken jede Blase einzeln zu dir 
ziehst und erschlägst oder mit einem Feuerpfeil alle Blasen herunterholst. Sobald der Kokon der Blasen 
beseitigt ist (entweder durch die Beseitigung aller Blasen oder durch das mehrmalige Treffen des 
geöffneten Auges), läuft das Warzenauge auf dem Boden Amok. Eile in die Ecke. Drei Schüsse mit den 
Feuerpfeilen ins große Auge genügen, um deinem Feind den Gar aus zu machen – zur Belohnung erhältst 
du eine große Truhe. In ihr ruhen die Eispfeile. Mhhm... damit kann man hier wohl eine Menge anstellen.

Deine neuen Eispfeile wollen wir gleich ausprobieren. Kehre zurück in den Raum, wo sich das rote 
Leitungsrohr befand, welches du vorhin zum Aufstieg benutzt hast. Stell dich unten  in der Ecke auf eine 
der beiden Plattformen. Du siehst ihn schon, den lästigen Oktrorok. Taya überlegt laut, was du tun 
könntest, um auf den roten Sockel zu gelangen. 

Schieße einen Eispfeil auf den Oktrorok, damit er sich kurzfristig in einen nützlichen Eisblock verwandelt. 
Nutze diesen Moment und spring dank des provisorischen Trittbretts auf den roten Sockel. Drehe hier 
den Hahn zu. Nachfolgend springe wieder ins Wasser und nehme den Ausgang unten rechts, um zurück 
zum zentralen Kessel mit dem Wasserrad zu gelangen. 

Vom großen Wasserrad aus geht es wieder in den Kanal, den wir am Anfang zuerst genommen haben; 
sprich auf der Karte unten rechts, in dem ein rotes und ein grünes Leitungsrohr abwandern. Du kommst 
zum Raum, wo du die Karte gefunden hast. Ziehe dich an der kleinen Uferplattform hoch, nimm dann 
deinen Enterhaken raus und ziehe dich zur offenen Truhe, in der sich einst die Karte befand. Nun hole die 
Eispfeile heraus: Unser Ziel ist es, die Tür am Ende dieses Raumes zu erreichen. Mit den Eispfeilen bist 
du in der Lage, Eisschollen im Wasser zu erschaffen. Nutze diese Fähigkeit aus und schaffe dir einen Weg 
zur Tür. Passiere sie. 

Du erkennst in einiger Entfernung einen weiteren roten Sockel mit einem Hahn. Doch der Sockel ist zu 
hoch, als das du einfach hinauf klettern kannst. Aber wozu gibt es den blauen Wabbler? Mit einem 
Eispfeil wird er zu einem Eisblock umfunktioniert. Schiebe ihn an den Sockel, klettere ihn hinauf und 
drehe den Hahn auf. Ist das getan, kehre nun in den Vorraum zurück. 

Nimm nun wieder den Kanal unten rechts an den Dexi-Händen vorbei, passiere auch die Dekurahna 
Kulturen, sodass du den Raum erreichst, wo wir den kleinen Schlüssel entdeckt haben. Falls du es noch 
nicht getan hast, kannst du dir aus der großen Truhe den Kompass holen. Anschließend nimm deine 
Eispfeile und erschaff dir einen Weg zu der Nische, in dem einige Eiszapfen stehen. Beseitige den 
Eisblock mithilfe eines Feuerpfeils und öffne dahinter die Tür. 

Es wird Zeit für einen weiteren Miniboss und du kennst ihn bereits: Gecko, der aggressive Frosch, hat es 
auf dich abgesehen. Diesmal hat er sich etwas Neues ausgedacht: Mithilfe zahlreicher Blasenfreunde will 
er dir das Leben zur Hölle machen. Aber du hast die Eispfeile. Sobald sich Gecko mitsamt Schleim oben 
an der Decke befindet, versucht er dich zu zerquetschen, doch du holst ihn besser mit einem Eispfeil 
herunter. Der Schleim gefriert und fällt in zahlreiche Stücke hinab – lass dich nicht davon treffen. Gecko 
ist ohne sein Gefolge eine Weile schutzlos. Bespicke ihn mit Pfeilen. Sollten dir Pfeile oder dein 
Magievorrat leer sein, schau in den Holzkisten nach. Nach einer Weile gibt Gecko auf und der Weg zur 
blauen Truhe ist frei. Du findest den Master-Schlüssel.

Nun wird es Zeit, dass du den Wasserstrom des Tempels in die andere Richtung änderst; nur so kannst 
du zum Endgegner gelangen. Klettere zunächst das Gitter neben der blauen Truhe hoch, dann nutze den 
schnellen Wasserstrom, um zum zentralen Kessel zurück zu kehren. Versuche zum Rand des Beckens zu 
gelangen und klettere ans Ufer. Nimm nun deine Eispfeile heraus: Ziel ist es, den Wasserstrom 
einzufrieden, welcher auf der Nordseite die Leiter zur oberen Tür blockiert. Ein Schuss genügt, den Strom 
zu unterbrechen. Die Kunst besteht nun darin, die Leiter zu erreichen. Dazu klettere im Süden des 
Raumes die Leiter hoch, springe auf die rotierende Schraube und lasse dich zur gegenüberliegenden 
Seite transportieren. Klettere die freigelegte Leiter hoch und passiere die Tür. 

Im kommenden Raum muss ein grüner Hahn aufgedreht werden. Erledige zunächst den blauen Arachno, 
dann erschaffe mithilfe der Eispfeile die nötigen Eisschollen, um zum grünen Sockel zu gelangen. Drehe 
den Hahn auf, damit das Wasser durch die grünen Rohre gepumpt wird. Bevor du gehst, sollten wir uns 
noch die Decke anschauen. Erschaffe dir eine Eisscholle oben links, sodass du unterhalb der Öffnung des 
Gitters stehst und einen Greifpunkt erkennen kannst, mit dem du dich per Fanghaken rasch hinaufziehst. 
Oben befindet sich in einer kleinen Truhe eine verirrte Fee (9). Sollte dir bei diesem Unterfangen die 
Magie ausgehen, befinden sich unter Wasser noch Krüge mit magischem Energievorrat - aber gib Acht 
auf die Knochenfische.

Kehre zurück in den Kesselraum mit dem großen Wasserrad. Passiere nun hier die Tür unten, die dich 
zum Vorraum bringt, in dem du deinen allerersten Wasserhahn aufgedreht hast. Von der Tür aus schaue 
nach rechts: Dank deines Einsatz ist die Wasserfontäne aus dem roten Rohr aktiv. Über diesem 
Wasserstrom ist oben an der Decke ein Greifpunkt. Zieh dich mittels Fanghaken dorthin und lass dich auf 
die Wassersäule herabsinken. Von dort ist es nur noch ein kleiner Sprung zum roten Wasserhahn. Drehe 
ihn auf, sodass ein Wasserstrom aktiv wird, der das Wasserrad in diesen Raum zum Stillstand bringt.

Nun springe ins Wasser und klettere auf den gelben oberen Sockel. Drehe den Hahn zu, sodass die 
Wasserfontäne des gelben Rohrs erlischt und die Schraube sich wieder zu drehen beginnt – aber in die 
andere Richtung. Nun hast du den Wasserstrom des Tempels gedreht.

Schwimme vom Kesselraum durch den Kanal unten rechts, an dem ein grünes Rohr abfließt. Zunächst 
holst du dir zwei weitere verirrte Feen: Eine verirrte Fee (10) befindet sich Unterwasser links oben vor 
dem Kanalausgang in einer Blase gefangen. Ein Schuss mit deinen flinken Zoraflossen und die Blase 
zerplatzt. Gehe aber nicht zu ihr hin, sondern schau dich um. Hier steigt das grüne Leitungsrohr auf. 
Nutze den Aufstieg des Rohes im rechten Teil des Raumes, um trockene Füße zu bekommen. Schau dich 
gleich um – du entdeckst zu deiner Seite unterhalb einer Plattform eine weitere verirrte Fee (11) in einer 
Blase. Ein guter Schuss lässt auch diese Blase zerplatzen. Nun setze deine Feen-Maske auf, um beide 
verirrten Feen zu dir zu rufen. Springe anschließend ins kühle Nass und nimm den Ausgang links oben, 
um zum zentralen Kesselraum zurückzukehren. 

Nimm nun hier vom Kesselraum den Kanal oben rechts auf der Karte: Pass beim Schwimmen auf die 
lästigen Dexi-Hände auf, ehe du schließlich zu einem großen Raum mit einem weiteren großen 
Wasserrad gelangst. Stell dich auf die bewegliche Plattformen oben links auf der Karte und schieße einen 
Eispfeil in die Wassersäule, die das Rad antreibt. Das Schaufelrad kommt nun zum Stillstand. In diesem 
Raum befinden sich zwei Nischen. Über das stillstehende Rad kannst du direkt an der Wand die erste 
Truhe mit einer Fee (12) erreichen. Weiterhin siehst du noch eine Plattform, welche an der Wand hängt. 
Normalerweise kann man diese mit einem beherzten Sprung erreichen. Falls nicht, muss die Schraube 
unter Verwendung der Feuerpfeile noch einmal zum drehen gebracht werden, bis die Plattform in der 
Nähe ist. Friere den Wasserfall ein, stelle dich auf die Plattform und schmelze ihn wieder. Nun kannst du 
dich in die zweite Nische mit den Fanghaken ziehen, wo ebenfalls eine Fee (13) wartet.

Über die Radachse und die Blätter des Getriebes gelangst du unten links zur Tür. Passiere sie und eile 
den Gang entlang. Springe hinab und schau dich um. Du erkennst einen Kanal im Boden, der mit Wasser 
gefüllt ist. Springe als Zora hinein und du findest dort unten ein Fass. Zerstöre es geschwind mit deinen 
Flossen, sodass eine verirrte Fee (14) zu dir findet. Schwimme wieder nach oben. Stell dich auf die erste 
Wippe und schnappe dir deine Feuerpfeile. Schmelze das Eis an der Decke, damit ein Wasserstrahl 
wieder aktiv wird und du von der Wippe nach oben getragen wirst. 

Springe nun zur nächsten Wippe und spiele dasselbe Spiel nochmal, in dem du einen weiteren 
Wasserstrahl mithilfe eines Feuerpfeils aktivierst. Jetzt trennt dich noch ein Sprung zum grünen Sockel 
und du kannst den Hahn aufdrehen. Ist das getan, laufe auf dem grünen Rohr entlang. Pass auf die 
explosive Krabbelratte auf (ziehe hier die Felsmaske an) und klettere das Rohr weiter hoch, sodass du 
über den abgesperrten Bereich gelangst. Dort springe hinab und zieh dich mit dem Fanghaken herüber 
zur kleinen Truhe. Die letzte verirrte Fee (15) ist dein.

Nun springe wieder hinab und stell dich unten auf die Wippe. Ein weiterer Schuss mit einem Feuerpfeil 
und die Wippe trägt dich in die Höhe zur Tür. Von dort aus kannst du über den ehemaligen Kartenraum 
zum zentralen Kesselraum zurückkehren. Nun nimm wieder den Kanal unten rechts, durch den ein 
grünes Rohr entlang fließt. Klettere jenes Rohr hinauf, um zum letzten grünen Sockel zu gelangen. Drehe 
den Wasserhahn auf, damit eine Wasserfontäne zwischen dir und der Tür zum Endgegner aktiviert wird. 
Ein letzter Sprung und du bist bei der Bosstür angelangt. Wenn du bereit bist, kannst du hindurch gehen.

Springe die Öffnung hinab, um dem Wächter dieses Tempels entgegen zu sehen: Es ist Gyorg, ein 
gigantischer hungriger Monsterfisch. Und er hat HUNGER auf Zoras. Er wird versuchen, dich zu fressen. 
Du hast die Wahl – ins Wasser mit dir oder auf der Plattform bleiben. Bleibst du auf der Plattform, wird 
Gyorg diese in Raserei rammen, um dich ins Wasser zu katapultieren. Zudem achte auf seinen 
Hechtsprung, mit dem er dich aus der Luft greifen möchte. Denn hat Gyorg dich einmal erwischt, zieht er 
dir im Fischmaul eine ganze Reihe Herzen ab. 

Strategie: Du hast zwei Möglichkeiten, ihn für kurze Zeit Kampfunfähig zu machen. Die leichtere Methode 
ist, Gyorg mit der Zielerfassung mit Pfeilen zu bespicken, sodass er für einen Moment gelähmt ist. Willst 
du einen richtigen Kampf, dann spring als Zora ins Wasser und attackiere seinen Kopf mit den 
Zoraflossen. Wurde Gyorg gelähmt, kannst du ihn mit einem weiteren Schlag mit den Flossen oder 
deinem elektrischen Schutzschild attackieren. 

Wiederhole das Spiel nun. Nach einer Weile wird Gyorg schwächer und ruft einen Schwarm lästiger 
Fische herbei. Diese kannst du mit dem Energieschirm abwehren und die Herzen einsammeln. Lass dich 
nicht von Gyorg fressen und der Fisch wird bald aufgeben. Nehme den Herzcontainer an dich und trete 
anschließend ins blaue Licht, um Gyorgs Vermächtnis anzunehmen.

Nach einer Sequenz im Himmelreich ist die Schildkröte sehr zufrieden mit dir. Sie bringt dich wieder 
zurück zur Zora Höhle. Bevor du zurück zu den Zoras gehst und ein wenig deinen Erfolg feierst, gibst du 
noch deine gesammelten Feen ab. Dazu schwimme zum Strand. Hier führen Steinplatten ins Meer. Mit 
dem Fanghaken kannst du dich von Palme zu Palme ziehen. Am Ende erreichst du einen Vorsprung, an 
dem ein großer Felsen eine Höhle versperrt, Lege eine Bombe und sprenge den Eingang frei. Gehe 
hinein.

Innen drin erwartet dich eine weitere Feenquelle. Die Große Fee des Muts ist dir Dankbar für deine 
Rettung und möchte dir ein Geschenk machen. Sie leiht dir ihre Stärke, sodass deine Rüstung verstärkt 
wird – du erkennst den Effekt an der weißen Kontur um deine Herzen. Jetzt  können Gegner dir nur noch 
halb so viel Schaden zufügen, als zuvor. 

## Kaptilel 19: EINKAUFSBUMMEL

Es wird Zeit, dass wir uns weitere Masken holen, ein paar Gegenstände mitnehmen und die 
Vogelscheuchen-Polka erlernen. Zunächst brich auf nach Unruh-Stadt. Gehe zum Basar in West-Unruh-
Stadt und sprich mit der Vogelscheuche. Spiele ihr ein Lied vor. Die Noten kannst du selbst bestimmen. 
Anschließend teilt dir die Vogelscheuche mit, dass du sie nun jederzeit rufen kannst. Nachdem sie sich in 
den Boden verkrochen hat, verlass den Laden. Du beherrschst nun die Vogelscheuchen-Polka. [Du 
kannst die Vogelscheuche einmal hier beim Gemischtwarenladen  als auch bei der Sternwarte finden.]

Falls du dir noch nicht den Riesenköcher geholt hast, kannst du das tun. Gehe dazu zur Schießbude in 
Ost-Unruh-Stadt und sprich mit dem Besitzer. Für 20 Rubine lässt er dich spielen. Ziel ist es innerhalb 
eines Zeitlimits so viele rote Oktoroks wie möglich zu treffen und die blauen Oktoroks zu vermeiden, da 
sie dir die Zeit kürzen. Erzielst du insgesamt 39 Treffer, erhältst du den Riesenköcher. Mit ihm kannst du 
50 Pfeile tragen.

Bleib noch ein wenig bei der Schießbude. Für jeden neuen Rekord, den du aufstellst, schenkt der Besitzer 
dir einen violetten Rubin im Wert von 50 Rubine. Schaffst du ein „Perfekt“, sprich alle 50 Ziele zu treffen 
ohne daneben zu schießen, winkt dir ein Herzteil entgegen. 

Du kannst dir ein weiteres Herzteil holen, wenn du bei Liebling & Schätzchens Krabbelminen-Schießbude 
teilnimmst. Dazu musst du an allen drei Tagen an den drei Spielen  teilnehmen, die dort veranstaltet 
werden. Erlangst du an allen drei Tagen ein „Perfekt“, schenken sie dir am ersten und zweiten Tag 
jeweils einen violetten Rubin im Wert von 50 Rubinen und am letzten Tag ein Herzteil.

Da du schon einmal in Ost-Unruh-Stadt bist, kannst du nachts die exklusive Milchbar besuchen. Die 
Eintrittskarte ist die Romani-Maske, welche du einst von Cremia erhalten hast, als du ihre Milchladung 
vor Räubern beschützt hast. In der Milchbar, sprich mit dem Zora Toto. Er findet es Schade, dass keine 
Live-Konzerte in der Bar stattfinden und bittet ihm zu helfen. Spiele die Ocarina in den verschiedenen 
Formen (Als Link und als Deku, Gorone und Zora) auf der Bühne in den richtigen Scheinwerfern. Am 
Ende erinnert sich der griesgrämige Zirkusdirektor an seinen alten Traum und schenkt dir wehmütig die 
Maske des Zirkusdirektor.

Jetzt besuche den Postboten, wenn er nachmittags nichts zu tun hat. Du findest ihn in seinem Zimmer in 
West-Unruh-Stadt. Er versucht gerade genau 10 Sekunden im Geist zu zählen und nicht dabei auf die 
Uhr zu schauen. Kannst du das auch? Vielleicht. Wir tricksen ein klein wenig mit den Hasenohren, mit der 
wir die genaue Zeit angezeigt bekommen. Überrascht, weil du so gut schätzen kannst, schenkt dir der 
Postbote ein Herzteil. 

Bevor du Unruh-Stadt verlässt, könntest du dir noch die Bude „Zur Schatzkiste“ ansehen. Sprich die 
Dame als Gorone an und spiele ein Spiel für 30 Rubine. Ziel ist es, auf dem Schachbrettfeld die 
Schatzkiste am Ende zu erreichen und den Hindernissen auszuweichen. In der Truhe winkt dir als Gorone 
ein weiteres Herzteil entgegen. PS: Probiere mal andere Verwandlungsformen und achte auf den Text.

Breche nun auf in die Ebenen von Termina und reite dort in Richtung Schädelbuchtküste. Dort steht bei 
den weißen Mauern vor dem Eingang zur Bucht ein einsamer Felsen. Zerschlage ihn als Gorone und lass 
dich ins Loch darunter fallen. Du landest in einer Höhle. Schieße die Wespennester herunter, 
anschließend tauche als Zora ab, um das Herzteil auf dem Grund einzusacken – aber pass auf 
die Bio-Dekuranhas auf.

Besuche nun die Schädelbuchtküste und eile zum südlichen Abschnitt, wo sich auch der Eingang zu Zoras 
Höhle befindet. Doch wir wollen den Wasserfall erklimmen. Dazu eile hier den Fluss entlang, bis du den 
Wasserfall erreichst. Hier kannst du dich an den Palmen hochziehen, die auf einigen Felsvorsprüngen 
stehen. Schnapp dir auf deinem Weg eine kleine Truhe mit einem gelben Rubin (20 Rubine). Am Ende 
erreichst du eine kleine Höhle, durch die du hindurch gehst.

Du erreichst die Stromschnellen. Sprich als Zora mit dem natürlichen Feind eines Deku – dem Biber. Er 
befindet sich im Wasser; wenn du dich ihm näherst taucht er auf den Grund. Sprich ihn an und fordere 
ihn zu einem kleinen Rennen. Du musst innerhalb von 2 Minuten durch 20 Ringe schwimmen. Es gibt 
zwei verschiedene Strecken; je nachdem, ob Tag oder Nacht herrscht. Schwimme durch alle 20 Ringe, 
ehe sich beim Erfolg deiner grandiosen Leistung der große Biberbruder einmischt. Er will dir den Gewinn 
nicht so einfach überlassen und verlangt ebenso nach einem Rennen. Nun sind es 25 Ringe, die in 
derselben Zeit durchschwommen werden müssen. Hast du auch diese Prüfung bestanden, geben sie dir 
widerwillig den Hauptpreis: Eine leere Flasche. 

Kehre nochmal zu den Biberbrüdern zurück und fordere sie erneut zu einem Wettkampf. Diesmal setzen 
sie die Zeiten noch knapper, doch das hält dich auch nicht auf. Nachdem du wieder beide Brüder besiegt 
hast, geben sie dir resignierend ein Herzteil. 

Wenn du noch etwas Zeit hast, können wir uns ein paar Herzteile holen, die wir bisher ausgelassen 
haben. Sie sind nicht essenziell für die Lösung wichtig, doch wir holen sie uns der Vollständigkeit wegen. 

Zurück in der Bucht, wo auch das Forschungslabor steht, solltest du auf Fischfang gehen. Du kannst an 
vielen Orten Fische fangen, zum Beispiel am Strand in einem Wassertümpel in der Nähe des Zugangs zur 
Piratenfestung. Fang dir ruhig mehrere Fische ein. Gehe damit zum Forschungslabor und füttere die 
beiden roten Fische im kleinen Aquarium. Hast du sie genügend mit Fischen gefüttert, überlassen sie dir 
glücklich ein Herzteil. 

Falls du dir noch ein Herzteil holen möchtest, gehe zu den Sümpfen und hole dir draußen vor dem 
Sumpf-Infocenter eine Wundererbse, indem du den Deku ansprichst. Fang dir in einer Flasche noch 
frisches Quellwasser ein, dann geht es zurück zur Schädelbuchtküste. Suche die Bucht auf, wo sich auch 
der Zugang zur Piraten-Festung befindet und schau dich um. Du kannst dich mithilfe des Fanghakens an 
die aufgestellten Pfeiler und dich so auf die Felsvorsprünge ziehen. Hier befindet sich ein weiches Beet. 
Pflanze deine Wundererbse ein und übergieße sie mit Quellwasser, damit sie wächst. Sie bringt dich zu 
einem weiter entfernten Felsvorsprung. Du siehst nun in einiger Distanz ein einsames Herzteil. Was tun? 
Richtig, du spielst die Vogelscheuchen-Polka. Zieh dich per Fanghaken an deinen Feldfreund heran und 
das Herzteil gehört dir.

Für ein weiteres Herzteil musst du nochmal Gyorg im Schädelbuchttempel besiegen. Begib dich 
anschließend tagsüber zur Schädelbuchtküste in den Abschnitt, wo auch das Forschungslabor steht. Da 
die Gewässer sich beruhigt haben, kannst du am Strand, nördlich vom Forschungslabor ein kleines Boot 
erkennen. Steige auf, damit es losfährt. Ziehe dich per Fanghaken zu den Palmen, um auf die Insel zu 
gelangen. Sprich mit dem Angler, der dir für 20 Rubine ein Minispiel anbietet. Ziel ist es, zur richtigen 
Insel mit der brennenden Fackel zu springen, um einen Punkt zu erhalten. Schaffst du insgesamt 20 
Punkte, schenkt er dir als Belohnung ein Herzteil. 

Für das nächste Herzteil benötigst du etwas Ausdauer und Geduld. Erinnerst du dich an die beiden 
Geckos, die wir im Dämmerwald-Tempel und im Schädelbucht-Tempel getroffen haben? Besiege sie 
jeweils nochmal und sie verwandeln sich in normale Frösche. Was auch immer für ein Fluch auf sie 
gewirkt hat, nimm Don Geros Maske heraus und sprich mit ihnen. Nun breche auf nach Unruh-Stadt und 
gehe zum Waschplatz. Dort sitzt ein weiterer Frosch. Setze auch hier Don Geros Maske auf und sprich 
mit ihm. Der nächste Frosch wartet im Sumpf, ganz im Süden der Karte auf einem schwimmenden 
Holzstück, in der Nähe des nächsten Sumpf-Abschnitts, der zum Dekupalast führt. Der letzte Frosch 
erscheint in den Bergen, sobald der Frühling eintritt. Besiege Goth ein weiteres Mal und du wirst 
automatisch zum kleinen See gebracht, wo die Frösche auf dich warten. Setze Don Geros Maske auf und 
sie werden mit dir musizieren. Für diesen großen Aufwand gibt es ein Herzteil.

Wenn du schon einmal in den Bergen bist und wieder Frühling herrscht, kannst du dir im Abschnitt, wo 
du Tingle und den Goronen Ältesten getroffen hast, einmal den See genauer anschauen. Abgesehen von 
einigen gefräßigen Fischen kannst du im Süden auf dem Grund des Sees eine Truhe finden. Darin 
schlummert ein weiteres Herzteil. 

Auf dem Weg zum Pic Hibernia gibt es auf halben Weg unsichtbare Plattformen. Mit dem Auge der 
Wahrheit kannst du diese Plattformen enthüllen. Auf der letzten Plattform angekommen, nimm die 
Ocarina heraus und spiele die Vogelscheuchen-Polka. Mithilfe des Fanghakens kannst du dich zu deinem 
Feldfreund ziehen und das Herzteil einsacken. 

Nun eile zu den Ebenen von Termina. Es gibt hier eine Menge versteckter Höhlen, die wir uns genauer 
ansehen. Für ein Herzteil kann Link vier verschiedene Höhlen aufsuchen und dort als Gorone das 
Goronische Schlummerlied spielen, um die Mythensteine rot zu färben. Du findest die dazugehörigen 
Erdlöcher:

Hinter dem Observatorium

•
• Wenn du aus Nord-Unruh Stadt kommst, rechts neben dem frostigen Vorsprung
•

Neben dem Ausgang aus West-Unruh-Stadt befindet sich davon rechts ein einsamer brauner 
Felsen
Südöstlich neben dem Einfang zu den Sümpfen des Vergessens, im Hohen Gras

•

Hast du bei allen Steinen vorgespielt, erhältst du ein Herzteil. Hinweis: Du kannst auch als Deku oder als 
Zora mit ihren Volksliedern dieses Herzteil holen. Versuch es mal und beobachte den Farbwechsel.

Vor dem Aufgang zu den Bergen solltest du dich auf der schneebedeckten Fläche umschauen. Wenn du 
also von Nord-Unruh-Stadt herauskommst, laufe zum Vorsprung und dann nach links herunter. Laufe 
hier unten weiter gen Westen und du entdeckst ein Erdloch. In der kleinen Höhle haben sich paar 
Dodongos bequem gemacht. Erledige sie beide und es erscheint eine kleine Truhe. Ein Herzteil 
findet sich darin.

## Kaptilel 20: DER IKANA FRIEDHOF

Zeit, dass wir dem Ruf der Geister folgen und uns dem Osten von Termina zuwenden. Verlasse Unruh-
Stadt nach Osten und rufe Epona herbei. Reite mit ihr nach Osten, sodass ihr die Straße nach Ikana 
erreicht. Hier warst du du bereits einmal, als du dem Soldaten geholfen hast, die Fels-Maske 
abzunehmen. Bevor wir weiter nach Ikana gehen, nimmst du die Abzweigung nach links, die dich zum 
einsamen Friedhof von Ikana führt.

Nachdem die einführende Sequenz vorüber ist, folge dem Wegverlauf nach oben, bis du am Ende ein 
Riesenskelett erreichst. Was das zu bedeuten hat?

Die Inschrift neben dem Riesenskelett erzählt dir mehr: Spiele die Sonate des Erwachens und das Skelett 
vor dir wird vor dir vor Wut toben. Zieh nun die Hasenohren auf – denn jetzt gilt es, das Riesenskelett in 
einem kleinem Wettrennen zu bezwingen. Das wäre eigentlich gar so nicht schwierig, würde das Skelett 
nicht jeweils Flammenwände mit zwei Knochengängern erzeugen, die dich aufhalten sollen. 

Besiege die Skelette, damit die Flammen verschwinden. Hast du das Riesenskelett eingeholt, solltest du 
ihn mit Schwert und Pfeil attackieren, um ihn zum Kampf herauszufordern. Nach einer Weile wird er 
versuchen auf dich zu springen, nutze seinen Schatten, um seinen Aufprall vorauszusehen. Nach einem 
kurzem Kampf gibt sich das Riesenskelett geschlagen und bittet dich um Waffenfrieden.

In einer kleinen Sequenz erzählt dir Riesenskelett Skull Kreeta von seinem Seelenpein, dem verlorenen 
Krieg von Ikana. Er bittet dich den rastlosen Seelen seiner Untergebenen die Nachricht vom Kriegsende 
zu überbringen, sodass seine Seele in Frieden ruhen kann. Nachdem du ihm das Versprechen gegeben 
hast, überlässt dir Skull Kreeta eine große Truhe. In ihr findest du die Maske des Hauptmanns. Setzt du 
diese Maske auf, werden alle Knochengänger deinen Befehlen gehorchen (und dir mehr oder weniger 
nützliche Informationen geben).

Warte nun auf dem unteren Friedhof, den du am Anfang passiert hast, bis die Nacht des ersten Tages 
anbricht. Dort bewachen drei Knochengänger einen Grabstein. Ziehe die Maske des Hauptmanns auf und 
sprich mit dem Skeletten. Gib ihnen den Befehl, das Grab zu öffnen, sodass sie ein Erdloch freilegen. 
Springe hinein. Unten angekommen, springe über die Plattformen zur anderen Seite und passiere den 
kurzen Korridor. Dahinter mach dich bereit für eine Horde Fledermäuse. Sind sie alle erledigt, gibt es zur 
Belohnung eine kleine Truhe mit einem violetten Rubin (50 Rubine). 

Zünde mithilfe der Feuerpfeile die 3 Fackeln an, damit sich die Tür entriegelt. Dahinter wartet der 
Wächter des Grabs: Ein Eisenprinz. Egal wie du dich ihm näherst, schlägt er vertikal auf dich ein oder er 
schleudert horizontal seine Axt hin und her. Egal was er auch tut, bleibe immer auf Distanz zu ihm. Am 
Besten ist es, dich ihm erst so zu nähern, dass er seinen Angriff macht, und sobald er ins Leere 
geschlagen hast, triffst du ihn mit einem zügigen kleinen Hieb, um dann erneut Distanz zu halten. Nach 
einer Weile wird der Eisenprinz wütend werden. Dann wird er auf dich zu rennen. Gib ihm dem Rest, 
sodass sich hinten der Vorhang hebt und der königliche Hofkomponist und Geist Moll in Erscheinung tritt.

Moll erzählt dir, dass sein Bruder Dur ihn verraten und einsperren ließ. Da der Komponist nicht vergessen 
kann, sollst du die Botschaft seines Leidens und seines Zorns in Form dieses Liedes erlernen, um künftig 
gegen seinen Bruder gewappnet zu sein. Trete näher an den Grabstein und du erlernst in einer kleinen 
Sequenz die Hymne des Sturms. Du wirst diese Melodie schon bald brauchen. Da es hier vorläufig nichts 
mehr gibt, verlasse erst einmal das Grab und den Friedhof. Es macht Sinn, die Zeit nochmal zurück zu 
drehen, um für die nächsten Aufgaben mehr Zeit zu haben.

## Kaptilel 21: DER IKANA CANYON

Reite wieder zur Straße nach Ikana, doch diesmal gehst du nicht zum Friedhof. Gönne Epona ruhig eine 
Pause und setze Garos Maske auf, die du einst beim Rennen gegen die Gorman Brüder gewonnen hast. 
Sprich mit dem vermummten Kerl, der auf der Klippe sitzt. Da du die Maske des Anführers trägst, dessen 
Ninja einst dieses Königreich bewacht haben, lässt dich die vermummte Gestalt passieren: Neben ihm 
erscheint ein Baumstumpf. Nimm den Fanghaken heraus und zieh dich hoch, damit du der Straße weiter 
folgen kannst. Sie führt dich direkt den Berg hinauf, dem Ikana Canyon.

Nach der einleitenden Sequenz, schau dich um. Vor dir liegt ein Fluss, der diesen Abschnitt in zwei 
Hälften teilt. Folgst du dem Weg weiter rechts, triffst du hier am ersten Tag auf ein bekanntes Gesicht – 
den Dieb Sakon, dem du einmal nachts im Zusammenhang mit der alten Dame getroffen hast. Er erzählt 
dir nichts nützliches (außer sein Interesse an deinem Schwert), aber wenn du dem Wegverlauf weiter 
folgst, befindet sich am Ende Sakons Versteck. Merke dir diese Stelle, denn sie wird einmal wichtig für 
die Geschichte um Kafei, jenen blau-haarigen Jungen, den du am Waschplatz getroffen hast. Oben 
befindet sich ferner noch ein Deku Händler, bei dem du ein blaues Elixier für 100 Rubine kaufen kannst.

Folgst du dem Flussverlauf, so erkennst du an einer Stelle auf der gegenüberliegenden Seite eine 
Anlegestelle. Davor machen zwei Oktoroks dir das Leben schwer. Kein Problem, wozu hast du die 
Eispfeile? Verwandele die beiden Monster in nützliche Eisblöcke, um auf die andere Seite zu gelangen.

Von dort brauchst du dich per Fanghaken nur an den hölzernen Pfeilern entlang zu ziehen, um die Klippe 
zu erklimmen. Oben angekommen, aktiviere die Eulenstatue direkt vor dir – ersparst du dir in Zukunft 
ein ganzes Stück Weg.

Suchen wir nun erst einmal Tingle. Du musst nicht lange suchen, denn sein roter Ballon ist nicht zu 
übersehen. Schieße diesen mit einem Pfeil ab und kaufe von ihm für schlappe 20 Rubine die Karte des 
Felsenturms. Mit der Karte im Gepäck wirst du dich leichter an diesem Ort orientieren können.

Bevor wir den Zwillingsbruder des Komponisten aufsuchen und das Spielhaus von seinen zahlreichen 
Mumien befreien, machen wir einen kleinen Abstecher zur Geisterjagd. Das dazugehörige Haus befindet 
sich oben links auf der Karte. Sprich mit der vermummten Gestalt, die dir rigoros 30 Rubine abknöpft 
und stelle dich dem Kampf. Innerhalb 3 Minuten musst du 4 Geister besiegen. Ocarina of Time Fans 
werden diese Geister bereits kennen. Bespicke die ersten drei mit Pfeilen. Der letzte Geist wird drei 
Kopien von sich erschaffen, um dich zu verwirren. Du erkennst den echten Geist daran, dass er sich beim 
Erscheinen einmal zusätzlich dreht. Sind alle Geister besiegt, erhältst du ein Herzteil.

In diesem Abschnitt des Ikana Canyon entdeckst du oben den Eingang einer Höhle. Gehe hinein, um den 
finsteren Bruder und Geist Dur zu treffen. Ehe er dich mit einem Schlaflied verhexen kann, spiele die 
Hymne des Sturms. Dur wird aufgeben, denn du hast soeben die vertrocknete Quelle wieder mit Wasser 
gefüllt und den Fluch gebrochen – die Mumien und alle anderen bösen Geister werden dank des Regens 
in die Unterwelt verbannt. 

Verlasse die Quelle und widme dich der Musikbox, einem sehr auffälligem Haus, welches sich hier oben 
befindet. Jetzt wo die Mumien verschwunden sind, entdeckst du davor ein kleines Mädchen. Kommst du 
ihr zu nahe, verschwindet es panisch hinter der Tür. Aber du hast ja.. die Fels-Maske. Setze die Maske 
auf und das Mädchen wird dich nicht bemerken. Um es herauszulocken, lege einfach eine Bombe oder 
warte zwei Minuten. Im Haus, eile die Treppen herunter und du kommst ihrem Geheimnis auf die Spur: 
Vor dir kommt der verfluchte Vater des Kindes entgegen. Spiele das Lied der Befreiung, damit der Fluch 
in Form einer Maske gebannt wird. Zufrieden beobachtest du, wie Vater und Tochter wieder 
zusammenfinden. Auf dem Boden liegt Gibdo Maske, die du besser an dich nimmst. 

## Kaptilel 22: BRUNNENGESCHÄFTE

Verlasse die Musikbox und gehe erneut den Weg zur Quelle rauf. Doch du gehst nicht in die Höhle 
sondern nimmst den Weg links. Du entdeckst ganz im Westen der Karte einen Brunnen. Bevor du aber 
reingehst, solltest du ein paar Besorgungen machen. Die meisten Sachen wie Fische, Irrlichter, 
Dekunüsse, Bomben oder die Milch kannst du im Brunnen finden. Ein blaues Elixier kannst du zum 
Beispiel beim Deku neben Sakons Versteck für 100 Rubine bekommen und 5 Wundererbsen beim Deku 
neben dem Sumpf-Infocenter. 

Du brauchst im Brunnen nicht alle Bereiche erkunden (außer du möchtest der Vollständigkeit halber 2 
violette Rubine im Wert von 50 Rubinen einsammeln).

Um im Spiel weiter voran zu kommen, benötigst du mindestens folgende Dinge: 5 Wundererbsen, 1 
Fisch, 10 Dekunüsse, 10 Bomben und 1 Milch. Milch erhältst du auch beispielsweise in der Milchbar oder 
melke die Kühe (auf der Romani Ranch), wenn du Eponas Lied vorspielst. Fische findest du überall in 
Tümpeln und Quellen. Zudem solltest du dir unbedingt noch ein Pulverfass besorgen – du wirst dieses 
zwar nicht im Brunnen verwenden, doch danach wirst du es brauchen.

Im Brunnen selbst, setze dir zunächst die Gibdo Maske auf. Nun werden dich die Mumien nicht weiter 
angreifen. Gehe nach rechts und sprich mit dem Gibdo. Gib ihm die 5 Wundererbsen und er lässt dich 
passieren. Im nächsten Raum gehe geradeaus (ignoriere links von dir den Eisatmer). Es gibt hier wieder 
zwei Gibdos. Schau dir die Mumie weiter hinten an, die von dir 10 Dekunüsse möchte. 

In der nächsten Kammer kannst du bei der nächsten Abzweigung rechts ein paar Käfer einsammeln. 
Schau dich anschließend um – auch hier stehen wieder zwei Gibdos. Gib dem Gibdo oben seine 10 
Bomben und er lässt dich passieren. Dahinter begegnest du einem Nachtschwärmer. Bespicke ihn mit ein 
paar Pfeilen und er wird rasch aufgeben. Fange die Flamme seines Geistes in einer Flasche ein. 

Eile nun den Weg zwei Räume zurück bis zu der Kammer, wo du einst dem Gibdo die 10 Dekunüsse 
gegeben hast. Sprich mit dem anderen Gibdo, der von dir einen Fisch verlangt. Tu wie geheißen und er 
lässt dich passieren. Hier kannst du rechts ein paar Dekunüsse einsammeln. Zwei weitere Gibdos stehen 
in diesen Raum. Sprich mit dem Gibdo links, der von dir Käfer haben möchte. Dahinter kannst du die 
unsichtbaren Fledermäuse erledigen (nutze dabei das Auge der Wahrheit) und entzünde die Fackeln. Es 
erscheint eine kleine Truhe, aus der du einen violetten Rubin (50 Rubine) entnehmen kannst.

Zurück geht es in den Vorraum. Sprich mit dem anderen Gibdo, der deinen Nachtschwärmer haben 
möchte. Gib ihm diesen und er lässt dich passieren. Dahinter versperren dir zwei Stachelwalzen den 
Weg. Verwandele dich in Dekulink, um die Dekublume davor zu nutzen. Auf der anderen Seite 
angekommen, sprich mit dem Gibdo, der eine Flasche Milch möchte. Gib sie ihm und auch er gibt dir den 
Weg frei.

Fast geschafft: Du erkennst in diesem Raum 4 Fackeln. Entzünde sie mit Feuerpfeilen, sodass eine große 
Truhe erscheint. In ihr findest du den sagenhaften Spiegelschild: Mit ihm bist du in der Lage, Sonnenlicht 
zu reflektieren. Das kannst du auch gleich ausprobieren, indem du mit dem Schild das Licht auf den 
Sonnenschalter an der Wand wirfst. Eine Leiter erscheint, mit der du neue unbekannte Gefilde erreichen 
kannst.

## Kaptilel 23: DIE IKANA RUINEN

Wenn du diese Ruinen meistern möchtest, solltest du zuvor ein Pulverfass gekauft haben – ein solches 
bekommst du zum Beispiel im Bomben-Laden in Unruh-Stadt. Es gibt zwei Eingänge zu den Ruinen – der 
eine befindet sich im Brunnen. Der andere Eingang befindet sich unterhalb der Musikbox auf der rechten 
Seite des Ikana Canyon. Um letzteren Eingang freizulegen, betrete die Ikana Ruinen vom Canyon aus, 
sodass ein großer Sonnenblock den Weg versperrt. Aktiviere dazu in deiner Nähe den Kristallschalter, 
sodass Licht zu dir herunterfällt. Lenke das Licht auf den Sonnenblock, sodass er sich auflöst.

Unabhängig davon, durch welchen Eingang zu kommst, landest du zunächst im Innenhof der Ikana 
Ruinen. Es empfiehlt sich, die Gibdo Maske weiter zu tragen – so werden aus den bedrohlichen Mumien 
und garstigen Untoten friedliche Tänzer. 

Eile nun vom Innenhof zur Eingangshalle der Ruinen und trete ein. Du entdeckst hier vier Säulen und 
drei weitere Wege. An zwei Säulen befindet sich jeweils ein vereistes Goldenes Auge. Spann deinen 
Bogen und schieße einen Feuerpfeil in das linke Goldene Auge, sodass sich die Tür auf der linken Seite 
entriegelt. Gehe hindurch.

In dieser Kammer gibt es eine rote heiße Platte, die wie ein Schachbrett gemustert ist. Sie hat die 
unangenehme Eigenschaft, dir auf den Kopf zu fallen. Zu Beginn liegt sie auf dem Boden. Um sie für 
kurze Zeit nach oben zu befördern, solltest du hier in deiner Nähe den Kristallschalter anschlagen. Nun 
renne zur nächsten Dekublume und tauche als Dekulink hinab, bevor die Decke auf dich stürzt. Springst 
du von der Dekublume in die Höhe, wird auch die Decke wieder für einen kurzen Moment nach oben 
fahren. Ziel ist es, auf der rechten Seite die Säule zu erreichen, auf dem du als Link den Bodenschalter 
niederdrücken kannst. Jetzt nur noch auf die andere Seite des Raumes und durch die Tür.

In dem nächsten Raum siehst du einem Abgrund entgegen. Nimm das Auge der Wahrheit zur Hand und 
springe links zum Bodenschalter. Nun wird die Tür auf der anderen Seite entriegelt. Mit dem Auge der 
Wahrheit kannst die Riesenskulltulas erkennen und in diesem Raum abschießen, ehe du über die 
Plattformen zur Tür springst.

 

Die beiden blauen Knochenfratzen brauchen dich nicht zu kümmern. Eile lieber die Treppen hoch, um 
zum Innenhof zurück zu kehren. Diesmal befindest du dich auf dem Dach. Deine Aufgabe ist es, über den 
Sims der Mauer links um die Ecke einen Bodenschalter auf einer Säule zu erreichen. Aktivierst du den 
Schalter, bricht unten an einer Stelle ein kleines Loch auf, sodass Licht in diesem Teil des Tempels hinab 
fällt.

Hier auf dem Dach kannst du rechts unten eine Säule erkennen, auf dem ein Kristallschalter steht. 
Schieße diesen ab, damit für eine kurze Zeit die Feuersäule um eine Dekublume erlischt. Eile zu dieser, 
um von der Dekublume aus zu einer weiteren Säule zu fliegen, auf der ein einsames Herzteil auf dich 
wartet.

Kehre zurück in die Eingangshalle und schieße das rechte vereiste Goldene Auge ab, sodass sich neben 
dir rechts die Tür öffnet. Erledige den Bodengrapscher mit etwas Sonnenlicht oder deinem Schwert, dann 
leite das Licht, welches nun durch die Decke fällt, mit deinem Schild auf den Sonnenblock. Nachdem 
dieser verschwunden ist, kannst du dahinter an den vier Untoten vorbei durch die Tür. 

Dahinter erwartet dich ein freudiges Wiedersehen mit Zaurob. Du kennst ihn ja bereits und seine 
verrückte Vorliebe für Teleporter. Das Prinzip hat sich bei ihm nicht geändert. Du musst seine wahre 
Gestalt von den Trugbildern unterscheiden, wobei sich der echte Zaurob um seine eigene Achse dreht 
und Taya immer auf den richtigen Zaurob zufliegt. Nach einigen Schlägen   gibt auch dieser Gegner auf 
und der Weg ist frei. Dahinter lass die Untoten ruhig tanzen und gehe die Treppe hoch. Oben entdeckst 
du auf der Dachterrasse eine auffällige Platte. Taya weist dich auf die Risse hin. Zeit, dein Pulverfass hier 
abzustellen. Mit einem ordentlichen Knall sorgst du für ein wenig Licht in der Eingangshalle.

Kehre wieder zur Eingangshalle zurück. Jetzt, wo genügend Licht zu dir herunter fällt, kannst du dieses 
auf den Sonnenblock lenken und dir so den Weg zur letzten Kammer freimachen. Dort im Thronsaal 
triffst du auf den König und sein Gefolge, welcher über dein Erscheinen nicht begeistert ist. Er ruft zum 
Angriff.

Zunächst verschaffst du dir etwas Licht in diesem Raum, indem du die Vorhänge mit  Feuerpfeilen 
niederbrennst. Das entsetzt deine modrigen Knochenfreunde. Die beiden Skelette besiegst du rasch, 
indem du sie zunächst mit dem Schwert niederstreckst und dann das Sonnenlicht auf sie lenkst. 
Nachdem ihre Knochen verbrannt sind, wird sich der König selbst zu dir herablassen. Er verträgt mehr 
als seine Untergebenen, aber sobald auch er am Boden liegt, gib ihm mit etwas Sonnenlicht den Rest. 

Nachdem du ihnen die Augen geöffnet hast, werden sie dir versonnen eine alte Melodie lehren: Die Elegie 
des leeren Herzens. Mit ihnen kannst du leere Hüllen von dir in Form von Statuen erschaffen – maximal 
bis zu 4 Stück! Dieses Lied wirst du brauchen, um den nächsten  Tempel meistern zu können. Nachdem 
die Geister dich verlassen haben, kannst du auch die Ruinen hinter dich lassen. 

Bevor wir zum nächsten Tempel aufbrechen, wollen wir uns noch rasch zwei Flaschen holen. Die erste 
befindet sich bei Madame Aroma. Spiele die Kafei-Quest soweit, dass du vom Kuriositäten-Händler die 
Eilpost von Mama erhältst. Bringe sie nach Mitternacht des letzten Tages zur Milchbar, setze die Kafei-
Maske auf und sprich mit Madame Aroma. Als Dank für deinen Einsatz überlässt sie dir eine Flasche mit 
Chateau Romani, einem speziellen Getränk. Trinkst du diese Supermilch, verändert sich deine 
Magieleiste von grün nach blau und du kannst unendlich oft Magie wirken. Der Zustand hält solange an, 
bis zur Dämmerung des Ersten Tages zurückkehrst. Du kannst diese Milch auch für 200 Rubine in der 
Milchbar kaufen.

Die andere Flasche befindet sich auf dem Ikana Friedhof. Kehre in der Nacht des Letzten Tages dorthin, 
setze die Maske des Hauptmanns auf und sprich mit den Knochengängern, die im unteren Abschnitt des 
Friedhofs ein Grab bewachen. Gib ihnen den Befehl, das Grab freizulegen und springe ins Erdloch. Unten 
triffst du auf dem Friedhofswächter Dampe, der nach einem königlichen Schatz sucht.

Da Dampe seine Fackel verloren hat, hilf ihm nach dem Schatz zu graben. Führe ihn zu den Erdlöchern 
und er beginnt zu graben. Es gilt an allen Löchern zu graben und die drei blaue Lichter aus den 
Erdhaufen zu befreien, sodass der Weg zum nächsten Raum frei wird. Erledige dahinter das Irrlicht in 
dieser Kammer, damit schließlich eine große Truhe erscheint. Eine leere Flasche liegt darin.

## Kaptilel 24: AUFSTIEG MIT HINDERNISSEN

Möchtest du den Felsenturm erklimmen und den anschließenden Tempel meistern, empfiehlt es sich, die 
Zeit zuvor zurück zu drehen und sich bequem per Eulenstatue zum Ikana Canyon zu teleportieren. Von 
dort aus ist es nicht mehr weit zum Felsenturm – der Eingang befindet sich neben der Quelle auf der 
rechten oberen Seite der Karte. 

Doch bevor du in den eigentlichen Tempel gelangst, musst du diesen erst einmal erklimmen. Nachdem 
die einleitende Sequenz vorüber ist, springe zunächst (mit den Hasenohren) über die Steinblöcke auf die 
andere Seite. Pass auf die rollenden Felsen auf – du entdeckst schließlich einen Schalter. Stell dich 
darauf und spiele die Elegie des leeren Herzens. Eine Statue erscheint und nimmt dort deinen Platz ein. 
Weil der Bodenschalter nun aktiv ist, schiebt sich einer der Blöcke hinter dir nach oben.

Ziehe dich per Fanghaken zu den nächsten höheren Ebene. Auch hier findest du einen Bodenschalter. 
Verwandele dich als Gorone und spiele erneut die Elegie des leeren Herzens. Eine weitere Statue bewacht 
nun den Schalter. Ziehe dich wieder per Fanghaken zum nächsten Block und verwandle dich beim 
nächsten Schalter in einen Zora. Auch diesmal spiele die Elegie des leeren Herzens. Wurden alle drei 
Schalter mit einer Statue von dir versehen, wurden die Steinblöcke von unten nach oben bewegt und 
bilden eine neue Brücke.

Auf der anderen Seite entdeckst du drei Bodenschalter nebeneinander – da du aber nicht wieder herunter 
möchtest, kannst du diese Schalter ignorieren. Ziehe dich mit dem Fanghaken zur nächsthöheren 
Plattform. Auch hier musst du nach und nach auf den folgenden Bodenschaltern deine Statue in deinen 
unterschiedlichen Formen ablegen. Manchmal lassen sich die nächsten Zielpunkte für den Fanghaken 
schlecht anvisieren; in diesem Fall musst du  entweder auf den Greifpunkt-Pfeiler klettern oder dich ganz 
an den Rand stellen. Hast du es geschafft, kannst du die nächste Brücke passieren.

Nimm auf der anderen Seite deinen Fanghaken heraus und ziehe dich über die Pfeiler nach oben. Du bist 
nun ganz oben – und dir gegenüber liegt der Felsenturm Tempel. Aktiviere zunächst die Eulenstatue, 
damit du in Zukunft nicht mehr den Aufstieg bewältigen musst. Bevor du den Felsenturm Tempel 
besuchen kannst, gilt es noch ein letztes Mal eine Brücke zu bauen. Die drei Bodenschalter sind hier oben 
nicht zu übersehen, doch sie sind in einer bestimmten Reihenfolge (mit dem Blick zur Wand) zu 
besetzen:

rechts, links, mitte

Spiele in die Elegie des leeren Herzens und befolge die Reihenfolge, sodass sich der Weg zur anderen 
Seite ebnet. Jetzt steht dir und dem Felsenturm Tempel nichts mehr im Wege.

## Kaptilel 25: DER FELSENTURM TEMPEL

Stelle sicher, dass du in diesem Tempel mindestens zwei Flaschen grünes Elixier mit dir führst – du wirst 
es später beim Endkampf gut gebrauchen. Ideal ist natürlich eine Flasche mit Chateau Romani, die du in 
der Milchbar für 200 Rubine kaufen kannst – mit diesem Milch kannst du unendlich viel Magie wirken, bis 
du zur Dämmerung des ersten Tages zurück reist.

Willkommen im Vorhof des Himmels! Du wirst schnell das wahre Wesen dieses Tempels kennen lernen – 
also wundere dich nicht, solltest du Truhen an den Decken finden. Nachdem die einleitende Sequenz 
vorbei ist, entdeckst du vor dir eine große Felsstatue. Im linken Auge befindet sich ein Goldenes Auge, 
dass du gekonnt mit einem Pfeilschuss aktivierst. Es erscheint eine kleine Truhe, die du per Fanghaken 
(oder einem guten Sprung mit deiner Hasenmaske) erreichen kannst. Eine verirrte Fee (1) ist dein. Um 
weiter zu kommen, gehe auf der linken Seite die Tür und eile hindurch.

Es folgt ein Raum mit explosiven Krabbelratten. Erledige diese mit paar Pfeilen. Anschließend schau dich 
um – du entdeckst hier zunächst eine rissige Wand. Nimm eine Bombe (oder deine Ka-Bumm Maske) 
und sprenge die Wand. Dahinter entdeckst du eine Holzkiste. Es gibt hier vier Bodenschalter und ein 
schwerfälliges Tor, welches sich nur dann hebt, sobald auf allen vier Schaltern ein Gewicht steht. Da die 
Statue von Dekulink zu leicht ist, wirst du ersatzweise auf einen Schalter eine Holzkiste platzieren. Auf 
den großen Schalter spiele als Gorone die Elegie des leeren Herzens; auf den verbleibenden zwei 
Schaltern platzierst du jeweils eine Statue von Link und als Zora. Nun steht das Tor offen.

Hier im kleinen Innenhof (pass auf die Drachenlibellen auf) entdeckst du eine Stelle, die dunkler ist als 
der Rest des Bodens und ohne Pflanzenbewuchs – lege eine Bombe dorthin und  ein Durchgang nach 
unten erscheint. Springe hinterher (falls du nicht mutig genug bist kannst du auch die Treppe nach unten 
nehmen). Du gelangst in den Keller. Da dank deines Einsatzes hier Licht hinab fällt, kannst du zunächst 
das Licht per Spiegelschild auf den Sonnenblock lenken, damit dieser verschwindet. Verwandele dich nun 
in einen Goronen, um dahinter die Lava zu überqueren. Umringt von vier Armosstatuen steht am Ende 
eine große Truhe, in der du die Labyrinth-Karte findest.

Sind alle vier Armosstatuen besiegt, erscheint eine weitere kleine Truhe. Darin findest du einen kleinen 
Schlüssel. Eile dann wieder in Richtung Treppe. Bevor du diese hoch gehst, schau dich um – du 
entdeckst an der Decke einen Greifpunkt. Ziehe dich per Fanghaken hoch und du findest in einer 
weiteren kleinen Truhe die nächste verirrte Fee (2). Nun steige die Treppen hoch zum Innenhof – mit 
dem kleinen Schlüssel kannst du hier die verschlossene Tür öffnen. Gehe hindurch. 

Der nächste Raum enthält ein großes Wasserbecken. Verwandele dich zum Zora und schwimme zur 
anderen Seite. Dort entdeckst du vor einem Wasserkanal eine Dexi-Hand. Zerstöre sie noch nicht, denn 
du brauchst sie nun. Lass dich von ihr ergreifen und sie wirft dich an ein Ufer mit einer kleinen Truhe, die 
auf einem dir zu hohen Absatz stand. Ein weiterer kleiner Schlüssel ist dein. Kehre zum Wasserkanal 
zurück, erledige die Dexi-Hand mit einem gekonnten Schuss deiner Flossen und schwimme hindurch.

Du kommst an einen Schalter vorbei, der jedoch an der Decke hängt. Im Augenblick kannst du nichts 
machen. Schwimme weiter und du gelangst zu einer weiteren Kammer mit einem ebenso großen 
Wasserbecken. Auf dem Boden entdeckst du einen Bodenschalter, der von drei  Stachelbomben umringt 
wird. Tritt darauf und es erscheint im Wasser eine Kiste... jedoch umgedreht. Schwimme zur Oberfläche 
und klettere hier in der Mitte ans Ufer. Schau dich um – du entdeckst ein vereistes Goldenes Auge. Ein 
Schuss mit einem Feuerpfeil lässt eine weitere Truhe erscheinen, aber auch diese befindet sich an der 
Decke.  

Bevor du hier durch die verschlossene Tür gehst, schau dir noch den unteren Teil des Raumes an. Eilst 
du hier in den Gang, entdeckst du einen Sonnenblock, der dir den Weg versperrt. Daneben befindet sich 
ein Spiegel. Wie du schon bemerkt hast, reflektiert dein Schild Licht, befindest du dich noch auf der 
Brücke. Lenke das Sonnenlicht zunächst auf den Spiegel – desto länger du das machst, desto länger wird 
dieser Spiegel das Licht speichern und anschließend zurückwerfen. Mach dies, eile schnell hervor und 
lenke nun das reflektierte Licht auf den Sonnenblock – dieser verschwindet und macht dir den Weg frei 
zur großen Truhe. Du findest den Kompass. Jetzt, wo du alles hast, kannst du auf der anderen Seite die 
verschlossene Tür öffnen.

In der nächsten Kammer befinden sich auffällig viele Sonnenschalter... aber wo ist das Licht? Verwandle 
dich in Goronenlink und schau dir die auffällige Säule am Eingang an. Du kannst die einzelnen Scheiben 
mit deiner Goronenfaust zur Seite schlagen. Ist das getan, fällt Licht hinein... . Im Grunde genommen 
musst du jetzt nur noch das Licht an die verschiedenen Spiegel und auf die Sonnen lenken, wäre da nicht 
der lästige schwarze Spuk. Er regeneriert sich nach kurzen Abständen immer wieder neu – das kann 
besonders lästig werden, insbesondere dann, wenn du gerade dabei bist, die Spiegel mit Licht zu fluten. 
Du kannst natürlich den harten Weg wählen und die Spuk immer wieder beseitigen (und dabei so schnell 
sein, bevor das Licht in den Spiegeln erlischt) – oder du nimmst einfach die Fels-Maske heraus. Mit 
dieser Maske ignorieren dich die Spuks. Lenke also zunächst das Licht auf den rechten Spiegel an der 
Säule, sodass du anschließend mit dessen Licht den Sonnenblock auf der rechten Seite verschwinden 
lassen kannst. Dahinter liegt eine kleine Truhe mit einer verirrten Fee (3). 

Nun widme dich den beiden Spiegeln auf der linken Seite, wobei du das Licht erst auf den Spiegel an der 
Säule lenken musst, anschließend mit dessen Licht den Spiegel an der Wand auflädst, um so am Ende 
den Sonnenblock hinten aufzulösen. [Bevor du weitergehst kannst du bereits das Licht auf die 
Sonnenschalter an der Wand lenken, um eine kleine Truhe mit einer kleinen Fee zu holen - unsere 
Lösung holt sich diese Fee erst später.] Gehe nun oben durch die Tür.

In diesen Abschnitt entdeckst du vor dir eine Dekublume. Du weißt, was du zu tun hast – verwandele 
dich zum Deku und fliege los, bis du links eine Nische erreichst, in der paar Rubine auf dich warten. Dort 
steht auch eine kleine Truhe, aus der du die verirrte Fee (4) befreist.

Fliege wieder zurück zum Anfang dieses Raums und verwandle dich zum Goronen. Du musst nun etwas 
Geschick beweisen: Unter dir befindet sich in der Lava ein gelber Steinsockel, den du als Gorone mit 
einer Stampfattacke herunterdrücken musst. Ist das getan, tickt die Zeit. Rolle dich zusammen und eile 
so schnell, wie du geschwind rollen kannst, zum Bodenschalter auf der anderen Seite dieses Raums, um 
diesen zu betätigen. Erst dann kannst du die kleine Truhe am Ausgang öffnen. Um auch dorthin zu 
gelangen, kehre ein zweites Mal zum Anfang des Raumes zurück und verwandele dich zum Deku. Nutze 
die Dekublume, um zum Ausgang dieses Raumes zu fliegen. Dabei nutze die heiße Luft, die aus den 
Öffnungen am Boden strömt. Ist das geschafft, kannst du dir aus der kleinen Truhe die verirrte Fee (5) 
herausnehmen und diesen Raum durch die Tür verlassen.

Nun kommt es zu einem Kampf, denn im folgenden Raum wartet ein Miniboss auf dich: Der Garo-Meister 
höchstpersönlich fordert dich zum Kampf. Seine Waffen sind zwei Feuerklingen, die du besser vermeiden 
solltest. Weiche seinem Angriff aus und schlage zu, sobald er mit ausgebreiteten Armen dasteht. Auch 
Eispfeile können sehr nützlich sein. Nach einigen Treffern gibt der Garo-Meister sich geschlagen und lässt 
dich an seinem Wissen teilhaben. Aus der großen Truhe nimmst du dir die Lichtpfeile heraus – eine sehr 
mächtige Waffe, die Gegner ordentlich einheizt und mit der du alle Arten von Sonnenschaltern aktivieren 
kannst. Wozu diese Waffe noch so gut ist, wirst du bald herausfinden. 

Es folgt ein kleiner Gang. Schau dir den Boden an, dort entdeckst du unterm Gitter einen Kristallschalter. 
Mit einer Bombe kannst du ihn aktivieren, sodass eine kleine Truhe erscheint.  In ihr findest du eine 
verirrte Fee (6). Schau nun an die Decke und zieh dich per Fanghaken hoch. Erledige oben den großen 
Käfer, indem du ihm erst mit dem Fanghaken die Maske vom Haupt abreist und anschließend mit dem 
Schwert eins überziehst. Eile auf die andere Seite zur Tür und gehe hindurch.

In folgenden Raum machst du schnell Bekanntschaft mit einem Roboter – dem Augor. Seine 
Schwachstelle ist das große blaue Auge. Hier können deine Lichtpfeile helfen, da sie ihn verwunden. Wird 
sein Auge gelb, kannst du auch mit dem Fanghaken nachhelfen. Wiederhole das Spielchen ein paar Mal, 
dann wird Augor schnell zu Boden fallen. Für deine Mühen erscheint eine kleine Truhe mit einer verirrten 
Fee (7). 

Gehe ein paar Schritte vor (pass auf die Drachenfliegen und die Krabbelratte auf) und schau dich um. Du 
entdeckst an einer Wand einen Sonnenschalter. Schieße einen Lichtpfeil, damit sich über dir eine kleine 
Truhe materialisiert. Momentan kannst du sie noch nicht erreichen.

Bevor wir nun fortfahren und das Geheimnis des Tempels lüften, holst du dir noch zwei verirrte Feen, die 
du bisher ausgelassen hast. Dazu kehre zunächst zum Wasserraum zurück, wo du die Bekanntschaft mit 
der Dexi-Hand gemacht hast – hier entdeckst du an den Seiten des Raumes Sonnenblöcke, mit denen du 
vorher nichts anfangen konntest. Mit dem Lichtpfeil ist das aber kein Problem mehr. Du entdeckst hier 
hinter der Absperrung einen Kristallschalter, den du aktivierst. Damit erscheint eine kleine Truhe, die du 
dir natürlich holst – eine verirrte Fee (8) befindet sich darin. 

Wenn du sie dir noch nicht geholt hast, dann fehlt noch die Fee in dem Raum, wo du als Gorone die 
Säule in Scheiben zerschlagen hast [schwimme von der Kammer mit dem Wasserbecken durch den Kanal 
zum Riesenwasserbecken und nimm dort den Weg nach oben, sodass du den Raum mit dem Spuk und 
den Spiegeln erreichst.] Hier kannst du an der Wand dir die Sonnensymbole genauer anschauen. Eine 
davon lässt eine Truhe erscheinen, aus der du  eine verirrte Fee (9) befreist. 

Jetzt wird es Zeit, dem Ratschlag des Garo-Meisters zu folgen. Verlasse den Tempel durch die 
Eingangshalle, sodass du vor dem Eingang stehst. Da die Steinbrücke verschwunden ist, musst du sie 
mithilfe der Schalter und der Elegie des Herzens nochmal bauen. Stell dich anschließend auf die Brücke 
und betrachte vom letzten Stein, der dich vom Tor zum Tempel trennt, eben jenen Eingang. Du findest 
ein rotes Symbol. Schieße einen Lichtpfeil darauf und du wirst Zeuge, das sich der Tempel verändert: 
Tatsächlich, du hast die Welt soeben auf den Kopf gedreht. Über die Brücke geht es wieder hinein in den 
Tempel.

[Es macht Sinn, ab dieser Stelle eine Flasche mit Chateau Romani zu verwenden, da du im weiteren 
Verlauf oftmals den Lichtpfeil einsetzen wirst. Du kannst diese Milch in der Milchbar für 200 Rubine 
kaufen. Trinkst du sie, färbt sich deine Magieleiste blau und du kannst unendlich oft Magie wirken.]

Nun, wo der Tempel gedreht ist, kannst du nun Truhen und Schalter erreichen, die sich vorher auf der 
Decke befanden. Vor dir steht dank deines bisherigen Einsatzes eine kleine Truhe, in der wieder eine 
verirrte Fee (10) schlummert. Von hier aus wähle den Weg nach rechts und gehe durch die Tür. Dahinter 
erwartet dich ein Sonnenblock – mit einem Lichtpfeil ist dieser schnell hinfort. Du kommst zur Halle, wo 
einst das Riesenwasserbecken gewesen ist. Nun befinden sich hier heiße Luftströme und eine Dekublume 
direkt vor dir.

Krieche in die Dekublume und begib dich in die Lüfte. Zunächst fliegst du nach rechts, um einen Schalter 
zu erreichen. Stell dich darauf, damit die Flammen um eine Kiste verschwinden. Spiele die Elegie des 
leeren Herzens, um eine Statue auf den Schalter zu platzieren. Anschließend verkriechst du dich wieder 
in deine Dekublume, um zur kleinen Truhe zu fliegen, die sich über dem Eingang befindet. Dort findest 
du die nächste verirrte Fee (11). 

Fliege nun als Deku zur Brücke, die die beiden Enden dieses Raumes verbindet. Schleich dich am 
Riesenkäfer vorbei und eile nach Norden durch den Gang. Du findest hier einen Schalter, der einst einmal 
an der Decke gewesen ist. Stampfst du ihn in den Boden, erscheint eine kleine Truhe. Nun geht es 
wieder zurück, vorbei an dem Käfer und ab zur Truhe. Du findest einen kleinen Schlüssel.  

Bevor du weiterziehst, schau dich bei der Truhe um, wo du eben den kleinen Schlüssel gefunden hast. 
Du findest an der Wand einen Sonnenschalter. Schieße einen Lichtpfeil ab, sodass eine kleine Truhe 
erscheint, die du zur Zeit nicht erreichen kannst. Nutze dann die heißen Luftströme, um zur 
verschlossenen Tür ganz oben links zu fliegen. Gehe hindurch.

In dieser kleinen Kammer entdeckst du das dir bekannte rote Symbol. Schieße einen Lichtpfeil, sodass 
sich der Raum dreht. Nun verwandele dich zum Goronen und rolle über die Lava zur anderen Seite. Da 
die Tür für dich unerreichbar ist, musst du einen weiteren Lichtpfeil auf das Symbol schießen, damit du 
durch sie hindurch gehen kannst.

Im nächsten Raum gilt es einen grauen Block in die dafür vorgesehene Senkung zu schieben, die sich auf 
der anderen Seite befindet. Hierzu musst du mehrmals den Raum drehen, indem du einen Lichtpfeil auf 
das rote Symbol schießt. Sollten dir Pfeile oder Magie ausgehen, kannst du dir Nachschub beim grünen 
bzw. dem blauen Wabbler holen. Landet der graue Block in seiner Fassung, entriegelt sich die Tür. 
Springe anschließend über den Block zu dieser und eile hindurch. 

Ja... da ist er wieder: Ein weiteres Mal begrüßt dich der verrückte Zaurob mit seinen Teleportern, die 
überall im Raum verteilt sind. Bleib einfach bei der Tür stehen und warte bis er erscheint. Mit paar 
Lichtpfeilen gibt er ganz schnell auf und hinterlässt dir eine kleine Truhe. Zieh dich per Fanghaken auf 
das Podest und hol dir aus der Kiste eine verirrte Fee (12). Daneben gehst du nun die Treppen runter. 

Hier solltest du besser nicht runter fallen. Zudem treiben Irrlichter ihr Unwesen. Verkrieche dich rasch in 
die Dekublume vor dir und fliege mit ihr zur anderen Seite. Dort suchst du schnell Schutz in der nächsten 
Dekublume, sodass du mit ihrer Kraft zur Nische oben rechts fliegen kannst. 

Du gelangst in einen Gang – früher einmal hattest du hier vier Schalter betätigen müssen. Jetzt treiben 
fliegende Schatten-Armos ihr Unwesen. Ziehe deine Fels-Maske auf, damit sie dich nicht sehen, 
andernfalls musst du sie mit Lichtpfeilen herunterholen. Du findest hier einen Schalter; spiele darauf die 
Elegie des leeren Herzens, sodass das Feuer um die kleine Truhe verschwindet. Eile zu dieser und du 
findest darin einen kleinen Schlüssel.

Kehre zurück zum Vorraum mit den Geistern und eile hier durch die Tür. Du gelangst in einen weiteren 
Raum mit einem großen Abgrund unter dir. Bevor du in die Dekublume hüpfst, schieße zunächst die 
Stachelbomben links von dir ab. Anschließend fliege dorthin und sinke zur Dekublume nieder. Mit ihr 
kannst du nun die andere Seite des Raumes erreichen und durch die Tür gehen. Eile dahinter den Gang 
entlang und lass dich nicht von den beiden blauen Knochenfratzen erwischen. 

In der nächsten Kammer erwartet dich ein neuer Kampf mit einem weiteren Miniboss: Gomess, der 
Herrscher der Fledermäuse will dich zur Strecke bringen. Es sind die Fledermäuse, die diesen Gegner 
beschützen – mit einem Lichtpfeil kannst du diese für einen kurzen Moment verjagen. Ziele einen 
weiteren Lichtpfeil auf sein gelbes Herz, dass auf der Brust des Ungeheuers zu sehen ist. Attackiere ihn 
anschließend mit dem Schwert. Achte auf die Sense deines Feindes, da Gomess eine große Reichweite 
mit dieser Waffe besitzt. Wiederhole paar mal dein Spielchen, dann wird auch Gomess in Asche 
aufgehen. Nun ist der Weg zur blauen Truhe frei und aus dieser holst du den Masterschlüssel.

Eile nun den Weg zurück, vorbei an den beiden Knochenfratzen, zum Raum mit dem großen Abgrund. 
Schieße hier wieder rechts die Stachelbomben ab, fliege dorthin und nutze hier die Dekublume, um auf 
die andere Seite zu gelangen. Gehe aber hier nicht zurück zum Raum mit den Geistern, sondern laufe zur 
Tür unten. Gehe hindurch. Hier setze die Fels-Maske auf und springe herüber zum Schatten-Armos 
(andernfalls musst du ihn erst erledigen). Tritt auf den Schalter und es erscheint eine Truhe an der 
Decke. 

Da dir nun nicht länger der Weg zum Endgegner versperrt ist, ist es besser nun die letzten verirrten Feen 
einzusammeln. Kehre zunächst in den Raum zurück, wo einst das Riesenwasserbecken war und sich 
stattdessen die Heißluftströme befinden. Verwandele dich zum Deku und fliege über die Ströme 
geradeaus zur anderen Seite, wo in der Nische eine kleine Truhe mit einer verirrten Fee (13) auf dich 
wartet. Eile anschließend aus dem Tempel und schieße wieder auf das rote Symbol, um den Tempel zu 
drehen. Gehe wieder hinein. Dank deines Einsatzes findest du in der Eingangshalle unten eine kleine 
Truhe mit einer weiteren verirrten Fee (14). 

Eile jetzt nochmal zum Riesenwasserbecken (von der Eingangshalle einfach die Tür rechts), welches dank 
des Drehens nun wieder Wasser enthält. Nun befindet sich hier im Wasser auf einem Podest eine kleine 
Truhe, die du vorhin nicht erreichen konntest. Darin versteckt sich die letzte verirrte Fee (15). Gehe 
wieder aus dem Tempel und drehe ihn erneut. Steht die Welt wieder auf dem Kopf und eilst du die 
Eingangshalle entlang, erinnert dich Taya daran, dass du dich doch per Fanghaken an die Truhe oben 
ziehen könntest. Gesagt getan und du erreichst oben den Zugang zu einer verschlossenen Tür, die du 
entriegelst.

Es folgt ein letzter Kampf mit einem weiteren Roboter – doch Augor hat diesmal mehr Tricks auf Lager 
und kann mit einem Laser schießen. Das ändert nichts daran, dass du seinen Strahl mit dem 
Spiegelschild abwehrst und ihm einige Lichtpfeile in sein Auge verpasst: Ein Treffer blendet ihn und färbt 
sein Auge gelb; ein weiterer Lichtpfeil verursacht Schaden. Nach einigen Wiederholungen wird auch diese 
Einheit aufgeben. Er hinterlässt dir seinen Schatz – die Maske des Giganten. Wozu die wohl gut ist …?

Eile durch die Tür in die letzte Kammer, die dich von deinem Endgegner trennt. Vor der Tür zum Boss gilt 
es noch die Stachelwalzen zu überqueren. Dazu schaue nach oben und nutze die kleine Truhe bzw. die 
Zielscheibe, um dich so nach und nach zum Ziel zu bringen. Entriegele das schwere Schloss und gehe 
durch die Tür. Dahinter musst du nur noch in den See der Dimensionen springen, das heißt, wenn du 
genügend magische Energie besitzt …

A Link to the Past Fans werden diesen Gegner kennen: Twinmold besteht aus zwei gigantischen 
Sandwürmern, die sich in dieser endlosen Wüste bequem gemacht haben. 

Strategie: Um diesen Gegner bezwingen, benötigst du sehr viel magische Energie... oder zumindest 
genug, um eine Weile die Maske des Giganten zu nutzen. Mit ihr verwandelt sich Link ebenso in einen 
Giganten – einziger Nachteil ist es, dass diese Maske nur solange funktioniert, wie du magische Energie 
vorrätig hast. Solltest du kein grünes Elixier oder einen Schluck Chateau Romani dabei haben und die 
magische Energie knapp werden, zerstöre hier die Säulen. Nimm dann die Maske ab, um in den 
Trümmern nach den wenigen Magieflaschen zu suchen. 

Die Würmer selbst besitzen zwei Schwachstellen – der Kopf und der Schwanz. An beiden Stellen kann 
Link als Gigant mit dem Schwert zuschlagen. Sollte der unglückliche Fall eintreten und die magische 
Energie zur Neige gehen, kann Link versuchen, mit Pfeilen den Schwanz zu treffen... doch dazu gehört 
perfektes Timing, ehe der Schwanz im Sand verschwunden ist. 

Nach einigen Treffern geben die Würmer auf und hinterlassen dir einen weiteren Herzcontainer. Nun 
kannst du die letzte Maske der Götter an dich nehmen: Twinmolds Vermächtnis. 

Nachdem Link eine weitere Sequenz im Himmelreich erlebt hat, erscheint ein merkwürdiges Licht über 
dem Ikana Canyon. Es scheint, als wurde ein alter Fluch gebrochen... nun gilt es nur noch, deine 
gesammelten Feen zurück zu bringen. Gehe dazu zur Felswand, die sich rechts von der Musikbox und 
nördlich vom Eingang zu den Ikana Ruinen befindet.

Innen erwartet dich die Feenquelle des Ikana Canyons. Trittst du näher, sammeln sich deine verirrten 
Feen und die Große Fee der Güte erscheint dir. Sie überlässt dir ihr Geschenk: Das Feenschwert. Diese 
Waffe ist so schwer, dass du sie mit beiden Händen festhalten musst. Dafür kann diese Waffe ordentlich 
austeilen und kann nicht von Knochenfratzen verflucht werden. Eine recht mächtige Schlagwaffe.

## Kaptilel 26: DAS SPINNENHAUS AM SUMPF

Um die mächtigste aller Masken zu erlangen und den Kampf gegen Majora's Maske zu erleichtern, musst 
du zunächst die beiden Spinnenhäuser meistern. Sie sind für das Lösen des Spiels  nicht von Nöten, doch 
wir wollen alle Masken sammeln – nur so kannst du die letzte Maske erhalten. Du kannst das 
Spinnenhaus am Sumpf bereits im frühen Spielverlauf erreichen, aber ohne den Fanghaken wirst du nicht 
alle Spinnen einfangen können.

Bevor du aufbrichst, stelle sicher, dass du paar Käfer in einer Flasche gefangen hast, den Fanghaken 
besitzt und zwei Wundererbsen (Quellwasser findest du in der Eingangshalle). Das Spinnenhaus im 
Sumpf befindet sich in den Sümpfen des Vergessens, nicht weit vom großen Wasserfall entfernt (jenem, 
wo sich auch der Eingang zum Dämmerwald Tempel befindet). Zunächst musst du das Spinnennetz am 
Eingang weg brennen (per Dekustab oder Feuerpfeile), bevor du hinein kannst. 

Am Eingang des befindet sich ein unglücklicher Mann auf dem ein Fluch lastet – nur wenn die 30 Spinnen 
besiegt sind, kann auch der Fluch gebrochen werden. Hinweis: Im Spinnenhaus bewegen sich einige 
Spinnen. In diesem Falle muss Link auf sie warten. Ein Raum ist erst dann „Spinnenfrei“, sobald er kein 
Krabbelgeräusch mehr hört. 

Das heißt im Umkehrschluss – solange Link ein Krabbeln hört, befindet sich in diesem Raum noch eine 
Spinne. Desto lauter dieses Geräusch ist, desto näher befindet sich die Spinne. 

[Der Zähler innerhalb der Screenshots folgt nicht chronologisch mit der Beschreibung; wir 
haben die Spinnen zur besseren Übersicht nach Räumen sortiert.]

# Haupthalle 1F [4 Stück]

#01 Diese Skulltula findest du oben links im Wasser herumkrabbeln.
#02 Mit einem Käfer in der Flasche kannst du zur rechten Wandseite der Haupthalle gehen. Du siehst ein 
Erbsenloch an der Wand. Lasse deinen Käfer davor frei und eine Skulltula springt zu dir heraus.
#03 Auf der linken Wandseite gibt es ebenfalls ein Erbsenloch: Lasse deinen Käfer davor frei und eine 
Skulltula kommt zu dir.
#04 Links unten, neben dem Ausgang, befindet sich ein einsamer Tonkrug. Zerschlag ihn für eine 
Skulltula.

# Haupthalle 2F [4 Stück]

Im oberen Stockwerk der Haupthalle befinden sich weitere Skulltulas. Links und rechts von der 
Haupthalle befindet sich jeweils ein weiterer Raum, durch die du wahlweise ins obere Stockwerk 
gelangen kannst. 

#05 Oben links im Raum, stelle dich an die Kante. Du siehst oben eine Spinne, die über dem Eingang 
einer sich weiter unten befindenden Tür von 1F krabbelt.
#06 Oben links im Raum findest du an einer Säule eine weitere Skulltula.
#07 Oben rechts im Raum findest du an einer weiteren Säule eine Spinne.
#08 Unten rechts im Raum kannst du an einem Erbsenloch an der Wand einen weiteren Käfer 
losschicken. Eine Skulltula kommt zu dir.

# Raum geradeaus von der Haupthalle [4 Stück]

#09 Eine Skulltula läuft bequem an der Wand entlang.
#10 Eine Spinne ruht direkt über dem Zugang, der zum einsamen Baum führt. 
#11 Eine Skulltula krabbelt rechts auf einer Säule.
#12 In einem Wespennest an der Decke.

# Raum mit dem einsamen Baum [6 Stück]

Um den Zugang zum Raum mit dem einsamen Baum erreichen, musst du zunächst den Kristallschalter 
aktivieren. Dazu musst du vom rechten Raum in diese Kammer gelangen. Der Kristallschalter lässt eine 
Leiter auf der anderen Seite des Raumes erscheinen. Pflanze dort in das Erdloch eine Wundererbse und 
gieße sie mit Quellwasser (aus der Eingangshalle).Mit dem Blatt kannst du den Zugang erreichen, der 
dich zum Baum führt. [Wer ein Profi ist, schafft man es auch mit Hasenohren in die Nische.]

#13 Eine Skulltula fühlt sich im hohen Gras wohl.
#14 Eine weitere Skulltula liebt ebenso das hohe Gras.
#15 Im Wespennest in der Baumkrone.
#16, 17, 18 Rolle als Gorone gegen den Baum und schon fliegen drei Skulltula zu dir herunter. Das war 
einfach oder? 

# Raum rechts mit den Krügen [7 Stück]

#19 Schau oben an die Decke und schieße die Wespennester ab.
#20 Auch diese Skulltula versteckt sich in einem Wespennest.
#21 Zertrümmer hier unten die kleinen Tonkrüge.
#22 Verwandele dich zum Goronen und führe eine Stampfattacke aus. Vor Schreck springt eine Skulltula 
aus einem Riesenkrug heraus.
#23 Eine zweite Skulltula versteckt sich ebenso in den Riesenkrügen.
#24 Taya weist dich auf den trägen Laubkerl hin. Spiele die Sonate des Erwachens und folge dem 
Laubkerl hinter dem Grasvorhang. Dort krabbelt eine weitere Skulltula.
#25 Eine Spinne findet es an der Wand ganz oben neben der Tür, die zum Raum geradeaus von der 
Haupthalle führt, ganz bequem.

# Raum links mit den Kisten [5 Stück]

#26 In einer Ecke stehen zwei Holzkisten. Rolle mal dagegen (oder lege eine Bombe), um sie zu 
zerschmettern. Unter einer Kiste wirst du fündig.
#27 Es gibt noch mehr Holzkisten in diesem Raum. Zerschlage auch diese für eine weitere Skulltula.
#28 Steige die Leiter hoch und du entdeckst eine Spinne auf der schmalen Säule die in der Mitte des 
Raumes steht. 
#29 Hier oben findest du an einer Fackel eine weitere Skulltula.
#30 Eine Spinne läuft gemütlich an der Wand (1F) entlang. 

Sobald die 30 Skulltulas besiegt wurden, ist der Fluch gebrochen. Für deine Heldentat zeigt sich der 
verfluchte Mann dir erkenntlich. Er schenkt dir die Maske der Wahrheit – mit ihr kannst du die 
Mythensteine verstehen und an ihrer Weisheit teilhaben, die überall in der Welt von Termina verstreut 
sind. Schau mal bei Gelegenheit in unseren Mythenstein-Guide.

## Kaptilel 27: DAS SPINNENHAUS AM MEER

Willst du die mächtigste aller Masken zu erlangen und den Kampf gegen Majora's Maske  erleichtern, 
kommst du nicht an den beiden Spinnenhäusern vorbei – so erwartet dich auch am Stand der 
Schädelbuchtküste ein weiteres Spinnenhaus. Die Spinnenhäuser sind für das Lösen des Spiels nicht von 
Nöten, doch wir wollen alle Masken (und die dazugehörigen nötigen Gegenstände) sammeln; nur so 
können wir die letzte Maske erhalten. 

Das Spinnenhaus am Meer ist nicht schwer zu finden: Von der Ebene Terminas eilst du mit Epona zur 
Schädelbuchtküste. Wichtig ist es, dass du das Spinnenhaus am ersten Tag meisterst – andernfalls wirst 
du nicht die Riesengeldbörse bekommen, sondern nur paar lausige Rubine. Hier gleich am Strand 
entdeckst du einen Eingang. Gehe hinein und du entdeckst eine weiße Wand. Lege eine Bombe davor 
und ein Durchgang nach unten zeichnet sich ab. Auch wieder wollen 30 Skulltulas gefangen werden. Um 
dieses Spinnenhaus zu meistern, solltest du folgende Gegenstände im Gepäck haben: Einen Fanghaken 
und die Feuerpfeile. 

[Der Zähler innerhalb der Screenshots folgt nicht chronologisch mit der Beschreibung; wir 
haben die Spinnen zur besseren Übersicht nach Räumen sortiert.]

# Eingangshalle [3 Stück]

#01, 02 Gleich im Tunnel zur Eingangshalle krabbeln dir die ersten beiden Skulltulas entgegen. Mit dem 
Fanghaken holst du sie schnell zu dir.
#03 Unten angekommen, schau nach oben. An der Decke sitzt im Spinnennetz eine weitere Skulltula. Mit 
dem Feuerpfeil brennst du das Netz nieder und holst sie mit dem Fanghaken.

# Treppenhaus 1F [4 Stück]

#04 Vier Krüge stehen im Treppenraum oben herum. Verwandele dich zum Goronen und lass es mit 
einer Stampfattacke krachen. Eine Spinne wird zu dir geflogen kommen.
#05 Auch die nächste Spinne versteckt sich in einem der Krüge. Zuvor musst du jedoch das Spinnennetz 
an dem Krug mit einem Feuerpfeil entfernen.
#06 Schau mal nach oben an die Decke. An einer der Balken krabbelt eine Skulltula.
#07 Über der Treppe hängen Masken. Aus einer krabbelt ab und zu eine Skulltula.

# Treppenhaus B1F [5 Stück]

#08 Hinter einer Maske krabbelt eine Skulltula manchmal hervor. 
#09 Hinter einem Spinnennetz an der Wand befindet sich ein finsteres Loch. Mit dem Fanghaken holst du 
dir eine weitere Skulltula.
#10 In einem kleinen Tonkrug auf einem Stapel Holzkisten.
#11 Auch hier stehen Krüge an den Wänden. Verwandele dich zum Goronen und schau mal am einsamen 
Krug ganz rechts an der Wand nach. Mit der Stampfattacke lockst du auch diese Spinne aus ihrem 
Versteck.
#12 Auf dem Mauersims oben, zwischen Decke und Wand. Gehe die Treppe zur Hälfte hoch und du 
kannst diese Spinne per Fanghaken erreichen.

# Bibliothek 1F [7 Stück]

Um zur Bibliothek zu gelangen, musst du im Treppenhaus 1F das Spinnennetz vor der Tür mithilfe eines 
Feuerpfeils weg brennen. Es macht Sinn, die Maske des Hauptmanns aufzusetzen und mit den 
Knochengängern zu sprechen.

#13 Ziehe bei den drei Kommoden an der Wand die Mittlere heraus. Dahinter ist ein kleines schwarzes 
Loch, in der eine Skulltula haust. 
#14 Schieße die Bilder an der Wand mit deinem Fanghaken ab. Unter einem Bild springt dir eine 
Skulltula entgegen. 
#15, 16 Auf der gegenüberliegenden Wand von der Tür kannst du unten ein Bücherregal zur Seite 
schieben. Dahinter verstecken sich zwei Skulltula in einer Nische.
#17 Klettere auf die Bücherregale und schieße oben in der Nische, zwischen Wand und Decke, eine 
einsame Skulltula auf dem Sims ab. (In der Nähe des verschiebbaren Bücherregals.)
#18 Klettere auf die Bücherregale und du findest auf einem Regal eine weitere Skulltula.
#19 Auch diese Spinne findet es unter einem Wandbild bequem.

# Esszimmer B1F [6 Stück]

Gehe die Treppe nach unten in den Keller und besuche den Raum rechts. Dort haben sich einige 
Knochengänger zum Essen verabredet. Vielleicht solltest du mal die Maske des Hauptmanns aufsetzen?

#20 Untersuche die Wandbilder im Esszimmer mit deinem Fanghaken.
#21 An einer Wandecke befindet sich ein Spinnennetz. Brenne es weg und schau mal oben auf den Sims, 
jene Nische zwischen Wand und Decke. Dort krabbelt eine Spinne.
#22 Die nächste befindet sich in einem Krug. Ein Goronenstampfer hilft, um die Spinne aus ihrem Nest 
hervor zu jagen.
#23, 24, 25 Ein Goronenstampfer auf dem Tisch jagt gleich drei Spinnen vom Kronleuchter herunter. Na 
das ging schnell.

# Abstellkammer B1F [5 Stück]

Vom Treppenhaus unten im Keller, versperrt links zunächst ein Spinnennetz die Tür. Mit einem Feuerpfeil 
ist das jedoch kein Problem.  

#26 Oben an der Decke kannst du das große Spinnennetz mit einem Feuerpfeil herunter brennen. 
Dahinter hat sich eine Skulltula versteckt.
#27 An der Wand gegenüber wartet eine Skulltula auf dich.
#28 Rolle mal gegen die Holzkisten. Unter einer wirst du fündig.
#29 Zerschlage auch weitere Holzkisten. Hinter einer findest du eine versteckte Nische, in der sich eine 
Skulltula zurückgezogen hat.
#30 Die letzte Skulltula hat sich oben auf der Holzdecke begeben. Um dorthin zu gelangen, klettere die 
Kisten hoch  und schau dich um. Du entdeckst oben einen Greifpunkt, mit dem du dich per Fanghaken 
hochziehen kannst. Oben auf der Zwischendecke kannst du nun die Tonkrüge zerschlagen. 

Hast du alle 30 Skulltula gefangen, solltest du dich wieder hoch zum Eingang begeben. Dort wartet – 
insofern du die Skulltula allesamt am ersten Tag erledigt hast, ein junger Mann, der dir vor lauter Freude 
die Riesengeldbörse schenkt. In ihr haben insgesamt 500 Rubine Platz. 

Ein einsames Herzteil befindet sich im Spinnenhaus. Wie dir vielleicht aufgefallen ist, haben sich einige 
Knochengänger im Spinnenhaus einquartiert. Hast du den Helm des Hauptmanns von Skull Kreta vom 
Ikana Friedhof abgeluchst, setze die Maske auf und sprich mit den Knochengängern in der Bibliothek und 
im Esszimmer. Sie nennen dir eine Kombination aus Farben. Tatsächlich befinden sich im Esszimmer 
links und rechts vom Kamin vier Masken. Es gilt, die Masken in der richtigen Reihenfolge mit Pfeilen 
abzuschießen.

Mit der richtigen Kombination wird sich das Gitter des Kamins heben. Dahinter befindet sich ein 
versteckter Zugang zu einer weiteren kleinen Kammer, die von einer Riesenskulltula bewacht wird. In 
der Truhe findest du schließlich das Herzteil.

## Kaptilel 28: LETZTE BESORGUNGEN

Bevor du zum Mond aufbrichst, kannst du dir nun die letzten Masken holen. Das Sammeln aller Masken 
ist für die Lösung nicht notwendig, aber du möchtest die mächtigste Maske dieses Spiels dein Eigen 
nennen, mit der der Kampf mit Majora's Maske dir garantiert Spaß machen wird. 

Zunächst widmest du dich dem Dilemma von Kafei und Anju. Schon während des Spielverlaufs bist du 
mehrmals dem blau-haarigen Jungen am Waschplatz und mit der schüchternen Dame aus dem Gasthof 
begegnet. Die beiden wieder zusammenzuführen, erfordert eine Reihe von Handlungen und die nötige 
Ausrüstung. 

Mit der Kafei-Maske, die du am ersten Tag zwischen 10-12 Uhr von Madame Aroma in der Residenz des 
Bürgermeisters in Ost-Unruh-Stadt bekommen hast, gehe um 14:00 zum Gasthof zum Eintopf, der sich 
ebenfalls in Ost-Unruh-Stadt befindet. Warte bis der Postbote kommt und mit Anju, der jungen Frau am 
Empfangstisch, spricht. Nachdem der Postbote gegangen ist, setze die Kafei-Maske auf und sprich sie an. 
Vollkommen irritiert, bittet sie dich um ein Treffen gegen 23:30 Uhr in der Küche. 

Tu wie geheißen und triff dich mit Anju. Sie gibt dir Anjus Brief, den du bitte für sie zustellen sollst. 
Werfe ihn in einen Briefkasten deiner Wahl. Wichtig: Du darfst in dieser Nacht keinesfalls der alten Dame 
in Nord-Unruh-Stadt helfen, ansonsten wird der Dieb Sakon im späteren Verlauf nicht seine Höhle 
aufsuchen. Also bitte ausnahmsweise keine gute Tat in dieser Nacht. 

Am nächsten Tag holt der Postbote deinen Brief ab und stellt ihn um 15:00 Uhr des zweiten Tages am 
Waschplatz zu. Läutet er an der Glocke und lockt somit den maskierten blau-haarigen Jungen aus seinem 
Versteck, ist das deine Chance durch die Hintertür zu gehen, während der Junge den Brief abholt. Warte 
im Haus auf ihm, bis er zurückkommt. Er nimmt seine Maske auf und stellt sich dir als Kafei vor. Er 
wurde vom Horrorkid in ein Kind verwandelt und zu allem Unglück wurde ihm die Hochzeitsmaske von 
einem Dieb gestohlen. Da er versprochen hat, Anju mit dieser Maske zu begrüßen, kann er nicht zu ihr 
zurück.

Weil Kafei Anju vermisst, übergibt er dir seinen Glücksbringer. Bringe ihn sogleich zum Gasthof zurück 
und sprich mit Anju, wenn sie wieder am Tresen steht. Dank dieses Zeichens ist Anju nun bereit, auf die 
Rückkehr ihres Geliebten zu warten.

Weil wir nicht wollen, das Anju und Kafei weiter leiden, kehre um 13:00 Uhr des letzten Tages zurück zu 
Kafeis Versteck am Waschplatz. Dort wartet der Kuriositäten-Händler auf dich, der  dir erzählt, dass Kafei 
letzte Nacht jenen Dieb gesehen hat, welcher ihm die Maske gestohlen hat. 

[Der Kuriositäten-Händler übergibt dir ferner die Eilpost und die Fuchs-Maske.  Die Eilpost kannst du in 
der Nacht des letzten Tages entweder dem Postboten geben, damit er sie zustellen kann und dir die 
Postboten-Mütze gibt; oder du bringst sie selbst zur Milchbar, wo dir Madame Aroma eine neue Flasche 
gefüllt mit Chateau Romani schenkt.] 

Jetzt beginnt deine eigentliche Aufgabe: Kehre zum Ikana Canyon zurück. Erinnerst du dich an den Fluss, 
der dieses Gebiet teilt? Richtig, folgst du dem Flussverlauf nach rechts, kommst du zu einer kleinen 
Passage, an dessen Ende eine von einem Felsen versperrte Höhle befindet. Es ist das geheime Versteck 
des Diebs Sakon, den du bestimmt einmal am ersten Tag hier im Ikana Canyon getroffen hast. Kafei 
wartet bereits hier. Sprich ihn an und er erzählt dir aufgeregt, dass er den Dieb ausfindig gemacht hat. 
Versteck dich und warte bis 19:00 Uhr des letzten Tages und du beobachtest, wie Sakon tatsächlich 
erscheint und die Höhle öffnet. (Sakon darf dich nicht sehen, sonst war alles umsonst.) Mutig rennt Kafei 
hinterher... was bleibt dir anderes über, als ihm zu folgen?

Dummerweise löst Kafei in Aufregung gleich die Alarmanlage aus und die Sonnenmaske wird auf einem 
Förderband zu einem Loch transportiert. Natürlich musst du das verhindern. Es gilt nun in Partnerarbeit 
verschiedene Rätsel zu lösen, bevor die Maske unwiderruflich ins Loch fällt. Dabei wirst du Link und Kafei 
jeweils abwechselnd steuern:

Zunächst laufe mit Link auf den blauen Schalter direkt am Startpunkt (jener Schalter, der die Alarm-
Anlage ausgelöst hat). Bleibe dort stehen. Schiebe mit Kafei im ersten Raum einen der violetten Steine 
auf den blauen Schalter. Eile mit Link in die Kammer gegenüber (linke Tür vom Startpunkt) und erledige 
hinten die einsame Dekurahna.

Nun kann Kafei in die zweite Kammer gehen. Hier gibt es rote, gelbe und blaue Schalter. Rote Schalter 
beschleunigen das Fahrband während gelbe Schalter das Band verlangsamen. Wähle den Weg links an 
den roten Schaltern vorbei, um Kafei oben rechts in der Ecke auf den blauen Schalter zu stellen. Link 
erledigt in der zweiten Kammer gegenüber zwei weitere Dekuranha.

In der dritten Kammer wird es kniffelig. Ziel ist es, einen Stein auf den gelben und auf den blauen 
Schalter zu stellen und danach zur Tür zu eilen. Gehe folgendermaßen vor: Zunächst schiebe die beiden 
Steine über den gelben und roten Steine jeweils links bzw. rechts, sodass sie nach außen an der Wand 
stehen. Nun kannst du den mittleren Stein zwischen dem gelben und dem roten Schalter nach links auf 
den gelben Schalter schieben. Ist das getan, schiebe unten den Stein auf den blauen Schalter. 
Währenddessen folgt Link in die dritte Kammer und wird gleich von einem Schneewolfo attackiert. 
Erledige ihn.

Bei Kafei ist jetzt die Tür offen, aber der Weg ist dank der Steine versperrt. Dazu kannst du den rechten 
Stein oben an der Wand wieder zu dir ziehen, sodass er ein Feld oberhalb des roten Schalters steht. Nun 
eile dahinter und schiebe den Stein vor der Tür weg nach unten. 

In der letzten Kammer stell dich mit Kafei schnell auf den blauen Schalter. 
Link folgt ihm ebenso in die letzten Kammer und tut es ihm gleich, indem er sich auf den anderen 
Schalter stellt. 

Nun stoppt das Förderband und Kafei kann sich die Sonnenmaske holen. Nachdem er den Canyon 
verlassen hat, eile nach Mitternacht zurück nach Unruh-Stadt und besuche im Gasthof Anjus Zimmer im 
oberen Stockwerk. Um 4:30 Uhr früh erscheint Kafei endlich und löst bei Anju sein Versprechen ein, 
indem sie beide ihre Masken tauschen – Anjus Mondmaske und Kafeis Sonnenmaske. Die beiden 
schenken dir die Maske der Liebenden, das Symbol ihrer Heirat. Nun kann sie nichts mehr trennen, auch 
nicht mehr der nächste Sonnenaufgang ...  Du aber solltest dich lieber mit der Hymne der Zeit in 
Sicherheit bringen. 

Mit einem neuen Durchgang kannst du die Maske der Liebenden gleich ausprobieren. Gehe dazu tagsüber 
nach Ost-Unruh-Stadt und eile in das Zimmer des Bürgermeisters. Du beobachtest das Streitgespräch 
zwischen dem Handwerks-Meister und dem Hauptmann der Wache. Setze die Maske der Liebenden auf 
und sprich mit dem Bürgermeister, sodass dieser erkennt, wie sinnlos die Debatte ist. Zum Dank für 
diese Erkenntnis schenkt er dir ein Herzteil.

Wenn du Rubine sammelst, solltest du vor der Rückreise zum ersten Tag sie beim Bankier in West-
Unruh-Stadt überlassen. Da sind sie nicht nur sicher, sondern das Sammeln von Rubinen hat auch einen 
Zweck. Häufst du insgesamt 5000 Rubine an, schenkt er dir ein Herzteil. Tipp: Rubine kannst du ziemlich 
schnell bei den tanzenden Untoten in den Ikana Ruinen einsammeln, da sie gern pro Untoten einen roten 
Rubin (20 Rubine) hergeben.

Nun holst du dir eine weitere Maske – die Maske der Nacht. Um diese zu bekommen, musst du zunächst 
der alten Dame helfen, die in der Nacht des ersten Tages um 1:00 Uhr in Nord-Unruh-Stadt von einem 
Dieb belästigt wird. Bring ihr das Beutegut zurück. [Dafür gibt es die Große Bomben-Tasche billiger.] 
Jetzt eile in der Nacht des letzten Tages um 23:00 zum Kuriositäten-Händler in West-Unruh-Stadt. Er 
bietet dir nun seine neuste seltene Ware an: Die Maske der Nacht. Für 500 Rubine ist sie dein.

Nun holst du dir für die Vollständigkeit noch ein paar Herzteile. Mit der Maske der Nacht kannst du 
tagsüber im Gasthof mit der alten Oma sprechen. Setze die Maske der Nacht auf und höre dir beide 
Geschichten an – für jedes Geschichte kannst du ein Herzteil bei ihr abholen: Beantworte bei der ersten 
Geschichte [Karneval der Zeit] ihre Frage richtig; bei der zweiten Geschichte [Die Vier Giganten] solltest 
du besser keine Ahnung haben.

Hast du schon einmal mit den Deku-Händlern Urkunden ausgetauscht? Du erinnerst dich: Um Zugang 
zum oberen Teil des Uhrturms zu erhalten, musst du die Mondträne dem Deku-Händler in Stadt Unruh 
geben. Er überlässt dir die Urkunde seiner Dekublume. Wenn du dir ein wenig Zeit hierfür nimmst, 
kannst du insgesamt 4 Herzteile einsammeln:

In den Sümpfen des Vergessens gibt es beim Sumpf-Infocenter einen Deku-Händler, der dir gewöhnlich 
Wundererbsen verkauft. Doch er sehnt sich nach dem Stadtleben. Gib ihm die Stadt-Urkunde und er 
überlässt dir die Sumpf-Urkunde. Mit der Dekublume kannst du das Dach des Sumpf-Infocenters 
erreichen, auf dem ein Herzteil wartet.

Breche nun auf in die Berge. Beim Goronen-Dorf befindet sich ein weiterer Deku-Händler, der dir 
gewöhnlich die Riesenbombentasche anbietet. Gib ihm deine Sumpf-Urkunde und er überreicht dir die 
Berg-Urkunde. Mit dieser Blume kannst du zur Nische im Berg fliegen. Auch hier befindet sich ein 
Herzteil. 

Gehe nun zur Zora-Höhle und vertreibe als Zora den Spanner vor Lulus Kammer. Sprich im Zimmer als 
Gorone mit dem Deku-Händler und zeig ihm deine Berg-Urkunde. Vor Freude übergibt er dir die Meeres-
Urkunde. Mit der Dekublume kannst auf einem Regal ein weiteres Herzteil an dich nehmen.

Jetzt fehlt nur noch ein Deku-Händler. Fliege zum Ikana Canyon und folge dem Flussverlauf nach rechts. 
In diesem kleinen Abschnitt entdeckst du neben Sakons geheimen Versteck auch einen einsamen Deku-
Händler, der dir gewöhnlich ein blaues Elixier verkauft. Sprich mit ihm und überlasse ihm deine Meeres-
Urkunde. Er schenkt dir einen goldenen Rubin (200 Rubine!). Nachdem der Deku seine sieben Sachen 
gepackt hat, fliege mit der Dekublume über den Fluss zum kleinen Felsvorsprung. Dort wartet neben 
einem Mythenstein ein Herzteil auf dich.

Schonmal am Hunderennen teilgenommen? Auf der Romani Ranch gibt es im Westen der Karte zwei 
weitere Zugänge. Der eine führt zu Grogs Hühnerfarm und der andere, wenn du dem Weg folgst, zu 
Mamamu Yans Hunderennen. Link kann dort bis zu 150 Rubine sammeln, setzt er auf den richtigen 
Hund. Mit der Maske der Wahrheit kannst du jedoch die Hunde hochheben und mit ihnen reden – so 
erfährst du, welcher Hund ganz bestimmt gewinnen wird. Gewinnst du 150 Rubine, winkt dir ein Herzteil 
entgegen.

Kehre in der zweiten Nacht zum Ikana Friedhof zurück. Dort tanzen drei Knochengänger um ein Grab. 
Setze den Helm des Hauptmanns auf und gib ihnen den Befehl, das Grab zu öffnen. Spring in das Erdloch 
und du findest dich in einer unterirdischen Höhle wieder. Springe über die Plattformen und nutze das 
Auge der Wahrheit, um die Tür zu finden. In der folgenden Kammer machen dir ein paar Riesenskulltulas 
das Leben schwer. Mit dem Auge der Wahrheit kannst du in der Ecke hinten links eine Wand entdecken. 
Lege eine Bombe davor. Nun steht dir noch ein letzter Kampf mit einem Eisenprinz bevor. Ist er besiegt, 
überlässt er dir eine Truhe mit einem Herzteil. 

Es gibt noch einen verborgenen Schrein in Ikana. Dazu musst du im Ikana Canyon nur dem Flussverlauf 
nach links folgen, indem du den Fluss entlang schwimmst. Du erreichst eine geheime Höhle, die von 
einem geheimnisvollen Fremden bewacht wird. In ihr gibt es vier Kammern, die du nur mit genügend 
Herzen auf deiner Lebensleiste betreten kannst. Allein für den letzten Raum benötigst du 16 Herzen. In 
jeder der Kammern wartet ein Miniboss aus den Tempeln auf dich, die jeweils eine Truhe mit einem 
silbernen Rubin (100 Rubine) bewachen. Meisterst du alle 4 Kammern, erhältst du beim Verlassen des 
Schreins ein weiteres Herzteil. 

Mehr dazu findest du auch in unserem Guide: Der verborgene Schrein von Ikana.

## Kaptilel 29: IM INNEREN DES MONDES

Jetzt, wo du die vier Giganten aus den Tempeln befreit hast, wird es Zeit den Kampf gegen das Horror 
Kid anzutreten. Kehre um Mitternacht nach Stadt-Unruh zurück und beobachte den Karneval. Du kennst 
deinen Weg: Über den Vorsprung am Uhrturm kannst du hinauf auf das Dach des Turms gelangen. 
Erinnerungen kommen hoch – erneut begegnest du dem Horror Kid. Und es ist sich sicher, dass es 
niemand mehr aufhalten kann...

Hole deine Ocarina heraus, denn nun wird es Zeit, das Versprechen der Giganten einzulösen. Spiele den 
Gesang des Himmels, um die Götter um Hilfe zu bitten. Tatsächlich erscheinen sie und bringen den Mond 
zum Stillstand....

Kaum sind die Feengeschwister wieder vereint, löst sich Majora's Maske von dem Horror Kid  und lässt 
dieses wie eine Marionette zu Boden fallen. Ihr Wille, alles zu Zerstören ist so stark, dass die Maske in 
das Innere des Mondes eindringt. Während die Götter versuchen, den bösartigen Mond vom Fall 
abzuhalten, folgt Link der Maske...

… und findet sich in einem Traum wieder: Inmitten einer idyllischen Landschaft befindet sich ein großer 
Baum, an dem fünf Kinder spielen. Du hast nun die Wahl: Hast du alle 23 Masken gesammelt, kannst du 
die 4 Kinder ansprechen, die die Masken der Tempelwächter tragen [Kaptilel 30-33], um die letzte Maske 
des Spiels zu holen. Möchtest du stattdessen direkt mit dem Kampf gegen Majora's Maske beginnen, 
sprich das Kind in der Mitte an, welches dessen Maske trägt [Kaptilel 34]. 

## Kaptilel 30: DAS DEKU-LABYRINTH

Um die letzte, 24. Maske im Spiel zu erhalten, musst du die anderen 23 Masken bereits besitzen. Sie sind 
dein Einsatz: Um mit den Kindern, die die Masken aus den Tempeln tragen, Verstecken spielen zu 
können, musst du sie mit Masken locken. Sprich das Odolwa Kind an, das von dir eine Maske haben 
möchte. Du gelangst sodann ins Deku-Labyrinth.

Dieses Labyrinth besteht aus einem einzigen großen Raum, in dem es gilt, mit den verfügbaren 
Dekublumen auf die andere Seite des Raumes zu fliegen. Dazu musst du die Dekublumen auf den 
hölzernen Drehscheiben nutzen und dabei rechtzeitig auf dem Drehgestell zu landen. Du solltest 
versuchen, rasch in der gelben Blume in der Mitte zu erreichen und in ihr abzutauchen, damit dich die 
Fallen der zweiten Stange nicht erwischen. 

Auf der anderen Seite dieses Raumes hast du zwei Optionen. Fliegst du erst einmal rechts, erreichst du 
in der Ecke einen Vorsprung, an dem nicht nur ein weiterer Mythenstein, sondern auch ein einsames 
Herzteil. Fliegst du anschließend nach links, also zur anderen oberen Ecke in diesem Raum, erreichst du 
die Tür. Dahinter befindet sich Odolwas Versteck. Sprich mit dem Kind noch einmal und schenke ihm 
eine weitere Maske. Dann lässt es dich gehen.

## Kaptilel 31: DAS GORONEN-LABYRINTH

Um die letzte, 24. Maske im Spiel zu erhalten, musst du die anderen 23 Masken bereits besitzen. Sie sind 
dein Einsatz: Um mit den Kindern, die die Masken aus den Tempeln tragen, Verstecken spielen zu 
können, musst du sie mit Masken locken. Sprich das Goth Kind an, das von dir zwei Masken haben 
möchte. Du gelangst sodann ins Goronen-Labyrinth.

Dieses Labyrinth ist wahrscheinlich das schwierigste von allen „Verstecken“ der Kinder. Als Gorone musst 
du einen Hindernis-Parcour bestehen, ohne dabei in den Abgrund zu fallen und während der Fahrt nicht 
auf eine der bunt-beleuchteten Platten zu gelangen, die dich wieder zurück zum Eingang schicken... 

Beginne damit, dass dich eins der beleuchteten Platten dich in die Startposition bringt. Von dort rollst du 
einfach geradeaus – und lässt beim ersten Sprung den Joystick los (!), der A-Knopf sollte dabei gedrückt 
bleiben. Der Trick dabei ist, genau in der Mitte der Bahn zu bleiben; vergleichbar bei einem Flipper prallst 
du an den hier aufgestellten Kisten ab und kommst so weiter.

Kniffelig wird es hinter den zwei grünen Tonkrügen, die deine Magieanzeige nochmals füllen: Dort musst 
du es schaffen, um die Kurven zu kommen. Ist der Slalom-lauf geschafft, brauchst du nur noch 
geradeaus die Bahn entlang zu rollen – am Ende erreichst du eine Plattform mit einem einsamen 
Herzteil. Um nun zum Versteck des Kindes zu kommen, kannst du einfach den Steg entlang laufen, bis 
du auf der linken Seite einen Holzsteg entdeckst (daneben sind zwei grüne Krüge). Schlage diesen Weg 
ein, eile zur vereisten Plattform am Ende und du erreichst die Tür. Dahinter befindet sich Goths Versteck. 
Sprich mit dem Kind noch einmal und schenke ihm zwei weitere Masken. Dann lässt es dich gehen.

## Kaptilel 32: DAS ZORA-LABYRINTH

Um die letzte, 24. Maske im Spiel zu erhalten, musst du die anderen 23 Masken bereits besitzen. Sie sind 
dein Einsatz: Um mit den Kindern, die die Masken aus den Tempeln tragen, Verstecken spielen zu 
können, musst du sie mit Masken locken. Sprich das Gyorg Kind an, das von dir drei Masken haben 
möchte. Du gelangst sodann ins Zora-Labyrinth.

Dieses Labyrinth besteht aus einer Reihe aus Tunneln, in denen eine starke Strömung herrscht. Du 
musst also die richtigen Abzweigungen nehmen. Um zunächst einmal hier das Herzteil zu bekommen, 
solltest du folgendermaßen schwimmen: 

rechts, links, links, links

rechts, links, links, rechts

Möchtest du anschließend das Versteck von Gyorg finden, folge diesem Weg:

Dort gehe einfach durch die Tür und sprich mit dem Kind. Schenke ihm drei weitere Masken, dann ist 
auch dieser Ort gemeistert. Das ging doch schnell, oder?

## Kaptilel 33: DAS LINK-LABYRINTH

Um die letzte, 24. Maske im Spiel zu erhalten, musst du die anderen 23 Masken bereits besitzen. Sie sind 
dein Einsatz: Um mit den Kindern, die die Masken aus den Tempeln tragen, Verstecken spielen zu 
können, musst du sie mit Masken locken. Sprich das Twinmold Kind an, das von dir vier Masken haben 
möchte. Du gelangst sodann ins Link-Labyrinth.

Im nachfolgenden Labyrinth wirst du dein Können nochmal unter Beweis stellen, denn in den 
kommenden Räumen werden einige Gegner auf dich warten. Erledige zunächst den Dinofol, den du leicht 
mit paar Schlägen oder einem Eispfeil das Fürchten lehrst. 

50

Zelda Europe Lösungen – Majora's Mask


Es folgt in der nächsten Kammer der Garo-Meister aus dem Felsenturm. Du kannst ihn verwunden, 
sobald dein Gegner beide Arme von sich streckt. Einige Hiebe mit deiner Klinge, und auch er wird in die 
ewigen Jagdgründe zurückkehren. Es erscheint eine kleine Truhe, an die du dich per Fanghaken 
hochziehst. Nimm die Pfeile heraus und eile in die nächste Kammer.

Die nächste Prüfung besteht darin, gegen einen Eisenprinz anzutreten – im Falle, dass du keine 
Krabbelminen bei dir hast, denn mit seinem Tod erscheint eine kleine Truhe mit 10 Krabbelminen. Hast 
du paar dieser mobilen Bomben dabei, kannst du auch einfach umschauen: Auf der rechten Wand zu 
deiner Seite befindet sich eine brüchige Stelle – mit dem richtigen Timing lässt du sie an der richtigen 
Stelle explodieren. Detoniert die Krabbelmine an der richtigen Stelle, erscheint ein Goldenes Auge. Ein 
Pfeilschuss genügt und  eine Leiter zum nächsten Raum erscheint. 

Hier kannst du dir zunächst das letzte Herzteil holen. Hoffentlich hast du noch nicht alle Krabbelminen 
verbraucht. Ein Blick an die Decke lässt dich einen weiteren Riss erkennen. Sende eine weitere 
Krabbelmine los, sodass sie genau über dem Riss hochgeht. Dahinter entdeckst du ein weiteres, diesmal 
vereistes Goldenes Auge. Mit einem Feuerpfeil wird sich die Tür entriegeln und du gelangst zum Versteck 
von Twinmold. Gebe ihm nun deine letzten vier Masken und er lässt dich gehen.

## Kaptilel 34: FINALE MAJORAS MASKE

Hier in der idyllischen Traumwelt kannst du das Kind ansprechen, welches Majoras Maske trägt. Es sitzt 
einsam unter einem Baum und möchte zum Verstecken herausgefordert werden.  Hast du alle 23 Masken 
gesammelt und die 4 Labyrinthe der anderen Kinder gemeistert, schenkt es dir die letzte und mächtigste 
Maske des Spiels: Die Maske der Grimmigen Gottheit. Unabhängig davon, ob du die Labyrinthe gelöst 
hast, fordert dich Majoras Maske zum Kampf heraus. 

Der Kampf gegen Majoras Maske besteht aus drei verschiedenen Phasen, in denen du gegen 
verschiedene Inkarnationen antreten wirst. Je nachdem, ob du in Besitz der Maske der Grimmigen 
Gottheit bist oder nicht, gibt es unterschiedliche Strategien.

Majoras Maske

Strategie [Ohne die Maske der Grimmigen Gottheit]: Zu Beginn des Kampfes fliegt die Maske durch den 
Raum und versucht dich mit ihren Stacheln zu erwischen. Warte bis sie dir die Rückseite zeigt, um sie 
mit einem Pfeilschuss zu Fall zu bringen. Ist sie am Boden, schlage mit dem Schwert auf sie ein. Du 
kannst auch mit einer aufgeladenen Wirbelattacke warten, bis sie zu dir kommt und sie dann in ihrer 
Nähe entladen. 

Nach wenigen Schlägen wird sie unruhig und ruft die 4 Wächter-Masken aus den Tempeln herbei, die mit 
Kugelblitzen dir das Leben schwer machen wollen. Schieße sie mit ein paar Pfeilen herunter. Majoras 
Maske versucht stattdessen dich mit einem Feuerstrahl einzuheizen. Nimm dein Spiegelschild und lenke 
den Strahl zurück. 

Strategie [Maske der Grimmigen Gottheit]: Mit der Maske der Grimmigen Gottheit fällt dir dieser Part 
gegen Majoras Maske wesentlich leichter. Du musst die Maske ledig drei mal mit deinem Schwert treffen 
– am besten, wenn sich die Maske in Augenhöhe oder niedriger befindet.

Majoras Inkarnation

Strategie [Ohne die Maske der Grimmigen Gottheit]: Jetzt wachsen der Maske Beine und Arme und rast 
mit einem atemberaubenden Tempo durch den ganzen Raum. Die Strategie hat sich nicht geändert – 
außer, dass du wahrscheinlich Schwierigkeiten hast, sie zu treffen. Du kannst entweder versuchen, sie 
mit dem Schwert zu erwischen oder mit Pfeilen bespicken. Landest du einen Treffer, fällt die Maske 
erneut zu Boden. Mit einigen Schwertstreichen kannst du auch diesen Part zügig beenden. 

51

Zelda Europe Lösungen – Majora's Mask


Strategie [Maske der Grimmigen Gottheit]: Majoras neue Form mag zwar schnell sein, doch du hast mit 
deinem Schwert einen Vorteil – die Reichweite. Mit ihr kannst du zügig der Maske ein paar Hiebe 
verpassen. Danach verwandelt sie sich in ihre letzte Form.

Majoras Magier

Strategie [Ohne die Maske der Grimmigen Gottheit]: Die Maske zeigt nun ihr wahres Gesicht.  Mit zwei 
mächtigen und langen Peitschen versucht sie dich in die Knie zu zwingen. Wenn du noch Pfeile über hast, 
kannst du noch versuchen, sie mit Pfeilen zu bespicken. Wahrscheinlich wirst du aber nun dich auf dein 
Schwert und Schild verlassen müssen: Mit dem Schild vor dir kannst du dich langsam gegen die 
Peitschenhiebe deines Feindes vorarbeiten und ihr von der Seite nähern. Bist du nah genug dran, schlage 
mit dem Schwert schnell zu und blocke die Angriffe mit dem Schild ab. Nach und nach wird deinem 
Gegner zunehmend die Luft ausgehen und schließlich fallen.

Strategie [Maske der Grimmigen Gottheit]: Mit schnellen Peitschenhieben versucht dich Majoras Magier 
zu erledigen, aber mit deinem Schwert kannst du diese Angriffe abwehren. Bist du nah genug dran, 
führst du ein oder zwei Schwerthiebe gegen die Maske aus. Drückst du den Z-Knopf, schleudert Links 
Schwert bei jedem Hieb blaue Blitze. Anschießend springt dein Feind außer Reichweite und das Spiel 
beginnt von Neuem. Wenn der Magier seine tödlichen Kreisel einsetzt, ist der Kampf fast vorbei – ein 
weiterer Schlag reicht, um dem Terror dieser Maske ein Ende zu setzen. 

Genieße nun das Ende des Spiels: Der Mond verschwindet und Termina ist gerettet. Für jede Maske, die 
du im Spielverlauf gesammelt hast, gibt es einen Abspann-Schnipsel. Hast du eine Maske nicht, taucht 
stattdessen nur die betreffende Maske auf. Was wohl mit dem Horror-Kid passiert... ?

Herzlichen Glückwunsch!
Du hast Zelda: Majora's Mask gemeistert.

52



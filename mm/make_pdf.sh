#!/bin/bash
docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/latex *.md -o majoras_mask.pdf

## Auf zum Gipfel

Wieder draussen trefft ihr auf Darunia, der euch den GORONEN-RUBIN schenkt.

Wenn ihr wieder die Kontrolle über Link habt könnt ihr ein wenig dem Pfad
nach Kakariko folgen und dort eine Wand zerstören, in der ihr die GOLDENE
SKULLTULA #18 findet. Geht nun zum Eingang von Dodongo's Höhle und lasst
einen Käfer in die weiche Erde krabblen um die GOLDENE SKULLTULA #19
erscheinen zu lassen. Begebt euch nun nach Goronia.

Hier gibt es nun einen Goronen, der die ganze Zeit herumrollt. Wenn ihr ihn
mit einer Bombe unter dem überdachten Ort stoppt erhält ihr ein
BOMBENTASCHEN UPGRADE 1! Geht nun ganz nach oben und dort links zu den
Steinen. Sprengt sie weg und ihr landet in einem Raum mit vielen Blöcken.
Bombt euch euren Weg bis nach ganz hinten um 100 Rubine und die GOLDENE
SKULLTULA #20 zu erhalten. Verlasst nun Goronia.

Wir müssen nun auf den Gipfel des Todesberges klettern. Dafür müsst ihr die
Felsen die im Weg sind wegsprengen. Hier oben müsst ihr nun auf die fallenden
Steine aufpassen, die manchmal runterkommen. Am Ende des Weges angelangt,
solltet ihr die Wand erst mal von den Skulltulas befeien. Dann könnt ihr
hinaufklettern. Betretet den Krater durch das Loch.

Hier drin ist es sehr heiss und ihr habt nur eine limitierte Zeit zur
Verfügung bevor ihr sterbt! Rollt in die Kiste direkt am Anfang um die
GOLDENE SKULLTULA #21 zu finden. Nun wäre es schlau den Krater neu zu
betreten, damit die Zeit zurückgesetzt wird. Nun rennt einfach gerade aus bis
zum Abgrund. Hier wird sich Link automtisch daran festhalten und ihr könnt
nach unten klettern. Ihr könnt ihr irgendwo das HERZTEIL #6 finden. Wenn ihr
es habt springt einfach in die Lava um wieder am Eingang zu sein und den
Krater zu verlasse.

Sprengt nun den anderen Eingang in der Nähe der Eule frei. Stellt euch im
Inneren auf das Triforce-Symbol und spielt Zelda's Wiegenlied. Eine grosse Fee
wird erscheinen und euch die Drehattacke beibringen. Mit der Drehattacke
bekommmt ihr auch die Magische Energie.

Geht nun nach draussen und sprecht mit der Eule. Sie wird euch nach Kakariko
bringen. Ihr werdet auf einem Haus lande, bevor ihr jedoch herunter
springt könnt ihr rechts auf eine kleine Fläche herunterspringen und dann
ins Haus hineingehen. Im Inneren findet ihr das HERZTEIL #7.

Nun könnt ihr euch im Skulltula Haus wieder einen Preis abholen. Den KRISTALL
DES WISSENS.

- Goronen-Rubin
- Stein des Wissens
- Bombentaschen Upgrade 1
- Goldene Skulltula #18
- Goldene Skulltula #19
- Goldene Skulltula #20
- Goldene Skulltula #21
- Herzteil #6
- Herzteil #7


##  Der Deku-Baum

Direkt vom Eingang könnt ihr links eine Leiter hinaufklettern. Tut dies und
folgt dem Weg um eine Truhe mit der Karte zu finden. Folgt dem Weg in dem ihr
über die Löcher im Boden hüpft und öffnet die Türe.

Hier drinnen müsst ihr den Laubkerl besiegen. Dafür müsst ihr einfach mit
eurem Schild eine seiner Nüsse zurückschleudern. Im nächsten Raum könnt ihr
über die schwebende Plattform zur grossen Truhe springen. Passt aber auf, da
die Plattform zerbricht. In der Truhe findet ihr die FEEN-SCHLEUDER.

Um wieder zurückzukommen müsst ihr die Schleuder direkt anwenden. Benutzt sie
um die Leiter mit einem gezieltem Schuss runterzuholen. Nun könnt ihr den Raum
wieder verlassen. Begebt euch zurück zur Kiste mit der Karte und besiegt die
Skulltulas (Spinnen) auf dem Netz mit der Schleuder. Nun könnt ihr nach oben
klettern. Hier oben könnt ihr eine Türe finden, durch die ihr gehen solltet.

Um den Raum wieder zu verlassen müsst ihr die Fackel hier oben mithilfe eines
Deku-Stabes entzünden. Wenn ihr noch keinen habt, könnt ihr einfach die
Dekuranhas besiegen, um welche zu bekommen. Springt auf den Schalter damit
die drei Plattformen ansteigen. Springt über sie so lange sie noch oben sind
zur anderen Seite. Hier in der Truhe findet ihr den Kompass.

Nun geht wieder zurück zum Schalter und betätigt ihn nochmal. Diesmal springt
ihr zur anderen Seite um dort eine Truhe mit einem Herz und die GOLDENE
SKULLTULA #1 zu finden. Verlasst den Raum und besiegt eine der Skulltulas,
wenn ihr draussen seit. Nun kommt der spassige Teil! Springt hinunter und
positioniert euch so, dass ihr das Spinnennetz trefft, damit es reisst und
ihr im Untergeschoss landet. Keine Sorge euer Fall wird vom Wasser gedämpft.

Geht ersteinmal an Land und dann könnt ihr am Netz, welches nach oben führt,
die GOLDENE SKULLTULA #2 entdecken. Besiegt sich mit der Schleuder und sammelt
dann das Symbol ein. Am einem Gitter könnt ihr die GOLDENE SKULLTULA #3
entdecken. Besiegt auch diese mit der Schleuder und dann springt von der
Plattform um das Symbol einzusammeln.

Betätigt den Schalter um die Fackel zu entzünden. Benutzt einen Deku-Stab um
das Netz vor der Türe zu verbrennen. Dabei müsst ihr achten nicht in das tiefe
Wasser zu treten! Im nächsten Raum müsst ihr wieder einen Laubkerl besiegen.
Sprecht mit ihm, wenn ihr ihn besiegt hat. Er wird euch eine Zahlenfolge
nennen die noch wichtig wird. Benutzt die Schleuder um das Auge über der Türe
zu treffen, damit die Türe geöffnet wird.

Springt hier ins Wasser und taucht mithilfe des A-Knopfes beim Schalter
um ihn zu betätigen. Nun schwimmt wieder an Land und springt auf die Plattform
da wir mithilfe des Schalters das Wasser gesekt haben, können die Stacheln
uns nichts anhaben. Am anderen Ende solltet ihr die Skulltula besiegen und
dann den Block verschieben, damit ihr auf ihn zur Türe klettern könnt. Im
nächsten Raum müsst ihr einfach alle Fackeln enzünden um die Türe
zu öffnen.

In diesem Raum müsst ihr auf die Gohma-Larven aufpassen, die von oben kommen.
Benutzt einen Deku-Stab um die Spinnweben zu verbrennen und kriecht durch das
Loch um wieder im ersten Raum dieser Etage zu erscheinen. Diesmal seit
ihr jedoch auf der erhöhten Plattform. Schiebt den Block ins Wasser und
entzündet hier einen Deku-Stab um dann die Spinnweben verbrennen zu können.
Lasst euch durch das Loch fallen.

Hier unten trefft ihr auf drei Laubkerle die ihr in der Reihenfolge besiegen
müsst, die der andere vorhin gesagt hatte. Für alle die, die Reihenfolge
vergessen haben, es ist 2-3-1. Wenn ihr alle in der Reihenfolge besiegt hat,
sollte sich die Türe zum Boss-Gegner öffnen.

Schaut nach oben um Gohma zu entdecken und den Kampf zu starten. Der Kampf ist
simpel: Greift das Auge von Gohma mit der Schleuder an, wenn es rot wird dann
könnt ihr es mit dem Schwert angreifen. Das müsst ihr so lange wiederholen
bis Gohma besiegt ist.

Nachdem Kampf könnt ihr den wohlverdienten HERZCONTAINER an euch nehmen und
durch das Blaue Licht treten.

- Feen-Schleuder
- Goldene Skulltula #1
- Goldene Skulltula #2
- Goldene Skulltula #3
- Herzcontainer


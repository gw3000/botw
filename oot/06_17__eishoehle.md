## Eishöhle

Die Eishöhle ist der erste der zwei Minidungeons im Spiel. Geht durch den
ersten Gang und im nächsten Raum müsst ihr alle Eisstatuen besiegen. Mit Dins
Feuerinferno funktioniert das am besten. Wenn ihr sie besiegt habt könnt ihr
weiter gehen.

Durch de nächsten Raum könnt ihr auch einfach gehen. Ihr befindet euch nun in
einem Raum mit einer riesigen Klinge. Euer Ziel ist es alle fünf Silber Rubine
einzusammeln, damit sich die Türe öffnet. Der Klinge könnt ihr ausweichen,
indem ihr duckt oder untendurch rollt. Die GOLDENE SKULLTULA #68 könnt ihr
direkt über einem Rubin entdecken. Geht in den nächsten Raum, wenn ihr könnt.

Hier könnt ihr das Blaue Feuer in einer Flasche aufbewahren. Ich rate euch
einige Flaschen zu füllen, damit ihr nicht immer hier zurück gehen müsst. Mit
dem blauen Feuer könnt ihr das rote Eis, welches ihr bestimmt schon gesehen
schmelzen. Geht ein wenig weiter und ihr könnt die Truhe mit der Karte
auftauen. Geht nun zurück in den Klingen Raum und schmelzt das Eis links von
euch und geht dort in den nächsten Raum.

In diesem Raum könnt ihr den Kompass finden. Mithilfe des Feuers könnt ihr
zusätzlich das HERZTEIL #24 erhalten. Zum Schluss könnt ihr noch die GOLDENE
SKULLTULA #69 an einem der Pfeiler finden. Geht zurück in den Klingen Raum und
geht nun durch den anderen Durchgang, der mit Eis versperrt ist.

In diesem Raum könnt ihr die GOLDENE SKULLTULA #70 an einer Wand liegend
finden. Nun um weiter zu kommen, müsst ihr alle Silber Rubine einsammeln und
um das zu schaffen müsst ihr die Blöcke verschieben. Wenn ihr den Block
falsch verschoben habt, könnt ihr ihn einfach in den Abgrund fallen lassen,
damit er wieder am Startpunkt erscheint. Schiebt den Block, wenn ihr alle
Rubine habt vor dem geöffneten Durchgang und geht weiter durch den nächsten
Gang um durch die Türe zu gehen.

Hier müsst ihr den Wolfsheimer besiegen und ihr erhält die EISENSTIEFEL.
Zusätzlich wird Shiek erscheinen und euch die SERENADE DES WASSERS beibringen.
Nun könnt ihr den Dungeon verlassen, aber achtet darauf, dass ihr mindestens
eine Flasche Blaues Feuer dabei habt und taut den Zora König auf um die
ZORA RÜSTUNG zu erhalten. Mit den Eisenstiefel und der Rüstung könnt ihr
in der Zora-Quelle am Grund des Sees das HERZTEIL #25 finden.

- Zora Rüstung
- Eisenstiefel
- Serenade des Wassers
- Goldene Skulltula #68
- Goldene Skulltula #69
- Goldene Skulltula #70
- Herzteil #24
- Herzteil #25


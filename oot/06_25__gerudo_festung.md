## Gerudo-Festung

Nun betreten wir den letzten Teil von Hyrule, den wir noch nie betreten haben.
Geht ins Gerudo-Tal und springt über die Brücke. Spielt die Hymmne der Sonne
um es Nacht zu machen. Hinter dem Zelt könnt ihr die GOLDENE SKULLTULA #87
finden und am Pfeiler die GOLDENE SKULLTULA #88. Geht weiter in die
Gerudo-Festung.

Lasst euch von einer Wache sehen um ins Verlies zu kommen und zieht euch dann
hier heraus und springt hinunter. Betäubt die Wache (mit dem Bogen oder
Enterhaken) und geht in die Türe direkt neben euch. Hier findet ihr den ersten
der vier Handwerker. Wenn ihr mit ihm spricht werdet ihr von einer Gerudo
Kriegerin angegriffen. Ihr müsst eigentlich nur auf ihre Sprungattacke
aufpassen und sie einfach besiegen. Am Schluss könnt ihr den Handwerker
befreien.

Geht raus und direkt wieder hinein. Betäubt hier die Wacheund geht rechts.
Draussen solltet ihr in die Türe, die ein wenig höher ist gehen. Ihr findet
hier den zweiten Handwerker.

Wenn ihr ihn befreit habt, solltet ihr die Ranken herunterklettern um im Raum
dahinter schon den dritten zu finden.

Geht hier in die linke Türe und zieht euch dann über den Abgrund. Springt
draussen herunter in die Türe, die man sonst nicht erreicht.
Betäubt hier nochmal die Kriegerinnen um dann den letzten Handwerker zu
befreien.

Ihr erhält den GERUDO-PASS als Belohnung von den Gerudos. Nun könnt ihr euch
frei in der Festung bewegen. Klettert über die Dächer um am Ende zu einer
Kiste mit dem HERZTEIL #34 zu kommen. Hier in der Nähe könnt ihr Nachts die
GOLDENE SKULLTULA #89 bekommen. Geht nun mit Epona oben zur Pferde-Arena.

Hier müsst ihr mithilfe des Bogens genügend Punkte machen. Für 1000 Punkte
erhält ihr HERZTEIL #35 und für 1500 Punkte das KÖCHER UPGRADE 2. In der
Nacht könnt ihr an einer der hohen Zielscheiben die GOLDENE SKULLTULA #90
finden.

- Gerudo-Pass
- Köcher Upgrade 2
- Goldene Skulltula #87
- Goldene Skulltula #88
- Goldene Skulltula #89
- Goldene Skulltula #90
- Herzteil #34
- Herzteil #35
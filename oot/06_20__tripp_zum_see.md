
## Trip zum See

Während wir auf dem Weg zum Tempel sind, können wir uns noch einige Dinge
holen. Im nordwesten von der Ebene von Hyrule, könnt ihr bei einem Baum, ein 
Loch
freisprengen. Im Inneren könnt ihr das HERZTEIL #26 im Wasser finden. Geht
nun zum Hylia-See.

Mithilfe der Pflanze könnt ihr auf das Labor gehen und dort das HERZTEIL #27
holen. Im Labor selber könnt ihr mithilfe der Eisenstiefel zum Grundtauchen
und dort unter der Kiste die GOLDENE SKULLTULA #71 finden. Unter Wasser könnt
ihr nur den Fanghaken als Waffe gebrauchen. Merkt euch das!

Geht nun in den kleinen Rest Wasser, den es hier noch hat und schiesst mit dem
Fanghaken auf den Krisstal, damit sich der Eingang zum Wassertempel öffnet.
Wenn ihr bereit, für Wasseraction seit, tretet ein.

- Goldene Skulltula #71
- Herzteil #26
- Herzteil #27
## Waldtempel

Im Eingangsraum werdet ihr von zei Wolfsheimern begrüsst, besiegt sie und
klettert dann hier die Ranken hoch um eine kleine Truhe mit einem kleinen
Schlüssel zu finden. Die GOLDENE SKULLTULA #49, könnt ihr mithilfe des
Fanghakens besiegen. Geht nun in den nächsten Raum besiegt, besiegt die
Skulltula mithilfe des Fanghakens und betretet den Hauptraum.

Hier seht ihr, wie vier Geister die Flammen stehlen. Geht geradeaus nehmt noch
die GOLDENE SKULLTULA #50 mit euch und geht durch die Türe. Den
Knochenschädel müsst ihr erst mit dem Schild auslöschen, dann könnt ihr ihn
mit dem Schwert angreifen. Im nächsten Raum, müsst ihr gegen zwei Stalfos
kämpfen. Wenn ihr sie besiegt habt, erscheint eine Truhe mit einem kleinen
Schlüssel. In den Krügen befinden sich Feen, wenn ihr sie benötigt.

Geht im Hauptraum durch die nordwestliche Türe, die mit dem Zeitblock. Rechts
solltet ihr die Skulltulas an den Ranken besiegen und dann hinaufklettern und
dann durch die Türe gehen. Besiegt hier den Knochenschädel und nehmt die Karte
aus der Truhe. Geht in den nächsten Raum.

Zieht euch an die Zielscheibe in der Nähe, mithilfe des Fanghakens und stellt
euch auf den Schalter. Das Wasser im Brunnen wird sinken. Springt hinunter.
Ihr könnt euch hier auf die Plattform im Wasser ziehen, dafür müsst ihr euch
an die Truhe ziehen. Hier oben findet ihr die GOLDENE SKULLTULA #51. Springt
nun in den Brunnen. Hier findet ihr einen kleinen Schlüssel in einer Truhe.
Wenn ihr in habt, solltet ihr wieder in den Hauptraum gehen.

Geht durch die westliche Türe, besiegt die Skulltula und geht weiter. In
diesem Raum müsst ihr ein Blockpuzzle lösen, welches aber dank Markierungen
auf dem boden, recht einfach ist. Am Schluss solltet ihr ganz oben durch die
verschlossene Türe gehen. Geht durch den komischen Gang und im nächsten Raum
über die Plattformen nach rechts. In diesem Treppenhaus könnt ihr auch
einfach durchlaufen und durch die nächste Türe gehen.

Hier müsst ihr insgesamt 3 Stalfos bekämpfen. Wenn ihr das geschafft habt,
erscheint eine Truhe mit dem FEEN-BOGEN. Geht zurück in den Blockraum und
betätigt hier den Augenschalter mit dem Bogen. Der verdrehte Raum ist nun
gerade und ihr könnt den Masterschlüssel aus der Truhe nehmen. Springt hier
durch das Loch. Hier müsst ihr die zwei Knochenschädel besiegen, damit sich
die Türe öffnet.

Hier draussen könnt ihr die GOLDENE SKULLTULA #52 erhalten geht nun in die
Türe, durch die ihr nicht gekommen seit. Hier müsst ihr einen Bodengrapscher
besiegen, damit ihr einen kleinen Schlüssel erhält. Verlasst den Raum und
geht durch die Türe rechts um wieder im Blockraum zu erscheinen. Geht hier
wieder zum Augenschalter und öffnet ihn mit einem Pfeil wieder.

Macht euch wieder auf den Weg zum Raum, indem ihr den Bogen erhalten habt,
aber im Treppenhaus, solltet ihr auf die Geisterbilder mit dem Bogen
schiessen. Wenn alle Bilder weg sind könnt ihr gegen den Geist kämpfen. Der
erste vo vier besiegt! Geht nun durch den Bogenraum in ein anderes
Treppenhaus, indem ihr, das gleiche machen solltet. Von den zwei Geister
solltet ihr auch noch einen kleinen Schlüssel und den Kompass erhalten.

Springt im nächsten Raum zum Durchgang und geht durch den Korridor. Öffnet die
Türe mit einem kleinen Schlüssel. In diesem Raum müsst ihr den Augenschalter,
entweder mit einem Pfeil, welcher durch eine Fackel geschossen wird, oder
durch Dins Feuerinferno treffen. Springt ins Loch im jetzt nicht mehr
gedrehten Raum.

Ihr müsst hier auf die Decke aufpassen, welche euch zerquetschen will. Im
nächsten Raum, solltet ihr mit einem Pfeil das Porträt treffen und dann müsst
ihr das Blockpuzzle in einer bestimmten Zeit lösen. Habt ihr das geschafft
könnt ihr gegen de Geist kämpfen. Geht durch die Türe hier wieder in den
Hauptraum, wo ihr den letzten Geist antrefft. Wenn sie besiegt ist könnt ihr
den Aufzug nach unten nehmen.

In diesem Raum müsst ihr den Raum drehen um drei verschiedene Schalter zu
aktivieren. Die GOLDENE SKULLTULA #53 könnt ihr auch noch finden. Wenn ihr
alle Schalter betätigt habt, öffnet sich der Zugang zur Bosstüre. Wenn ihr 
bereit seit könnt ihr hindurchtreten.

Der Boss ist Phantomganon! 
Er wird in die Bilder verschwinden und wenn er wieder am herauskommen ist,
solltet ihr in mit einem Pfeil treffen. Nach drei Treffer wird er sich nicht
mehr verstecken. 

Nun müsst ihr seine Energiebälle zurückschleudern, damit er betäubt wird.
Dann könnt ihr ihn mit dem Schwert angreifen, nach einigen Treffern wird
er besiegt sein und einen HERZCONTAINER hinterlassen. Sammelt ihn ein und
geht durch das blaue Licht um in einer Sequenz das AMULETT DES WALDES zu
erhalten.

- Amulett des Waldes
- Feen-Bogen
- Goldene Skulltula #49
- Goldene Skulltula #50
- Goldene Skulltula #51
- Goldene Skulltula #52
- Goldene Skulltula #53
- Herzcontainer


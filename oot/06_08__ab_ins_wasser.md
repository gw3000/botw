## Ab ins Wasser

Begebt euch zum Marktplatz. Wenn ihr nicht so viele Rubine habt würde ich nun
einige hier im Wachhaus sammeln, denn wir werden nun einige Minispiele
spielen.

Geht zum Haus mit der Zielscheibe. Hier drinnen müsst ihr alle Rubine mit
der Schleuder abschiessen. Wenn ihr es geschafft hast, erhält ihr das 
DEKU-KERNE UPGRADE 2, so dass ihr nun 50 Kerne tragen könnt. Als nächstes geht
in die Bowling Bahn.

Hier drinnen müsst ihr alle Löcher mit den Krabbelminen treffen. Die Preise
varrieren immer. Man kann das HERZTEIL #8 und das BOMBENTASCHEN UPGRADE 2.
Ändert nun die Zeit zu Nacht. Geht nun dorthin, wo der Weisse Typ aus dem
Fenster schaut und berührt den Weissen Hund. Er wird euch nun folgen, passt
auf, dass ihr keinen anderen mehr berührt. Geht nun in die Seitengasse und an
einem Ort findet ihr eine Türe die offen ist. Geht hindurch und sprecht
mit der Frau. Sie wird euch mit dem HERZTEIL #9 dafür danken, das ihr, ihren
Hund zurückgebacht habt. Geht nun zum Schloss.

Hier könnt ihr im Südöstlichen Gebiet eines Fels in die Luft spregen und dann
durch ein Loch kriechen. Im Inneren solltet ihr wieder Zelda's Wiegenlied auf
dem Triforce-Symbol spielen und es wird eine Grosse Fee erscheinen, die euch
Dins Feuerinferno schenkt.

Verlasst nun Hyrule-Stadt und folgt dem Fluss bis ihr in einem neuen Gebiet
angekommen seit. Hier trefft ihr wieder auf die Eule und sie wird euch wieder
nerven. Wenn sie fertig gesprochen hat, könnt ihr in den Baum rollen um die
GOLDENE SKULLTULA #22 erscheinen zu lassen. Zerstört nun die Felsen hier und
geht weiter. Hier seht ihr einen Typen der euch Magische Erbsen verkauft.
Kauft am besten so viele wie ihrr gerade könnt. Und pflanzt direkt eine hier
in die weiche Erde. Wir werden in Zukunft sehen, für was sie gut sind.

Nehmt nun das Huhn und fliegt auf die andere Seite. Das Huhn solltet ihr
immer noch behalten, da wir mit ihm einige Sachen erledigen können. Folgt dem
Weg bis ihr das HERZTEIL #10 seht, welches ihr mithilfe des Huhnes einsammeln
könnt. Nehmt wieder das Huhn und läuft ziemlich weit nach hinten, bis ihr zu
einem grossen Wasserfall ankommt. Hier könnt ihr noch das HERZTEIL #11
mithile des Huhnes einsammeln. Macht es nun acht und ihr könnt die GOLDENE
SKULLTULA #23 an einer Leiter besiegen. Stellt euch nun vor dem Wasserfall und
spielt Zelda's Wiegenlied damit sich ein Durchgang öffnet, durch den ihr gehen
könnt.

Geht hier ganz nach oben, wo ihr König Zora trefft. Nehmt eines Deku-Stab und
entzündet mitdessen Hilfe alle Fackeln in diesem Gebiet um eine Kiste mit dem
HERZTEIL #12 erscheinen zu lassen. Hinter dem Wasserfall hat es auch noch zwei
Fackeln.

Geht nun von König Zora auf nach links und sprecht dort mit dem Zora. Er wird
einige Rubine in das Wassser werfen. Wenn ihr alle in der Zeit eingesammelt
habt könnt ihr euch beim Zora die Silberne Schuppe abholen. Schwimmt nun
durch das Loch hier um beim Hylia-See herauszukommen.

Hier könnt ihr im Wasser eine Flasche entdecken, die ihr einsammeln könnt,
indem ihr tauscht. Die LEERE FLSCHE #3 beinhaltet einen Brief für den Zora
König. Bevor wir ihm den Brief bringen, könne wir hier noch eingie Dinge tun.

Geht zu den Vogelscheuchen und sprecht mit der unteren. Ihr könnt ihr nun
8 Noten auf der Ocarina vorspielen. Ihr solltet eine Kombination nehmen, die
ihr euch gut merken könnt. In der Nähe des Labors könnt ihr einen Käfer in
die weiche Erde stecken um die GOLDENE SKULLTULA #24 zu bekommen. Pflanzt auch
gerade eine Magische Wundererbse.

Spielt die Hymmne der Sonne um es Nacht zu machen und geht dann zu der kleinen
Plattform im See. Hier findet ihr die GOLDENE SKULLTULA #25. Geht nun zum
Fischteich.  Hier könnt ihr für 20 Rubine so lange fischen wie ihr wollt.
Wenn es euch gelingt den grössten Fisch zu fangen, erhält ihr das
HERZTEIL #13. Geht nun zurück zu König Zora und zeigt ihm den Brief. Er wird
euch den Weg freimachen. Bevor ihr jedoch weitergeht, müsst ihr noch schnell
einen Fisch in einer Flasche fangen. Das könnt ihr hier in der Zora-Höhle in
der Nähe des Wasserfalls tun.
Folgt nun dem Weg hinter König Zora und ihr trefft auf Lord Jabu-Jabu, lasst
den Fisch vor seinem Mund aus der Flasche und ihr werdet in den nächste
Dungeon geschluckt.

- Dins Feuerinferno
- Magische Erbsen
- Silberne Schuppe
- Deku-Kerne Upgrade 2
- Bombentaschen Upgrade 2
- Leere Flasche #3
- Goldene Skulltula #22
- Goldene Skulltula #23
- Goldene Skulltula #24
- Goldene Skulltula #25
- Herzteil #8
- Herzteil #9
- Herzteil #10
- Herzteil #11
- Herzteil #12
- Herzteil #13


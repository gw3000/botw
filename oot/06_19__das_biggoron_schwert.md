## Das Biggoron-Schwert

Wir werden uns nun um eine grosse Quest kümmern bei der wir das dritte Schwert
erhalten. Geht zur Hühnerfrau in Kakariko (Als Erwachsener) und sprecht sie
an um ein Ei zu bekommen. Mit dem Ei fängt die Quest an.

Lässt das Ei schlüpfen in dem ihr zweimal die Hymmne der Sonne spielt. Geht
nun in das Haus vor dem Anstieg und im Inneren findet ihr Talon am schlafen,
zeigt ihm das Huhn um ihn aufzuwecken. Geht zurück zur Hühnerfrau um ein
blaues Huhn zu bekommen.

Geht mit dem Huhn in die Verlorenen Wälder dorthin, wo früher das Horrokid
war. Um zeigt das Huhn dem komischem Typen. Er wird eich einen seltsamen Pilz
geben den ihr in Kakariko bei der Hexe abgeben solltet. Ihr könnt euch nicht
warpen sonst geht die Zeit sofort auf 0:00. Deshalb ist Epona pflicht bei
dieser Quest. Von der Hexe erhält ihr ein spezielles Elixir, das ihr dem
Typen im Wald bringen sollt. Dort findet ihr jedoch nur noch seine Säge.

Geht zum Gerudotal und springt über die Kaputte Brücke. Sprecht vor dem Zelt
mit dem Anführer der Handwerker um das Zerbrochene Biggoron-Schwert zu
erhalten. Bringt das Biggoron, welcher auf dem Gipfel des Todesberges lebt.
Ihr erhält das Rezept, welches ihr König Zora bringen sollt. Von ihm
erhält ihr jedoch nur einen Frosch, den ihr in drei Minuten zum Labor am See
bringen sollt.

Von ihm erhält ihr endlich die Augentropfen für Biggoron, leider verfallen sie
in vier Minuten, deshalb müsst ihr schnell bei Biggoron sein. Von ihm
erhält ihr das Zertifikat und müsst nur noch einige Tage (Im Spiel!) warten,
bis ihr das BIGGORON-SCHWERT abholen könnt. Das Schwert macht doppelt soviel
Schaden wie das Master-Schwert. Leider könnt ihr nicht das Schild zum blocken
benutzen.

- Biggoron-Schwert

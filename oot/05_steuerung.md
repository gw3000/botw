# Steuerung

- Steuerkreuz   | Nichts
- Slidepad      | Bewegen, Zielen
- A-Knopf       | Aktion ausführen
- B-Knopf       | Schwert benutzen
- X-Knopf       | Item benutzen
- Y-Knopf       | Item benutzen
- L-Taste       | Objekte anvisieren
- R-Taste       | Schild einsetzen
- Start         | Pause-Menü öffnen
- Select        | Pause-Menü öffnen
- Touchscreen   | Items benutzen
- Gyrosensor    | Zielen


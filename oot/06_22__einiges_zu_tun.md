## Einiges zu tun

Das Wasser im See ist zurückgekehrt und damit können wir auch neue Dinge
erledigen. Steht auf der brauen Plattform hier am See und schiesst einen
Pfeil in die Sonne, wenn sie am aufgehen ist. Ihr erhält die FEUER-PFEILE.
In der Nacht könnt ihr die GOLDENE SKULLTULA #77 auf dem Baum bekommen.

Als Erwachsener könnt ihr auch wieder Fischen gehen. Fängt ihr nun den
grössten Fisch erhält ihr die GOLD-SCHUPPE mit der ihr länger tauchen könnt.
Geht nun ins Labor und taucht dort im Becken ganz nach unten um das
HERZTEIL #28 zu erhalten.

Geht nach Kakariko um nach einer Sequenz die NOCTURNE DER SCHATTEN von Shiek
zu erlernen. Geht auf dem Friedhof in Boris Grab und macht wieder ein
Wettrennen gegen ihn. Diesmal solltet ihr ihn jedoch schalgen um das
HERZTEIL #29 zu erhalten. Am Schluss könnt ihr euch mit dem Enterhaken zur
Fackel ziehen.

Zieht euch in Kakariko zum dem Typen auf einem dach und sprecht mit ihm um das
HERZTEIL #30 zu bekommen. Geht wieder in die Zitadelle und geht in die
Vergangenheit. Geht wieder den Weg, den ihn zu Zelda machen müsst. Dort, wo
ihr Talon getroffen habt, ist ein Baum in der Nähe. Wenn ihr hier die
Hymmne des Sturms spielt öffnet sich ein Loch in dem ihr die GOLDENE
SKULLTULA #78 zu finden. Geht zum Zora Fluss und stellt euch dort bei den
Fröschen auf den Baumstamm. Spielt ihnen jedes der normalen Lieder vor, um
das HERZTEIL #31 zu bekommen. Wenn ihr sie nun wieder ansprecht wollen sie,
dass ihr ihre Noten nachspielt. Wenn ihr das aiuch geschafft habt, erhält ihr
das HERZTEIL #32. Geht nun nach Kakariko, geht in die Windmühle und spielt dem
Typen hier die Hymmne des Sturms vor um den Brunnen zu leeren. Geht dort
hinein für einen Minidungeon.

- Nocturne der Schatten
- Feuer-Pfeile
- Goldene Skulltula #78
- Goldene Skulltula #79
- Herzteil #28
- Herzteil #29
- Herzteil #30
- Herzteil #31
- Herzteil #32
# Walkthrough

Das hier ist der Hauptteil dieses Dokumentes. Ich werde euch genau aufzeigen
wie ihr das Spiel zu 100% durchspielen könnt. Am Ende jedes Kapitels könnt
ihr eine Zusammenfassung der eingesammelten Items finden.

## Der Anfang

Nach der Intro Sequenz werdet ihr von Navi der Fee geweckt. Verlasst das Haus
und ihr trefft auf ein Mädchen namens Salia, welches euch einen schönen
Morgen wünscht. Begebt euch nun auf den Hügel und springt oder läuft zwischen
den Zäunen hindurch. Hier hinten findet ihr ein Loch durch das ihr mithilfe
des A-Knopfs kriechen könnt. Ihr werdet in einem Art Labyrinth herauskommen.
Läuft hinter dem Felsen nach und ihr solltet zu einer Kiste kommen in der ihr
das KOKIRI-SCHWERT findet. Vergesst nicht das Schwert durch das Pause-Menü
auszurüsten.

Nun solltet ihr wieder durch das Loch zurückgehen. Unser Ziel ist es nun 40
Rubine für ein Schild zu sammeln. Läuft durch das Gras und schaut auch in die
Gebäude rein um Rubine zu finden. Wenn ihr 40 Rubine habt geht in den
Kokiri-Shop und kauft euch das KOKIRI-SCHILD. Rüstet es aus und sprecht mit
Mido vor dem Durchgang zum Deku-Baum. Er wird euch durchlassen. Am Ende des
Durchganges findet ihr den Deku-Baum. Er wird euch eine Geschichte erzählen.
Wenn er fertig ist könnt ihr in durch seinen Mund sein Inneres betreten.

+ Kokiri-Schwert
+ Kokiri-Schild


## Lord Jabu-Jabu

Dieser Dungeon spielt sich im Inneren dieses Fischesd/Wales ab. Für diesen
Dungeon ist es besser den Deku-Schild ausgrüstet zu haben, da ihr auf viele
Oktoroks trefft. In diesem Raum müsst ihr nur den komischen Schalter der an
der Decke hängt mit der Schleuder betätigen, damit sich die Türe öffnet.

Passt in diesem Raum auf die Quallen auf und geht einfach weiter in den
nächsten Raum. Hier haben wir schon Prinzessin Ruto gefunden! Aber sie fällt
leider durch ein Loch. Also nicht wie hinterher. Sprecht Ruto mehrmals an,
dann könnt ihr sie aufheben. Geht mir ihr durch die Türe hier.

Springt hier herunter und werfet Ruto auf die Erhöhung und stellt euch auf den
Schalter damit das Wasser steigt. In diesem Raum könnt ihr auch noch die
GOLDENE SKULLTULA #26 finden. Geht weiter und passt im nächsten Raum auf den
Oktorok auf, der schon auf euch wartet. Springt auf die Plattform und, wenn
ihr oben seit geht wieder in den Raum, indem ihr Ruto getroffen habt.

Geht hier geradeaus durch die Türe. Passt dabei auf nicht in ein Loch zu
fallen. Geht hier im Gang nach rechts und stellt dort zusammen mit Ruto auf
den Schalter, damit ihr durch die Türe gehen könnt. Stellt hier Ruto bei der
Türe ab und besiegt alle vier Gegner. Wenn ihs das getan habt, erscheint eine
Truhe mit dem BUMERANG! Geht nun zurück in das Gangsystem.

Geht hier auf die andere Seite und stellt hier Ruto auf den Schalter, damit er
unten bleibt. Im Inneren müsst ihr einen Tentakel besiegen um dann an die
Truhe mit der Karte zu gelangen. Geht nun, nachdem Ruto euch angeschrieen hat,
durch die Türe links, durch die ihr nun gehen könnt, da der Tentakel
verschwunden ist.

Besiegt hier alle Gegner im Zeitlimit um den Kompass zu erhalten. Geht wieder
Raus und dann in den Raum der direkt neben dem Bumerang Raum ist.
Hier müsst ihr wieder einen Tentakel besiegen. Nun könnt ihr noch durch die
letzte Türe gehen und dort noch den letzten Tentakel besiegen. Geht nun wieder
in den Hauptraum.

Springt nun in das Loch oben links, von der Karte aus gesehen. Von hier aus,
könnt ihr die GOLDENEN SKULLTULA #27 & #28 erhalten. Wenn ihr sie habt, geht
durch die Türe. Hier wird Prinzessin Ruto den Zora-Saphir finden. Wenn ihr
euch jedoch ein wenig bewegt wird sie von einem riesigen Oktorok...gefressen?

Egal, was mit ihr passiert ist, ihr müsst nun den Oktorok besiegen. Ihr müsst
einfach von hinten angreifen. Mit dem Bumerang könnt ihr ihn für einige Zeit
betäuben, was hilfreich ist. Wenn ihr ihn besiegt habt, könnt ihr mithilfe der
Plattform nach oben gehen. Verlasst hier den Raum.

Hier müsst ihr mit dem Bumerang die Gummi-Gegner "einfrieren", damit ihr über
sie zur Türe springen könnt. Springt hier auf die andere Plattform, damit sie
runterfährt. Ihr seid nun wieder am Anfang des Dungeons und vor euch ist ein
Schalter auf dem ihr eine Holzkiste drauflegen sollt, damit er gedrückt
bleibt. Geht durch die Türe und ihr seit dann im Raum vor dem Boss.

Hier könnt ihr die GOLDENE SKULLTULA #29 finden. Um die Türe zu öffnen, müsst
ihr weit oben mit dem Bumerang einen Schalter betätigen. Wenn ihr bereit seit,
geht durch die Türe!

Barinade hat es in sich! Bei der ersten Form müsst ihr die drei Tentakel,
an denen Barinade angemacht ist, zerstören.

In der zweiten Form müsst ihr Barinade erst mit dem Bumerang betäuben und dann
mit dem Schwert die kleinen Quallen besiegen. 

Die Dritte ist wie die zweite Form, nur das sich Barinade nun auch bewegt.

In der vierten und letzen Form, könnt ihr Barinade betäuben und dann endlich
selber mit dem Schwer angreifen. Nach einigen Treffern ist Barinade besiegt
und ihr könnt den HERZCONTAINER an euch nehmen und zu Prinzessin Ruto in das
blaue Licht gehen.

- Bumerang
- Goldene Skulltula #26
- Goldene Skulltula #27
- Goldene Skulltula #28
- Goldene Skulltula #29
- Herzcontainer


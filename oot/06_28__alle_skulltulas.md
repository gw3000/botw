## Alle Skulltulas

Geht nach dem Tempel, wie von Rauru verlangt in die Zitadelle der Zeit. Ihr
erhält hier nach der Sequenz die LICHT-PFEILE von Zelda. Bevor wir sie jedoch
retten, erledigen wir noch die restlichen Sachen.

Geht zur Zora-Quelle und zerstört mithilfe der Silberhandschuhe den Silbernen
Felsen hier. Ihr gelangt in einen Gang mit unsichtbaren Skulltulas und am Ende
findet ihr endlich die GOLDENE SKULLTULA #100! Nun haben wir alle!

Geht zum Skulltula Haus um den letzten Preis abzuholen... 200 Rubine! Ihr
werdet nun wohl nie mehr Rubine brauchen, aber ihr könnt hier jedesmal wenn
ihr das Haus neu betretet wieder die 200 Rubine bekommen.

Geht nun in die Gerudo-Festung und dort in die Gerudo-Arena. Hier können wir
noch das letzte Item erhalten.

- Licht-Pfeile
- Goldene Skulltula #100

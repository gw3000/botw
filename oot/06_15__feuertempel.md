## Feuertempel

Geht im ersten Raum einfach geradeaus und durch die linke Türe. Hier treffen
wir auf Darunia, der gegen den Boss kämpfen will. Nach der Sequenz, solltet
ihr nach links gehen um dort einen Goronen zu befreien und einen kleinen
Schlüssel zu erhalten. Geht zurück in den Startraum und dort durch die
verschlossene Türe.

Geht in diesem Raum erst in die Türe auf der linken Seite um einen kleinen
Schlüssel bei einem Goronen zu bekommen. Verlasst nun den Raum und dreht euch
um und spielt die Hymmne der Zeit um den Block zu verschieben. Betretet die
Türe hier oben.

Hier findet ihr Bodenplatten, die euch angreifen und die GOLDENE
SKULLTULA #58. Geht wieder in den grossen Raum und diesmal auf die andere
Seite hier könnt ihr die Wand aufsprengen und dort einen Goronen und einen
Schlüssel finden. Geht nun durch die verschlossene Türe.

Hier müsst ihr den Block auf die Lavafontäne schieben, so dass der Block nach
oben befördert wird. Geht hier durch die Türe. Kletter in diesem Raum nach
oben und werft eine Bombe von oben auf den Schalter, so dass ihr das Gitter
hinauklettern könnt, bevor das Feuer zurückkommt.

Geht in diesem Labyrinth nun einfach nach rechts bis ihr einen Goronen trefft.
Nehmt den Schlüssel und geht ein wenig zurück um eine Wand aufzusprengen,
hinter der sich die GOLDENE SKULLTULA #59 befindet. Geht weiter und ihr findet
eine weitere Türe, in der ihr wieder einen Goronen und einen Schlüssel findet.
Im Labyrinth könnt ihr nun durch die geschlossene Türe gehen.

Betätigt den Augenschalter und geht durch die Türe um die Karte zu erhalten.
Geht zurück und geht dirch die verschlossene Türe. In diesem Raum müsst
ihr nur auf die andere Seite gehen, aber ich werdet nach einer Zeit von
einer Flammenwand verfolgt. Am andere Ende findet ihr eine Türe durch die
ihr gehen solltet.

Geht in den Süden vom Raum um einen Goronen und einen Schlüssel zu finden.
Geht nun ein wenig zurück und sprengt den rissigen Boden auf, um wieder einen
Schlüssel und einen Goronen zu finden. Klettert wieder nach oben und spielt
die Vogelscheuchen-Polka um eine Vogelscheuche weiter oben erscheinen zu
lassen. Zieht euch hier zur anderen Plattform.

Hier findet ihr die GOLDENE SKULLTULA #60. Geht in den nächsten Raum um hier
die GOLDENE SKULLTULA #61 zu erhalten. Ihr könnt hier auf einen Schalter
springen um die Truhe für kurze Zeit von den Flammen zu befreien. In der
befinden sich 200 Rubine. Wenn ihr fertig seit könnt ihr hier runter in den
kleinen Raum von vorhin springen.

Geht wieder in den Flammenwand Raum und geht diesmal durch die geschlossene
Türe mittendrin. Ihr seit jetzt in einem Raum, wo es unsichtbare Flammen hat,
die erscheinen, wenn ihr euch nähert. Navigiert zu der Türe rechts. In dem
Raum findet ihr den Kompass. Geht zurück und nun begebt euch zur andere Seite
mit der geschlossenen Türe.

Geht hier einfach gerade aus. Hier müsst ihr einfach zur Türe links kommen und
hindurch gehen. Hier müsst ihr den Gegner besiegen. Ihr müsst einfach Bomben
auf ihn werfen und dann mit dem Schwert angreifen. Springt auf die Plattform
um nach oben zu gehen.

Aktiviert hier den Schalter, damit ihr nach oben klettern könnt. Im nächsten
Raum ist wieder das Spiel mit der Truhe und dem Feuer, nur diesmal findet ihr
den STAHLHAMMER in der Truhe! Mit dem könnt ihr hier den Boden zerbrechen und
durch das Loch gehen. 

Platziert hier eine der Holzboxen auf den Schalter, damit ihr durch die Türe
gehen könnt. Ihr solltet nun wieder im Feuerlabyrinth sein. Schlägt
auf den rostigen Schalter um die Türe zu öffnen. Drinnen solltet ihr die
Hymmne der Zeit spielen und dann den rostigen Schalter betätigen um den
Schlüssel und den Goronen zu befreien. Geht wieder oben raus und schlägt auf
die grosse Bodenplatte um sie nach unten fallen zu lassen. Geht hinterher.

Zerstört im Eingangsraum die Statue um eine Türe erscheinen zu lassen. Geht
hindurch. Besiegt alle Gegner und geht weiter um im nächsten Raum alle Gegner
zu besiegen und die GOLDENE SKULLTULA #62 zu erhalten. Im nächsten Raum müsst
ihr wieder den Miniboss besiegen um dann im nächsten Raum den Masterschlüssel
und den letzten Goronen zu finden. Geht nun zurück in den Raum vor dem
Boss und betretet die Türe, wenn ihr bereit seit.

Euch erwartet hier Volvagia der Drache, auch wenn er mehr wie ein Regenwurm
aussieht.
Der Kampf ist simpel, wenn er seinen Kopf aus einem Loch hält, schlägt mit dem
Hammer darauf, um ihn nachher mit dem Schwert anzugreifen. Wenn er am
rumfliegen ist, könnt ihr ihn auch mit Pfeilen verletzen. Das Gefährliche an
diesem Boss ist, dass er relativ viel Schaden macht.

Er hinterlässt wie alle Bosse einen HERZCONTAINER. Wenn ihr ins blaube Licht
tretet, erhält ihr das Amulett des Feuers.

- Amulett des Feuers
- Stahlhammer
- Goldene Skulltula #58
- Goldene Skulltula #59
- Goldene Skulltula #60
- Goldene Skulltula #61
- Goldene Skulltula #62
- Herzcontainer


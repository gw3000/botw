## Gerudo-Arena

Über dem Eingang könnt ihr einen Augenschalter betätigen und damit 2 Truhen
erscheinen lassen. Geht durch die linke Türe. In diesem Raum müsst ihr die
zwei stalfos in einer Minute schlagen um einen Schlüssel zu erhalten. Geht
weiter.

In diesem Raum habt ihr 1:30 Minuten um alle Silber Rubine einzusammeln. Am
Anfang macht euch auch noch ein Deckengrapscher das Leben schwer. Geht einen
Raum weiter.

Besiegt die Gegner und zieht euch dann mithilfe des Enterhakens und des Auge
der Wahrheit zu dem Schalter in einer Anhöhe. Wenn ihr in gedrückt habt,
solltet ihr den Block verschieben und durch die Türe gehen. Hier findet ihr
den Schlüssel in einer versteckten Truhe. Geht zurück zum Schalter und geht
durch die Türe dort.

Schiesst auf alle Augen der Statue um einen kleinen Schlüssel zu erhalten.
Geht durch die obere Türe um dort im Inneren auch einen kleinen Schlüssel
zu finden. Geht nun im Drehraum durch die andere Türe.

Besiegt alle Gegner und benutzt den Stahlhammer um die Statuen zu zerstören.
Unter einer findet ihr einen Schalter, mit dem ihr Zugang zu einem kleinen
Schlüssel habt. Geht in den nächsten Raum.

Sammelt hier mithilfe der Gleitstiefel alle Rubine um die östliche Tür zu
öffnen. Mit einem Schalter könnt ihr das Feuer auslöschen. Von der mittleren
Plattform aus könnt ihr die Hymmne der Zeit spielen um zu dem kleinen
Schlüssel hier oben zu gelangen. Geht durch die östliche Türe.

Lasst die Blöcke verschwinden und sammelt im Wasser alle Rubine für einen
kleinen Schlüssel. Geht nun im Lavaraum durch die südliche Türe.

Besiegt hier alle Gegner um einen kleinen Schlüssel zu erhalten und geht
weiter um wieder im Startraum zu sein. Geht hier durch die mittlere Türe.

Öffnet hier alle Türe um zur Kiste in der Mitte zu gelangen. Um alle Türen
öffnen zu können braucht ihr 9 Schlüssel einen findet ihr in einer
unsichtbaren Nische in der Decke. Öffnet am Schluss die grosse Truhe um die
EIS-PFEILE zu erhalten.


- Eis-Pfeile

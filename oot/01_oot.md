# The Legend of Zelda: Ocarina of Time

(kurz TLoZ: OoT oder OoT; Zeruda no Densetsu: Toki no Okarina, wörtlich: Die Legende von Zelda: Okarina der Zeit) 
ist ein Videospiel vom japanischen Spieleentwickler und Spielkonsolenhersteller Nintendo. 
Es wird den Genres Action-Adventure beziehungsweise Action-Rollenspiel zugeordnet und erschien 1998 als fünfter Teil der The-Legend-of-Zelda-Reihe für die Nintendo-64-Konsole. 
Bei dem Titel handelt es sich um den ersten Teil der Serie mit 3D-Grafik.

Der Spieler übernimmt die Rolle des Protagonisten Link. Das Ziel des Spiels besteht darin, den Antagonisten Ganondorf davon abzuhalten, das magische Triforce zu seinen Gunsten an sich zu reißen.

Ocarina of Time wurde unter dem Arbeitstitel Zelda 64 von der Nintendo-Abteilung Nintendo Entertainment Analysis & Development entwickelt und von dem externen Unternehmen SRD programmiert. Der Serienschöpfer Shigeru Miyamoto war als Produzent involviert. Mit insgesamt 120 Mitwirkenden und drei Jahren Entwicklungsdauer handelt es sich um eine der umfangreichsten Videospielproduktionen der damaligen Zeit.

The Legend of Zelda: Ocarina of Time hat durchweg positive Kritiken bekommen und gilt laut einigen Einzelpublikationen als bestes Videospiel aller Zeiten. Mit weltweit über 7,6 Millionen verkauften Einheiten ist es der finanziell dritterfolgreichste Zelda-Teil, hinter Twilight Princess mit ca. 8,9 Millionen verkauften Einheiten und Breath of the Wild mit ca. 12,8 Millionen verkauften Einheiten. Zusammen mit Ocarina of Time 3D ist es mit 13,2 Millionen verkauften Einheiten sogar der erfolgreichste Teil der Reihe.[2] Ocarina of Time beeinflusste nicht nur die zukünftigen Zelda-Spiele, sondern auch das Action-Adventure-Genre. Auf der Website Metacritic, welche Kritiken von verschiedenen Redaktionen zusammenfasst, ist Ocarina of Time mit der Durchschnittswertung von 99 % das höchstbewertete Videospiel.

Die Originalfassung ist seit ihrer Erstveröffentlichung mehrmals auf anderen Spielkonsolen erneut erschienen. Zusätzlich gibt es eine offizielle Abänderung mit dem Untertitel Master Quest. 2011 erschien eine überarbeitete Neuauflage von Ocarina of Time für die tragbare Spielkonsole Nintendo 3DS. 

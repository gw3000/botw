-- -- -- -- -- -- -- -- -- -- -- -- --
6.26 Durch die Wüste
-- -- -- -- -- -- -- -- -- -- -- -- --
Sprecht mit dem Gerudo auf dem Turm neben dem grossen Tor um es zu öffnen.
Benutzt den Enterhaken um euch über denTreibsand zu ziehen und folgt dann
den Flaggen, bis ihr zu einem Gebäude kommt.

Im Inneren des Gebäudes findet ihr die GOLDENE SKULLTULA #91. Geht nun auf
das Dach und aktiviert das Auge der Wahrheit. Ein Geist wird erscheinen und
euch denWeg durch die Wüste zeigen. Folgt ihm einfach um zum Wüsten-Koloss
zu gelangen.

Betretet den Tempel und verlasst ihn wieder um von Shiek das REQUIEM DER
GEISTER zu erlernen. Kommt nun als kleiner Link zurück. Bevor wir den Tempel
betreten, können wir noch einige Sachen erledigen.

Neben dem Tempel Eingang könnt ihr einen Käfer in das Loch stecken für die
GOLDENE SKULLTULA #92 und pflanzt auch eine Wundererbse. Kommt als Erwachsener
zurück und reitet die Pflanze um das HERZTEIL #36 und die GOLDENE SKULLTULA
#93 auf den Felsformationen zu erhalten.

Im Norden des Gebiets könnt ihr eine Wand freisprengen und dann von der
grossen Fee NAYRUS UMARMUNG zu erhalten. An einer Palme an der Oase könnt ihr
bei Nacht die GOLDENE SKULLTULA #94 finden. Kommt nun als kleiner Link zurück
und betretet den Tempel.

- Requiem der Geister
- Nayrus Umarmung
- Goldene Skulltula #91
- Goldene Skulltula #92
- Goldene Skulltula #93 
- Goldene Skulltula #94
- Herzteil #36
## Klettertour

Endlich können wir in der Story weiterfahren. Geht zum Eingang zum Todesberg
und zeigt der Wache den Brief von Zelda. Er wird euch durchlassen.

Folgt dem Weg aber passt auf die Gegner auf. Nach einiger Zeit kommt ihr an
einem grossen Eingang vorbei der jedoch versperrt ist. Keine Sorge hier werden
wir in Kürze wieder vorbeikommen. Folgt dem Weg weiter aber passt auf den
Goronen auf, der zwischendurch auf euch zurollt. Am Ende könnt ihr in Höhle
eintreten die nach Goronia, der Stadt der Goronen führt.

Springt hier ganz nach unten und steht vor der geschlossenen Türe. Nehmt die
Ocarina hervor und spielt Zelda's Wiegenlied. Die Türe sollte sich öffnen.
Im Inneren trefft ihr auf Daruni, den Goronen Häuptling. Spielt ihm Salia's
Lied vor und er wird euch das GORONEN ARMBAND geben. Mit dem könnt ihr nun
Donnerblumen tragen und werfen.

Nehmt nun einen Deku-Stab und entzündet ihn dann verlasst Darunia's Raum.
Entzündet nun alle Fackeln hier unten damit der riesige Krug sich anfängt zu
bewegen. Geht nun ein wenig nach oben und versucht nun eine Donnerblume in den
Krug zu werfen. Wenn ihr es schafft eine Donnerblume hinein zu werden, wenn
das strahlende Gesicht euch anschaut erhält ihr das HERZTEIL #5. Verlasst nun
nachdem das auch erledigt ist Goronia.

Geht hier draussen nach rechts und hier könnt ihr bei einem Goronen eine
Donnerblume finden. Mit der Donnerblume könnt ihr den versperrten Eingang von
vorhin aufsprengen wenn ihr das gemacht hat betretet den zweiten Dungeon des
Spiels: Dodongo's Höhle!

- Goronen Armband
- Herzteil #5


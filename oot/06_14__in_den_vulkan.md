## In den Vulkan

Begebt euch zurück in die Zitadelle der Zeit um die KANTATE DES LICHTS von
Shiek zu lernen. Geht nun zu den Vogelscheuchen am Hylia-See und sprecht mit
der unteren. Sie will euren Song wieder hören und dann erlernt ihr die
VOGELSCHEUCHEN-POLKA. Dieser Song basiert auf euren Noten. Mit dem könnt ihr
an manchen Stellen eine Vogelscheuche erscheinen lassen.

Geht nach Kakariko und ins Skulltula Haus um das HERZTEIL #20 für 50
Skulltulas zu bekommen. Geht in die Schiessbude, in der ihr nun mit dem Bogen
spielen könnt. Es ist das gleiche Spiel wie früher und der Preis ist das
KÖCHER UPGRADE 1. Geht nun vor Dodongo's Höhle und reitet das Magische Blatt
um das HERZTEIL #21 auf der Höhle zu bekommen. Geht in Dodongo's Höhle.

Begebt euch hier in den Raum mit den grossen Treppen, jedoch von oben, so dass
sie noch nicht gesprengt sind. Sol gelangt ihr an die GOLDENE SKULLTULA #54.
Geht nun wieder am anfang in den Raum mit den Baby-Dodongos. Hier könnt ihr
die Vogelscheuchen-Polka spielen um an die GOLDENE SKULLTULA #55 zu gelangen.
Mit diesen Skulltulas in der Hand, solltet ihr nach Goronia gehen.

Hier findet ihr nun einen Goronen, den ihr mit einer Bombe anhalten solltet,
um die GORONEN-RÜSTUNG zu bekommen. Ganz oben dort, wo ihr über die Seile
laufen könnt, könnt ihr am Podest die GOLDENE SKULLTULA #56 bekommen. Geht
nun in Darunia's Raum und schiebt die Statue zur Seite.

Zieht euch über die kaputte Brücke um von Shiek danach den BOLERO DES FEUERS
zu erlernen. Kommt hier als Kind wieder zurück und lässt einen Käfer hier in
die weiche Erde krabbeln um die GODLENE SKULLTULA #57 zum Vorschein zu
bringen. Pflanzt noch eine Wundererbse und kommt wieder als Erwachsener 
hierhin.

Ihr könnt nun die Pflanze reiten und das HERZTEIL #22 einsammeln. Betretet nun
den Tempel.


- Kantate des Lichts
- Bolero des Feuers
- Vogelscheuchen-Polka
- Köcher Upgrade 1
- Goldene Skulltula #54
- Goldene Skulltula #55
- Goldene Skulltula #56
- Goldene Skulltula #57
- Herzteil #20
- Herzteil #21
- Herzteil #22


## Schattentempel

Öffnet den Eingang zum Tempel mit Dins Feuerinferno, das ist der einige Ort,
an dem man Dins Feuerinferno benötigt.

Zieht euch über den Abgrund und geht durch die falsche Wand. Geht im Raum
durch die falsche Wand. Folgt hier der rechten Wand um zu einer Türe zu
kommen und besiegt den Zombie für die Karte. Folgt dem Gang wieder weiter an
der rechten Wand entlang und ihr gelangt in einem Raum, wo ihr einen
Hirnsauger besiegen müsst. Ihr erhält die GLEITSTIEFEL als Belohnung. Mit
denen könnt ihr über eine kurze Entfernung gleiten.

Wieder im Startraum. Dreht die Staue, so dass sie zur einigen richtigen
Fackel zeigt. Das Auge der Wahrheit wird euch zeigen, welche das ist. Springt
dann mithilfe der Gleitstiefel auf die andere Seite.

Geht durch die rechte falsche Wand und besiegt die Mumien hier drin. Die
Hymmne der Sonne eignet sich dazu Perfekt. In der Kiste findet ihr den
Kompass. Geht nun durch die andere falsche Wand.

Hier drinnen müsst ihr der Klinge ausweichen und dazu alle Silber Rubine
einsammeln um einen kleinen Schlüssel zu erhalten. Wieder bei der Kreuzung
könnt ihr noch die letzte Wand sprengen und dann durch die verschlossene Türe
gehen.

Passt in diesem Gang auf die zwei versteckten Skulltulas auf, die von oben
kommen und geht einfach weiter und geht dann an den Guillotinen vorbei.
Springt den an den letzen Guillotinen vorbei um auf der Plattform gegen einen
Stalfos zu kämpfen. Geht links von dort, wo ihr hergekommen seit über die
unsichtbaren Plattformen zur Türe.

In diesem Raum müsst ihr alle Gegner besiegen um zu zwei Truhen und der
GOLDENEN SKULLTULA #82. Geht wieder zurück und geht diesmal runter zu den
Silber Rubinen und den Klingenfallen.

Sammelt alle Rubine ein um die Türe zu öffnen. Einer ist unter dem Beamos
versteckt. Wenn ihr alle habt könnt ihr in den nächsten Raum gehen. Hier könnt
ihr mithilfe des Auge der Wahrheit einen Block herausziehen und ihn als
Deckung von den Stacheldecken benutzen. Zwischendurch könnt ihr noch die
GOLDENE DKULLTULA #83 aus einer Zelle holen. Springt am Schluss über die
Stacheldecken und nehmt einen kleinen Schlüssel aus der Truhe.

Geht nun noch durch die letzte Türe, durch die ihr könnt. Ihr befindet euch
nun in einem Raum mit Silber Rubinen und unsichtbaren Stacheln. Sammelt alle
Rubine ein und geht durch die neugeöffnete Türe. Werft hier eine Bombe in
den Schädel um zur GOLDENEN SKULLTULA #84 und einem kleinen Schlüssel zu
kommen. Geht durch die verschlossene Türe im Stachelraum.

Geht durch diesen winigen Gang mithilfe der Eisenstiefel um am Schluss in
einem Raum mit vielen Ventilatoren zu gelangen. Hier müsst ihr mithilfe eines
Ventilators in die kleine Nische kommen. In dem anderen Raum hier, hat es
nichts wichtiges.

Besiegt hier beide Mumien um Bomben zu erhalten. Sprengt dann den Dreckhaufen
in die Luft um eine unsichtbare Kiste mit einem Schlüssel zu finden. Geht
dann in den nächsten Raum.

Zieht den grossen Block um eine Abkürzung zu einem früheren Raum herzustellen.
Geht nun auf das Boot und spielt hier die Vogelscheuchen-Polka um euch zu der
GOLDENEN SKULLTULA #85 zu ziehen. Spielt Zeldas Wiegenlied, damit das Boot
losfährt. Ihr müsst nun einige Stalfos besiegen, bis ihr am anderen Ende
aufs Land springen könnt. Geht hier durch die rechte Türe.

Ihr seit in einem unsichtbaren Labyrinth. Geht erst durch die südliche Türe.
Hier drinnen müsst ihr den Bodengrapscher besiegen um einen kleinen Schlüssel
erhalten. Geht nun ein den westlichen Raum.

In diesem Raum könnt ihr die Schädel sprengen, aber ihr werdet nicht bekommen.
Das einzig wichtige in diesem Raum ist die GOLDENE SKULLTULA #86. Geht nun
zum Schluss durch die nördliche Türe.

Hier werdet ihr von Stachelwänden eingeengt. Benutzt einfach Dins Feuerinferno
und ihr habt Zugriff zum Master-Schlüssel. Geht zurück in den Raum vor dem
Labyrinth.

Schiesst einen Pfeil auf die Donnerblumen, auf der anderen Seite um einen
Übergang zu erstellen und geht dort durch die Türe und dann, wenn ihr bereit
seit durch die Bosstüre.

Der Boss dieses Dungeons ist Bongo-Bongo, der leichteste Boss meiner Meinung
nach. Komischerweise finden die meisten Leute ihn extrem schwierig...

Betäubt seine Hände und dann seinen Kopf mit Pfeilen um ihn dann mit dem
Schwert angreifen zu können. Mit dem Biggoron-Schwert sollte er dann schon
besiegt sein. Ich kann ihn meisten in zwischen 15 unde 30 Sekunden schlagen.

Wenn ihr in besiegt habt erhält ihr den HERZCONTAINER und dann von Impa das
AMULETT DES SCHATTEN.

- Amulett des Schatten 
- Gleitstiefel
- Goldene Skulltula #82
- Goldene Skulltula #83
- Goldene Skulltula #84
- Goldene Skulltula #85
- Goldene Skulltula #86
- Herzcontainer
## Grund des Brunnen

Geht geradeaus, besiegt die Skulltula und geht dann durch die falsche Wand. 
Geht auf die andere Seite des grosse Raumes und spielt dort Zeldas Wiegenlied
auf dem Triforce-Symbol. Das Wasser wird nun sinken. Geht nun zurück zum
Anfang und ihr könnt hier nun durch ein Loch krabbeln. Geht am Ende durch die
Türe.

Hier müsst ihr gegen einen Hirnsauger kämpfen, diese können nervige Gegner
sein. Lasst euch von den Händen festhalten, damit der Hirnsauger erscheint
und ihr ihn angreifen könnt. Wenn ihr ihn endlich besiegt habt, erhält ihr
das AUGE DER WAHRHEIT. Mit diesem Item könnt ihr unsichtbare Dinge entdecken.

Ihr könnt den Dungeon nun verlassen, aber es gibt noch drei Goldene Skulltulas
zu finden.

Geht durch den westlichen Durchgang im Hauptraum und klettert dort auf der
anderen Seite hoch. Hier könnt ihr in einem Sarg einen kleinen Schlüssel
finden. Geht zurück zum Hauptraum. Ihr könnt hier zwei versteckte Schlüssel
entdecken. Hinter Wänden. Sie befinden sich am Inneren Block. Öffnet jeweils
die Truhe um den Schlüssel zu erhalten.

Geht in das Zentrum des Raumes. Hier könnt ihr hinter zwei verschlossenen
Türen die GOLDENE SKULLTULA #79 und #80 finden. Im nordosten des Raumes könnt
ihr durch ein Loch krabbeln und auf der anderen Seite euren letzten Schlüssel
gebrauchen. Hier müsst ihr euch nun mithilfe des Auges der Wahrheits zur Türe
bewegen. Hinter der Türe müsst ihr noch einen Raubschleim besiegen dann könnt
ihr die GOLDENE SKULLTULA #81 von der Wand einsammeln.

Ihr habt nun alles wichtige im Dungeon. Es gibt noch andere Kisten und sogar
noch die Karte und den Kompass, abe die sind relativ unwichtig. Aber für die,
die es interessiert, man kann die Karte ganz unten in einer Gasse finden und
den Kompass im Hauptraum in der grossen Kiste, welche man nur durch einen
Geheimgang betreten kann.

Mit dem Auge der Wahrheit könnt ihr das Schatzkisten Spiel auf dem Marktplatz
nun leicht gewinnen und das HERZTEIL #33 erhalten. Das Minispiel kann man nur
abends spielen.

- Auge der Wahrheit
- Goldene Skulltula #79
- Goldene Skulltula #80
- Goldene Skulltula #81
- Herzteil #33
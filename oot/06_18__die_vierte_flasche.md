## Die vierte Flasche

Wir werden jetzt, die Quest für die letzte Flasche machen. Geht zum ehemaligen
Wachhaus beim Marktplatz im Inneren findet ihr einen merkwürdigen Typen, der
Irrlichter von euch will. Wenn ihr 10 beringt, erhält ihr die letzte Flasche.
Irrlichter kann man nur an bestimmten Punkten auf der Ebene von Hyrule finden.
Die beste Technik ist auf Epona mit dem Bogen zu reiten und die Irrlichter
dann abzuschiessen. Irrlichter könnt ihr in Leeren Flaschen aufbewahren.
Nachfolgend liste ich die 10 Fundorte der Irrlichter auf.

1.  Direkt wenn ihr den Marktplatz verlässt bei dem Schild.
2.  Bei dem Baum bei der Lon Lon-Farm
3.  Im nordosten von der Ebene von Hyrule hat es viele Büsche. Bei einem wird 
das
    Irrlicht erscheinen
4.  Zwische der Lon Lon-Farm und dem Gerudo Tal bei dem Baum.
5.  Beim Eingang zum Gerudo Tal, wo es unter einem Überhang durchgeht.
6.  Im süden von der Ebene von Hyrule, bei den vielen Bäumen, auf einem
    Grassfleck.
7.  Am selben Ort beim Baum südlich des braunen Felses.
8.  Auf der Y-Kreuzung der Strasse.
9.  Im Ecken der V-Wand, nahe der Lon Lon-Farm.
10. Auf dem Überhang in der Nähe von Kakariko.

Wenn ihr ihm alle 10 Irrlichter gegeben habt erhält ihr endlich die LEERE
FLASCHE #4.

- Leere Flasche #4



## 7 Jahre später...

So, wie wir von Rauru erfahren haben, sind wir 7 Jahre in der Zukunft gelandet
und Ganondorf hat die Macht an sich gerissen. Wir müssen nun alle Weisen
befreien, damit wir ihn schlage können. Wenn ihr die Zitadelle verlässt merkt
ihr, dass überall auf dem Marktplatz nun Zombies sind. Verlasst nun den
Marktplatz und begebt euch in die Lon Lon-Farm.

Sprecht hier mit Ingo um Pferde zu reiten. Spielt Epona's Lied um Epona zu
euch zu rufen und mit ihr zu reiten. Fragt Ingo für ein zweites Mal reiten und
diesmal sprecht ihn an während ihr auf Epona sitzt. Er wird euch zu einem
Rennen herausfordern, welches ihr leicht gewinnen solltet. Er will aber noch
eine Revanche, aber das ist auch nicht viel schwieriger. Ihr habt nun Epona
gewonnen, aber um die Farm verlassen zu können müsst ihr einfach über die Wand
der Farm springen.

Ihr könnt nun Epona jederzeit mithile des Liedes zu euch rufen. Geht nun auf
den Friedhof in Kakariko. Dort, wo ihr die Wundererbse gepflanzt, ist nun ein 
Blatt,
welches euch nach oben befördert. Hier oben findet ihr unter der Kiste das
HERZTEIL #18. Zieht das Grab, welches direkt neben dem Blatt ist zurück und
springt hinunter.

Hier trefft ihr auf den toten Boris, der euch herausfordert. Ihr müsst mit ihm
ein Rennen machen. Wenn ihr gewinnt, erhält ihr den FANGHAKEN. Folgt dem Weg
und ihr findet einen Block. Den könnt ihr mit der Hymmne der Zeit verschwinden
lassen. Ihr findet euch in der Widmühle wieder und könnt das HERZTEIL #19
einsacken. Sprecht mit dem Typ hier um die HYMMNE DES STURMS zu erlernen.

In Kakariko könnt ihr euch mithilfe des Fanghakens auf ein Haus ziehen und
dort die GOLDENE SKULLTULA #44 finden. Geht nun schnell zurück zum Schloss
Hyrule, welches nun Ganondorf's Schloss ist. Hier könnt ihr die GOLDENE
SKULLTULA #45 mithilfe des Fanghakens bekommen. Sich am Steinbogen, der über
der Lava liegt. Geht nun in den von Monster bewohnten Kokiri-Wald.

Hier könnt ihr in der Nacht die GOLDENE SKULLTULA #46 am haus, gegenüber, des
Shops finden. Wenn ihr sie habt, könnt ihr in die Verlorenen Wälder gehen.
Geht rechts, links, rechts, links und links und hier könnt ihr dank der 
Wundererbse
die GOLDENE SKULLTULA #47 bekommen. Verlasst die Lichtung und geht links,
links und rechts ihr seit wieder auf der Heiligen Lichtung.

Hier sind nun überall Moblins. Ihr könnt die entweder von hinten oder von der
Seite aus mit dem Fanghaken besiegen. Am Schluss des Labyrinths, könnt ihr
hinter euch eine Leiter entdecken. Oben könnt ihr die GOLDENE SKULLTULA #48
einsammeln. Geht nun dorthin, wo ihr Salia's Lied gelernt habt.

Ihr werdet von Shiek überrascht und erlernt von ihm, das MENUETT DES WALDES.
Mit diesem Lied könnt ihr euch immer hier hin warpen. Benutz nun den Fanghaken
um euch zum Tempeleingang hochzuziehen und betretet den Waldtempel.

- Fanghaken
- Hymmne des Sturms
- Menuett des Waldes
- Goldene Skulltula #44
- Goldene Skulltula #45
- Goldene Skulltula #46
- Goldene Skulltula #47
- Goldene Skulltula #48
- Herzteil #18
- Herzteil #19


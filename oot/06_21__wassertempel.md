## Wassertempel

Geht durch den östlichen Durchgang und schiebt hier den Block, das wird
noch wichtig für später sein.

Sinkt ganz nach unten und geht dann durch den östlichen Durchgang. Hier
trefft ihr auf Prinzessin Ruto. Nach dem Gespräch solltet ihr nach oben gehen
und im nächsten Raum alle Spikes besiegen um die Karte zu erhalten. Geht
zurück und spielt Zeldas Wiegenlied vor dem Triforce-Symbol um das Wasser
sinken zu lassen.

Springt hinunter und entzündet beide Fackeln um in den nächsten Raum zu gehen.
Hier müsst ihr die Muscheln besiegen um einen kleinen Schlüssel zu erhalten.
Geht zurück in den Hauptraum und geht durch den südlichen Durchgang.

Lasst euch hier durch das Loch fallen und geht auf der andere Seite wieder
nach oben. Stellt euch auf den Schalter und zieht euch dann mit dem Fanghaken
auf die andere Seite. Besiegt hier die Gegner und aktiviert den Kristall
Schalter durch eine Drehattacke um zur GOLDENE SKULLTULA #72 zu gelangen. Geht
zurück in den Hauptraum.

Geht nun durch den westlichen Durchgang. Schiebt den Block nach ganz hinten,
so dass ihr weitergehen könnt. Schalgt den Schalter um mithilfe eines
Wasserstrahls auf die andere Seite springen zu können. In diesem Raum dreht
sich das Wasser. Ihr müsst den Schalter im Mund des Drachen aktivieren und
dann schnell durch das Tor gehen. Hier findet ihr einen kleinen Schlüssel
geht nun wieder in den Hauptraum und betretet die mittlere Säule.

Zieht euch hier nach oben und spielt Zeldas Wiegenlied beim Symbol um das
Wasser wieder ein wenig ansteigen zu lassen. Nun könnt ihr nach unten durch
ein neues Loch sinken. Das ist ein Teil, der viele Leute bei der N64 Version
irritiert hatte, da es dort nicht in der Sequenz gezeigt worden war.

Aktiviert den Schalter und besiegt dann alle Gegner um ein Gitter zu öffnen,
wo ihr dann einen kleinen Schlüssel finden könnt. Geht zurück zur Säule und
geht durch die obere Türe. Geht durch den östlichen Durchgang um dort am Ende
den Kompass zu finden. Geht zurück in den Hauptraum und lasst euch dort runter
sinken, wo ihr Ruto getroffen habt.

Schwimmt hier wieder nach oben um die Wand aufsprengen zu können und einen
kleinen Schlüssel zu finden. Geht nun im Hauptraum durch den westlichen
Durchgang auf mittlere Ebene. Aktiviert ihr die Wassersäule um nach oben zu
gehen und geht dort durch die Türe. Spielt Zeldas Wiegenlied um den
Wasserstand zu normalisieren. Geht durch die westliche Türe.

Zieht euch hier von Plattform zu Plattform um auf die andere Seite zu
gelangen und geht durch die verschlossene Türe. Hier müsst ihr immer
abwechselnd das Wasser mithile des Schalters heben und sinken lassen, damit
ihr euch mit dem Fanghaken immer wie weiter ziehen könnt. Geht am Schluss
durch die Türe.

Hier werdet ihr mit Dark-Link konfrontiert. Das wichtigste bei diesem Kampf
ist, ihn nicht anzuvisieren. Die beste Technik ist nämlich ihn einfach mit
dem Schwert ohne anzuvisieren anzugreifen, dann ist er schnell besiegt. Wenn
ihr mit dieser Technik Probleme habt, könnt ihr auch Dins Feuerinferno oder
den Stahlhammer benutzen. Wenn ihr ihn besiegt hat, könnt ihr weitergehen und
den ENTERHAKEN aus der Truhe nehmen.

Spielt die Hymmne der Zeit um den Block verschwinden zu lassen und lasst
euch ins Loch fallen. Hier werdet ihr von einer Strömung gezogen. Passt auf
die Wirbel im Wasser auf. Nach dem ersten Wirbel könnt ihr die GOLDENE
SKULLTULA #73 von einer Wand schnappen. Folgt der Strömung bis zum
Schluss. Klettert hier wieder an Land und aktiviert den Augenschalter um zu
einer Kiste mit einem kleinen Schlüssel zu gelangen. Geht hier zurück zum
Hauptraum.

Geht schnell wieder durch den westlichen Durchgang ganz oben und schnappt euch
die GOLDENE SKULLTULA #74 rechts von euch. Ändert den Wasserstand wieder erst
zum tiefsten dann zum mittlersten. Beim zweiten Triforce Symbol könnt ihr
auch noch die GOLDENE SKULLTULA #75 bekommen.

Geht zum südlichen Durchgang und betätigt den Augenschalter, um euch schnell
auf die andere Seite ziehen zu können. Schiebt den Block vom Anfang weiter
in ein Loch um zu einem kleinen Schlüssel zu gelangen.

Geht im Hauptraum ganz nach unten und geht hier durch den nördlichen
Durchgang. Geht hier am Ende durch die Türe. Geht in diesem grossen
Wasserraum einfach auf die andere Seite. Besiegt hier am besten alle Gegner
und sprengt dann zwei Löcher in die Wand um einen Block herauszuschieben und
auf den Schalter zu schieben. Geht in den nächsten Raum.

Springt hier einfach über die Wassersäulen auf die andere Seite. Hier könnt
ihr, von dort, wo die Felsen herauskommen, die GOLDENE SKULLTULA #76
bekommen. Geht durch das Loch im Wasser, wenn kein Felsen kommt um im
nächsten Raum endlich den Master-Schlüssel zu finden. geht zurück zum Haupt
Raum. Ändert den Wasserpegel zum höchten,damit ihr euch zur nordlichen Türe
ziehen könnt. Geht dann an den Klingenfallen vorbei zum Bossraum.

Morpha ist der Boss und ist auch relativ simpel. Wenn er mit seinen Tentakeln
angreift, solltet ihr ausweichen und dann sein "Gehirn" mithilfe des
Enterhakens herausziehen und angreifen. Nach ein paar Mal sollte er besiegt
sein und ihr könnt den HERZCONTAINER an euch nehmen. Geht durch das blaue
Licht um von Ruto das AMULETT DES WASSERS zu erhalten.

- Amulett des Wassers
- Enterhaken
- Goldene Skulltula #72
- Goldene Skulltula #73
- Goldene Skulltula #74
- Goldene Skulltula #75
- Goldene Skulltula #76
- Herzcontainer
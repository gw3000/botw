##   Treffen mit der Prinzessin

Draussen werdet ihr vom Deku-Baum wieder mit einer Geschichte begrüsst. An
deren Ende bekommmt ihr den KOKIRI-SMARAGD. Nun könnt ihr endlich den Wald
verlassen. Auf der Brücke, wenn ihr durch den Ausgang geht, trefft ihr auf
Salia. Von ihr erhält ihr die FEEN-OCARINA.

Willkommen auf der Ebene von Hyrule! Ein wenig vorwärts gegangen und ihr 
werdet
schon von einer Eule genervt. Hört euch alles an was sie zu sagen hat und dann
macht euch auf den Weg zum Schloss, welches ihr am anderen Ende der Ebene
sehen solltet. Wenn ihr es nicht vor Sonnenuntergang zum Schloss schafft müsst
ihr bis zum Sonnenaufgang draussen warten. Betretet, wenn ihr könnt das
Schloss oder besser gesagt den Marktplatz.

Begebt euch in direkt in das Wachhaus, das ihr seht. Im Inneren könnt ihr
unter einer der Kisten die GOLDENE SKULLTULA #4 finden. Dieser Platz eignet
sich auch dazu Rubine zu sammeln. Betretet nun den richtigen Marktplatz und
geht von dort aus weiter zum Schloss.

Wenn es Nacht ist könnt ihr in den Baum hineinrollen um die GOLDENE
SKULLTULA #5 zu entdecken.
Hier solltet ihr erstmal von einer Wache gefangennommen werden. Nun könnt
mit Malon sprechen. Beim zweiten Mal bekommt ihr ein Ei. Nun könnt ihr euch
an den Wachen vorbeischleichen. Am Schluss solltet ihr nicht beim normalen
Schlosseingang durch sondern links hinaufklettern und dann in das Wasser
springen. Hier trefft ihr auf den Vater von Malon: Talon. Weckt ihn mit dem
Huhn auf das nach einen Tag/Nach Zyklus aus dem Ei schlüpfen sollte. Nun da
er nicht mehr im weg ist könnt ihr die Kisten in das Wasser schieben und auf
die andere Seite springen und durch das Loch kriechen.

Hier müsst ihr wieder an den Wachen vorbeischleichen. Tut dies einige Male und
ihr werdet bei Prinzessin Zelda ankommen. Sie wird euch viel erzählen und euch
am Schluss einen Brief mit ihrer Unterschrift geben. Beim Verlassen des
Schlosses trefft ihr auf Impa, die euch ZELDA'S WIEGENLIED beibringt und euch
aus dem Schloss befördert. Sie wird euch auch sagen, dass ihr euch auf dem
Weg zum Todesberg machen sollt. Aber zuerst gibt es einige Dinge zum
erledigen.

- Kokiri-Smaragd
- Feen-Ocarina
- Zelda's Wiegenlied
- Goldene Skulltula #4
- Goldene Skulltula #5


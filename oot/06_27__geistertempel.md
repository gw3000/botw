-- -- -- -- -- -- -- -- -- -- -- -- --
6.27 Geistertempel
-- -- -- -- -- -- -- -- -- -- -- -- --
Sprecht mit Nabooru und krabbelt dann durch das Loch. Besiegt hier alle Gegner
und geht durch die linke Türe. Besiegt den Stalfos und den Knochenschädel und
aktiviert den Schalter mit dem Bumerang. Hier müsst ihr den Anubis besiegen.
Einmal Dins Feuerinferno eingesetzt und er ist besiegt.

Besiegt die Gegner und sammelt alle Rubine ein. Die GOLDENE SKULLTULA #95 kann
am Gitter gefunden werden. Entzündet die Fackeln für einen Schlüssel und geht
mit dem in den nächsten Raum.

Klettert hinauf und sammelt die GOLDENE SKULLTULA #96 ein. Wir müssen nun den
Felsen mit Krabbelminen zerstören um in den nächste Raum zu kommen. Schiebt
die Statue hinunter und geht durch die nächste Türe.

Sammelt alle Silber Rubine und entzündet dann alle Fackeln um die Türe zu
öffnen. Sammelt in diesem Raum noch die GOLDENE SKULLTULA #97 ein und geht
weiter.

In diesem Raum müsst ihr gegen einen Eisenprinz kämpfen. Mit seiner Axt kann
er bis zu 4 Herzen Schaden machen. Man kann ihn auch mit Bomben und
Krabbelminen angreifen. MAcht am Besten immer eine Sprungattacke und dann
einen Rückwärtssalto um seinem Angriff auszuweichen.

Im nächsten Raum könnt ihr die SILBERHANDSCHUHE in der Kiste finden und
Nabooru wird entführt. Zeit als grosser Link in den Tempel zu gehen.

Schiebt den grossen Block um in einen neuen Raum zu gelangen. Aktiviert den
Schalter und geht durch die linke Türe. Spielt Zeldas Wiegenlied um den
Kompass zu erhalten. Geht nun durch die andere Türe.

Sammelt hier alle Silber Rubine und, wenn ihr fertig seit könnt ihr die Hymmne
der Zeit spielen um die GOLDENE SKULLTULA #98 zu finden. Im Raum findet ihr
einen Raubschleim und einen kleinen Schlüssel. Öffnet damit die Türe und geht
weiter.

Besiegt den unsichtbaren Bodengrapscher und stellt den Spiegel zum dritten
Sonnensymbol, damit sich die Türe öffnet. die zweite Kiste firert euch ein.
Wir sind wieder im Hauptraum. Springt auf die eine Hand und spielt Zeldas
Wiegenlied. Schlägt den rostigen Schalter in der Nähe.

Öffnet die Kiste auf der anderer Hand für einen Schlüssel und spielt die
Vogelscheuchen-Polka für die GOLDENE SKULLTULA #99. Springt hinunter und
etzündet die Fackeln für die Karte. Ihr könnt durch die mittlere Türe gehen um
eine Abkürzung zu diesem Raum zu machen. Geht durch die verschlossene Türe,
wenn ihr fertig seit.

Geht durch den Korridor und besiegt im nächsten Raum die Anubis mit Dins
Feuerinferno oder den Feuerpfeilen. Geht durch die neue Türe. Steht hier
auf der rechten Seite vor der verschlossenen Türe und schiesst einen Pfeil auf
einen Armos, so dass er auf den Schalter tritt und ihr schnell durch die Türe
gehen könnt. Besiegt hier den Eisenprinz, welcher als Ewachsener viel
einfacher ist und nehmt im nächsten Raum den SPIEGELSCHILD an euch.

Geht wieder in den Armos Raum und reflektiert das Licht, so dass sich die
Türe öffnet und ihr einen kleinen Schlüssel bekommt den ihr im Anubis Raum
benutzen könnt. Bei den beweglichen Plattformen, könnt ihr euch auch ganz
einfach mit dem Enterhaken ganz nach oben ziehen. Spielt Zeldas
Wiegenlied um die Türe zu öffnen.

Besiegt alle Gegner und aktiviert den Augenschalter hinter einer der falschen
Türen. Dann könnt ihr euch nach oben auf den Schalter ziehen und somit unten
an den Master-Schlüssel gelangen. Geht wieder raus und in den nächsten Raum.

Aktivert den Kristall Schalter und geht durch die nächste Türe. Hier müsst
ihr die Spiegel so umdrehen, dass sie das Locht zum grossen Spiegel
reflektieren. An einem Ort müsst ihr die Wand aufsprengen. Geht wieder zum
grossen Spiegel.

Reflektiert das Licht auf die Statue um euch dann dort ranzuziehen. Im
nächsten Raum müsst ihr gegen einen Eisenprinz kämpfen. Der Eisenprinz ist
in Wirklichkeit Nabooru. Geht nun durch die nächste Türe für den richtigen
Bosskampf.

Ihr müsst mit dem Schild die Magieattacken der Hexen auf die jeweils andere
reflektieren. Wenn sie sich vereinigt haben, müsst ihr euer Schild mit
jeweils drei Magie Attacken aufladen und dann auf die richtige Hälfte zielen.
Nun könnt ihr sie mit dem Schwert angreifen. Nach einigen Mal habt ihr auch
diesen Kampf überstanden und erhält den letzen HERZCONTAINER und das
AMULETT DER GEISTER.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Amulett der Geister                                                     +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Silberhandschuhe                                                        +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Spiegelschild                                                           +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Goldene Skulltula #95                                                   +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Goldene Skulltula #96                                                   +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Goldene Skulltula #97                                                   +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Goldene Skulltula #98                                                   +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Goldene Skulltula #99                                                   +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+[ ] Herzcontainer                                                           +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
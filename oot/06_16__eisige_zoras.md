## Eisige Zoras

Verlasst den Feuertempel und geht über die Brücke wieder zurück. Geht diesmal
jedoch nicht in Darunia's Raum sondern ein wenig weiter. Hier könnt ihr drei
rote Felsen zerstören (mit dem Hammer) und eine grosse Fee treffen. Sie wird
euch das MAGIELEISTEN UPGRADE geben. Verlasst den Krater nun von oben. Geht
hier ein wenig dem Weg entlang um bei Nacht unter einem Felsen die GOLDENE
SKULLTULA #63 zu finden. Neben dem Eingang zu Goronia könnt ihr die Wand mit
dem Hammer zerstören und die GOLDENE SKULLTULA #64 finden.

Geht zum Zora-Fluss. Hier könnt ihr bei Nacht noch zwei Skulltulas bekommen.
Für die erste müsst ihr auf der grossen Plattform stehen (Ihr kommt mithilfe
des Blatts hierhin) und die andere ist in der Kurve des Flusses. Wenn ihr
die GOLDENEN SKULLTULAS #65 & #66 habt, könnt ihr in Zora's Reich gehen.

Hier ist alles nun gefroren :(. Am gefrorenen Wasserfall könnt ihr bei Nacht
die GOLDENE SKULLTULA #67 ehalten. Geht neben König Zora weiter zur Quelle.
Lord Jabu-Jabu ist nicht mehr da... Springt über die Eisschollen um das
HERZTEIL #23 zu erhalten. Wenn ihr es habt könnt ihr in die Höhle springen,
welche ein kleiner Dungeon ist.

- Magieleisten Upgrade
- Goldene Skulltula #64
- Goldene Skulltula #65
- Goldene Skulltula #66
- Goldene Skulltula #67
- Herzteil #23


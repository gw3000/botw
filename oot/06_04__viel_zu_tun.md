## Viel zu tun

Begebt euch nach Kakariko, wie Impa es euch gesagt hat. Hier solltet ihr euch
zum Friedhof begeben. Begebt euch zum grosse Grabstein und spielt, während
ihr auf dem Triforce Sybol steht, Zelda Wiegenlied. Der Grabstein wird
explodieren und ein Loch hinterlassen in das ihr springen solltet.

In diesem Raum hier solltet ihr alle Flederbeisser besiegen, damit sich
die Türe öffnet. Im nächsten Raum solltet ihr die Zombies und die Grüne
Flüssigkeit meiden und zum anderen Ende des Raums gehen. Hier findet ihr einen
grossen Stein mit einer Inschrift, wenn ihr sie lest werdet ihr die HYMNNE DER
SONNE erlernen. Mit dem Lied könnt ihr nun die Zombies einfrieren, was sehr
bequem ist. Verlasst das Grab.

Wenn ihr von hier aus zum Eingang des Friedhofs schaut solltet ihr das vierte
Grab von links in der nächsten Reihe nach hinten ziehen. Im Inneren dieses
Grabes findet ihr einen Zombie den ihr besiegen solltet. Wenn er besiegt ist
könnt ihr die Hymmne der Sonne spielen um eine Truhe mit dem HERZTEIL #1
erscheinen zu lassen.

In der ersten Reihe der Gräber hat es eines mit Blumen davor. Im Inneren
könnt ihr das HYLIA SCHILD aus einer kleinen Truhe erhalten, welches ideal
für den nächsten Dungeons ist.

Nun geht wieder auf die Ebene von Hyrule und von dort zur Lon Lon-Farm. Dort
angekommen, betretet direkt das Gebäude links. Im inneren findet ihr Talon
der ein Spiel mit euch spielen will. Ihr sollt drei spezielle Hühner in einer
limitierten zeit einfangen. Für das Gewinnen erhält ihr die LEERE FLASCHE #1.

Geht nun auf die richtige Farm und rollt in den Baum links um die GOLDENE
SKULLTULA #6 zu erhalten. Geht nun zum Zenter des Feldes und sprecht dort
mehrmals mit Malon. Wenn ihr nun die Ocarina hervornehmt, will sie euch
EPONA'S LIED beibringen. Spielt nun die Hymnne der Sonnne um es Nacht zu
machen.

Begebt euch zu dem Silo am anderen Ende der Farm. Während dem Weg dorthin,
könnt ihr noch die GOLDENE SKULLTULA #7 am Zaun einheimsen. Wenn ihr beim Silo
seit betretet es. Im Silo müsst ihr die Kisten so verschieben, dass ihr
Zugang zum Loch hat, durch das ihr kriechen könnt. Hier findet ihr das
HERZTEIL #2. Verlasst nun die Farm und begebt euch zurück zum Kokiri-Wald.

Macht es Nacht und dann könnr ihr beim Haus auf dem Hügel die GOLDENE
SKULLTULA #8 auf der Rückseite entdecken. Ihr müsst die Sprungattacke
einsetzen um das Symbol einzusammeln. Geht nun in die Verlorenen Wälder, die
man vom höchsten Punkt aus betreten kann.

Die Verlorenen Wälder sind ein Labyrinth, welches euch wieder zum Startpunkt
befördert, wenn ihr den falschen Weg nehmt. Geht erst einmal nach rechts und
schiesst hier mit der Schleuder 3-mal in die Mitte der Zielscheibe, damit ihr
das DEKU-KERNE UPGRADE 1 bekommt, mit dem ihr 40-Kerne halten könnt. Als
nächstes springt hier herunter und steht auf den Baumstamm und nehmt eure
OCarina hervor. Ihr müsst die Noten der Horrorkids nachspielen. Wenn ihr das
zum dritten Mal erfolgreich gemacht habt erhält ihr das HERZTEIL #3.

Nun müsst ihr zur heiligen Lichtung gelangen, dafür müsst ihr vom Startpunkt
folgenden Weg gehen: Rechts, links, rechts, links, vorwärts, links und rechts.
Hier werdet ihr von einem Wolfsheimer begrüsst, besiegt ihn und geht durch
das kleine Labyrinth mit den Laubkerlen. Am Ende trefft ihr auf Salia und
lernt SALIA'S LIED! Mit dem Lied geht wieder zum Startpunkt in den Verlorenen
Wäldern und dann rechts. Steht hier auf dem Baumstamm und spielt Salia's
Lied. Zur Belohnung erhält ihr HERZTEIL #4 vom Horrorkid.

Geht nochmal links und hier in diesem langen Raum ganz nach hinten. Besiegt
hier den Laubkerl, damit er euch ein DEKU-STAB UPGADE 1 für 40 Rubine anbietet.
Kauft es und begebt euch zurück nach Kakariko.

Wir sind immer noch nicht fertig mit unserem Sammelwahn. Sprecht mit dem
Mädchen nahe des Eingangs zum Friedhof. Sie will das ihr, ihre 7 Hühner zurück
ins Gehege bringt.

-Das erste Huhn ist gerade beim Gehege.

-Das zweite ist beim Skulltula Haus, um zu ihm zu gelangen müsst ihr mit einem
 anderen Huhn dorthin fliegen.

-Das dritte ist beim Eingang zum Dorf.

-Das vierte ist in der Kiste bevor man die Treppen hinauf geht.
-Das fünfte ist vor dem Eingang zum Todesberg.

-Des sechste ist hinter dem nicht erreichbaren Zaun beim Haus mit dem
 blauen Dach. Fliegt einfach mit einem Huhn darüber.

-Das siebte ist auch direkt hier. Klettert einfach die Leiter hinauf um es zu
 finden.

Wenn ihr alle Hühner zurückgebracht habt erhält ihr die LEERE FLASCHE #2.

Nun können wir hier in Kakariko noch einige Skulltulas einsammeln. Geht auf
den Friedhof und ändert die Zeit zu Nacht. Hier auf der linken Seite findet
ihr weiche Erde. Wenn ihr einen Käfer darüber loslässt, erscheint die
GOLDENE SKULLTULA #9 aus dem Loch. Käfer kann man in Flaschen aufbewahren und
man kann sie unter Büschen und Steine finden. Verlasst den Friedhof nun
wieder.

Hier in Kakariko können wir 5 Goldene Skulltulas finden. GOLDENE SKULLTULA #10
kann man auf der Baustelle finden. Die GOLDENE SKULLTULA #11 ist am Skulltula
Haus. Die GOLDENE SKULLTULA #12 erscheint wenn ihr in den Baum am Eingang
rollt. Die GOLDENE SKULLTULA #13 klettert auf der Leiter zu dem Turm und zum
Schluss könnt ihr die GOLDENE SKULLTULA #14 am Haus, welches am nächsten
zum Todesberg ist finden.

Geht nun ins Skulltula Haus um den ersten Preis für die Skulltulas abzuholen.
Sprecht im Inneren mit dem Jungen, der wieder ein Mensch geworden ist. Zum
Dank schenkt er euch die GROSSE GELDBÖRSE mit der ihr bis zu 200 Rubine tragen
könnt.

- Hylia-Schild
- Grosse Geldbörse
- Deku-Kerne Upgrade 1
- Deku-Stab Upgade 1
- Leere Flasche #1
- Leere Flasche #2
- Hymmne der Sonne
- Epona's Lied
- Salia's Lied
- Goldene Skulltula #6
- Goldene Skulltula #7
- Goldene Skulltula #8
- Goldene Skulltula #9
- Goldene Skulltula #10
- Goldene Skulltula #11
- Goldene Skulltula #12
- Goldene Skulltula #13
- Goldene Skulltula #14
- Herzteil #1
- Herzteil #2
- Herzteil #3
- Herzteil #4



## Dodongo's Höhle

Im ersten Raum könnt ihr direkt die Wand vor mit einer Donnerblume sprengen.
Nun seit ihr erst richtig im Dungeon.

Vergesst nicht den Hylia-Schild auszurüsten, da der Deku-Schild hier schnell
vebrennt! Springt über die Plattformen erst nach vorne und dann links. Passt
debei auf den Beamos auf. Auf der linken Seite angekommen, könnt ihr mit
einer Donnerblume die zwei Wände zerstören. In der einen findet ihr eine
Truhe mit der Karte und in der anderen einen Laubkerl der euch Deku-Schilder
verkauft. Begebt euch nun auf die andere Seite des Raumes.

Zerstört hier den Beamos und danach die Wand mit Donnerblumen. In diesem Gang
solltet ihr auf die Baby-Dodongos aufpassen. Um die Türe zu öffnen müsst ihr
etwas auf den Schalter stellen. Die Armos-statue eignet sich toll dafür. Tut
dies und ihr könnt durch die Türe gehen. Der nächste Raum hat nur ein paar
Flederbeisser. Geht deshalb direkt in den nächsten Raum.

Hier müsst ihr die zwei Gegner besiegen, dann könnt ihr durch die Türe gehen.
Im nächsten Raum trefft ihr auf drei Dodongos. Besiegt sie, denn sonst wird
die nächste Aufgabe ein wenig schwieriger. Ihr müsst mithilfe eines
Deku-Stabes alle Fackeln anzünden, damit sich die Türe öffnet. Wir sind wieder
im ersten Raum. Geht auf den Schalter damit sich auf der anderen Seite eine
Türe öffnet, durch die ihr gehen solltet.

Nehmt hier eine Donnerblume und zerstört die Wand. Im Inneren müsst ihr einen
Armos zerstören, damit eine Kiste mit dem Kompass erscheint. Kehrt zurück in
den vorherigen Raum. Legt eine Donnerblume zwischen alle anderen damit alles
explodiert und die riesigen Treppenstufen runterkommen. Kletter hinauf dann
um die Kurve. Hier könnt ihr die GOLDENE SKULLTULA #15 an den Spinnweben
besiegen und dann hinaufklettern um das Symbol einzusammeln.

In diesem Raum müsst ihr einfach auf den Schalter stehen, damit sich die Türe
öffnet. Dafür müsst ihr die Armos-Statue vor der Leiter wegziehen. Wir sind
nun im Hauptraum, jedoch weiter oben. Geht einfach über die Brücke und
betretet den nächsten Raum.

Passt in diesem Raum auf die Klingenfallen auf. Geht zum anderen Ende des
Raumes und zieht den Block ein wenig zurück so das ihr auf die Plattform
klettern könnt. Hier in der Truhe findet ihr 20 Rubine. Nun müsst ihr
versuchen mit einer Donnerblume die Wand zu zerstören die direkt vor euch ist.
Wenn ihr es geschafft hat, geht weiter durch den neuen Durchgang.

In diesem Raum müsst ihr einen Augenschalter aktivieren, damit die Flammen
veschwinden und ihr weitergehen könnnt. Hier müsst ihr nun wieder 2 Lizalfos
besiegen. Geht durch die Türe, wenn ihr sie besiegt hat.

Aktiviert hier wieder den Augenschalter und springt dann auf die Plattform.
Dreht euch hier nach links um noch einen Augenschalter zu aktivieren und geht
dann weiter. Springt hier auf die andere Seite und öffnet die Truhe für die
BOMBENTASCHE! Begebt euch nun weiter.

Wir sind wieder im Hauptraum. Stellt euch auf den Schalter um eine Abkürzung
nach hier oben zu erstellen. Nun müsst ihr jeweils eine Bombe durch die Löcher
der Brücke fallenlassen, damit die Augen der riesigen Statue aktiviert werden
und einen neuen Druchgang erschaffen.

Springt nun hinunter und wieder dorthin, wo wir ganz am Anfang waren. Im Raum
mit den Baby-Dodongos könnt ihr eine Wand sprengen, in dessen Raum ihr dann
die GOLDENE SKULLTULA #16 findet. Geht nun wieder in den Hauptraum und dort
durch den Schädel.

Geht in diesem Raum einfach gerade rechts weiter. In diesem Raum würde ich die
Feuerbeisser besiegen, da sie einem nur nerven. Am anderen Ende könnt ihr
eine Wand sprengen und dann in den Raum gehen. In diesem Raum findet ihr die
GOLDEE SKULLTULA #17. Geht wieder zurück.

Klettert hier nun auf die Wand und läuft so zum unerreichbaren teil des
Raumes. Im nächsten Raum müsst ihr den Block herunter schieben und dann in das
Loch, damit sich die Türe zum Boss öffnet. Geht durch die Türe. Hier findet 
ihr
noch eine Truhe mit Bomben. Wenn ihr bereit seit, könnt ihr hinunter springen.

Der Boss ist King Dodongo! Er ist eigentlich sehr simpel, wenn er Luft holt um
Feuer zu speien könnt ihr ihm eine Bombe in den mund werfen und ihn danach mit
dem Schwert bearbeiten. Wenn er auf euch zurollt müsst ihr einfach das Hylia-
Schild benutzen, dann erhält ihr keinen Schaden. Nach ein paar Schwert Hieben
wird er schon besiegt sein. Sammelt noch den HERZCONTAINER ein und tretet in
das blaue Licht.

- Bombentasche
- Goldene Skulltula #15
- Goldene Skulltula #16
- Goldene Skulltula #17
- Herzcontainer



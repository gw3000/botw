## Letzte Aufträge

Ich weiss es jetzt schon, dass dieses Kapitel riesig wird...
Begebt euch zum Schloss und ihr werdet eine Cutscene antreffen. Es ist die aus
dem Intro des Spiels, wenn sie fertig ist, springt ins Wasser um die OCARINA
DER ZEIT einzusammeln. Eine Vision von Zelda bringt euch dazu noch die HYMNNE
DER ZEIT bei. Geht nun zur Lon Lon-Farm und macht es Nacht.

Am Fenster am Gebäude könnt ihr die GOLDENE SKULLTULA #32 mit dem Bumerang
einsammeln. An der Wand rechts vom Silo könnt ihr die GOLDENE SKULLTULA #33
finden. Macht euch nun auf dem Weg nach Kakariko. Bevor ihr die Treppen hinauf
geht solltet ihr mithilfe einer Bombe ein Loch bei diesem einsamen Baum
ausprengen. Im Inneren findet ihr die GOLDENE SKULLTULA #34.

Geht ins Skulltula Haus und holt euch die Riesen Geldbörse für 30 Skulltulas.
Verlasst nun Kakariko und spielt die Hymmne der Sonne. Wenn ihr das
Wolfsheulen hört geht schnell hinein nach Kakariko. Fragt den Ritter dort
welche Zeit es ist. Wenn es zwischen 18- und 21 Uhr ist seit ihr richtig.
Geht nun zum Friedhof und sprecht mit Boris um ein Spiel zu spielen. Er wird
für 10 Rubine jeweils etwas ausgraben und das HERZTEIL #14 gehört auch dazu.
Wenn ihr fertig seit könnt ihr noch die GOLDENE SKULLTULA #35 an einer Wand
finden. Pflanzt noch schnell eine Magische Wundererbse hier und vor Dodongo's
Höhle und begebt euch zurück zum Deku-Baum.

Geht hinein und springt herunter. Geht dann über dem Block auf die andere

Seite und kriecht durch das Loch. Hier können wir mithilfe der Bomben einen
neuen Raum freilegen in dem ihr die GOLDENE SKULLTULA #36 findet. 

Neben dem Deku-Shop könnt ihr aus der weichen Erde mithilfe eines Käfers die
GOLDENE SKULLTULA #37 entlocken. Pflanzt auch gerade eine Magische 
Wundererbse, wenn
ihr schon hier seit. Geht nun in die Verlorenen Wälder.

Geht zweimal links um wieder weiche Erde zu finden. Steckt einen Käfer hinein
für die GOLDENE SKULLTULA #38 und pflanzt eine Wundererbse. Geht nun vom 
Startpunkt:
rechts, links, rechts, links, links. Ihr seit nun auf einer Lichtung auf der
ihr wieder eine weiche Erde findet. Ihr wisst schon: Käfer für GOLDENE
SKULLTULA #39 und eine Wundererbse pflanzen.

Geht wieder zum Startpunkt und diemal geht ihr: rechts, links, rechts, links,
vorwärts, links. Hier findet ihr einen Felsen, den ihr wegspregen könnt. Im
Loch darunter, findet ihr einen Laubkerl, der euch das DEKU-NUSS UPGRADE 1
verkauft.

Geht nun auf die Ebene von Hyrule. Dort, wo es zum Hylia-See geht könnt ihr
zwischen den Zäunen ein Loch freilegen. Im Inneren könnt ihr von einem
Laubkerl das HERZTEIL #15 für 10 Rubine kaufen. Geht nun zum Hylia-See.

Am Labor, kann man von der Brücke aus die GOLDENE SKULLTULA #40 einsammeln,
jedoch nur wenn es Nacht ist. Geht nun zur Ebene von Hyrule im Westen, wo es
ins Gerudo-Tal geht. Dort könnt ihr in der Mitte eines Steinkreises ein Loch
freilegen. Im Inneren liegt die GOLDENE SKULLTULA #41. Geht nun ins
Gerudo-Tal.

Hier bei diesem kleinen Wasserfall kann man in der Nacht die
GOLDENE SKULLTULA #42 einsammeln. Geht ein wenig weiter und nehmt das Huhn.
Fliegt mit ihm in den Wasserfall rechts von der Brücke. Dort drinnen könnt ihr
das HERZTEIL #16 finden. Springt nun ins Wasser und schwimmt an Land. Hier
könnt ihr noch weiche Erde finden. Die GOLDENE SKULLTULA #43 wird
herauskommen und vergesst nicht, noch eine Wundererbse zu pflanzen.

Folgt dem Fluss um wieder im Hylia-See zu landen. Geht wieder ins Gerudo-Tal.
Nehmt wieder das Huhn und fliegt auf die Klippe links von der Brücke, auf der
eine Kiste liegt. Zerstört die Kiste für das HERZTEIL #17. Begebt euch nun zum
Marktplatz.

Geht ins Maskengeschäft und sprecht mit Shigeru Miyamoto... ähhm ich meine mit
dem Maskenhändler. Ihr müsst nun vier von seinen Masken ausliefern um eine
spezielle Maske zu bekommen. Die Fuchsmaske will der Ritter am Eingang zum
Todesberg.

Mit der Skelletmaske, können wir uns noch ein Upgrade holen. Geht in die
Verlorenen Wälder und geht vom Eingang: rechts, links, rechts, links und
nochmal links. Ihr seit wieder auf dieser Lichtung. Hier könnt ihr im hohen
Gras ein Loch entdecken. Im Inneren bekommt ihr, wenn ihr die Maske trägt,
das DEKU-STAB UPGRADE 2. Die Maske könnt ihr an das Horrorkid verkaufen.

Die Horrormaske will das Kind auf dem Friedhof, welches immer Boris nachmacht.

Zu guter letzt die Hasenohren. Auf der Ebene von Hyrule, gibt es einen Mann, 
der
immer rumrennt. Ihm könnt ihr die Maske verkaufen, aber nur wenn er
stillsteht. Für alle diese Lieferaufträge erhält ihr die MASKE DES WISSENS.

Mit der können wir uns nochmal ein Upgrade in den Verlorenen Wäldern holen.
Geht wieder vom Start: rechts, links, rechts, links, links und springt ins
Loch, aber diesmal mit der Maske des Wissens. Ihr erhält dafür das DEKU-NUSS
UPGRADE 2.

Geht nun (endlich) in die Zitadelle der Zeit beim Marktplatz und spielt dort
die Hymmne der Zeit. Das riesige tor wird sich öffnen und ihr könnt aus einem
Podest das Master-Schwert ziehen! Ihr werdet zusätzlich noch in die Zukunft
geschickt.

- Master-Schwert
- Okarina der Zeit
- Hymmne der Zeit
- Riesen Geldbörse
- Deku-Nuss Upgrade 1
- Deku-Nuss Upgrade 2
- Deku-Stab Upgrade 2
- Goldene Skulltula #32
- Goldene Skulltula #33
- Goldene Skulltula #34
- Goldene Skulltula #35
- Goldene Skulltula #36
- Goldene Skulltula #37
- Goldene Skulltula #38
- Goldene Skulltula #39
- Goldene Skulltula #40
- Goldene Skulltula #41
- Goldene Skulltula #42
- Goldene Skulltula #43
- Herzteil #14
- Herzteil #15
- Herzteil #16
- Herzteil #17


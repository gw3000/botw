6.10 Zora-Saphir

Draussen erhält ihr von Ruto den ZORA-SAPHIR! Schwimmt zur kleinen Insel
im unteren rechten Ecken der Karte und rollt in den Baum für die GOLDENE
SKULLTULA #30. Legt nun eine Bombe neben den silbernen Fels um die Wand in die
Luft zu spregen im Inneren trefft ihr auf eine grosse Fee, die euch FARORE'S
DONNERSTURM gibt. Geht nun wieder nach draussen und spielt die Hymmne der
Sonne. Schwimmt nun zu dem Baumstamm, der sich links von Lord Jabu-Jabu
befindet und klettert hinauf. Hier könnt ihr die GOLDENE SKULLTULA #31 mit dem
Bumerang einsammeln. Das ist alles was wir hier bisher machen können.

Zusammenfassung:
----------------
- Zora-Saphir
- Farores Donnersturm
- Goldene Skulltula #30
- Goldene Skulltula #31



## Ganons-Schloss

Geht in Ganons-Schloss einfach durch den ersten Gang um zum Hauptraum zu
gelangen. Hier müsst ihr alle siegel brechen. Fängt mit dem Schattensiegel
an.

Schiesst einen Feuer-Pfeil in die Fackel und rennt über die Plattformen.
Besiegt den Raubschleim und schiesst wieder einen Feuer-Pfeil in die Fackel.
Trete auf den Schalter den ihn auf einer Plattform sehen könnt um die
GOLDHANDSCHUHE zu erhalten. benutzt das Auge der Wahrheit um einen anderen
Schalter zu entdecken um die Türe zu öffnen. Schiesst einen Licht-Pfeil in
das Siegel.

Ihr könnt nun noch das letzte Upgrade des Spiels holen. Dafür müsst ihr den
grossen Pfeiler vor dem Feenbrunnen wegnehmen. Ihr erhält von der Fee
das VERTEIDIGUNGS UPGRADE. Geht nun zum Waldsiegel.

Besiegt alle Gegner und entzündet die Fackeln. Im nächsten Raum müsst ihr
mithilfe der Ventilatoren alle Rubine einsammeln. Wenn ihr das habt könnt
ihr das Siegel lösen.

Im Feuersiegel müsst ihr auch alle Rubine sammeln. Passt auf, dass ihr nicht
in die Lava fällt, denn sann müsst ihr alles von vorne starten. Den Pfeiler
könnt ihr dann, wenn ihr ihn weggeschossen habt, auch als Plattform brauchen.

Im Wassersiegel müsst ihr erst mit blauem Feuer das Eis schmelzen. Nehmt euch
noch etwas Feuer mit ihr werdet es brauchen. Im nächsten Raum müsst ihr
Innerhalb eines Zeitlimits die Blöcke so verschieben, dass ihr auf die höhere
Anhöhe klettern könnt.

Sammelt beim Geistersiegel erst alle Silber Rubine um die Tür zu öffnen. Dann
müsst ihr mit Krabbelminen den schalter aktivieren um am Schluss noch ein
kleines Sonnenrätsel zu lösen. Mit einem Feuer-Pfeil könnt ihr das Licht
hindurchscheinen lassen.

Besiegt bei dem Licht-Siegel alle Gegner. Drei sind unsichtbar. Nehmt den
kleinen Schlüssel und geht durch die Türe. spielt hier einfach Zeldas
Wiegenlied um einen Schlüssel zu bekommen und geht weiter. Sammelt alle Rubine
bei den Felsen um die Türe zu öffnen. Dieser Siegelraum ist eine Fälschung.
Geht mithilfe des Auge der Wahheit zum richtigen Siegel.

Wenn alle Siegel gebrochen sind könnt ihr in den Turm gehen. Geht einfach
immer die Treppen hinauf und besiegt die Gegner um unteranderem auch den
Master-Schlüssel zu bekommen. Am Schluss solltet ihr vor der Boss-Türe stehen.
Geht noch weiter hoch um bei der richtigen Türe zu sein. Wenn ihr bereit seit
könnt ihr eintreten.

Schlägt Ganondorfs Energiebälle zurück um ihn zu betäuben. Benutzt nun einen
Licht-Pfeil um ihn zu Boden zu bringen und schlägt mit dem Schwert auf ihn
ein. Wenn er besiegt ist wird er das Schloss zum Einsturz bringen.

Ihr habt drei Minuten um mit Zelda nach unten zu fliehen. Zwischendurch
müsst ihr noch schnell zwei Stalfos besiegen. Draussen angelangt freuen sich
die Helden über ihren Sieg...

Ganondorf ist noch nicht besiegt und kehrt als Ganon zurück. Er wird euer 
Master-Schwert wegkatapultieren. Deshalb müsst ihr entweder mit dem 
Biggoron-Schwert oder dem Stahlhammer kämpfen. Rollt zwischen seine Beine 
hindurch und schlägt auf seinen Schwanz. Nach einiger Zeit erhält ihr das 
Master-Schwert zurück. Kämpft nun mit dem Master-Schwert und er sollte 
schnell besiegt sein und ihr habt The Legend of Zelda: Ocarina of Time 3D 
erfolgreich 100% durchgespielt. Geniesst nun noch das Ende und die Credits.

- Goldhandschuhe
- Verteidigungs Upgrade
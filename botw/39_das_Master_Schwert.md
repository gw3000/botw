
# Das Master-Schwert

Wenn ihr unserer Lösung gefolgt und möglichst viele Schreine geknackt habt, solltet ihr viele zusätzliche Herzen und einige Ausdauer-Container bei Göttinnen-Statuen eingetauscht haben. Angesichts der Menge an Zeichen der Bewährung (für jeweils vier bekommt ihr im Tausch einen Herz- oder Ausdauer-Container) dürftet ihr zumindest die erste Leiste komplett gefüllt haben und somit längst in der Lage sein, das Master-Schwert zu bekommen. Ihr benötigt für den Erwerb des Master-Schwerts mindestens 13 Herzen in der Lebensleiste.

Solltet ihr bislang Vorlieb mit Ausdauer-Containern genommen haben, so könnt ihr diese (vorübergehend oder dauerhaft) eintauschen, und zwar wenn ihr mit der dunklen Statue in der Stadt Hateno sprecht, die in der Nähe des westlichen Teichs an einem Hügel steht. Nur sie tauscht Ausdauer gegen Herzen und umgekehrt. Wenn auch nur gegen eine saftige Gebühr.

Das Master-Schwert findet ihr in einem Gebiet, dass sich die Region der Wälder nennt. Es liegt westlich des Todesberges, direkt nördlich des Schlosses. Ihr erreicht es am leichtesten, wenn ihr euch zum Ranelle-Turm teleportiert und von dort aus der Straße nach Nordwesten folgt. So könnt ihr den Turm der Wälder gar nicht verpassen.

Diesen Turm zu besteigen ist nicht so leicht, wie es aussieht. Der große Sumpf sowie einige Anlagen voller Feinde halten euch von der Besteigung ab. Die leichteste Methode, an den Turm heran zu kommen, ist um das Sumpfbecken herum zu laufen und den Felsen dahinter hoch zu kraxeln. Wenn ihr oben steht, seht ihr schon den Wald. Dreht euch nun um und segelt zum Holzbau der direkt im Sumpf steckt. Dort werde ihr mindestens einen Gegner antreffen, doch könnt ihr den Kampf verkürzen, wenn ihr die roten TNT-Fässer mit einem Feuerpfeil in die Luft jagt. Lauft dann den Steg entlang und arbeitet euch zum Turm vor. Steigt hinauf und aktiviert den Turm. Die Karte des Waldgebietes, die ihr daraufhin erhaltet, ist extrem nützlich, wenn ihr das Master-Schwert finden möchtet.

Segelt vom Turm aus wieder zurück zum Wald-Eingang und folgt dem Pfad nach Norden, bis es plotzlich nebelig wird und ihr am Zugang eines geschlossenen Waldstücks steht, der die Verlorenen Wälder heißt. Hier seht ihr große Fackeln, die ihr Licht durch den dichten Nebel werfen.

Nun müsst ihr einem vorbestimmten Weg präzise folgt. Weicht ihr von ihm ab, wird der Nebel plötzlich undurchdringbar dicht und Link zurück an den Eingang des Waldstücks versetzt.
[The Legend of Zelda: Breath of the Wild Bild 1 | Datum: 24.03.2017]
![der Eingang des Waldstücks](pics/6f3a4534ec5228815e8f3dec0fcfc7d9.jpg)

Schaut auf unsere Karte, und beachtet folgende Anweisungen ganz genau. Lauft an den ersten drei Fackeln entlang. Dreht euch an der dritten Fackel um 90 Grad nach links und lauft weiter, anschließend wieder links, immer an den Fackeln entlang, die ihr durch den Nebel erspähen könnt. Schaut euch immer genau um. Bei der letzten Fackel die ihr seht, biegt ihr nach rechts ab. Die kommenden Fackeln sind ein wenig weiter weg und daher nicht gleich sichtbar, aber wenn ihr ein paar Meter gelaufen seid, seht ihr sie in der Ferne. Nun steht ihr an zwei Fackeln. Ab hier müsst ihr euch auf den Wind verlassen. Nur er weist euch noch die Richtung. Entzündet eine Fackel und bleibt stehen. Beobachtet, in welche Richtung die Funken fliegen und folgt ihnen immer weiter.

Seid ihr regelmäßig stehen geblieben, lichtet sich der Wald und alle Farben strahlen. Das saftige grün verrät euch, dass ihr im Wald der Krogs angekommen seid. Folgt dem Pfad weiter zu einem Stein, in dem das Master-Schwert steckt. Dahinter wartet der große Deku-Baum auf euch.
[The Legend of Zelda: Breath of the Wild Bild 1 | Datum: 24.03.2017]
![das Masterschwert](pics/01f0380b711bc491632212a88916008b.jpg)

Wie schon erwähnt, benötigt ihr 13 Herzen oder mehr, um das Master-Schwert aus dem Stein ziehen zu können. Wenn ihr damit beginnt, verliert ihr kontinuierlich an Lebenskraft, bis Link entweder stirbt oder das Schwert aus dem Stein gezogen hat.

Es folgen einige Erinnerungssequenzen und eine Rekapitulation des aktuellen Standes. Zudem könnt ihr links vom Schwertsockel eure Krog-Samen gegen weitere Inventar-Plätze eintauschen.
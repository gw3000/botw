# Auf nach Kakariko

Mit dem Parasegel im Gepäck könnt ihr endlich das vergessene Plateau verlassen und in die buchstäblich große weite Welt Hyrules schreiten. Oder eher segeln, da ihr so gefahrlos vom Plateau hinunter kommt. König Rhoam erteilt euch die Hauptaufgabe Impa,die Weise und schickt euch zur Zofe Zeldas. Sie heißt Impa und wohnt im Dörfchen Kakariko, das östlich des Turms liegt. Ihr könnt direkt vom Fester des Turms aus nach Osten auf die Straße vor euch gleiten und der Straße folgen.

## Die Region der Zwillingsberge

Die Straße führt am Präludia-Fluss entlang und führt zum Shiekah-Turm der Zwillingsberge. Klettert auf den Turm und setzt euren Shiekah.Stein hinein, um die Übersichtskarte dieses Gebiets zu erhalten. Folgt ihr der Straße nun weiter nach Osten, führt sie euch durch einen Spalt direkt durch die Zwillingsberge hindurch.

Direkt am Fuß des nördlichen Bergs könnt ihr am Fels hochklettern, um einen Schrein zu erreichen . Den Ri-Dahi-Schrein. Ein einfacher Schrein, bei dem ihr schnell ein weiteres Zeichen absahnen könnt.

## Ri-Dahi-Schrein

Schaut am Start des Schreins nach einem Bodenschalter und tretet drauf. Das bringt eine Kugel dazu, in eine Mulde zu rollen, die eine bewegliche Plattform aktiviert. Geht auf die Plattform und lasst euch zum nächsten Bodenschalter bringen.

Die folgende Aufgabe benötigt gutes Timing. Wenn ihr auf den zweiten Schalter tretet, kippt eine weitere Plattform und eine zweite Kugel bewegt sich, droht aber abzustürzen, weil sie an der darunterliegenden Plattform vorbeifällt. Kurz bevor sie das Ende der oberen Plattform erreicht, tretet ihr vom Schalter weg, damit die Plattform wirder nach oben kippt und die Kugel mit weniger Schwung nach unten fällt. Mit dem richtigen Timing landet die Kugel sicher und rollt in die Mulde auf der linken Seite. Habt ihr das geschafft, könnt ihr wieder durch eine Schwebe-Platte zum nächsten Schalter vortreten.

Wieder ein Bodenschalter, der eine Plattform kippt und eine Kugel rollen lässt Dieses mal müsst ihr den Schalter zeitlich so abgestimmt verlassen, dass der Rückschwung der Plattform die Kugel in die Luft schleudert, damit sie über den Abgrund fliegt und in die Mulde fällt, Wartet dafür auf den Moment, denn die Kugel gerade so den kleinen Schatten auf der Plattform verlassen hat.

Tretetet auf die schwebende Plattform und lasst euch rübertragegen, geht aber noch nicht zum Weisen, denn hier gibt es auf der linken Seite noch eine Schatzkiste mit einem wertvollen Gegenstand. Um an die Kiste zu kommen, aktiviert ihr euer Magnetmodul und schaut nach rechts-unten, Da stehen zwei schwere Fässer. Schnappt euch eins mit eurem Magneten und fahrt damit zum Schalter von eben. Stellt das Fass auf den Schalter, damit die Plattform vor der Kiste nach oben kippt. So kommt ihr an sie heran und könnt aus der Kiste das Kletterkopftuch abstauben. Ein sehr nützliches Utensil, das euch schneller Klettern lässt. Anschließend holt ihr euch das Zeichen beim Weisen und kehrt zur Oberwelt zurück.

## Hayu-Dama-Schrein

Geht nun weiter nach Osten. Das ist nicht ganz ungefährlich, denn hier haust ein weiterer Stamm Bokblins. Rüstet euer Bomben-Modul aus , bewerft sie aus der Ferne und macht euch im Anschluss auf einen harten Kampf gefasst, bei dem ihr notfalls auch ein wenig weglaufen müsst, um ein paar der roten Fieslinge für eine Zeit lang loszuwerden.

Habt ihr alle besiegt, könnt ihr euch ans Plündern machen. An der Felswand findet ihr viele schwarze Gesteinsbrocken,. Das sind Erz-Adern. Ihr könnt sie entweder mit harten Waffen öffnen (zum Beispiel einem großen Schlachthammer) oder einfach mit Bomben aufsprengen, Dafür erhaltet ihr Erze oder Salz, die ihr später verarbeiten oder verkaufen könnt. Salz ist zum Beispiel wertvoll fürs Kochen, aus Harzen und Erzen lassen sich viel später im Spiel Accessoires fertigen.

Geht am Fluss entlang weiter nach Osten, bis zu einer Kreuzung mit einer Brücke, bei der der Weg nach Norden führt. Folgt dem Weg nach norden. Nur wenige Meter weiter findet ihr gleich zwei Anlaufpunkte. Zum einen den Hayu-Dama-Schrein, der in einem kleinen Teich angesiedelt ist, und zum anderen den Stall der Zwillingsberge. Direkt am Schrein könnt ihr eine Frau ansprechen.

Der Hayu-Dama-Schrein ist umzäunt, daher kommt ihr zu Fuß nicht ran, Kein Problem, schaut im Südosten von euch nach einer kleinen Anhebung. Von dort oben könnt ihr über die Umzäunung schweben und den Schrein betreten.

Im Inneren des Schreins warten Wasser-Aufaben auf euch, Die ersten beiden Lücken schließt ihr mit eurem Cryomodul. Erschafft schlicht Eisblöcke an den Wasserfällen auf der rechten Seite.

![Wasserfälle und Eisblöcke[(pics/8cf6a97aba1a5ce57d1729cb19ba904f.jpg)

Rennt die Treppe hinauf und dreht euch zu eurer Rechten. Da seht ihr, wie eine Kugel rollt, über eine Lücke springt und ins Bodenlose fällt. Die Kugel muss aber auf die rechte Seite in eine Mulde fallen. Lösung: Erschafft einen Eisblock an der Stelle, wo die Kugel fällt. Der Eisblock soll eine Blockade sein, damit die Kugel ihre Rollrichtung ändert und auf die Waage darunter rollt. Damit die Waage nicht nach links kippt, erschafft ihr einen Eisblock unter deren linker Seite. Das reicht aber noch nicht, denn die Kugel hat nicht genug Schwung, um dann in die Mulde zu rollen, Helft ihr, in dem ihr einen weiteren Eisblock rechts von der Waage erschafft, auf dem die Kugel weiter ausrollen kann. Siehe unser Screenshot

## Stall der Zwillingsberge

Der Stall der Zwillingsberge, der gegenüber des Hayu-Dama-Schreins liegt, ist eine wichtige Zwischenstation, denn hier könnt ihr zum ersten mal ein Pferd zähmen und registrieren, Es gibt sogar eine Nebenaufgabe, die sich damit beschäftigt. Der Wildfang.

Nebenaufgaben behandeln wir gesondert am Ende eines Kapitels, aber da das Pferd so wichtig ist, machen wir hier eine Ausnahme. Sprecht den mann an, der vor dem Schalter steht. Er wettet mit euch, das ihr es nicht schafft, eine wildes Pferd innerhalb von zwei Minuten zu fangen und zu ihm zu bringen. Schafft ihr es doch, spendiert er einen lila Rubin.

Die Aufgabe ist einfach zu meistern. Hinter dem Stall auf dem Feld grasen viele Pferde. Da ihr noch keine gute Ausdauer habt, solltet ihr euch ein ruhiges Pferd aussuchen . Das sind die gefleckten. Nähert euch schleichend von hinten einm Pferd an und springt mit dem A-Knopf auf, sobald es angezeigt wird. Nun versucht ihr auf dem bockenden Pferd zu bleiben und tätschelt es mit dem L-Knopf. Ein geflecktes Pferd wird sich nur kurz wehren und kann dann zum Stall gebracht werden.

Solltet ihr es nicht in der Zeit schaffen, dann behelft euch mit dem einfachen Trick, schon vor der Annahme der Aufgabe ein Pferd zu zähmen (es aber nicht im Stall zu registrieren) und es direkt neben dem Stall abzustellen.

Euer Pferd könnt ihr anschließend am Schalter registrieren (ihr müsst den Mann am Schalter im Freien ansprechen. Im Gbäude bietet er euch nur ein Bett an), wodurch es einen Sattel erhält und benannt werden kann. Ein registriertes Pferd hört auf euer Pfeifen (Steuerkreuz nach unten) und sucht euch auf, sofern es nicht zu weit entfernt steht oder ein Hindernis im Weg ist .

Euer Pferd wird am Anfang noch bockig sein. Tätschelt es beim Aufsteigen oderwenn es etwas gut gemacht hat, damit es Zuneigung zu euch aufbaut. Dann hört es eher auf euch und ist angenehmer zu reiten.

Macht euch nun auf den Weg nach Norden über die Kakariko-Brücke. Achtung, hinter der Brücke auf der linken Seite steht ein reisender. Wenn ihr ihn ansprecht, wird er ein oberflächliches Gespräch anfangen und euch anschließend angreifen, Er ist ein Shiekah-Krieger und nicht gerade leichte Beute, da er sich Teleportieren kann. Seid ihr noch nicht gut ausgerüstet, dann umgeht ihn lieber, Könnt ihr ihn aber besiegen, so erhaltet ihr einige Rubine, einen Satz Bananen und – ganz wichtig – einen Kopfsammler. Das ist eine große Sichel. Eine sehr starke Waffe, die ihr gut gebrauchen könnt.

Reitet nun weiter nach Norden, bis ihr das Dorf Kakariko erreicht. In Kakariko reitet ihr die Hauptstraße herunter (nicht wundern, das Pferd will in einer Ortschaft nicht galoppieren). Unten an der Gabelung seht ihr ein großes Haus. Ein Mann steht davor. Dies ist Impas haus. Geht hinein und sprecht mit der uralten Frau. Das ist Impa . Sie wird euch einen weiteren Teil der Vorgeschichte erzählen und dann weiter in die nächste Stadt schicken, nach Hateno.

Bevor ihr nach Hateno geht, solltet ihr aber noch ein paar Dinge erledigen. Zum beispiel Lebensmittel einkaufen – siehe das gebäude direkt neben Impas Haus. Dort erhaltet ihr Rüben, Eier und andere wertvolle Zutaten, die ihr draußen an der Feuerstelle zu leckeren und stärkenden Mahlzeiten verarbeiten könnt. Für passende Rezepte rten wir zu einem Blick in unser Kochrezepte-Kapitel.

Außerdem findet ihr hier einige nebenaufgaben vor, die wir am Ende des Kapitels besprechen, sowie einen weiteren Schrein. Geht zurück zur Hauptstraße, am Hühnergehege vorbei und schaut nach Norden. Da führt ein Seitenweg den Berg hinauf. Folgt ihr dem Weg, so erreicht ihr den Taro-Nihi-Schrein. Lasst euch nicht bitten – betretet ihn!

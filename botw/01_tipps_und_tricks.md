# The Legend of Zelda: Breath of the Wild


The Legend of Zelda: Breath of the Wild: Komplettlösung mit allen wichtigen Tipps, Rezepten zum Kochen und Guide zu allen Hauptmissionen und Nebenmissionen - inklusive Karte zu jedem Gebiet! Mit unserem Walkthrough navigiert ihr sicher durch Hyrule.

Wenn ihr in der Wildnis des zerstörten Landes Hyrule überleben wollt, dann haltet euch an folgende Hinweise. Sie können euch oft die Haut retten.

## Waffen und ihre Haltbarkeit

Link findet bei beinahe jedem humanoiden Gegner eine Waffe. Spätestens wenn ihr ihm das Lichtlein ausgeblasen habt, hinterlässt er sie auf dem Boden. Ihr könnt eine begrenzte Anzahl an Waffen und Schilde aufsammeln, solltet aber gut sortieren, denn sie nutzen sich mit der Zeit ab und zerbrechen irgendwann. Einmal zerstört, ist die Waffen beziehungsweise der Schild nicht mehr zu gebrauchen und macht Platz im Inventar.
Angeknackste Waffen und Schilde leuchten im Inventar. Am ehesten trifft es die Modelle aus Holz, zum Beispiel Keulen, Knüppel und Fackeln. Findet ihr eine stärkere Waffe, solltet ihr angebrochene Holzwaffen möglchst schnell loswerden. Entweder ihr werft sie weg (R-Knopf halten), oder ihr entfernt sie manuell über euer Inventar. Wollt ihr große oder gut gepanzerte Gegner angreifen, dann meidet es, eine angeknackste Waffe zu verwenden. Tut euch selbst den gefallen.

## Nehmt ausreichend Proviant und Medizin mit

Link kann an Lagerfeuern mit Kochtöpfen stärkende Gerichte kochen. Zutaten dafür findet ihr überall: Pilze, Äpfel, Nüsse, Fische, Krabben, wilde Tiere und vieles mehr. Nehmt euch Zeit für das Sammeln von Zutaten und das Jagen von Tieren, denn nur so könnt ihr überleben, wenn ihr mal in eine brenzlige Situation kommt. Das wird öfter passieren als ihr zuerst denkt.

## Gekocht ist stärker als roh!

Rohe Äpfel oder Pilze zu essen, solltet ihr nur als Notlösung betrachen, Roh geben sie euch kaum Lebenskraft zurück und spendieren wenn überhaupt nur sehr schwache Nebeneffekte. Gekochte Zutaten sind erheblich stärker, egal in welchem Belang. Ob ihr nur Herzen auffüllen, Ausdauer verlängern oder leise schleichen wollt, ist völlig egal. Mit einem gekochten Gericht geht es immer besser. Ideen für Rezepte findet ihr in unserer Lösung.

## Je schwächer ihr seid, desto öfter müsst ihr schleichen

Link startet mit wenig Lebenskraft und muss trotzdem gelegentlich starke Gegner bekämpfen. So lange er schwach ist, solltet ihr Frontalangriffe und unbedachte Aktionen vermeiden. Link kann schleichen, wenn ihr den linken Analogstick drückt. Je langsamer ihr euch bewegt, desto weniger Geräusche macht ihr während des Schleichens. Achtet dafür auf die Tonwelle links neben der kleinen Übersichtskarte. Um so mehr die Welle ausschlägt, desto lauter ist Link. Wollt ihr besonders leise schleichen, dann kocht euch ein Gericht, das euer Schleichen noch leiser macht. Schaut dazu in unsere Rezepteliste.

## Meidet bei Gewitter Waffen und Schilde mit Metallteilen

Wenn es blitzt und donnert, solltet ihr kein Metall an Waffen, Schild und Rüstung an euch herumtragen. Im Inventar könnt ihr sie behalten, aber ihr solltet sie nicht aktiv tragen, da ihr damit Blitze anzieht, die Link stark schaden.

## Jede Art von Schild ist ein guter Schild

Egal ob ein Stück Holz, ein Topfdeckel oder ein gut geschmieter Schild – so lange ihr euch nur damit verteidigen könnt, steigen eure Chancen im Kampf. Drückt im Kampf den ZL-Knopf, um den gewählten Schild vor euch zu tragen. Tut ihr das, wird sich Link obendrein auf den nächsten Gegner konzentrieren und ihm immer frontal gegenüberstehen. So behaltet ihr Gegner im Auge und könnt schnell zum Angriff übergehen.

## Teilt eure Ausdauer gut ein

Rennen, klettern, schwimmen und Abgründe herabzusegeln kostet euch Ausdauer. Ist der grüne Kreis aufgebraucht, haut es euch ihr wie einen Sack auf den Boden der Tatsachen. Geht also sparsam damit um und beginnt große Kletter- und Schwimmpassagen immer mit voll aufgefüllter Ausdauer. Schwimmt ihr schneller oder springt während ihr klettert, schrumpft die Leiste noch zügiger.

## Herzcontainer und Ausdauercontainer

Im Verlauf des Spiels sammelt ihr Symbole, die ihr bei Statuen gegen Herz-Container und Ausdauer-Container eintauschen könnt. Ihr braucht jeweils vier für eine Einheit. Gesteigerte Lebenskraft ist gerade am Anfang sehr wichtig, aber ihr solltet gelegentlich trotzdem zu einem Ausdauercontainer greifen, denn mit steigender Spieldauer werden eure Hindernisse immer größer. Ihr werdet zum Beispiel bei Bosskämpfen schwimmen – da sollte die Ausdauer am besten das kleinste Problem sein. Wir raten zur Formel „Auf drei Herzen folgt einmal Ausdauer“. Ihr habt später im Spiel die Gelegenheit, Herzen und Ausdauer gegeneinander auszutauschen

## Auf der Jagd mit dem Pferd

Sobald ihr ein Wildpferd zähmt, wird das Jagen von Wild und Geflügel erheblich leichter, denn ihr könnt Hirsche, Ziegenböcke und diverse Vögel einfach über den Haufen rennen, wenn ihr genug Tempo habt.

## Manchmal muss man Regen aussitzen

Wenn es regnet, sind einige Aktionen nur eingeschränkt oder gar nicht möglich. Zum Beispiel Feuer machen oder kochen. Ihr könnt bei Regen kein Feuer entfachen, keine Fackel tragen und nichts dergleichen. Zudem wird klettern extrem mühselig, da Link von glatten Flächen abrutscht und viel Ausdauer verliert. Statt euch ewig abzumühen, solltet ihr lieber das Wetter aussitzen. Vielleicht gibt es ja noch eine andere Tätigkeit, die ihr inzwischen angehen könnt.

## Vorsicht bei Nacht und Blutmond

Bei Nacht trefft ihr auf ganz andere Gegner als tagsüber. Skelette werden aus dem Boden steigen, die nur dann klein beigeben, wenn ihr deren Köpfe vernichtet. Tut ihr das nicht, vereinen sich Kopf und Körper wieder, um neu anzugreifen. Kann lästig werden, daher ist es manchmal sinnvoll, bis zum Morgen abzuwarten. An einem Lagerfeuer könnt ihr etwa direkt zum nächsten Morgen springen. Zudem steigt in regelmäßigen Abständen der Blutmond auf – in solchen Nächten sind Ganons Schergen zahlreicher und stärker als sonst. Seid also vorsichtig.

## Speichert regelmäßig

Das Spiel wird regelmäßig automatische Spielstände anlegen. Fünf davon verwaltet das Spiel selbst, einen könnt ihr manuell bestimmen. Nutzt diese Option oft und speichert manuell, denn wenn ihr sterbt, kehrt das Spiel zum letzten Speicherpunkt zurück. Liegt der einige Minuten zurück, müsst ihr alles, was nicht gespeichert wurde noch einmal machen. Gerade wenn ihr zuvor viel gekocht habt oder andere Errungenschaften erreicht habt, lohnt es sich auf jeden Fall zu speichern – das vermeidet unnötige Wutanfälle.

## Sucht zuerst den Turm

In jeder Teilregion von Hyrule befindet sich ein Turm. Aktiviert ihr ihn, erhaltet ihr eine detaillierte Karte des Gebiets samt eingezeichneten Straßen. Eine praktische Angelegenheit, da ihr euch leicht in Hyrule verlauft und die Topographie nicht selten vertrackt ist. Die Karte des Turms wird euch also immer nützlich sein.

## Nehmt jeden Schrein mit

Es gibt etliche Schreine. Alle sind relativ kurz und mit einem Rätsel versiegelt. Knackt ihr das Rätsel, erhaltet ihr ein Zeichen der Bewährung. Je vier davon könnt ihr beim Beten an einer Götterstatue gegen Herzcontainer oder Ausdauer tauschen, also lasst keinen Schrein aus und hört auf das Signal eures Shika Steins. Er piept, wenn ein Schrein in der Nähe ist (sofern ihr die Funktion nicht abschaltet).


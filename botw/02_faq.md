# Frequently Asked Questions

## Was tun, wenn mir die Waffen ausgehen?

Gehen euch die Waffen aus, weil sie alle zerbrochen sind, bleiben euch zwei Möglichkeiten: Entweder ihr arbeitet euch eine kurze Zeit mit dem Bomben-Modul durch, das euch unendlich viele Bomben gewährt oder ihr flüchtet zur nächsten Siedlung. Dort sollten Waffen an allen erdenklichen Ecken herumliegen, Im Notfall könnt ihr auf Zweige zurückgreifen, die bei Bäumen herumliegen.

Die besten Waffen erhaltet ihr allerdings durch das Besiegen starer gegenr – dafür bracht ihr natürlich erst einmal eine brauchbare Waffe, also macht eines nach dem anderen.

## Was tun, wenn alle Pfeile aufgebraucht sind?

Pfeile kann man entweder in Dorf-Shops erwerben, was nicht ganz billig ist oder man sammelt sie bei Gegnern auf. Bogenschützen zielen auf Link, treffen aber nicht immer. Deren gebrauchte Pfeile könnt ihr auflesen. Bögen erhaltet ihr von besiegten Bogenschützen oder als Belohnung in gewissen Schatzkisten.

## Wie zähme ich ein Pferd?

Pferde könnt ihr zähmen, wenn ihr das erste Kapitel des Spiels beendet und das Plateau verlassen habt. Es gibt unterschiedliche Pferderassen mit unterschiedlich hohem Temperament. Gefleckte Pferde sind in der Regel zahmer und können auch von Anfängern recht einfach eingefangen werden, sind dafür aber langsamer als einfarbige Exemplare, die wilder sind. Für die wilderen Pferde braucht ihr vor allem einen hohen Ausdauerwert.

Schleicht euch von hinten an das wilde Pferd eurer Wahl heran. Wenn ihr nahe genug dran seid, bekommt ihr die Option, per A-Knopf auf das Pferd zu springen. Das Tier wird sich – je nach temperament – eine Zeit lang wehren, ihr könnt es mit dem L-Knopf tätscheln, um es zu beruhigen. Reicht eure Ausdauer nicht dafür aus, wirft euch das Pferd ab.

Ist das Pferd zur Ruhe gekommen, so könnt ihr es einreiten. Es wird am Anfang nicht allen Befehlen gehorchen, sondern auch mal bockig sein und sich verweigern. Um es weiter zu zähmen, könnt ihr es tätscheln, sobald ihr aufsteigt oder wenn es etwas gut gemacht hat. Gefällt dies eurem Pferd, dann sehr ihr eine grafische Reaktion darauf. Ihr könnt das Pferd auch mit Rüben füttern, ihr müsst die Rüben nur in die Hand nehmen (ähnlich wie beim Kochen) und vor dem Pferd stehen. Es wird die Rübe schnappen und sich freuen.

Ihr solltet das Reittier zeitig zu einem Stall bringen und es dort registrieren lassen. Beim registrieren könnt ihr dem Pferd einen Namen geben. Es wird dadurch auch einen Sattel erhalten. Ein registriertes Pferd wird auf euer Pfeifen hören und zu euch kommen, wenn es den Pfiff hören kann (Steuerkreuz unten). Ist ein Hindernis im Weg oder die Distanz zu groß, wird das Pferd jedoch einfach stehen bleiben. Ihr könnt auf eurer Karte sehen, wo es sich befindet.

## Kann Link andere Tiere reiten?

Ja, Link kann eine Weile lang andere Tiere reiten, aber nicht unbedingt zähmen. Zum Beispiel Bären, die leichter an gewissen Hängen hochkommen als Pferde.

## Wie bekomme ich mehr Herzen?

Durch das Lösen der Rätsel in Schreinen erhält Link sogenannte **Zeichen der Bewährung**. Vier davon kann man bei einer Götterstatue gegen ein weiteres Herz oder mehr Ausdauer eintauschen. Weitere Herzcontainer erhält Link für das besiegen von gefährlichen Bossgegnern.\

## Wo finde ich Feen?

Feen sind praktische Begleiter. Sie verhindern Links Tod, wenn er alle Herzen einbüßt. Vorausgesetzt er hat mindestens eine Fee im Inventar. Feen findet man in der Nähe von Feenquellen oder im wilden Gras. Um sie im Gras zu finden, muss man es mit einer scharfen Klinge mähen, etwa einem Schwert. Taucht eine Fee auf, muss man sie schnell per Druck auf den A-Knopf einfangen. Es ist immer nützlich, ein paar Feen im Gepäck zu haben.


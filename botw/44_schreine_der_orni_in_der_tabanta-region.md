

# Schreine der Orni in der Tabanta-Region

Wie nicht anders zu erwarten, haben die meisten Schreine der Tabanta-Region einen Bezug zu Wind oder luftigen Höhen. In der folgenden Auflistung haben wir alle Lösungswege für euch verzeichnet.

## Ako-Vata-Schrein

Dieser Schrein befindet sich direkt im Dorf der Orni. Wenn ihr eintretet, entdeckt ihr viele Windräder und Turbinen in vier Reihen. Kümmert euch zuerst nicht darum, sondern lauft rechts neben euch die Treppe hinauf in einen Raum hinter dem Eingang. Ihr seht eine Schatzkiste in einer versperrten Kammer und rechts von euch ein Windrad. Etwas weiter rechts ist ein Kristallschalter auf der anderen Seite einer großen Schlucht. Schießt einen Pfeil auf den Kristallschalter, damit eine Turbine hochfährt. Das Windrad dreht sich und die Tür geht auf, In der Kiste liegt ein wertvoller Reaktorkern.

Nun zurück in den Hauptraum mit den vielen Windrädern. Euer Ziel ist, alle Turbinen so zu drehen, dass alle Windräder angetrieben werden. Die Turbinen dreht ihr durch einen Schlag auf den Kristallschalter, der vorne an jeder Turbine befestigt ist. Schaut auf unser Bild, für das Schema der richtigen Lösung.

## Vorida-No-Schrein

Ihr könnt diesen Schrein erst lösen, wenn ihr das Vah-Medo-Dungeon geknackt habt. Direkt südlich des Orni Dorfes (auf der anderen Seite der Schlucht) findet ihr eine runde Platte im Boden. Etwa gegen 12:30 Uhr beginnt sie zu leuchten, Wartet, bis der Schatten der Berge um 12:40 an der Platte vorüberzieht und schießt dann einen Feuerpfeil auf die Platte, damit der Schrein erscheint.

Im Inneren des Schreins ist eine Kanone zu sehen, davor ein Podest. Klettert an der Leiter auf die obere Seite des Podests, legt eine Bombe in das Rohr vor euch und zündet die Bombe, damit sie von der Kanone an die Wand vor euch katapultiert wird. Die Bombe wird eine leuchtende Platte freilegen, aber auch ein paar bewegliche Hindernisse losschicken, Ihr müsst nun eine weitere Bombe abschießen, aber zeitlich so geschickt abpassen, dass sie an den Hindernissen vorbeikommt und das leuchtende Ziel trifft. So öffnet sich der Weg zum Weisen, wo ihr das nächste Zeichen der Bewährung abholt. Ihr könnt noch eine Schatztruhe abstauben, wenn ihr hinter dem Weisen im zweiten Raum die gleiche Aufgabe noch einmal löst – allerdings mit einem deutlich knapperen Timing für die Bombe.

## Sha-Tawa-Schrein

Der Schrein am Flugplatz ist sehr einfach zu lösen. Nach dem betreten entdeckt ihr einen Auftrieb aus warmem Wind, Nutzt ihn mit dem Parasegel, um hinaufgeschleudert zu werden. Es folgen weitere Auftriebe – aber Vorsicht vor dem Nano-Wächter auf der linken Seite, den solltet ihr vorsichtshalber beseitigen. Nach mehreren Auftrieb-Säulen scheint der letzte nicht kräftig genug, um euch bis ganz nach oben zu tragen. Kein Problem. Schwebt einfach auf die Rückseite der großen Steinsäule vor euch, Auf der Rückseite ist ein kleiner Durchgang, der zu einem weiteren Aufwind führt, Er bringt euch direkt zum Weisen wo ihr das nächste Zeichen der Bewährung einsackt.

## Uuta-Do-Schrein

Auch dieser Schrein ist erst zugänglich, wenn ihr den Vah-Medoh-Titanen befreit habt. Sucht im Dorf der Orni in der Nähe des ## Dorf-Schreins nach Amali. Sie ist die Mutter fünf kleiner Orni-Kinder. Sie sucht ihr Kind Khil, das ihr westlich des Dorfes am Brüder-Felsen findet. Sprecht ihr mit Khil, so wird die Schrein Aufgabe „Die Brüder-Felsen“ fortgesetzt. Sie möchte nämlich ein Lied proben und benötigt dazu die Anwesenheit ihrer vier Schwestern. Alle vier sind im Dorf der Ori. Eine befindet sich ganz unten am Teich, eine im Geschäft auf der Spitze, eine auf dem Felsen in der Nähe ihrer Mutter (über ihr auf Höhe des Rüstungsladens). Die letzte ist an der Kochstelle. Damit sie diese verlässt, müsst ihr einen Knusperlachs für sie kochen. Alles was ihr dafür benötigt, findet ihr im Krämerladen der Orni, beziehungsweise den Fisch fangt ihr ganz unten am Fuß des Dorfes im Teich (Zutaten: Maxi-Lachs, Tabanta Weizen, Ziegenbutter).

Überreicht Genli den gekochten Maxi-Knusperlachs, dann habt ihr alle vier Geschwister beisammen. Nun sucht erneut den Brüderfelsen auf, wo alle fünf in Reihe stehen und singen. Sprecht mit der kleinen ganz rechts in der Reihe, um eine Krog-Fächer zu erhalten, Stellt euch nun hinter die Mädels auf die Platte am Boden, die zwischen mehreren Felsen steht. Die Felsen haben Finger, die Zahlen angeben. Blast nun mit dem Fächer Wind durch die Löcher der Felsen in folgender Reihenfolge: 4-5-3-1-2. Daraufhin fährt ein Schrein aus dem Boden.

Im Schrein klettert ihr eine sehr hohe Leiter hinauf. Oben aktiviert ihr den Kristallschalter, der eine Falltür öffnet. Schwebt mit dem Parasegel hinab vor den Käfig mit dem Weisen, Ihr braucht einen Schlüssel, um die Tür zu öffnen. Schwebt nun mithilfe der Turbinenwinde im Uhrzeigersinn von Plattform zu Plattform, bis ihr eine Schatztruhe findet, die den Schlüssen beinhaltet. Die Kunst in diesem Schrein ist, nicht zu lange zu schweben, sondern zeitig das Parasegel einzustecken, damit Link nicht gegen die Stachelwände fliegt. Bei einem Sprung müsst ihr zudem links um eine Stachel-Plattform herumsegeln. Aber nicht zu großzügig, sonst reicht der Wind nicht bis hinüber.

## Ka-Okyo-Schrein

Dieser Schrein ganz im Süd-Westen des Tabanta-Gebiets versteckt sich unter einer Steinplatte. Ihr könnt die Platte ganz einfach verschieben, wenn ihr ein oder zwei Oktorok-Ballons in die Hand nehmt und sie auf der Steinplatte fallenlasst. Die Ballons kleben fest, blasen sich auf und tragen die Platte davon.

Die Aufgabe im Schrein nennt sich Herr des Windes, und das ist wahrlich Programm. Daher erhaltet ihr gleich in der ersten Schatztruhe einen Krog-Fächer, den ihr dringend benötigt. Stellt euch an das Gattertor, dreht euch um und weht mit dem Krog-Fächer Wind auf das Windrad, damit sich das Gatter öffnet. Schreitet hindurch und weht die fliegende TNT-Kiste aus dem Weg. Nun weiter die Rampe entlang zu einem weiteren Windrad. Blast ihr Wind darauf, so taucht eine Plattform auf, die an vier Ballons hängt. Stellt euch drauf und fächert euch in die gewünschte Richtung. Hinter euch ist eine Schatztruhe mit 300 Rubinen, vor euch geht es weiter zum Saal mit dem Weisen, Dessen Kammer ist allerdings verschlossen, darum müsst ihr weiter in den nächsten Raum, wo ihr die südliche Wand mit einer Bombe sprengt.

Nun kommt der knifflige Teil. Hinter dem Gang steht ihr eine Plattform und ein Windrad dahinter. Stellt euch auf die Plattform und weht Wind per Krog-Fächer auf das Windrad. Dadurch wird Link in die Luft geschleudert. Landet auf der Plattform mit der Schatztruhe und schwebt dann nach rechts runter zum nächsten Katapult. Schleudert euch damit zur Säule rechts von euch. Klettert die Leiter an der Säule hinauf und legt eine Bombe auf den bröckligen Boden, Zündet sie aber erst dann, wenn ihr außer Reichweite seid, sonst fliegt ihr tief.

Ein Aufwind trägt euch auf eine höhere Plattform mit dem nächsten Windrad. Weht Wind darauf, um eine weitere Plattform mit Ballons zu generieren, Auf dieser Plattform schwebt ihr an den Stacheln vorbei Arbeitet nicht zu schnell, denn die Ballons platzen bei Berührung mit den Stacheln. Das folgende Windrad ist wieder der Auslöser für ein Katapult, das Link nach oben schleudert

Im Raum hinter euch generiert ihr durch das Windrad ein schwebendes TNT Fass, das ihr mit dem Krog-Fächer in die große Halle weht. Weht es weiter nach rechts in unmittelbare Nähe des großen bröckligen Steinturms. Ist es nah genug dran, dann lasst das Fass mit einem Feuer- oder Bombenpfeil hochgehen, damit der Turm zerbröselt und einen Aufwind freigibt. Lasst euch links von eurer Position am Katapult hochschleudern und nutzt den Aufwind nach oben zu einer Schatztruhe, die einen Schlüssel enthält. Habt ihr den Schlüssel eingesackt, dann lasst euch au die Plattform daneben fallen und segelt zur Öffnung an der Südwand. So erreicht ihr wieder den Saal, wo die Tür den Weg zum Weisen versperrt, Schließt sie mit dem Schlüssel au und holt euch das Zeichen der Bewährung ab.

## Teina-Kyoza-Schrein

Hier erwartet euch ein schwerer Nano-Wächter der vierten Generation. Arbeitet mit Block-Taktiken sowie perfektem Ausweichen, um ihn kalt zu machen. Außerdem solltet ihr ein paar Vorräte zum Heilen bereithalten.
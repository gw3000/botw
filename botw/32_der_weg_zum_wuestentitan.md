
# Der Weg zum Wüsten-Titan

Zurück bei der Anführerin der Gerudo stößt Link auf Begeisterung: Er hat sich
als vertrauenswürdig erwiesen, wobei er sich noch immer als Frau verkleiden
muss, auch wenn sein falsches Spiel längst durchschaut wurde. Nun kann er auf
die Unterstützung der Gerudos hoffen – und am allerwichtigsten auf die
Verwendung des Helmes, der ihn vor dem Einfluss des Donner-Titanen schützt.
Trotzdem darf er den Helm nicht selbst verwenden. Das bleibt Riju vorbehalten.

Ihr verabredet euch mit Riju bei einem Aussichtsturm südlich der Gerudo-Stadt.
Bevor ihr euch eine Sandrobbe schnappt (die ihr übrigens vor der Stadt umsonst
einfangen könnt, wenn ihr euch sehr langsam annähert), solltet ihr euch
entsprechend ausrüsten. Was ihr dringend benötigen werdet, sind Pfeile. Ihr
braucht einerseits Holzpfeile und andererseits Bombenpfeile. Zwanzig von jeder
Sorte, besser mehr, wobei euch Riju am Treffpunkt mit Bombenpfeilen versorgen
wird. Kauft lieber trotzdem ein paar Exemplare mehr, bevor ihr zum
Aussichtsturm aufbrecht. Vergesst auch nicht, Mahlzeiten zur Heilung und zur
Stärkung der Verteidigungskraft zu kochen, und zwar reichlich! Der Boss im
Titan gehört nämlich zu den schwersten des Spiels.

Am Aussichtsturm angelangt, klettert ihr die lange Leiter empor und sprecht mit
Riju. Sie spendiert 20 Bombenpfeile und erklärt euch, wie der Angriff auf das
riesige Wächter-Kamel ablaufen wird.

Ihr werdet gemeinsam auf Sandrobben zum Titanen düsen, wobei die Sicht immer
schlechter wird. Bleibt immer in Rijas Nähe, bis ihr beim Titanen angelangt
seid. Nun müsst ihr den Titanen beobachten. Droht er, einen Donnerschlag
loszulassen, so müsst ihr innerhalb des Kraftfelds bleiben, das Rija mit ihrem
Helm erzeugt. Andernfalls kostet euch der Angriff ungemein viel Lebenskraft.

In den Pausen zwischen den Attacken des Titanen könnt ihr das Kraftfeld
verlassen und nahe an die Beine des Giganten heran surfen. Packt eure
Bombenpfeile aus und zielt direkt auf die rosa und lila leuchtenden Hufe. Ihr
benötigt jeweils zwei gute Treffer direkt auf die Hufen des Titanen, damit sie
erlöschen. Euer Ziel ist es, alle Hufen zu deaktivieren, damit der Titan Vah
Naboris kurz stehenbleiben muss. Erst dann kann Link den Titanen betreten.

Wichtig ist das Timing. Behaltet immer den Rhythmus im Auge, in dem der Titan
zum Angriff ansetzt und haltet Ausschau nach Rija. Da sie sich nicht von selbst
näher an den Titanen herantraut, müsst ihr immer vorpreschen, Bombenpfeile
abfeuern und euch dann rechtzeitig zurückziehen. Eure Sandrobben vollziehen
inzwischen stets einen Slalom im Sand, was das Zielen erschweren kann, darum
ist es nützlich, mehr als genug Bombenpfeile dabei zu haben.

Kommt Vah Naboris zum Stehen, wird euch Rija verlassen. Willkommen im dritten
und kompliziertesten Dungeon von Zelda: Breath of the Wild.


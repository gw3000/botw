
# Nebenaufgaben in Akkala

Die Region von Akkala ist nicht sehr dicht bevölkert. Es gibt hier kein Dorf, nur zwei Ställe und das Akkala Institut ganz im Nordosten. Eine Siedlung soll später einmal entstehen, und zwar auf der kleinen Insel des östlichen Akkala-Sees. Der Ort nennt sich Taburasa und ist abseits eines Einzelnen Arbeiters völlig verlassen. Dennoch sind hier ein paar Aufgaben zu erfüllen, die ihr teilweise erst abschließen könnt, wenn ihr einige Kapitel weiter seid, denn ihr müsst viele andere Städte besuchen um Personen zu treffen und Zutaten zu kaufen.

## Auf Expansionskurs

Dies ist die Fortführung einer Aufgabe, die ihr in Hateno begonnen habt. Sucht im Osten des Akkala Landstichs die kleine Insel im See auf, die nur von der Ostseite aus zugänglich ist. Hier trefft ihr einen alten Bekannten, nämlich Dumsda. Er hat den Auftrag, an dieser Stelle ein Dorf zu errichten und bittet Link um Hilfe. Zuerst möchte er zehn Holzbündel von euch. Kein Problem, sprengt einfach ein paar der umstehenden Bäume mit Bomben um und sprengt dann deren Stamm. So kommt ihr an viele Holzbündel, ohne eine Waffe abzunutzen. Überreicht ihr ihm das Holz, wird er nach Gehilfen mit Muskelkraft fragen – am liebsten nach einem Goronen. Dessen Name muss aber laut Arbeitsvorschrift mit „da“ enden.

Im folgenden Kapitel werden wir der Hauptquest nach Goronia folgen, dort trefft ihr definitiv auf den einen oder anderen Goronen. An der Südmine auf dem Todesberg arbeitet ein Gorone namens Grada. Ihr könnt ihn erst abends nach seiner Schicht wegen des Anliegens ansprechen. Aber er wird nah Taburasa kommen, wenn ihr ihn fragt. Er nimmt seinen Sohn Pupunda mit.

Kehrt ihr nun zu Dumsda nach Taburasa zurück, sind die beiden Goronen schon am werkeln. Dumsda benötigt nun 20 Bündel Holz. Nach dem Beschaffen der Holzbündel braucht Dumsda jemanden, der seine Kleidung nähen kann. Er vermutet jemanden aus dem Gerudo-Stamm im Südwesten der Karte. Wenn ihr in späteren Kapiteln zu den Gerudos stoßt, könnt ihr nach jemanden Ausschau halten, deren Name auf „da“ endet. Die entsprechende Person findet ihr beim Wüstenbasar, noch vor der Gerudo-Stadt unter einem Mini-Zelt. Die Frau heißt Pauda. Ihr dürft sie nicht ansprechen, wenn ihr als Frau verkleidet seid.

Zurück in Taburasa geht das Spielchen weiter. Erst sind 30 Bündel Holz gefragt, dann jemand, der sich mit Vertrieb und Logistik auskennt – also ein Orni. Den passenden Orni mit „da“ am Namensende findet ihr im Dorf der Orni, zu dem ihr in späteren Kapiteln gelangt. Sprecht dort mit Peda, der nahe der Göttinnen Statue verweilt.

Erneut fragt Dumsda in Taburasa nach Holzbündeln. inzwischen sind es 50 Bündel. Danach fehlt noch ein Priester. Dumsda wünscht sich einen Zora-Priester. Reist also ins Zora-Dorf und sprecht mit Kapoda oberhalb des Schreins im Zora-Dorf. Abschließend kehrt ihr nach Taburasa zurück und sprecht noch einmal mit Dumsda. Passend zur Einweihung sollt ihr nach Hateno zurückkehren und seine Kollegen Landa und Torda einladen. In Taburasa kann nun die Zeremonie beginnen. Für die Erfüllung dieser langen Aufgabe kassiert ihr letztendlich drei Diamanten.

## Zeitvertreib für Neureiche

Sobald ihr die ersten drei Fremden nach Taburasa gebracht habt, stehen die ersten Häuser. Im Norden des Dorfes sitzt ein neureicher Fuzzi an der Klippe, der sehr abfällig mit Link spricht und ihm für eine Aufgabe 100 Rubine im Voraus zahlt. Er möchte, dass ihr euch in die Sümpfe begebt, die er geradezu beobachtet, und dort zwei Wächter zerstört.

Für diese Aufgabe benötigt ihr unbedingt antike Waffen oder - noch besser – antike Pfeile, die ihr den Ungetümen ins Auge feuern könnt. Dafür erhaltet ihr jede Menge antikes Baumaterial. Nicht nur von den zerstörten Wächtern, sondern auch von den vielen umliegenden inaktiven Wächtern, die vor sich hin rosten. Aber aufgepasst, sollte zu diesem Zeitpunkt der Blutmond aufkommen, dann werden zerstörte Wächter zurückgebracht. Rechnet also nicht zu knapp mit eurer Munition.

## Das kleine Kuchenmonster

Diese Aufgabe wird erst dann verfügbar, wenn Dumsda erste Teile der Stadt Taburasa gebaut hat (siehe „auf Expansionskurs). Wenn das Haus im Süden der Stadt steht, könnt ihr ab 21 Uhr ein Gespräch verfolgen, das auf der Rückseite durch ein halb geöffnetes Fenster zu belauschen ist (stellt euch an das Fenster und drückt den A-Knopf zum Lauschen.). Ruli und Hagi unterhalten sich darüber, dass ihre Tochter nur Kuchen isst.

Begebt euch nun zum Totenkopfteich ganz oben im Nord-Westen des Akkala-Gebiets, dort wo der Zuna-Kai-Schrein steht. Auf der zweiten kleinen Insel des Teichs steht ein Verkäufer mit einem riesigen Wagen. Er heißt Kilton und betreibt einen Monsterladen, den er demnächst in den Städten Hyrules eröffnet. Sprecht mit ihm, damit er seine Pläne wahr macht, Von nun an trefft ihr ihn am Rande von allen bekannten Dörfern. Teleportiert euch bei Nacht zu einem beliebigen Dorf und sucht außerhalb der Dorfgrenzen (irgendwo in der Nähe nach Kilton und seinem Laden. In Kakariko befindet er sich zum Beispiel südlich des nahegelegenen Sees hoch auf einem der Berge nahe Impas Haus.

Kilton hat eine eigene Währung für seinen Laden, die Monni genannt wird. Ihr könnt sie nur bei ihm bekommen und auch nur bei ihm verwenden, Monni erhaltet ihr im Tausch gegen Monster-Loot. Also gegen alles, was beim Töten von Monstern so abfällt. Tauscht etwas ein, bis ihr 9 Monni habt und erwerbt dafür eine Monster Essenz bei ihm.

Nun müsst ihr einen Monster-Kuchen an einem Kochtopf herstellen. Man nehme eine Monster Essenz, einmal Rohrzucker, einmal Tabanta-Weizen und einmal Ziegenbutter. Die Butter könnt ihr in Kakariko einkaufen, den Rest gibt in es in Gerudo Stadt. Händigt den Kuchen an Ruli in Taburasa aus und sprecht danach noch einmal mit ihr, um einen goldenen Rubin einzusacken.

## Verdächtiger Verkäufer

Ein Mann namens Holm, der am Ost-Stall von Akkala lebt, interessiert sich für ein Foto des seltsamen Monsterhändlers Kilton (siehe die Nebenaufgabe „das kleine Kuchenmonster“). Macht ein Foto von ihm und zeigt es Holm. Die Belohnung dafür ist ein silberner Rubin.

## Kleine, große Wünsche

Am Stall von Süd Akkala trefft ihr bei Tage Jana. Auf ihre Frage hin entgegnet ihr am Besten, dass ihr nur ein Reisender seid (oder ein Wanderer). Gebt ihr ein paar Bündel Rüstgras, um die Aufgabe fortzuführen. Rüstgras wächst an vielen Orten, Unter anderem im nahegelegenen Sumpf.

Nun sprecht mit der kleinen Schwester Janas. Sie heißt Glima und verrät euch ein Geheimnis. Sprecht nun noch einmal mit Jana. Woraufhin die echte Aufgabe zutage kommt: Ihr sollt Insekten beschaffen. Zitterlibellen findet ihr bei Regen in den steinigen Stellen der Gerudo-Wüste, oft auch auf Anhebungen. Glutlibellen findet ihr in der Ebene von Hyrule auf dem Weg zum Schloss. Frostlibellen findet ihr in der Nähe des Tabanta-Grenzlandes. Wenn ihr alle Libellen beisammen habt, dann kehrt zum Stall zurück, sprecht mit beiden und kassiert einen silbernen Rubin.

## Die große Fee von Akkala

Nur ein wenig Südlich des Stegs, der nach Taburasa führt, liegt ein kleines Wäldchen mit einem Tümpel. Direkt auf dem Tümpel schwimmt eine riesige Kastanie. Untersucht sie, um die Stimme der zweiten großen Fee zu vernehmen. Sie hat eine arge Söldnerseele und benötigt 500 Rubine, um wieder zu Kräften zu kommen. Im Ausgleich dafür kann sie eure Rüstung im Austausch gegen gewisse Materialien ein zweites Mal aufwerten.
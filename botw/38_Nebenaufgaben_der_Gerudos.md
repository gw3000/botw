
# Nebenaufgaben der Gerudos

Im Gebiet der Gerudos findet ihr nicht nur eine rekordverdächtige Anzahl an Schreinen, auch an Nebenaufgaben wird hier nicht gespart. Könnt ihr alle Aufgaben lösen, winkt als Belohnung der Donnerhelm.

## Pferdehandel

Solltet ihr von Osten her auf dem normalen Pfad Richtung Wüste laufen, dann findet ihr schon sehr bald einen verzweifelten Mann namens Zucch, der dringend ein Pferd sucht. Eines in „normaler Größe“, wie er sagt. Ihr habt zwei Möglichkeiten. Entweder ihr lauft weiter die Schlucht entlang zum Stall , holt eines von euren registrierten Pferden und schenkt es ihm. Möglichkeit zwei ist schneller erledit. Wenn ihr ein Stück ostwärts zurück den Pfad entlang geht, seht ihr zwei berittene Bokblins. Schießt sie mit Pfeilen von ihren Pferden und reitet eines davon zu Zucchi, der es mit Freuden in Empfang nimmt.

## Verschwunden im Tal

Am Stall der Schlucht steht Goman. Dessen vier Freunde stehen in der Umgebung des der Umetake Hochebene. Er möchte, dass du seine vier Freunde findest. Um sie zu finden, müsst ihr auf die Holzstege steigen, die über der Schlucht installiert sind. Dort sind sie in Kämpfe verwickelt.

## Spurte Dich

Ebenfalls am Stall der Schlucht zugegen ist Pirou, der von Link gerne 55 Spurtlinge hätte. Diese Pilze findert man an vielen Orten, vornehmlich aber beim Klettern auf Bergklippen und Wänden. Es geht um die lilafarbenen Pilze, die aus Felsen herauswachsen.

## Ein Mittel gegen die Hitze

Sprecht im Wüstenbasar mit Ghinemar. Er benötigt Kühlungs-Medizin. Könnt ihr ihm schnell an der Kochstelle zubereiten (Rezept: Frostflügler oder Frostlibelle + Echsalfoss-Horn + Echsalfoss-Sporn). Sollte euch ein Frostflügler fehlen, dann geht zurück zum Stall der Schlucht. Dort wandert der rotnäsige Terri in der Schlucht auf und ab. Terri solte Frostflügler dabei haben.

## Das Moldora Herz

Sprecht in der Gerudo-Stadt mit Malena, die nach Moldora-Herzen verlangt. Moldora ist ein gigantische Sandwurm, den ihr auf dem Weg zum To-Kayuu-Schrein. Schaut auf unserer Wüstenschrein-Seite nach, wie man den Sandwurm besiegt und wo man ihn findet (in der Nähe des besagten Schreins). Besiegt ihr den Wurm, gehören herzen zu den Schätzen, die ihr von ihm abgreift.

## Die achte Kriegerin

Sprecht in Gerudo_Stadt mit einem Mann namens Tuska, sobald ihr die Damen-Kleidung der Gerudos angelegt habt. Fragt nach seinen Schuhen, um diese Aufgabe zu starten.Er möchte ein Foto von einer Gerudo-Kriegerinnen-Statue, die ihr nördlich der Wüste auf der Spitze eines Berges findet (in der Gerudo-Hochebe). Dass passende Foto schießt ihr, wenn ihr euch auf die Hände der Statue stellt und ihren Körper fotografiert. Bringt Tuska dann das Foto.

## Das vergessene Schwert

Habt ihr „die achte Kriegerin“ abgeschlossen, dann sprecht Tuska noch einmal in Frauenkleidung an. Er bemerkt, dass der achten Kriegerin das Schwert fehlt. Ihr sollt es einzeln fotografieren, Schaut nach dem Schwert auf dem Gerudo-Gipfel in der Gerudo-Hochebene nördlich der Wüste. Fotografiert es und zeigt Tuska euer Foto.

## Edle Steine

Aisha in Gerudo-Stadt macht euch das Angebot, aus Edelsteinen wertvollen Schmuck zu fertigen. Dieser Schmuck schützt euch vor gewissen elementaren Einflüssen, ist also als Rüstungsteil zu gebrauchen. Allerdings braucht sie als Startkapital für ihren Handel zehn Feuersteine. Feuersteine findet ihr immer wieder in Erzvorkommen.

## Geheimnsikrämerei

Sucht auf der Rückseite des Kleidungsladens in Gerudo-Stadt nach einer Tür, bei der ein Passwort für den Einlass verlangt wird. Das Passwort kennt ihr nicht, darum stattet mal der Bar einen Besuch ab. Dort halten sich ein paar Frauen auf, die das Passwort kennen. Ihr erfahrt es aber nur, wenn ihr euch an das Fenster der bar stellt und lauscht. Mit diesem Passwort könnt ihr den Raum hinter dem Rüstungsladen betreten.

## Der Schatz der Gerudo

Wenn ihr Vah-Naboris im Rahmen der Haupthandlung bezwungen habt, sprecht noch einmal mit Rija, der Anführerin der Gerudos. Neben ihrem Thron steht der Donnerhelm. Schaut ihn euch genau an (A-Knopf drücken). Rija verspricht, ihn Link auszuleihen, wenn er sämtliche Nebenaufgaben der Gerudos knackt.

## Die Suche nach Baretta

Diese Aufgabe könnt ihr erst nach Abschluss des Vah-Naboris Dungeons angehen. Startet die Aufgabe „Der Schatz der Gerudo“ (siehe oben). Sprecht nun mit der Ausbilderin Lanja, die sich mit ein paar der Soldatinnen auf dem Hof bei Rijas Throngebäude befindet. Sie vermissen eine Kollegin namens Baretta. Um Baretta zu finden, kauft ihr euch zuerst hier in der Stadt einen Maxi-Durian (eine runde, leicht stachelige Frucht). Nun reist ihr in den äußersten Südwesten der Wüste (quer durch einen kleinen Sandsturm) zu einem großen Skelett. Hier findet ihr nicht nur einen Schrein und eine Feenquelle, sondern auch Baretta. Der ihr die Frucht gebt. Kehrt nun in die Stadt zurük und sprecht ein letztes Mal mit Lanja.

## Der Umweltsünder

Diese Aufgabe könnt ihr erst nach Abschluss des Vah-Naboris Dungeons angehen. Außerdem müsst ihr die nebenaufgabe „Der Schatz der Gerudo“ gestartet haben (siehe oben). Nun sucht im Norden der Gerudo-Stadt nach einer Frau namens Talia. Sie hat festgestellt, dass die Wasserquelle der Stadt unrein ist und sucht den Verantwortlichen.
Die Schuldige befindet sich im Westen der Stadt auf einem Hausdach, wo sie Frostmelonen verspeist. Bietet ihr als Alternative Wildbeeren an, damit sie aufhört. Waldbeeren findet ihr nördlich Wüste im Gerudo-Hochland. Zum Beispiel auf dem Felsvorsprung nördlich des Gerudo-Turms, aber auch an weiteren Stellen.

## Die Quelle der großen Fee

Hinter dem Fossil-Skelett ganz tief im Südwesten der Wüste (beim Hawa-Kai-Schrein) findet ihr eine weitere Quelle einer großen Fee. Auch sie möchte erst Rubine haben, bevor sie euch beim Aufrüsten eurer Kleidung hilft. Wie viel hängt davon ab, wie viele Feen ihr schon gefunden habt. Die erste Fee will 100, die zweite 500, die dritte 1000 und die letzte 10000 Rubine. Sprecht mit der Fee und gebt ihr das Geld, um diese Aufgabe zu finden und zugleich abzuschließen.


# Durch die Minen der Goronen

Jetzt, da ihr eure Anti-Feuer-Rüstung (Hose und Rüstung) durch zwei Feen-Upgrades total feuerfest gemacht habt, könnt ihr zurück nach Goronia. Euer erster Ansprechpartner ist der Gorone Bludo. Er erteilt euch die Hauptaufgabe „Feuertitan Vah Rudania“ . Allerdings benötigt ihr zuvor die Hilfe eines Goronen namens Yunobo, der noch in der Nordmine stecken soll.

Lauft den östlichen Dorfweg entlang am Schrein vorbei Vorsicht vor den Staubsauger-Oktoroks. Die könnt ihr einfach kalt machen, indem ihr eine Bombe werft, während sie saugen. Ist die Bombe in deren Schlund verschwunden, zündet sie – Bumm! Nun geht zu einem Goronen namens Drak. Auf das Geheiß, Link wäre vom Chef geschickt worden, lässt er euch weiter. Nun schaut euch die Kanone an, Ihr könnt ihre Ausrichtung ändern, wenn ihr den Schalter an der Kanone durch einen Schlag umlegt. Legt ihr nun eine Bombe in den Schacht und zündet sie, so feuert die Kanone. Mitunter auf die Lager der Feinde direkt vor euch. Ein Tipp zu den Kanonen:

Die Ausrichtung der Kanonen ist ein fließender Prozess. Ihr könnt also schon feuern, während sich die Kanone noch bewegt. Ihr müsst nicht warten, bis sie stehenbleibt oder gar einrastet.

Mithilfe der warmen Aufwinde und eures Parasegels könnt ihr nun von Plattform zu Plattform segeln, Nutzt die Kanone, um euch eurer Feinde zu entledigen und bewegt euch weiter auf die Nordmine zu. Oben angelangt seht ihr noch eine Kanone. Richtet sie aus, sodass sie sich dem Geröll an der Bergwand zuwendet und feuert eine Bombe im richtigen Moment ab. In der Höhle, die sich auftut, findet ihr Yunobo. Sprecht mit ihm, damit er wieder nach Goronia zurückkehrt. Dort solltet ihr auch hin. Sprecht mit dem Chef Bludo.

un, da ihr Yunobo geholt habt, sollt ihr zur Brücke von Eldin gehen. Der weg ist fast derselbe wie der zur Nordmine, nur geht ihr dann nicht zur ersten Kanone, sondern ihr lauft weiter um den Berg herum, bis ihr Yunobo wiederseht.

Der kleine Fratz kann mit einer Kanone abgeschossen werden, was ihr gleich mit der Kanone vor euch ausprobiert. Ihr müsst wieder eine Bombe in den Schacht legen, dann den Hebel umlegen und die Bombe zünden, wenn die Kanone Richtung Brücke zeigt. Wenn Yonobo die Brücke trifft, sinkt sie herab und der Kleine erfährt in einer Einspielung vom Erbe seines Vorfahren.

## Der Weg zum Titanen

Dank Yunobos Schild könnt ihr euch zum Titanen wagen, der das nächste Dungeon darstellt. Dummerweise versperren Wachen den Weg, sogenannte Prupellas schweben über dem Pfad. Wenn sie Link oder den kleinen Goronen entdecken, schlagen sie Alarm und machen euch die Hölle heiß.

An diesen Prupellas vorbeizukommen ist in der Regel trotzdem nicht all zu schwer. Ihr könnt einerseits ihre Bewegungsmuster beobachten und Yunobo immer erst dann folgen lassen, wenn die Luft rein ist (ihr könnt mit Steuerkreuz unten nach ihm pfeifen, wie nach einem Pferd).

Außerdem bleibt euch immer wieder die Möglichkeit, an den Felswänden hochzuklettern und die Wachen zu umgehen, Das wird dem kleinen Goronen natürlich nicht helfen, aber hoch oben auf Felsen findet ihr oft Steine und Metall-Kisten, die ihr mit dem Magnetmodul heben könnt, um sie mit Schwung auf die Prupellas zu katapultieren. Ihr müsst die Kisten nicht einmal mit dem Magneten loslassen, haut sie ihnen einfach mehrfach gezielt auf das Dach. Bomben nach unten werfen hilft übrigens auch sehr oft. So lange ihr über den Prupellas bleibt, kann euch nichts geschehen.

Im schlimmsten Fall könnt ihr eine Metallkiste zwischen zwei Felsstücke klemmen und euch darunter verstecken, sodass ihr nicht in einen der Scheinwerfer geratet. Ihr seht, es gibt diverse Lösungswege. Bleibt kreativ und klettert immer mal die Wände hoch, Neben den Standorten der Prupellas findet ihr immer wieder Waffen und Abwehrmittel.

Auf dem Weg den Todesberg hinauf werdet ihr immer wieder Kanonen finden. Schießt Yunobo mit diesen Kanonen direkt auf den großen Titanen in Echsenform, der am Rand des Vulkans steht. Trefft ihr ihn, dann wird er sich weiterbewegen. Folgt ihm und schießt Yunobo mit jeder Kanone wieder auf den Titanen, bis er über der Spitze des Vulkans zum Stehen kommt und letztendlich hinunterfällt. Nun kann Link ihm hinterher springen und landet im zweiten großen Dungeon, Vah Rudania.
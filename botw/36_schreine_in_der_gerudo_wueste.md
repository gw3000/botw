
# Schreine in der Gerudo-Wüste

Im Gerudo-Gebiet sind manche Schreine schwer zu erreichen oder mit viel
Kletterei verbunden. Beachtet unsere Karten und die genauen Beschreibungen zu
jedem Schrein.

## Jin-Yoh-Schrein

Hier benötigt ihr das Stasismodul und Pfeile. Stellt euch am Anfang so hin,
dass ihr die Kugel-Fassung auf der anderen Seite vor euch sieht. Wartet, bis
die Kugel auf dem Fließband direkt über der Kugel-Fassung ankommt und stopp sie
per Stasismodul. Schießt nun einen Pfeil auf die Kugel, damit sie nach der
Stasis nach vorne geschleudert wird und in der Fassung landet. Dadurch öffnet
sich rechter Hand eine Tür. Im folgenden Raum geht ihr ähnlich vor, nur dass
ihr vorher die beiden Nano-Wächter beseitigt (auch per Pfeilschuss). In Raum
drei schnappt ihr euch die Kugel, die rechts neben dem Eingang liegt und bringt
sie ans Ende des Fließbands, wobei ihr auf den Rhythmus der Laser-Strahlen
achten müsst. Legt die Kugel kurzzeitig auf der mittleren Plattform ab und
haltet den zweiten Laser mit dem Stasismodul an. Nehmt die Kugel wieder mit und
lauft zum dritten Laser. Wartet, bis der Laser-Strahl durch den großen Block
auf dem anderen Laufband unterbrochen wird, sodass ihr durchlaufen und die
Kugel in die Fassung legen könnt.

## Suma-Sama-Schrein

Dieser Schrein liegt im Südwesten der Wüste hoch oben auf einem Berg auf dem es
sehr kalt ist – ihr benötigt Winterkleidung oder Kältemedizin. Südlich der
Bergspitze findet ihr die Überreste einer Hütte, in der ein Tagebuch liegt.
Lest das Tagebuch, um die Schrein-Aufgabe „Das alte Tagebuch“ zu starten.
Danach nehmt ihr euch den übergroßen Schneeball direkt neben der Hütte und
schaut südlich des Gebäudes auf den schrägen Fels. Dort liegt eine Plattform,
die ihr nicht betreten könnt, weil ihr abrutschen würdet. Ihr müsst nun bis
etwa 16.30 Uhr abwarten und auf der Bergseite, wo auch die Hütte ist,
stehenbleiben, um einen Schatten direkt auf die Mitte der Platte zu werfen. Das
Spiel wertet leider sehr pingelig! Der Schatten der großen Schneeballs, den ihr
über dem Kopf haltet, muss die Mitte der Platte komplett ausfüllen, damit der
Schrein erscheint, daher müsst ihr leicht schräg von der Fläche stehen. Im
Schrein ist nichts weiter zu bewältigen, sodass ihr euch einfach das Zeichen
der Bewährung abholen könnt.

![Schreine in der Gerudo-Wüste](pics/8269eb5e84921bf1808900e5c782a3d3.jpg)

## Deira-Ma-Schrein

Inmitten einer Labyrinth-Festung im Südosten der Wüste liegt der
Deira-Ma-Schrein. Ihr könnt euch die Mühe machen, das Labyrinth normal zu
durchforsten. Dafür könnt ihr unsere Labyrinth-Karte verwenden. Aber es geht
deutlich einfacher: Teleportiert euch zum Suma-Sama-Schrein östlich vom
Labyrinth, springt dann vom Berg herab und gleitet mit dem Parasegel auf die
Oberseite des Labyrinths. Lauft auf der Mauer entlang, bis ihr leicht
südöstlich von der Position des Schreins dunkle Materie im Labyrinth-Gang unter
euch entdeckt. Klettert dort runter. Unterhalb der dunklen Materie ist der
Durchgang zum Schrein, in dem ihr nichts weiter erledigen müsst, aber eine neue
Rüstung erhaltet, nämlich Barbarenkleider, die eure Angriffskraft stärken.

![südliches Irrschloss, Gerudo-Wüste (Deira-Ma-Schrein)](pics/a8f8056475040b6a2bc08f846cb87c0d.jpg)

## Mih-Suh-Schrein

Inmitten des Südostteils der Wüste liegt der Mih-Su-Schrein. Ihr könnt ihn
leider nicht betreten, weil eine verdurstende Frau namens Pokki den Zugang
versperrt. Ihr benötigt einen "Vaai-will-Vooi“-Trank, den ihr nur in der
Gerudo-Stadt besorgen könnt (Schrein-Aufgabe „Ein Trank für müde Geister“).
Schaut in der nördlichen Seitenstraße von Gerudo-Stadt nach einem Haus mit
einem Tresen. Dahinter steht Furosa, die euch den Trank zubereitet, sofern ihr
Eis besorgt. In der Wüste keine einfache Sache, aber es gibt einen Eiskeller
nördlich der Stadt, ganz in der Nähe. Lauft einfach vom Haupteingang der Stadt
schnurstracks nach Norden durch die Ruinen. An deren Ende findet ihr einen
großen Felsen, der von einigen kleineren Felsen getragen wird. Darunter ist
eine Klapptür, die in den Untergrund führt.

Um dort Eis zu bekommen, müsst ihr tagsüber erscheinen und mit Ante sprechen.
Nun ist Eile angesagt, denn das Eis schmilzt euch buchstäblich aus den Händen.
Bringt den Eisblock an den Eingang der Ruinen, wo Furosa das Eis entgegennehmen
wird. Begegnungen mit Gegnern solltet ihr möglichst schnell abhandeln (etwa mit
Eis-Pfeilen). Kehrt anschließend zum Schrein zurück und erzählt der jungen
Dame, dass ihr Drink auf sie wartet, worauf sie in die Stadt eilt. Ihr könnt
den Schrein betreten und euch ein Zeichen abholen.

## Keh-Noi-Schrein

Hier bekommt ihr es erstmals mit Stromleitungen zu tun, die ihr mithilfe von
metallenen Gegenständen schließen könnt. Klappt übrigens auch mit euren
Metall-Waffen, wenn ihr sie ablegt. Stromleitungen erkennt ihr anhand ihrer
Leitungs-Fassungen an der Wand oder im Boden. Stehen sie unter Strom, leuchten
sie grünlich.

Eine schnelle Demonstration bekommt ihr durch die erste Kugel, die vor euch
liegt. Hebt sie und legt sie in die rechte Fassung, um die Elektrode zu
speisen. Daraufhin öffnet sich die Tür in der Mitte.

Der folgende Raum ist horizontal gestreckt. Rechts wartet ein Nanowächter auf
euch. Dahinter hängt eine Kugel an einem Seil. Durchschießt das Seil mit einem
Pfeil, damit die Kugel fällt. Nun tragt die Kugel auf die linke Seite und legt
sie in die Elektrodenfassung, damit das Laufband aktiviert wird und der große
Klotz vor euch wegtransportiert wird. Er bildet eine Brücke zu einer
Schatztruhe.

Nun lauft die Treppe der mittleren Gangs hinauf, macht den Nano-Wächter platt
und schreitet in den letzten Raum. Hier nehmt ihr per Magnetmodul einen
Metallwürfel aus dem linken Wasserbecken und setzt ihn in die Mitte des rechten
Wasserbeckens, um den Stromkreis zu schließen. Nun könnt ihr zum Weisen, um ein
Zeichen der Bewährung abzuholen.

## Dako-Tawa-Schrein

Folgt dem Flur, besiegt den Nano-Wächter und schaut im kommenden Raum nach
rechts. Da fährt eine Plattform über einen Abgrund, direkt neben einer
gespeisten Stromleitung. Der Strom läuft dort nur dann, wenn es eine Verbindung
zum Strom-Würfel gibt, der am Ende des Raums liegt. Fahrt mit der Plattform da
hin, nehmt den Würfel per Magnetmodul und haltet ihn immer in der Nähe der
Stromleitung. Tut ihr das nicht, bleibt die Plattform stehen und ihr kommt
nicht zurück. Also tragt den Würfel stets per Magnetmodul auf der Seite der
Stromleitung und schleift ihn an der Leitung entlang.

Nun aufgepasst: Auf der anderen Seite angekommen, geht ihr rückwärts die Rampe
hoch und bringt den Würfel hinter euch mit. Steht ihr auf der Plattform am
Abgrund, dann legt den Würfel direkt an die Elektrode, damit die Plattform nach
oben fährt. Der Grund für den Rückwärtslauf ist einfach: Legt ihr den Würfel zu
früh ab, könnte die Plattform ohne euch losfahren. An sich kein Problem, es sei
denn der Würfel fällt aus irgend einem Grund auf der Reise hinunter– dann
müsstet ihr den Schrein neu starten, weil ihr nicht mehr an die Plattform kämt,
und sie sich nicht mehr zu euch bewegt. Durch das rückwärts Laufen, eliminiert
ihr jedes Risiko.

Oben angelangt, nehmt ihr den Würfel mit, und legt ihn vor dem Zaun ab.
Bekämpft erst die Nano-Wächter vor euch und holt euch dann den Würfel wieder.
Achtung, haltet den Würfel nicht zu nah an den Boden, sonst wird er elektrisch
geladen! Nun schiebt ihr den Würfel per Magnetmodul vor den großen Metallblock
an der Schiene. Dank des kleineren Würfels und seiner Ladung wird der Block
nach hinten versetzt und ihr könnt durch den schmalen Gang laufen.

Schnappt euch abermals den kleinen Würfel, geht mit ihm die Rampe hoch und
fahrt mit dem Aufzug nach oben, wo ihr den Würfel auf die kleine runde Fläche
stellt, damit eine Plattform in der Ferne losfährt. Stellt euch auf diese
Plattform, sobald sie angekommen ist, und haltet euer Magnetmodul bereit. Ihr
nähert euch einem großen Block, den ihr mit dem Magnetmodul nach vorne schiebt,
während die bewegliche Plattform, auf der ihr steht, weiterfährt. Schiebt den
Block ganz nach hinten. Ihr landet zum Schluss direkt vor dem Weisen. Wollt ihr
die Schatztruhe hinter dem großen Block von eben plündern, fahrt von hier aus
zurück und schiebt den Block wieder auf die alte Position. Andernfalls holt
euch einfach das Zeichen der Bewährung ab.

Guko-Chise Schrein

Dies ist der Schrein direkt an der Gerudo-Stadt. Die Aufgabe besteht hier in
dem Verbinden elektrischer Leitungen auf dem Boden. Ihr müsst die Lücken an den
Knoten schließen. Eure Hilfsmittel sind große Metallquader und Fässer, die ihr
mit dem Magnetmodul herumtragt und zwischen die Knoten, die am beiden
aufgezeichnet sind, legt. Wobei ihr einige dieser Helferlein erst aus den
Käfigen holen müsst. Schaut dazu auf unsere Bilder.

TIPP: Weiter unten beschreiben wir den offiziellen Lösungsweg, aber wenn ihr
viele Metallwaffen besitzt, könnt ihr das ziel leichter erreichen, indem ihr
eure Metallwaffen zwischen den Knoten ablegt. Auch so könnt ihr den Stromfluss
garantieren. Vergesst sie später nur nicht wieder einzusammeln.

Die ersten Schritte unternehmt ihr auf der linken Hälfte des Raumes, also links
vom Eingangssockel. Nehmt zuerst das Fass und stellt es links vom Eingang an
das Ende der langen grünen Leitung auf dem Boden. Dann schnappt euch den
Metallblock, mit dem ihr Die Lücke dahinter schließt, damit das Gatter ganz
links aufgeht.

![Guko-Chise Schrein](pics/e04624ad302ebeefa07347b14cc433e9.jpg)

Holt dort das Fass aus seinem Gefängnis und setzt es links neben dem
Eingangssockel an die Unterbrechung bei der kleineren grünen Leitung. Entfernt
nun das erste Fass von seiner Stelle und schließt die Lücke ganz außen links.
Dann rückt ihr auch den Block ein ganzes Stück nach links, um die Stromleitung
zum zweiten verschlossenen Raum zu legen, der sich daraufhin öffnet. Öffnet die
Schatzkiste, die sich darin befindet und holt die leere Truhe aus dem kleinen
Raum heraus.

Die nächsten Schritte finden auf der rechten Hälfte des Raumes statt. Dort
verteilt ihr erneut alle Fässer auf die Lücken zwischen den Leitungen und den
großen Metallquader stellt ihr zwischen die beiden festen Blöcke. So befreit
ihr einen zweiten Metallquader aus seinem Gefängnis. Nun müsst ihr nur noch
beide großen Quader jeweils auf eine Seite neben dem verschlossenen Gatter
stellen, das zum Weisen führt, damit sich das Gatter öffnet.

## Rayku-Uro-Schrein

Der Rayko-Uro-Schrein befindet sich nur wenige Meter südlich der Gerudo-Stadt.
Um ihn betreten zu dürfen, müsst ihr zuerst an einem Sandrobben-Wettrennen
teilnehmen. Gewinnt ihr es, winkt als Preis eine Kugel. Setzt sie in die
Fassung, um diesen Schrein zu öffnen. Im Inneren des Schreins ist nichts weiter
zu tun.

## To-Kayuu-Schrein

Dieser Schrein liegt im Westen der Wüste und wäre ganz einfach
heraufzubeschwören, wenn in dessen unmittelbarer Umgebung nicht ein fieser
Gegner hausen würde – der Sandwurm Moldora! Eigentlich müsst ihr nur westlich
der Gerudo-Stadt vier Fackeln anzünden, die auf Stein-Plattformen stehen. Nur
ist das riskant, wenn ständig der große Moldora umher zieht. Dieses gefährliche
Tier gräbt sich durch den Boden und ist in der Lage, Link sämtliche Lebenskraft
zu nehmen, wenn er ein mal richtig ausholt. Ihr müsst ihn daher unbedingt platt
machen, bevor ihr die Fackeln anzündet.

![Moldora](pics/e4bb8e48b2b5f8c47cd5c781df1d8efa.jpg)

Stellt euch irgendwo auf einen Felsen – möglichst weit nach oben. Beobachtet
den Sandwurm, wie er seine Kreise zieht. Wenn er in der Nähe ist, zücken ihr
eine Bombe und hofft, dass er sie bemerkt. Er wird sie für Link halten, darauf
zu rasen und sie verschlucken. Zündet die Bombe dann, wenn der Wurm aus dem
Sand springt, um die Bombe zu schlucken. Der betäubte Wurm wird einige Sekunden
am Boden liegen – euer Zeichen, wild drauf los zu schlagen. Lasst euch aber
nicht zu lange Zeit, denn der Wurm erwacht wieder. Bekommt er Link in die
Fänge, verliert ihr massig Herzen! Also lieber schnell auf einen Felsen
flüchten.

Wiederholt das Spielchen, bis der Wurm besiegt ist. Zur Belohnung erhaltet ihr
seltenen Monster-Loot und einige Schatzkisten. Der Wurm ist nicht permanent
besiegt, kommt ihr irgendwann wieder, ist er auch wieder da. Zündet nun die
vier Fackeln an und betretet den Schrein, in dem weiter nichts zu tun ist.

## Hawa-Kai-Schrein

Wenn ihr euch von der Gerudo-Stadt aus ein Sandrobbe schnappt und in den
Südwesten der Wüste reist, erreicht ihr ein Gebiet, in dem ihr weder etwas
sehen noch die Karte aufschlagen könnt, denn ein Sandsturm nimmt euch die
Orientierung. Versucht dennoch möglichst genau auf die Südwest-Ecke der Karte
zuzuhalten. Dort findet ihr ein gigantisches Skelett, ein Fossil aus alten
Zeiten. Fotografiert es, ihr braucht das Bild für eine Mission. Sowohl ein
Schrein als auch ein Feenbrunnen nutzen dieses Fossil als Dach.

Im Inneren des Schreins findet ihr zwei Metallblöcke, zwei kleine Sockel, einen
Elektro-Kristall und eine Elektrode. Hebt einen der Metallquader auf den Sockel
und stellt ihn so nah an den gelben Elektro-Kristall, dass er aufblitzt. Nun
hebt auch den zweiten Quader per Magnetmodul an und setzt ihn daneben, sodass
er nahe an die Elektrode herankommt und der Strom durchfließt. Die Tür zum
folgenden Raum wird sich öffnen.

Im zweiten Raum hängt eine Metallkugel über einer Kette an einem
Elektro-Kristall. Damit ihr die Kugel zur gegenüberliegenden Elektrode heben
könnt (per Magnetmodul), müsst ihr die gewickelte Kette erst einmal um die
Säule bewegen, sonst ist die Reichweite der Kette zu kurz. Habt ihr die
Elektrode mit Strom versorgt, wird ein kleiner Aufzug aktiv, mit dem ihr den
oberen Steg erreicht.

Der Steg besteht aus zwei Teilen, einem festen und einem beweglichen, auf dem
drei Elektroden stehen. Nehmt euch die zweite, frei bewegliche Elektro-Kugel
auf dem Steg per Magnetmodul und lauft über den Steg – allerdings so, dass die
Kugel nie auch nur in die Nähe der Elektroden kommt, sonst werdet ihr ins
Wasser geschleudert. Genau das macht euch allerdings die Arbeit mit den drei
Nano-Wächtern, die auf der kommenden Plattform stehen etwas leichter. Ihr könnt
sie von der Plattform schleudern, indem ihr die Elektroden in der Nähe
aktiviert. Haltet einfach die Kugel dran, damit der Boden kippt.

Legt danach die Kugel in die Fassung. Ihr werdet sehen, dass sich ein Zahnrad
dreht, aber die Tür zum Weisen verschlossen bleibt. Ihr benötigt noch ein
Zahnrad, das im Raum zu euer Rechten liegt. Leider kommt ihr noch nicht dran.

Stellt euch auf den Schalter am Rand der Plattform, damit die Balken mit den
Steinen vor euch kippen. Nun ist Timing gefragt. Ihr müsst die Steine mithilfe
des Stasis-Moduls so aufhalten, dass die vorderen Steine beim Kippen
stehenbleiben. Nun nehmt das Magnetmodul und schiebt den Metallblock
dazwischen. Euer Ziel ist, die vier Elektroden am hinteren Ende in eine Reihe
zu bekommen, damit der Strom fließt. Schaut auf unseren Screenshot!

![Hawa-Kai-Schrein](pics/410a5e00004034aa270dbeb8b600c240.jpg)

Wenn sich das Gatter öffnet, nehmt ihr das Zahnrad darin per Magnetmodul mit
und setzt es im Nebenraum über der Tür auf die Fassung zwischen die anderen
beiden Zahnräder. So öffnet sich die Tür zum Weisen.

## Kima-Zuusu-Schrein

Diesen Schrein findet ihr nur nach einer kleinen Odyssee durch den Wüstensturm
im Nordwesten der Wüste. Ihr könnt hier nicht navigieren, da euer Shiekah-Stein
im Sturm unbrauchbar wird. Achtet stattdessen auf die Statuen, die in der Wüste
herumstehen und mit ihrem Schwert in eine Richtung zeigen. Folgt stets der
Richtung die deren Schwerter anzeigen. So gelangt ihr von Statue zu Statue und
zuletzt auch zum Schrein, Wundert euch nicht, dass euer Schrein-Warnsystem
still bleibt.

Im Schrein nehmt ihr das Magnetmodul zur Hand, stellt euch an eine der beiden
Seiten (möglichst nah an die Kugel) und transportiert die Metallkugel, die
geschleudert wird, über den großen Zaun auf der linken Seite. So kassiert ihr
einen Säbel in der Schatzkiste. Wollt ihr zum Weisen, dann stellt die Kugel
schlicht links vom Altar des Weisen nahe an die halbdurchsichtige Elektrode.
Das genügt schon, um das Tor zu öffnen.

![Kima-Zuusu-Schrein](pics/3b99140745654d814be385b77194df0d.jpg)

## Kyoshi-Oh-Schrein

Weit im Osten der Wüste, direkt vor dem Gebirge im Osten, findet ihr mehrere
gigantische Kriegerstatuen, die im Kreis zueinander stehen. Zu ihren Füßen sind
Fassungen für Kugeln aufgebaut. Die passenden Kugeln liegen hier überall im
Umkreis herum und tragen Symbole, die ihre Zugehörigkeit anzeigen. Eine
Ausnahme besteht, denn eine Kugel liegt auf den Händen einer der riesigen
Statuen. Ihr müsst auf eine der nördlich gelegenen Statuen klettern. Jede der
Kugeln muss der passenden Statue zugewiesen und in die davor liegende Fassung
gelegt werden. Das Symbol der Statue ist manchmal schwer erkennbar, weil es
weit oben angebracht ist. Schaut auf unsere Karte oder nutzt die Zoom /
Fernglas-Funktion eures Shiekah-Steins. Ihr könnt an ihnen aber auch empor
klettern.

Ein paar Seiten zuvor haben wir euch beschrieben, wie ihr auf den Turm der Gerudo-Hochebene steigen könnt. Von hier aus erreicht ihr einige Schreine sehr leicht. Den ersten Schrein dieser Seite könnt ihr aber auch schon finden, wenn ihr auf dem Weg in die Wüste durch die Schlucht neben dem Start-Plateau stapft.

## Juni-Shi-Schein

Auf dem Weg in die Wüste könnt ihr an eine der Klippen eine Gruppe Goronen antreffen, die vor zwei großen Heizflächen stehen. Wollt ihr den Schrein betreten, so müsst ihr die Hitze auf beiden Flächen länger ertragen als die Goronen. Bei der ersten Heizfläche dürft ihr noch Anti-Feuer-Kleidung tragen, bei der zweiten müsst ihr euch mit einer Brandschutz-Medizin behelfen (Rezept: Zwei Löschechsen, ein Bokblin Horn).

## Sasa-Kai-Schrein

Segelt von der Spitze des Gerudo-Turms nach Südosten zu einer kleinen runden Plattform auf einem Felsvorsprung. Dreht euch zum Turm und wartet, bis die Sonne direkt über den Turm gewandert ist. Nun schießt einen Pfeil Richtung Sonne, wenn die Sonne direkt über dem Turm steht. Daraufhin fährt ein Schrein hoch.

Bezwingt im Inneren einen mittelschweren Nano-Wächter. Wenn ihr den perfekten Block und den Salto-Konter beherrscht, sollte dies kein größeres Problem mehr darstellen.

![weitere Schreine in der Gerudowüste](pics/698717c94fa488752df6444c404ab0ad.jpg)

## Shi-Jito-Schrein

Dieser Schrein erscheint erst, wenn ihr gegen Abend einen Leuchtstein auf die orange leuchtende Plattform gelegt habt. Ist auch eine ganz einfache Angelegenheit, da die umliegenden Erzadern Leuchtsteine beinhalten.

Im Inneren entdeckt ihr linker Hand eine Holzbarriere, die ihr mit einer Bombe sprengt. Geht in den Raum dahinter. Ihr seht eine Säule, die weit oben ein Gitter trägt. Auf der Rückseite der Säule ist eine Schatztruhe mit 100 Rubinen drin. Vor der Säule ist ein Stein-Katapult. Legt eine quadratische Bombe direkt an die Säule auf das Katapult und lasst die Bombe hinaufschleudern. Wenn sie ihren höchsten Punkt am Gitter erreicht, sprengt ihr sie, um den Schalter im Gitter zu aktivieren. Daraufhin öffnet sich eine Tür auf der rechten Seite des Eingangsraums.

Im zweiten Raum geht ihr ähnlich vor, nur müsst ihr das Katapult dieses Mal selbst bedienen und zwar über einen Kristallschalter auf der Rückseite der Säule. Dadurch seht ihr nicht, wie hoch eure Bombe fliegt und müsst nach Gefühl sprengen. Erwischt ihr den Schalter, öffnet sich die zentrale Tür im Eingangsraum des Schreins. Lauft hindurch in einen weiteren Raum.

Zwei Katapulte stehen hier, eines in jeder Ecke. In der Mitte wartet ein Kristallschalter, den ihr nicht mit Pfeilen oder anderen Waffen erreicht. Geht zu einem der zwei schrägen Katapulte und legt eine eckige, sowie eine runde Bombe darauf. Nicht nacheinander, sondern gleichzeitig, ohne sie zu sprengen. Nun stellt euch auf eine der Erhöhungen neben der Eingangstür.

Die Katapulte werden beide Bomben hin und her schleudern. Ihr könnt die jeweilige Bombe zünden, wenn ihr das zugehörige Modul ausgewählt habt. Sprengt also erst die eckige Bombe, sobald sie über dem Kristallschalter ist. Ihr werdet auf der Erhöhung hochgefahren – nur ist die Tür zum Weisen jetzt versperrt. Stellt euch direkt vor die Tür zum Weisen und sprengt mit dem anderen Bombenmodul die runde Bombe, sobald sie über dem Kristallschalter ist. So kommt ihr durch und könnt euch das Zeichen der Bewährung abholen.

## Kui-Takka Schrein

Bevor ihr ihn betreten könnt, müsst ihr diesen Schrein von seiner festen Eis-Hülle befreien. Legt dazu einfach ein paar Lagerfeuer um den Schrein herum. Funktioniert ganz einfach: Nehmt aus dem Menü einen Feuerstein und ein Holzbündel in die Hand. Lasst beides direkt vor Link Fallen und schlagt dann mit einer Metallwaffe über Holz und Feuerstein (vorzugsweise mit einem Schwert). So entzündet ihr das Holz. Das Eis braucht eine Weile, bis es komplett geschmolzen ist, aber danach könnt ihr den Schrien betreten.

Im Inneren schnappt ihr euch den Eisblock und lauft am Rand die Plattform entlang. Euer Ziel lautet, mit ein wenig Eis beim Weisen anzukommen, damit er euch die Tür öffnet. Ist bei den Feuerstrahlen hier gar nicht so leicht.

An den ersten könnt ihr noch prima vorbei, doch an den Wandstrahlern wird es schwer. Durch die ersten müsst ihr einfach durch. Seid ihr schnell genug, schrumpft der Eisblock zwar, aber es bleibt noch genug übrig. Schreitet weiter über einen Metallblock zur nächsten Feuerwand. Legt den Eisblock ab, schnappt euch per Magnetmodul den Metallblock von eben, der in der Lava liegt, und stellt ihn als Blockade an die Feuerwand, bevor ihr mit dem Stück Eis vorbei geht.

Die folgende Feuerwand ist kniffeliger, da die Hitze von oben strahlt. Legt den Eiswürfel in der Bitte des Ganges ab und schnappt euch erneut den Metallblock von eben per Magnetmodul. Haltet den Block hoch, sodass ein paar der Feuerstrahlen aufgehalten werden und lauft nun gegen den Eiswürfel. Link wird das Eis sozusagen „kicken“ oder zumindest vor sich her schieben, sodass es die Feuerwand passiert. Dahinter erreicht ihr bereits den Weisen, wenn ihr ihm den Eiswürfel zeigt.

## Kima-Kosasa-Schrein

Diesen Schrien erreicht ihr ohne Probleme. Mal abgesehen von der Kälte in dieser Gegend. Im Inneren erwartet euch eine schwierige Kraftprobe gegen einen sehr aggressiven Nano-Wächter. Nehmt viel Proviant mit und vertraut auf eure Block – und Konter-Fertigkeiten.

## Kiha-U-Schrein

Schaut auf unsere Karte weiter oben. Wenn ihr den verzeichneten Standpunkt erreicht habt, entdeckt ihr ein riesiges kreisrundes Symbol an der Felswand, das Blitze zeigt. Ihr müsst genau in der Mitte des Symbols eine Platte treffen, und zwar mit Elektro-Pfeilen. Klettert dafür auf die hohe Felswand links davon und schwebt mit dem Parasegel herab. In der Luft holt ihr den Bogen Heraus und schießt genau in die Mitte des Symbols an der Felswand. Bei einem korrekten Treffer wird der Schrein aus dem Boden steigen, Im Inneren wartet keine weitere Aufgabe.
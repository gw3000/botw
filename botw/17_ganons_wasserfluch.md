# Ganons Wasserfluch

Ganons Wasserfluch wird in mehreren Phasen agieren, je weiter ihr seine
Lebenskraft reduziert, desto aggressiver wird er. Zu Anfang ist es ratsam,
relativ nahe an ihn heranzutreten und mit einer großen Waffe zu bearbeiten.
Ganons Wasserfluch erleidet viel Schaden, wenn ihr ihn am Kopf trefft,
möglichst am blau leuchtenden Auge an der Stirn. Weicht seinen Angriffen so gut
es geht aus. Geht er mal zu Boden, dann verpasst ihm ordentlich was mit einer
großen Waffe.

![Ganons Wasserfluch](pics/89d059c9af137454a7d834118167bc53.jpg)

Hat er etwa die Hälfte seiner Lebenskraft eingebüßt, wird die ganze Nummer
etwas schwerer. Er hebt den Wasserspiegel an und lässt Link nur vier
Plattformen zum Stehen. Sucht euch eine aus und greift nun zum Bogen. Schießt
ihm Eis- oder Feuer-Pfeile direkt ins Auge. Notfalls genügen auch normale
Pfeile, die aber weniger Schaden anrichten und weniger Schadensradius haben.
Trefft ihr ihn, wird er irgendwann seine Kraft verlieren und zu Boden gehen.
Schwimmt so schnell es geht zu ihm und haut ihm mit einer starken Waffe über
die Rübe. Schon bald erwacht er erneut.

Er wird sich zurückziehen und Eisklötze aus der Ferne schießen – gleich fünf
auf einmal. Wollt ihr schnell agieren, dann schießt einfach den mittleren
Eisblock mit einem Pfeil ab und schießt sofort einen weiteren Pfeil auf
gleicher Höhe hinterher. Trefft ihr ihn dadurch wieder am Kopf, werden die vier
anderen Eisklötze verschwinden und der Zyklus beginnt von neuem. Seid ihr zu
spät dran, rettet euch ins Wasser hinter einer Plattform. Die Eisblöcke sollten
auf der Fläche vor euch brechen.

Habt ihr Ganons Wasserfluch besiegt, wird Link mit zwei Geschenken belohnt. Er
erhält einen Herzcontainer und eine passive Spezialkraft, nämlich Miphas Gebet.
Diese Spezialkraft braucht eine ganze Weile, um sich aufzuladen. Ist sie aber
voll, wird Link automatisch mit zusätzlichen Herzen geheilt, wenn er zu sterben
droht. Sehr praktisch!

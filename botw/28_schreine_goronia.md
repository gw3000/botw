
# Schreine bei den Goronen im Eldin-Gebiet

## Moa-Kishito-Schrein

In diesem Schrein, der direkt am Stall am Berge zu finden ist, geht es um reines Timing mit dem Stasis-Modul. Ihr müsst die Kugeln, die auf den Rampen herunterrollen per Stasis anhalten, noch bevor sie die Rampe berühren – also noch im Fall aus dem Schacht. Danach rennt ihr die Rampe hoch zum Steg. Tut ihr das gleiche auf der rechten Seite noch einmal, winkt eine Schatzkiste, links geht es zum Weisen und dem Zeichen der Bewährung. Allerdings ist die letzte Rampe etwas trickreicher.

Hier müsst ihr wirklich extrem früh ansetzen, weil mehrere Kugeln mit einem Schlag anrollen. Haltet eine der Kugeln sehr früh im Fall auf und dann spurtet was das Zeug hält, denn die nachfolgenden Kugeln werden die angehaltene Kugel nach dem Abflauen der Stasis zusätzlich beschleunigen. Zum Zielen solltet ihr links oder rechts durch die Gitter schauen, Stellt euch nicht direkt vor die Rampe, sonst werdet ihr überrollt.

## Sadarj-Schrein

Die Unteraufgabe dieses Schreins trägt den Namen „Die Macht des Feuers“, und das ist wahrlich Programm. Zündet einfach das Gestrüpp an der Wand vor euch an. Entweder mit einem Pfeil, den ihr an der Fackel vorher entzündet oder mit einem vorgefertigten Feuerpfeil. Hinter der Wand wartet ein Nano-Wächter, der euch aber kaum etwas entgegensetzen kann. Der Flur führt um die Ecke zu einer weiteren Wand mit Gestrüpp, das ihr abermals entzündet. Funktioniert hier noch einfacher, denn ihr könnt das Seil der darüber liegenden Lampe mit einem normalen Pfeil durchschießen. Die Lampe fällt auf den Boden und entzündet das Laub.

Ganz genauso macht ihr es im folgenden Raum, in dem ein Bodenschalter hinter einem Gitter wartet. An den Schalter kommt ihr nicht selbst heran, aber ein Steinblock könnte ihn betätigen, wenn er herunterfiele. Kein Problem, schießt einfach links und rechts über dem Gatter die Halteseile der beiden Lampen durch, damit sie beide Seiten des darunterliegenden Laubs entzünden. Der Block wird fallen, den Schalter herunterdrücken und somit eine Tür links von eurer Position öffnen. Dort holt ihr euch beim Weisen ein Zeichen der Bewährung als Belohnung ab.

![Schreine bei den Goronen in Eldin (Süd)](pics/5d650685f4ba1e70af2c8262e64c73de.jpg)

## Kyu-Ramuhi-Schrein

Vor euch seht ihr mehrere Pfeiler mit Plattformen. Eine davon trägt eine Holzkiste. Schießt einen Feuerpfeil auf die Holzkiste, damit sie verbrennt. Ohne belastendes Gewicht wird die Plattform nach oben fahren und die gegenüberliegende Plattform rechts davon, ähnlich einer Waage, nach unten fahren. So kommt ihr an eine Kiste mit einem kleinen Schlüssel heran. Lasst aber unbedingt die beiden Fässer auf der Plattform mit der Schatzkiste stehen.

Der Schlüssel öffnet die Tür zum Käfig, in dem drei Metall-Quader warten. Ihr könnt die Quader per Magnetmodul herausnehmen und als Gewichte verwenden, um an die Schatzkiste an der linken Wand zu kommen. Dazu platziert ihr einen Quader auf die vordere Plattform an der linken Wand und eine auf die Plattform auf der zuvor die Holzkiste stand.

Habt ihr die beiden Quader platziert, dann stellt euch selbst auf die Plattform, auf der die Holzkiste stand und entfernt das Gewicht des Quaders, damit sie hochfährt. Nun könnt ihr zur anderen Plattform hinübergleiten, dort ebenfalls den Quader wegschieben und zur Kiste hochfahren.

Um zum Weisen zu gelangen, geht ihr ähnlich vor, nur auf der gegenüberliegenden Seite. Problem: Eine Wand versperrt den Weg und oben an der Decke warten schmerzhafte Stacheln. Halb so wild. Die Wand ist bröckelig, die könnt ihr mit einer Bombe sprengen. Um Kontakt mit den Stacheln zu verhindern, stellt ihr einfach eine der drei Metallquader auf die Plattform, Das Gewicht der gegenüberliegenden Plattform erhöht ihr, in dem ihr die anderen beiden Metallquader darauf stellt. Das Gewicht genügt dann, um Link und die dritte Metallkiste nach oben zu fahren. Holt euch oben euer Zeichen der Bewährung ab.

## Daka-Ka-Schrein

Sieht kompliziert aus, ist aber ganz einfach. Stellt euch auf die große Bodenplatte in der Mulde. Fährt sie nach oben, dann werdet ihr nach oben geschleudert, ebenso wie der riesige Block in der Mitte. Dank des Parasegels sinkt ihr langsamer und könnt auf der Oberseite des großen Blocks landen, wo ein gelber Kristall aufgestellt steht. Nun macht euer Stasis-Modul bereit und wartet auf das nächste Mal, wenn ihr hochgeschleidert werdet. Haltet den Stein an, wenn er den obersten Punkt erreicht, sodass Strom durch den Kristall fließt. So öffnet sich das Gatter, das zum Weisen führt. Ihr müsst einfach nur hinunter schweben, so lange der Strom noch fließt.

## Shimo-Itose-Schrein

Dies ist der Schrein inmitten des Goronen-Dorfes. Nehmt euch im ersten Raum einen Feuerpfeil und verbrennt das Gestrüpp an der Wand, damit der Holzbalken, der das Fass oben hält, verbrennt. Stellt das Fass dann auf den Schalter, der die Tür öffnet. Dahinter könnt ihr links noch mehr Gestrüpp verbrennen, um eine Schatzkiste freizulegen. Nun schaut nach rechts zu dem Wasserrad. Vorsicht, um die Ecke warten drei Nano-Wächter. Kümmert euch um sie und lauft ganz hinten die Rampe hoch ins nächste Stockwerk.

Geht die Treppe hoch und stellt euch auf dem Podest auf den Schalter, damit zwei Feuerlampen von der Decke kommen. Deren Seile durchtrennt ihr mit einem normalen Pfeil (gut zielen). Die Lampe fällt dann zu Boden. Nun schießt einen Pfeil durch die Flamme, damit das Laub am Boden verbrennt. Macht schnell, denn die Flamme hält nicht lange. So erhaltet ihr ein weiteres Fass, das ihr auf den Bodenschalter direkt daneben stellt. Die Tür geht auf.

![Schreine bei den Goronen in Eldin (Nord)](pics/362055283b6861ed1b392979d7da44b8.jpg)

## Schora-Hahn-Schrein

Ein langer und komplexer Schrein, den ihr auch nur mit ein wenig Vorarbeit erreicht. Sucht nördlich des Goronen-Dorfes nach Schienen mit einer Lore davor. Sollte die Lore nicht schon auf den Schienen sitzen, dann hebt sie mit dem Magnetmodul darauf. Nun stellt euch in die Lore, bearbeitet sie mit dem Stasis-Modul, damit sie nach der gesammelten Kraft nach vorne geschleudert wird. Achtung – haut nicht zu oft drauf, sonst entgleist ihr. Es genügen drei bis vier Schläge, um die Lore auf den Schienen fahren zu lassen. Wiederholt den Vorgang, bis ihr an einer Insel ankommt in deren kleiner Höhlenausbuchtung ein Schrein steht.

Im Inneren des Schreins hebt ihr das Stachelbett per Magnetmodul an und geht darunter durch bis zur erloschenen Fackel. Schreitet nach links, wo ihr die beiden Metallblöcke per Magnetmodul ausrichtet, damit ihr zur blau brennenden Fackel rüber könnt. Nehmt euch dort den Stock am Boden und zündet ihn an. Tragt das Feuer zur Fackel auf dem Steg, wo ihr gerade hergekommen seid. Brennt diese Fackel, dann entzündet einen Pfeil daran, lauft weiter den Steg entlang an den ersten Wasserfontänen vorbei und schießt den brennenden Pfeil auf die erloschene Fackel ganz am Ende des Stegs. Dort lauft ihr auch hin und schießt einen weiteren (brennenden) Pfeil an die Fackel, die auf der anderen Seite des Magmabeckens auf dem Aufzug steht.

Steigt auf die Treppe, die nun heruntergefahren wurde und tretet oben auf den Bodenschalter. Kehrt sofort um, damit ihr der rollenden Stachelkugel ausweichen könnt. Ist die an euch vorbei, schreitet die Rampe hinauf. Ihr seht am Ende des Stegs den Aufzug von eben. Ignoriert ihn zuerst, denn rechts von euch greifen einige Nano-Wächter an. Macht sie fertig. Danach entzündet ihr einen Pfeil und lauft zur ersten der drei Fackeln am Ende des Ganges vor. Ziel ist nun, alle drei Fackeln gleichzeitig zu entzünden. Das geht aber wegen der Wasserstrahlen nicht so einfach. Ihr müsst die Fontäne per Stasis-Modul anhalten und dann sehr schnell reagieren, um alle beide Fackeln zu entzünden.

Ähnlich verhält es sich mit den nächsten drei Fackeln, die ihr mit Pfeil und Bogen entflammt. Nutzt den Luftstrom, um zur anderen Seite zu schweben und nehmt eich eine Fackel nach der anderen vor.

Etwas weiter haltet ihr den Wasserstrahl per Stasismodul an und schiebt den Block, auf dem die Fackel steht, per Magnetmodul an der Fontäne vorbei.

Mit den folgenden drei Nano-Wächtern werdet ihr schnell fertig, wenn ihr einfach das Laub am Boden entzündet. Schreitet weiter, bis ihr rechter Hand einen Fackelkreis entdeckt. Auch hier müssen alle Fackeln gleichzeitig brennen, allerdings lassen die Fontänen das nicht zu wenn ihr es einzeln probiert. Nehmt euch also einen entzündeten Stock und haltet den Y-Knopf gedrückt, damit Link zu einer Dreh-Attacke ansetzt. Steht ihr währende Links Wirbelattacke in der Mitte des Fackelkreises, so entflammt ihr alle auf einmal. Nun könnt ihr zum Weisen durch, um euch das Zeichen der Bewährung abzuholen.

## Gu-Achitoh-Schrein

Damit ihr den Schrein betreten dürft, müsst ihr zuerst die Kletter-Herausforderung des Goronen namens Baket schaffen. Er sitzt direkt am Schrein. Bei dieser Herausforderung sollt ihr unter Zeitdruck den Berg erklimmen, auf dem der Schrein steht und dabei mindestens 100 Rubine einsacken. Bereitet euch entsprechend vor, indem ihr euch an der Feuerstelle Mahlzeiten kocht, die eure angebrochene Ausdauer auffrischen. Zum Beispiel Ausdauer-Schmorfisch (Ausdauerbarsch, Steinsalz und Apfel). Der Berg bistet zwar ständig Vorsprünge zum Ausruhen, aber die Zeit tickt gegen euch, darum solltet ihr Ausdauer-Mahlzeiten in Reserve haben – besonders dann, wenn ihr eure Ausdauer noch nicht oft genug mit Zeichen der Bewährung erweitert habt.

Besteht ihr die Prüfung, so könnt ihr den Schrein betreten, in dem keine weitere Aufgabe wartet. Holt euch einfach das Zeichen ab.

## Keha Rama-Schrein

Nach Abschluss des Vah Rudania Dungeons sprecht ihr in einer Hütte im Osten des Goronen-Dorfes mit Bladon. Er vermisst jemanden, der sich im Marugo-Tunnel aufhalten soll. Schreitet also den Weg des Todesbergs hinunter (nicht Richtung Süd-Mine, sondern der Weg hinter der Dorfbrücke bei den warmen Quellen). Folgt den Fackeln, bis ihr am Marugo-Tunnel angelangt seid und geht hinein. Dort findet ihr einen arg geschwächten Goronen.

Helft ihm mit einem Stück Schor-Fels auf die Beine. Ihr findet den Fels-Braten wenn ihr vom Höhleneingang südlich bergab an das Ende eines Tals geht, da liegen gleich drei davon herum. Ihr müsst den Braten nur noch in die Höhle schaffen, was angesichts der umliegenden Feinde etwas mühselig sein kann. Ist der Gorone wieder bei Kräften, beseitigt er ein großes Stück Fels, hinter dem der Keha-Rama-Schrein liegt.

In dessen Inneren passiert nicht viel. Hinter dem Eingangssaal wartet eine sehr lange, hohe Schräge, auf der euch große und kleine Kugeln, aber auch gelegentlich Rubine entgegen rollen. Weicht den Kugeln aus und rennt nach ganz oben, um euer Zeichen der Bewährung zu kassieren.
# Rezepte

## Medizin

### Kraft-Medizin
- - Effekt: Fünf Minuten Stärke erhöht (lvl 1)
- - Rezept: Schwertkäfer + 2 x Bokbin-Horn + Bokbin Hauer

### Spurt Medizin
- Effekt: 2:10 min extra Spurt (lvl 1)
- Rezept: Spurtechse + Bokbin-Horn

### Fitness-Medizin
- Effekt: Füllt die Ausdauer über das Maximum hinaus auf
- Rezept: Fitkröte, 2 x Moblin-Horn,Moblin Hauer, Oktorok-Tentakel

### Schleich-Medizin
- Effekt: Senkt den Puls und erhöht die Schleich-Effizienz (Lvl 1)
- Rezept: Schleichwürmchen, Bokbin-Horn, Hinox -Fußnagel

### Ausdauer-Medizin
- Effekt: Füllt angebrochene Ausdauer wieder auf.
- Rezept: Ausdauerschrecke, Bokbin-Hauer, Bokbin-Herz. Echsalfos-Sporn

### Kühlungs-Medizin Level 2
- Effekt: Kühlungs Effekt Level 2 für 6:20 Min (wirksam in der Wüste)
- Rezept: Frostflügler + Echsalfoss-Horn + Echsalfoss-Sporn + Hinox-Fußnagel

### Elektro-Medizin Level 2
- Effekt: Schutz vor Stromschlägen (Level 2) für 6:20 min
- Rezept: 2 x Zitterlibelle + Echsalfos-Horn

### Brandschutz-Medizin
- Effekt: Brandschutz Level 1 für 6:10 min
- Rezept: 2 x Löschechse + Bokblin-Horn

### Maxi-Medizin
Effekt. Maximale Heilung + 5 Extra-Herzen
- Rezept: Maxi-Echse + Bokblin-Hauer + Echsalfos-Horn + Eis Flederbeißerflügel

## Einfache Gerichte zur Heilung

### Butterapfel
- Effekt: 2 Herzen Heilung
- Rezept: Apfel + Ziegenbutter

### Schmorbraten (klein)
- Effekt: 2 Herzen Heilung
Zutaten: Wild + Ziegenbutter + Steinsalz

### Dampffleisch (klein)
- Effekt: 4 Herzen Heilung
- Rezept: Hyrule Gras + Wild

### Schmorbraten (mittel)
- Effekt: 5 Herzen Heilung
- Rezept: Eichel + Hyrule gras + Geflügel + Steinsalz

### Dampffisch
- Effekt: 5 Herzen Heilung
- Rezept: Apfel + Hyrule Gras + Hyrulebarsch

### Pilzspieß
- Effekt: 5 Herzen Heilung
- Rezept: Eichel + Ausdauerling + Schleichling + Hyrule-Gras

### Fleisch Reisbällchen (klein)
- Effekt: 5 Herzen Heilung
- Rezept: Apfel + Geflügel + Hyrule Reis

### Fleischspieß
- Effekt: 6 Herzen Heilung
- Rezept: Geflügel + Wild + Rüstling + Schwertbanane

### Festtagsfisch
- Effekt: 7 Herzen Heilung
- Rezept: Hyrule Pilz, Ausdauerhonig, Rüstungskarpfen

### Dampffleisch (groß)
- Effekt: 8 Herzen Heilung
- Rezept: Eichel + Hyrule Gras + Wild

### Meersfrüchtespieß
Effekt. 10 Herzen Heilung
- Rezept: Rüstling, Maxi-Rübe, Rüstungskarpfen, Rüstungskrabbe

### Fleisch- Reisbällchen (groß)
- Effekt: 11 Herzen Heilung
- Rezept: Maxi-Rübe + Spurkarotte + Edelwild + Hyrule Reis

### Luxusschmorbraten
- Effekt: 15 Herzen Heilung
- Rezept: Luxuswild + Ziegenbutter + Steinsalz + Fee

### Maxi-Luxusschmorbraten
- Effekt: Maximale Heilung + 4 Extra-Herzen
- Rezept: Maxi-.Durian + Luxuswild + Steinsalz + Fee

### Maxi-Luxuswild-Reis
- Effekt: Maximale Heilung + 3 Extra-Herzen
- Rezept: Maxi-Rübe + Luxuswild + Hyrule-Reis + Steinsalz + Hyrule-Barsch

### Maxi-Kochobst
- Effekt: Maximale Heilung + 4 Extra-Herzen
- Rezept: Wildbeere + Eichel + Maxi-Durian + Geflügel

## Rezepte, die die Lebenskraft kurzzeitig erweitern

### Maxi-Fleischspieß
Effekt. Maximale Heilung + 1 temporäres Extra-Herz
- Rezept: Eichel, Maxi-Trüffel, 2 x Edelwild

### Maxi-Dampffleisch
- Effekt: maximale Heilung + 4 temporäre Extra-Herzen
- Rezept: 3 x Geflügel + Große Maxi-Rübe

### Maxi-Edelschmorbraten
- Effekt: maximale heilung + 4 temporäre Extra-Herzen
- Rezept: Hyrule-Gras, Maxi-Rübe, Edelwild, Ziegenbutter, Steinsalz

### Maxi-Edelschmorbraten (Version 2)
- Effekt: maximale Heilung + 6 temporäre Extra-Herzen
- Rezept: Hyrule Pilz, Hyrule-Gras, Große Maxi.Rübe, Edelwild, Steinsalz

## Wärmende Gerichte (anti Kälteschaden)

### Scharfes Obst mit Pilzen
- Effekt: 3 Herzen Heilung, verringert Kälteschaden für 3:30 min. (Lvl1)
- Rezept: Chili, 2 x Hyrule Pilz

### Scharfes Kochobst
- Effekt: 3 Herzen Heilung, verringert Kälteschaden für 3:50 min. (Lvl1)
- Rezept: Chili, Eichel, Apfel

### Scharfes Wildgemüse
- Effekt: 5 Herzen Heilung, verringert Kälteschaden für 4 min. (Lvl1)
- Rezept: Apfel, Hyrule-Gras Glutkraut, Edelwild

### Scharfer Schmorbraten
- Effekt: 3 Herzen Heilung, verringert Kälteschaden für 5:20 min. (Lvl1)
- Rezept: Chili, Geflügel, Ziegenbutter, Steinsalz

## Effekt-Gericht: Schleichen

### Schleich-Pilzspieß
- Effekt: 4 Herzen Heilung + 3 minleise schleichen (lvl 1)
- Rezept: Hyrule-Pilz + Schleichling (Pilz) + Wild

### Fleischspieß mit Pilzen
- Effekt: 5 Herzen Heilung + 8:20 min leise schleichen (lvl 1)
- Rezept: Eichel + Schleichling + Geflügel

### Schleich-Wildgemüse
- Effekt: 5 Herzen Heilung + 8:50 min leise schleichen (lvl 1)
- Rezept: Eichel + Hyrule-Gras + Schleichglöckchen + Wild

### Effekt-Gericht: Ausdauer

### Ausdauer Dampfpilze
- Effekt: 6 Herzen Heilung + Füllt die angebrochene Ausdauer auf
- Rezept: Eichel, 2 x Ausdauerling + Hyrule-Gras + Milch

### Ausdauer-Honigapfel
- Effekt: 5 Herzen Heilung, füllt angebrochene Ausdauer wieder auf
- Rezept: Apfel + Ausdauerhonig

### Ausdauer-Festtagsgulasch
- Effekt: 8 Herzen Heilung + füllt angebrochene Ausdauer auf.
- Rezept: Edelwild, Milch, Ausdauerhonig, Steinsalz

## Effekt-Gericht: Spurt / Eile

### Spurt-Gemüsecremesuppe
Effekt. 2 Herzen Heilung + schneller laufen (Lvl 1)
- Rezept: Spurtkarotte + Milch + Steinsalz

### Spurt-Schmorbraten
- Effekt: 5 Herzen Heilung + 4:30 min schneller laufen (lvl 1) 
- Rezept: Spurtlotos, 2 x Spurtling, Geflügel, Steinsalz

##  Effekt-Gericht: Verteidigung

### Abwehr-Schmorfisch
Effekt; 5 Herzen Heilung + 2:50 min erhöhte Abwehr (lvl 1)
- Rezept: Hyrule-Pilz, Hyrule-Gras, Steinsalz, Rüstungskarpfen

### Abwehr-Fleischkürbis
- Effekt: 3 Herzen Heilung + erhöhte Abwehr (lvl 1)
- Rezept: Rüstungskürbis + Wild

### Abwehr-Schmorkrabbe
- Effekt: 8 Herzen heilung + 3:50 min Erhöhte Abwehr (lvl1)
- Rezept: Apfel, Ei, Steinsalz, Rüstungskrabbe

## Effekt-Gericht: Elektro-Schutz

### Elektro-Edelschmorbraten
- Effekt: 5 Herzen Heilung + Erhöht Resistenz gegen Stromschläge für 4:30 min
- Rezept: Apfel + Zitterfrucht + Edelwild + Steinsalz

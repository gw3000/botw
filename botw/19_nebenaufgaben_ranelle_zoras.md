# Nebenaufgaben in Ranelle-Gegend bei den Zoras

Bei den Zoras gibt es ebenfalls viele Nebenaufgaben zu erfüllen. Es bietet sich an, sie nach dem Abschließen des Vah Ruta Dungeons anzugehen. Die meisten Auftraggeber findet ihr im Dorf der Zoras vor.

## Der Kopfsprung
Für diese Aufgabe braucht ihr die Zora-Rüstung. Sucht im Dorf der Zoras unweit vom Thronsaal Rytt auf. Sprecht mit ihm und springt dann vom Rand der Plattform ins Wasser, Link wird einen Kopfsprung machen. Über den Wasserfall gelangt ihr dank der Zora-Rüstung wieder nach oben. Sprecht noch einmal mit Rytt.

## Eilzustellun
Für diese Aufgabe müsst ihr das Vah-Ruta-Dungeon gelöst und den Boss dort besiegt haben. Sucht nun das Tal auf, durch das ihr zum Dorf der Zoras kommt, und sucht dort das Ufer der Träume. Unterhaltet euch dort mit der Dame namens Fine, die eine Nachricht auf dem Seeweg vertreibt. Folgt dem Brief bis zu einer Bucht, wo ihr einen Mann namens Sasan antrefft. Sprecht mit ihm und geht dann zurück ins Zora-Dorf: Dort trefft ihr erneut auf die beiden.

## Gedenksteine
Für diese Aufgabe müsst ihr das Vah-Ruta-Dungeon absolviert haben. In der Nähe des Thronsaals steht Jiato. Er beauftragt Link, nach zehn Gedenksteinen zu suchen. Diese Gedenksteine sind überall in der Ranelle-Gegend verteilt. Schaut auf unsere Karte für die Fundorte.

![Gedenksteine in der Ranelle-Gegend](pics/31a657dc5a9ff2711605ff99c0b9d375.jpg)

## Frischer Froschregen
In der Nähe des Schreins im Zora-Dorf läuft ein Jungspund namens Tembo. Sprecht mit ihm. Er beauftragt Link, fünf Spurtkröten zu besorgen. Die findet ihr bei regnerischem Wetter beinahe überall, wo es Tümpel und Teiche in der Nähe des Zoga-Dorfes gibt. Bringt ihm fünf der Kröten.

## Ein kleines Riesenproblem
Für diese Aufgabe müsst ihr das Vah-Ruta-Dungeon abgeschlossen haben. Sucht nun im Zora-Dorf nach Torfeau – zu finden auf dem Platz vor dem Thronsaal. Die erteilte Aufgabe ist, einen Hinox zu erledigen, der südwestlich des Dorfes Unruhe stiftet. Erledigt den Hinox und kehrt dann in das Zora-Dorf zurück.

## Die Suche nach dem Leunen
Als ihr zum Vah Ruta-Dungeon aufbrechen solltet, musste ihr Elektropfeile auf dem Berg davor sammeln und dem Leunen aus dem Weg gehen. Ein Zora namens Amol möchte ein gutes Foto vom Leunen haben. Solltet ihr keines gemacht haben, als ihr zum Dungeon aufgebrochen seid, dann schießt einfach jetzt eins von ihm, indem ihr erneut auf den Berg steigt.

## Davongetrieben
Für diese Aufgabe müsst ihr das Vah-Ruta Dungeon abgeschlossen haben. Sprecht nun im Zora-Dorf mit Fuger, östlich des Krämerladens. Sucht nun seine Frau auf, die sich am Hylia-See befindet, und zwar auf einer kleinen Insel im Osten des Hylia-Sees.

## Leuchtsteinbruch
Für diese Aufgabe müsst ihr das Vah-Ruta-Dungeon abgeschlossen haben. Sprecht mit Letogan. Er beauftragt Link, zehn Leuchtsteine zu sammeln. Ihr findet Leuchtsteine nordwestlich vom Zora-Dorf hinter der Brücke.

## Versunkene Schätze
Sucht den Stall der Sümpfe auf. Er befindet sich nicht in der Nähe des Zora-Dorfs, sondern ganz im Westen des Ranelle-Gebiets. Dort sprecht ihr mit Isra, der eine Schatzkiste ausheben will. Kein Problem für euch, denn ihr habt ja das Magnetmodul.

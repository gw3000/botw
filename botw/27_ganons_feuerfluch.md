
# Ganons Feuerfluch

Ganons Feuerfluch ist ein verdammt flinker Boss, dessen Angriffe schnell und verheerend sein können. Haltet stets den L2-Knopf gedrückt, damit ihr ihn im Auge behaltet, und verwendet das perfekte Ausweichen. Bzw. den perfekten Konter, wenn er angreift (in Angriffsposition auf den Sprungknopf drücken und dann zuschlagen).

Wenn er mit Feuerbällen angreift, könnt ihr nur noch ausweichen. Seid geduldig und wartet darauf, dass er zu euch kommt. Ihr könnt ihn euch allerdings auch durch Fernangriffe vom Leib halten. Er kann Eispfeile nicht leiden, darum schießt ihm welche ins blau leuchtende Auge, sofern ihr Eispfeile vorrätig habt. Auch Bombenpfeile wirken.

![Ganons Feuerfluch](pics/153a1ff94e6309193b53fdcc7bf54ee2.jpg)

Habt ihr ihm einiges an Lebenskraft abgezapft, wird Ganons Feuerfluch beginnen, ein Energiefeld zur Verteidigung zu errichten. Ihr solltet nicht damit in Berührung kommen. Weicht aus und versteckt euch bei Annäherung hinter einem Gegenstand, der genug Deckung verspricht. Versucht ihn in einen Nahkampf zu locken und greift stets mir gutem Timing an.

Sollte er seinen Laser auspacken, habt ihr die Wahl: Entweder ausweichen und verstecken, oder es mit einem perfekten Block probieren, der extrem gut zeitlich abgepasst werden muss, Schafft ihr das, wird sein Laser reflektiert und fügt ihm selbst Schaden zu. Nach wie vor können Eispfeile und Bombenpfeile helfen, den Kampf zu verkürzen, wenn er sich zu weit auf Distanz hält. Zielt immer schön auf das blaue Auge. Wenn ihr ihn besiegt habt, erhaltet ihr einen weiteren Herzcontainer.
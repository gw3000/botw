# Kochen und Zutaten

Durch das Kochen von Mahlzeiten und Medizin kann Link sich bei Kräften halten und vorübergehend von zusätzlichen Effekten profitieren. Daher ist es wichtig, regelmäßig Gerichte zuzubereiten. Passende Zutaten findet ihr permanent am Wegesrand. Vor allem Pilze, Kräuter und Früchte könnt ihr quasi im Vorbeigehen mitnehmen. Sie roh zu essen, kann im Notfall nötig sein, ist aber ineffektiv. Gekocht entfalten Zutaten erst ihre volle Wirkung.

Kochen könnt ihr an Feuerstellen mit einem Kochtopf. Schaltet mit der Plus-Taste in euer Inventar und sucht dort das Feld mit den Zutaten. Drückt ihr bei der Auswahl einer Zutat den A-Knopf, werdet ihr gefragt, ob ihr es gleich essen oder in die Hand nehmen möchtet. Zum Kochen müsst ihr Zutaten oin die Hand nehmen. Nach der ersten Zutat landen die nachfolgend gewählten Teile ebenfalls in Links Hand. Habt ihr alles zusammen, was ihr kochen wollt, dann drückt den B-Knopf um auf das Spielfeld zurück zu kehren. Steht ihr nun vor einem Kochtopf, ergibt sich für den A-Knopf die Option „kochen“.

Link wird alle Zutaten in den Topf werfen, wo sie augenblicklich tanzen und innerhalb weniger Sekunden zu einer Mahlzeit verarbeitet werden. Ist das Rezept gut, so bekommt ihr den Namen der Mahlzeit verraten und sie wird in eurem Inventar für Mahlzeiten abgelegt. Mischt ihr Monstermaterialien mit herkömmlichen Lebensmitteln, wird daraus eine unappetitliche Pampe. Die ist zwar in der Regel nicht giftig, wird aber wegen ihrer Unansehnlichkeit verpixelt. Matsch spendiert meist nur wenig Heilung und keinen Effekt.

## Geläufige Zutaten:

### Fleisch (Wild, Edel-Wild, Geflügel, Edel-Geflügel)

Fleisch ist die Basis vieler Gerichte und füllt vornehmlich Lebenskraft auf. In Verbindung mit weiteren Zutaten wird dieser Effekt erhöht. Aber auch umgekehrt. Die Effekte von Kräutern, Pilzen und Früchten werden durch gutes Fleisch verstärkt. Für Fleisch müsst ihr wilde Tiere wie Wildschweine, Rehe oder Enten erlegen.

### Früchte, Gemüse und Nüsse (Äpfel, Möhren, Eicheln usw.)

Früchte, Gemüsesorten und Nüsse haben in der Regel eine ähnliche Funktion wie Fleisch, Sie gelten als Basis eines Menüs und spendieren Herzen, wenn auch aufgrund des geringeren Nährwerts weniger als Fleisch. Ausnahmen bestätigen die Regel, denn es gibt Möhren, die Effekte mitbringen – zum Beispiel erhöhte Laufgeschwindigkeit.

### Pilze

Pilze heilen meist wenig, bringen aber interessante Effekte mit. Von erhöhten Schleichfertigkeiten bis zu verbesserter Stärke. Die Namen der Pilze verraten bereits ihren Effekt, siehe z.B., Rüstling oder Schleichling.

### Gewürze (Chili, Glutkraut, Salz)

Wichtig gegen Umwelteinflüsse. Ein Gericht, das mit Chili oder Glutkraut angereichert wird, wärmt Link, sodass er eine gewisse Zeit in der Kälte auskommt, ohne dabei Schaden zu erleiden. Salz verstärkt hingegen die Wirksamkeit vieler Zutaten und von Fleisch.


### Gräser, Wurzeln, Reis

Solche Zutaten verfeinern ein Gericht, heben Effekte an und bringen manchmal eigene Effekte mit.

### Fische und Krabben

Fischsorten und Krabben kombinieren oft die Eigenschaften von Fleisch und Pilzen oder Gräsern. Sie spendieren also Lebenskraft-Herzen, bringen aber auch nützliche Effekte mit, die man anhand ihrer Namen erkennt. Fische fängt man schlicht mit der Hand in allen erdenklichen Gewässern.

### Knochen, Zähne und Schleime
  
Mit Knochen und Schleimen von Feinden bekommt ihr keine leckeren Gerichte gekocht, aber Medizin, die noch wirksamer sein kann.

### Insekten und Frösche / Kröten
  
Auch diese Zutaten können nahrhaft sein, eignen sich aber eher für das Zubereiten von Medizin und Heiltränken.

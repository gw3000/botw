
# Nebenaufgaben der Orni

Im Dorf der Orni und Umgebung gibt es nur wenige weitere Aufgaben, die ihr angehen könnt. Hier unsere Liste. Ihr könnt sie quasi nacheinander abklappern.

## Curry gegen die Kälte

Am Stall der Orni, östlich der Stadt, steht ein Mann namens Leif an der Kochstelle. Er benötigt für sein Gericht etwas, das ihr bei den Goronen bekommt, nämlich das Goronengewürz. Besorgt ihm eine Flasche aus Goronia.

## Flitterwochen in Gefahr

Der Orni Jogo steht beim Gasthaus im Dorf der Orni. Sprecht bei Tage mit ihm. Alles was er benötigt, ist ein Feuerstein. Den solltet ihr mit großer Wahrscheinlichkeit dabei haben. Wenn nicht, müsst ihr ein paar Erzvorkommen an Bergen plündern.

## Liebe geht durch den Magen

Gleich neben dem Haus des Orni-Häuptling Kanelis im Orni-Dorf steht Juno. Verlangt wird ein Röstapfel. Einfache Sache: Legt einen Apfel auf ein Feuer oder legt ihn auf den Boden und haut mit einem Feuerschwert drauf,. Auch die Behandlung mit einem Feuerpfeil sollte Wirkung zeigen.

## Der Glacirok im Blizzar-Tal

Diese Aufgabe könnt ihr erst angehen, wenn ihr den Vah-Medoh-Titanen geschafft habt. Sucht die Holzbrücke auf, die über die Schlucht zum Dorf führt. Sprecht dort mit Gesane, der sich über einen Eisriesen beschwert, der im Blizzar-Tal nördlich des Dorfes sein Unwesen treibt. Bevor ihr zu Glacirok aufbrecht, solltet ihm im Krämerladen der Orni ein paar Feuerpfeile kaufen, Sie werden euch von Nutzen sein, denn ohne ist er sehr kalt und kostet euch beim Besteigen Lebenskraft. Glacirok ist anderweitig nicht gefährlicher als einer der anderen Steinmonster. Den Eiskoloss findet ihr weit im Norden, jenseits der Grenzen der Tabanta-Region.

## Die Quelle der großen Fee

Wie und wo ihr die große Fee entdeckt, haben wir einige Seiten zuvor genau beschrieben. Kurz angerissen: Schaut südlich des Tabanta-Turmes in einer Schlucht nahe des Weges nach ihr.
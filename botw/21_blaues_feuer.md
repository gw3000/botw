

# Blaues Feuer für antike Waffen

Die beiden Wissenschaftler des Akkala-Instituts schicken euch zum nahe gelegenen Flammenborn, um blaues Feuer zu holen. Ohne das Feuer funktioniert deren Shiekah-Ofen nicht. Nehmt dafür eine der Fackeln aus dem Institut mit. Aber Vorsicht: Der Weg ist dieses mal bei Weitem nicht so einfach, wie beim ersten Institut bei Hateno, da ihr auf einige starke Gegner treffen werdet. Ihr solltet gut ausgerüstet sein und viele stärkende Mahlzeiten dabei haben. Ist dem nicht so, dann teleportiert euch irgendwo hin, wo ihr kochen könnt, kocht hochwertige Rezepte und kehrt dann zum Institut zurück.

Der Weg zum Flammenborn ist kurz, wenn ihr vom Institut aus schlicht nach Westen wandert und von der Klippe aus zum Welke Plateau hinüber schwebt – immer dem blauen Licht entgegen. Ihr werdet ein kleines Stück klettern müssen, darum wartet bei Regen lieber auf gutes Wetter, sonst rutscht ihr ab.

Am Flammenborn angekommen, geht der Stress richtig los. Ihr könnt dieses Mal nicht am Stück durchlaufen, darum ist es wichtig, jede Zwischenstation (also jede Lampe), zu entflammen, damit ihr nach Kampfeinlagen nicht ständig den ganzen Weg zurück rennen müsst.

Den genauen Weg findet ihr auf unserer Karte, die euch einiges an Ärger und Suchen erspart. Hier noch eine genaue Beschreibung: Lauft vom Flammenborn aus ein kleines Stück nach Osten und rutscht die Erhebung hinunter. Zweigt nun nach Norden ab und haltet nach einem verwüsteten Haus Ausschau. Direkt vor dem Haus ist eine weitere Fackellampe – aber auch ein großer Gegner, Entflammt die Lampe und schaltet augenblicklich auf eine andere Waffe um, damit ihr kampfbereit seid.

![Pfad für die blaue Flamme des Instituts](pics/ac1fb35e8c70fb64f927443a7b64f985.jpg)

Vom zerstörten Haus aus führt euer Weg kurz nach Norden und dann stetig nach Osten, bis ihr einen großen runden Felsen erreicht. In diesem Felsen haben Bokblins ihr Lager aufgeschlagen. Darum sieht der Felsen, von der Vorderseite gesehen, aus wie eine Totenkopf. Direkt vor dem zweiten Auge des Totenkopfes ist die nächste Fackellampe. Kümmert euch um die Gegner, denn sie sind zu dritt, und ihr Anführer ist einer der stärkeren Sorte.

Nun geht den Hügel hinab zum Ausläufer des Sees. Hier könntet ihr theoretisch nach Süden, um eine wietere Lampe zu entzünden, nur ist das sinnlos, weil es sich um eine Sackgasse handelt. Dahinter ist ein Hindernis, das ihr mit der brennenden Fackel in der Hand nicht überwinden könnt. Haltet euch am See lieber nördlich und umkreist den See an seiner Nordseite. Weitere große Gegner werden euch auf die Pelle rücken.

Euer Weg führt nun nach Süden an einem Felsen samt weiterer Lampe vorbei, und einer Brücke, die über den See führt. Lauft weiter nach Süden und umkreist die Südseite des Sees. Aufgepasst, hier nervt ein Oktorok im Wasser. Mal ganz abgesehen von weiteren Landgegnern.

Schlagt nun nach Norden ein und haltet euch am Rand der Klippe auf der Seite des Sees, bis ihr nicht mehr weiter nach Norden könnt. Am Fels schlagt ihr den Weg ostwärts ein und stapft den kleinen Hügel hoch. Nun wird es gleich brenzlig, denn ein unbeweglicher Wächter hält am Pfad zum Institut Wache. Augen zu und durch zum Institut, wo ihr die letzte Fackel direkt am Haus entzündet und somit Cherry Energie spendet.

Betretet nun abermals das Akkala-Institut und sprecht den komischen Ofen Cherry in der Mitte des Raums an. Ja, er kann sprechen und offeriert euch nun die Herstellung antiker Waffen im Tausch gegen Rubine und antike Bauteile. Nicht günstig, die Angelegenheit, aber im Kampf gegen Wächter ein wertvolles Mittel.

Rüstet euch nach Belieben und nach Budget aus, aber nehmt unbedingt einige antike Pfeile mit! Mit diesen Pfeilen könnt ihr euch sehr gut aus der Distanz gegen große Wächter verteidigen, indem ihr ihnen direkt in das Laser-Auge schießt. Da die meisten Wächter „nur“ über 500 Trefferpunkte verfügen, sollte ein gut platzierter Schuss bei den meisten Wächtern ausreichen. Wenn ihr einen Wächter besiegt, erhaltet ihr jede Menge weiterer antiker Bauteile, die ihr wiederum in antike Waffen investieren könnt.

Sprecht noch einmal mit Jerrin im Institut, Sie erteilt euch die Nebenaufgabe „Linkes Auge des Schädels“. Die Lösung der Aufgabe findet ihr auf der nächsten Seite unter der Beschreibung des Zuna-Kai-Schreins.

Nun habt ihr die freie Wahl. Ihr könnt in das benachbarte Goronen-Gebiet gehen, oder zu den Gerudos in der Wüste ganz im Südwesten der Landkarte. Für beide Expeditionen empfehlen wir, hier in der Akkala-Gegend noch ein paar Schreine zu knacken, um weitere Zeichen der Bewährung einzusacken. Ihr werdet jeden Herzcontainer benötigen.

# Nebenaufgaben bei den Goronen in Eldin

Bei den Goronen gibt es nur wenige Nebenaufgaben zu lösen, aber manche lassen wertvolle Gegenstände springen. Seht in unsere Auflistung

## Löschechsenjagd

Ein Mensch, der sich an der Südmine auf dem Weg zum Goronen-Dorf aufhält, sucht nach zehn Löschechsen. Sie sind ihm zu flink. Löschechsen findet ihr direkt in der Umgebung der Südmine. Schleicht euch an sie heran oder lest welche unter Steinen auf. Für zehn Löschechsen gibt er euch den ersten wichtigen Teil eurer Anti-Feuer-Rüstung.

## Das Geheimnis des Todesbergs

In den warmen Goronenquellen hinter der Brücke des Dorfes sitzt ein Jungspund namens Spafel. Sprecht ihn an und begebt euch auf den Weg Richtung Brücke von Eldin. Noch davor entdeckt ihr einen Haufen brüchigen Gerölls bei einem Magmafall. Sprengt das Geröll und nehmt den dahinterliegenden Erzbohrer an euch. Geht dann zu Spafel zurück und erstattet Bericht.

## Lehrgeld

Sucht im Dorf der Goronen nach Hugo. Er will, dass ihr den Magmarok - ein großes, glühendes Felsmonster – bezwingt. Magmarok findet ihr an der Nordwestseite des Darunia-Sees. (die genaue Position findet ihr auf der Übersichtskarte im vorherigen Kapitel mit den Schreinen). Aber Vorsicht: ist eure Anti-Feuer-Rüstung nicht mindestens auf Level 2 (dank Aufrüstung durch zwei große Feen), so könnt ihr den Magmarok nicht besteigen, ohne Schaden davonzutragen. Aber es gibt eine Zwischenlösung. Habt ihr einen Eis-Stab oder einen Eispfeil zur Hand, so könnt ihr das große Monster vorübergehend abkühlen. Seine Schwachstelle ist wie bei seinen verwandten der große Erzfels auf seinem Kopf. Klettert am Rücken des Monsters hinauf und bearbeitet diesen dunklen Erzblock mit dem Schwert. Passt auf, dass ihr während des Kampfes nicht in die Lava geschleudert werdet.

![Magmarok](pics/72bd8db19d8632fede4425bae1dcf849.jpg)

## Edelstein-Import:

Habt ihr Vah-Rudania bezwungen, unterhaltet euch im Goronen-Dorf mit einer Gerudo-Frau, die dort wandert. Sie möchte zehn Bernstein-Blöcke von eich haben. Solltet ihr diese noch nicht besitzen, dann knackt im Umkreis des Todesbergs Erzvorkommen. Da fallen genügend Bernsteinadern ab. 
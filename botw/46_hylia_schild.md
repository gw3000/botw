
# Der Hylia-Schild

Nun geht es los: Das große Finale in dem Link dem fiesen Ganon endlich die Stirn bieten wird. Allerdings ist dies nicht zwingend das Ende des Spiels – es gibt noch viel zu erforschen und Dinge zu finden. Darum folgen in dieser Lösung auch weitere Schrein-Beschreibungen und Aufschlüsselungen diverser Gegenden.

Wenn ihr zu Ganon aufbrechen wollt, solltet ihr gut vorbereitet sein. Rüstet auf so gut es geht. Mahlzeiten, Tränke, Pfeile, Waffen, Schilde, Rüstungen und nicht zuletzt Herzen in der Lebensleiste – all das sollte in optimaler Anzahl vorhanden sein. Je mehr Schreine ihr geknackt (und Zeichen gegen Herzen eingetauscht) habt, desto besser. Aber denkt dran: Lebenskraft ist nicht alles. Zwei volle Kreise Ausdauer sollten mindestens zur Verfügung stehen. Besser zweieinhalb.

Beachtet, dass es mehrere Wege zu Ganon gibt. Welchen Weg ihr wählt, steht euch frei. Doch egal welchen ihr wählt, ihr werdet euch mit der Bekämpfung von antiken Wächtern und von Leunen auseinandersetzen müssen. Macht euch also spätestens jetzt mit den Techniken des perfekten Blocks und des perfekten Ausweichens vertraut, sonst seht ihr wenig Land.

Der offensichtlichste „offizielle“ Weg zu Ganon führt über die „Ebene von Hyrule“ durch die Überreste von Hyrule Stadt direkt an den Haupteingang des Schlosses. Es gibt aber auch einige alternative Pfade. Einen dieser alternativen Wege schlagen wir in dieser Lösung zuerst ein, weil wir Links mächtiges Hylia-Schild sichern wollen.

Zum Schild gelangt ihr am schnellsten, wenn ihr euch zum Turm der Wälder nordöstlich des Schlosses von Hyrule teleportiert und von dort aus leicht nach Richtung Süden und stark Richtung Westen gleitet, bis ihr auf einer Felsengruppe angelangt, die punktgenau nördlich des Schlosses liegt. Die Sicht wird euch durch einen großen Obelisken mit roten Ornamenten versperrt. Alles kein Problem. Steigt so hoch ihr könnt und gleitet nun direkt in südlicher Richtung über die Schlucht an den Nordpunkt des Schlosses. Klettert über die Mauern und verschafft euch Einlass. Nun müsst ihr am Rand des Schlosses ein kleines Stück nach Westen ausscheren und dabei die nördliche Küste im Auge behalten. Ihr gelangt an eine Stelle mit einer winzigen Anlegestelle aus Holz, an der ein antiker Wächter Wache hält.

Den antiken Wächter müsst ihr platt machen. Dafür habt ihr mehrere Möglichkeiten, Entweder ihr nehmt antike Pfeile zur Hand, die ihr ihm ins Auge schießt, oder ihr wendet den Perfekten Block mithilfe eures Schildes an und reflektiert seinen Laserstrahl mehrmals auf ihn zurück. Tipp: ein antiker Schild entsprechender Größe und Stärke wird den Laserstrahl immer zurückschleudern, auch ohne perfekten Block. Allerdings verliert er dann schnell an Haltbarkeit.

[Bild: KarteGefägnis]

Ist der Wächter erledigt, dann betretet das offenstehende rechteckige Gebäude daneben. Das ist das Gefängnis des Schlosses. Ein Tor versperrt den Weg ins innere, doch das bekommt ihr mithilfe des Cryomoduls hochgestellt. Legt einfach einen Eisblock ins Wasser direkt unter das Tor.

IM Gefängnis entdeckt ihr jede Menge dunkle Materie. Schießt einen Pfeil in das Auge, das links von euch hinter den Gitterstäben blinzelt, damit sich die dunkle Masse auflöst. Anschließend folgt ihr dem Pfad in das Gefängnis hinein und kämpft gegen ein paar mittelstarke Gegner. Obacht, wenn ihr die Schalter in der Mitte des Flurs umlegt, öffnet ihr Zellen und befreit so manchen stärkeren Feind. In dem Fall ist es praktisch, Elektropfeile bereit zu halten, die sie lähmen und sofort entwaffnen.

Am Ende des Flures seht ihr linker Hand eine Schatztruhe (Rubine) und rechter Hand einen Durchgang und einen großen Raum voller Knochen. Sobald ihr diesen Raum betreten habt, wird sich die Tür automatisch schließen und die Knochen erwachen zum Leben.

## Stalhinox

Die einst leblosen Knochen formen das Gerüst eines Hinox. Solche Riesen kennt ihr bereits von der Oberwelt Hyrules, nur lässt dieser sich leider nicht von Elektrizität beeindrucken oder gar lähmen, Zudem habt ihr sehr wenig Platz zum rangieren, was den Kampf hektischer machen kann als nötig.

Wie üblich solltet ihr mit Pfeilen auf das Auge des Stalhinox zielen. Trefft ihr , so sackt auch dieser zusammen und lässt euch einige Sekunden zum Zuschlagen. Da ihr euch schon im Schloss Hyrule befindet, werdet ihr feststellen, dass euer Master-Schwert leuchtet und die doppelte Stärke auffährt (nämlich 60 Stärke-Punkte). Greift es euch, wenn der Stalhinox zusammensackt und ladet eine dreifache Wirbelattacke ein (Y-Knopf lange gedrückt halten). Das sollte bereits eine Menge Lebenskraft abziehen. Haltet euch ansonsten nicht zu lange zwischen seinen Beinen auf und versucht ihn rücklings anzugreifen, wenn ihr gerade nicht an sein Auge herankommt.

Habt ihr Stalhinox besiegt, dann öffnet die Schatztruhe, die nahe der Wand lagert. In ihr findet ihr den Hylia-Schild, der stolze 90 Verteidigungspunkte gutschreibt. Nun seid ihr optimal gerüstet, um der Verheerung Ganon die Stirn zu bieten.
# Die Schreine von Hateno

Die Schreine der Hateno-Gegend könnt ihr nicht alle von Anfang an finden. Der einzige Schrein, der offen in der Landschaft steht, ist der Myama-Gana-Schrein, an dem ihr auf dem Weg nach Hateno automatisch vorbei kommt.

## Myama-Gana-Schrein

Dieser Schrein ist vergleichsweise einfach zu knacken. Geht die Treppe hoch und stellt euch an das leuchtende Gebilde. Im Anschluss wird die Kamera eine andere Perspektive annehmen, bei der ihr von oben auf ein Labyrinth schaut. Eine Kugel fällt hinein und soll mithilfe des Gyro-Sensors in eurem Controller zum Ausgang dirigiert werden. Allerdings steuert ihr nicht die Kugel, sondern ihr kippt und dreht das Labyrinth.

Tipp: Ihr könnt das Labyrinth von vornherein so drehen, dass die Kugel nahe am Ausgang landet, so spart ihr euch ewiges herumrollen. Die Kugel muss dann linker Hand auf der Plattform landen und in eine Fassung Rollen. Ist die Kugel angekommen, geht auch die Tür unten auf und ihr könnt euer Zeichen einsacken.

## Tahno-A-Schrein

Dieser Schrein ist in einer Höhle im Gebirge versteckt und daher nicht gleich sichtbar. Ihr müsst in dieser Gegend gegen Kälte geschützt sein, sonst erfriert Link schnell. Reist zu dem Ort, der auf unserer Karte markiert ist und schaut nach einem Geröllhaufen in der Mauer. Den könnt ihr mit einer Bombe sprengen. Das war es auch schon, was die Herausforderung angeht – für das Finden des Schreins erhaltet ihr ein Zeichen der Bewährung.

![Hateno Schreine in der Umgebung](pics/fe81d538e1eecacbc4d1e05f6ce33f6e.jpg?fileId=2284#mimetype=image%2Fjpeg&hasPreview=true)

## Jitah-Sami-Schrein

Für diesen Schrein solltet ihr unbedingt eine ganze Menge Pfeile dabei haben. Reist notfalls vorher nach Kakariko und kauft welche. Außerdem benötigt ihr Schutz gegen Kälte. Also nehmt viele wärmende Gerichte mit Chili mit. Schaut auf unsere Karte – ihr müsst hier auf die Spitze des Bergs. Dort ist eine Gottesstatue, die von einem **Drachen-Geist namens Naydra** umschlungen wird. Der Drache ist eigentlich ihr Beschützer, wurde aber von Ganons Kräften eingenommen. Schießt mit Pfeilen auf die herausschauenden großen Augen an seinem Körper.

Mit jedem Auge, das ihr trefft, wird der Drache eine neue Position einnehmen. Zierst an der Bergspitze, die ihr am ehesten erreicht, wenn ihr die warmen Strömungen mit eurem Parasegel ausnutzt. Später verzieht er sich an den Fuß des Berges. Eilt ihm hinterher und nehmt nacheinander jedes Auge ins Visier. Klappt mit einfachen Holzpfeilen recht gut, wenn ihr euch genug Zeit zum Zielen nehmt. Denkt daran, dass ihr beim Gleiten in der Luft auf eine Zeitlupe zurückgreifen könnt, wenn ihr mit Pfeil und Bogen schießt. Das dürfte gut Zeit zum Zielen verschaffen.

Sind alle Augen vernichtet, färbt sich der Drache hellblau und kehr zur Statue zurück, Schießt noch einmal an eine beliebige Stelle des Drachen. Er wird sterben und euch eine **Naydra-Schuppe** hinterlassen. Sammelt sie auf und legt dann die Schuppe ins Wasser vor die Statue. Dadurch wird eine Geheimtür am Felsen geöffnet und ein Schrein wird freigelegt. Dort erhaltet ihr ein weiteres Zeichen der Bewährung

## Dau-Nae-Schrein

Diesen Schrein erreicht ihr erst nach einer ausgiebigen Kletter-Tour – schaut erneut auf unsere Karte. Der Schrein liegt hinter einem Wasserfall. Ihr gelangt zu ihm, wenn ihr seitlich über die Ruinen durch eine Höhle schreitet oder euch durch den Wasserfall fallen lasst und am Gestein hochklettert.

Die Aufgabe im Schrein ist nicht schwer. Ihr findet einen gigantischen Bodenschalter vor. Um ihn runter zu drücken, müsst ihr drei Schatzkisten auf ihm per Magnetmodul platzieren. Eine Kiste befindet sich im Wasser, eine auf dem Aufzug ind er Ecke des Raumes und eine auf der höheren Ebene an der Wand gegenüber. Um diese Kiste mit dem Magnetmodul zu erreichen, solltet ihr ganz nah an den Rand des Schalters treten, sonst genügt die Reichweite des Magneten nicht.

Stehen alle drei Kisten auf dem Schalter, öffnet ich eine Tür. Dort zieht ihr einen Metallkasten aus einer Kammer uns stellt ihn auf den Aufzug. Auf der anderen Seite des Stegs. Stellt euch ebenfalls auf den Aufzug und lasst euch hochfahren. Von der oberen Ebene aus könnt ihr prima auf den Kasten springen, wenn er herunter fährt. Von dort aus geht es problemlos auf die Ebene, wo ihr dem Weisen gegenübertretet und euch das Zeichen der Bewährung abholt.

## Kamu-Yuo-Schrein

Für diesen Schrein müsst ihr die Schrein-Aufgabe „Die verfluchte Statue“ lösen. Sucht dafür die Stelle auf der Karte nördlich des Hateno-Turms auf, an der mehrere Statuen in einer Schlucht stehen (siehe unsere Karte). Nach 21 Uhr leuchten die Augen einer dieser kleinen Statuen. Schießt einen Pfeil in ihr Gesicht, um den Schrein anzuheben.

Im Schrein lauft ihr die Treppe hinunter und tretet in das Innere des rotierenden Gebildes. Dort drehen sich mehrere Plattformen mit. Lasst euch von einer davon nach oben befördern und passt dabei auf, nicht von den Stachel-Kugeln getroffen zu werden. Wenn die Plattform waagerecht steht, kommt ihr auf das Zahnrad in der Mitte. Von dort aus erreicht ihr eine Schatzkiste, die in Richtung Eingang in einer Aussparung der Wand steht. Leert sie, dreht euch um und schnappt euch den Inhalt der Kiste auf der rotierenden kleineren Plattform.

Nun arbeitet euch wieder auf das Mittel-Zahnrad und achtet auf die kleinen Plattformen. Eine hat eine Treppe auf einer Seite. Springt auf diese Plattform, während sie noch seitwärts steht und arbeitet euch vorsichtig auf die Treppenseite vor, während sie langsam rotiert. Ihr braucht dafür Fingerspitzengefühl. Über die Stufen der Treppe erreicht ihr das obere Stockwerk und den Weisen, der euch mit dem Zeichen der Bewährung versorgt.



# Der Leune und die Elektropfeile

Geht zurück zum zentralen Platz des Dorfes. Dort findet ihr einen Schrein – wie
ihr den löst, erfahrt ihr im folgenden Kapitel zu den Schreinen von Ranelle.
Sprecht mit dem schlecht gelaunten Muzu. Er traut euch nicht, aber dank des
Zuspruchs von Dorephan legt auch er seine Hoffnungen in Link. Ihr werdet zum
Wassertitan Vah Ruta geschickt. Dies ist der erste echte Dungeon des Spiels.
Aber noch ist es nicht so weit, denn es braucht ein wenig Vorbereitung.

Legt die neue Rüstung der Zoras an und sprecht noch einmal mit Muzu. Er
beauftragt euch, 20 Elektropfeile zu sammeln, die ihr unbedingt benötigt. Dazu
sucht ihr ein besonders gemeines Monster namens Leune auf der Bergspitze des
Donnerhorn auf. Es ist ratsam, vor dem Aufbruch noch ein paar Mahlzeiten zu
kochen. Zum Beispiel Wild mit Zitterlingen, die euch vor Elektroschocks
bewahren. Eine Kochstelle findet ihr ganz in der Nähe, wenn ihr rechter Hand
die Treppen hoch lauft.

Es bleibt euch überlassen, ob ihr zuvor den Schrein löst (Neji-Yoma-Schrein).
Den Lösungsweg findet ihr im folgenden Schein-Kapitel. Solltet ihr Amiibos
besitzen, dann wäre jetzt ein guter Zeitpunkt die einzusetzen, was ein mal am
Tag möglich ist, Aktiviert einfach das Amiibo-Modul eures Shiekah-Steins und
haltet euer Amiibo über den rechten Analogstick. Nun könnt ihr den Effekt
aktivieren wie jedes andere Fertigkeiten-Modul, müsst dazu aber auf eine flache
Fläche zielen. Schatztruhen fallen vom Himmel, und zwar mit unterschiedlichen
Inhalten, abhängig vom verwendeten Amiibo. Mit ein wenig Glück sind dort
bereits ein paar Elektropfeile enthalten.

Den Donnerhorn-Berg erreicht ihr dank der neuen Rüstung relativ leicht, denn
sie ermöglicht euch, auf Wasserfällen in irrer Geschwindigkeit aufwärts zu
schwimmen. Verlasst das Zora-Dorf über die Treppen in östlicher Richtung und
peilt die Wasserfälle an. Direkt an einem Wasserfall drückt ihr den A-Knopf, um
dank der neuen Rüstung nach oben zu zischen. Es schadet nicht unterwegs ein
paar Fische aus den Wasserströmen einzusammeln und abseits der Wasserfälle nach
Pilzen und Gräsern zu suchen.Tut dies, bis ihr das Plateau auf dem Berg
erreicht. Von dort aus ist es nicht mehr weit bis zur Spitze, wo der Zentaur
Leune seine Kreise dreht.

Es macht keinen Sinn, den Zentaur Leune anzugreifen. In eurem jetzigen Zustand
seid ihr viel zu schwach, um es mit ihm aufzunehmen. Er haut euch mit ein bis
zwei Schlägen zu Brei, wenn ihr euch ihm nähert. Darum haltet lieber Abstand
und schleicht in der Gegend herum. An allen erdenklichen Orten wie an
Baumstämmen und im Gras, findet ihr Elektropfeile, die er verschossen hat.
Schaut dazu auch auf dem Hügel südlich von euch nach weiteren Pfeilen. Solltet
ihr mutig sein, könnt ihr den Zentaur aus der Ferne provozieren, Er wird ein
paar Pfeile abschießen, Die könnt ihr aber nur einsammeln, wenn er einen Felsen
oder einen Baum trifft. Ganz schön gefährlich für euch. Schlicht die Gegend
nach weiteren Pfeilen absuchen (vor allem an Bäumen) ist einfacher. Habt ihr
die Chance, dann macht ein gutes Foto vom Leunen – das könnt ihr für eine der
Nebenaufgaben gebrauchen.

Habt ihr 20 Elektropfeile beisammen, werdet ihr aufgefordert an den
nahegelegenen östlichen Stausee zu kommen. Geht dazu auf die Erhebung an der
Bergspitze und gleitet mit dem Parasegel zu Sidon herunter. Sprecht mit ihm und
macht euch für eine Action-Sequenz bereit.

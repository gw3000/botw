# Der erste Dungeon: Wasser-Titan Vah Ruta

Sidon wird auf dem Wasser schwimmen und Link auf dem Rücken tragen. Dadurch
habt ihr eine gute Geschwindigkeit, um den Titan zu umkreisen, Ganons Mächte
werden den Titan bis aufs Blut verteidigen und Eisklötze auf euch feuern. Die
könnt ihr mit eigenen Eisklötzen, die ihr über das Cryo-Modul erschafft,
abwehren, sofern euer Timing passt. Benötigt ein wenig Eingewöhnung, klappt
aber gut, wenn ihr die gegnerischen Eisbrocken relativ nah an euch herankommen
lasst. Alternativ könnt ihr die Blöcke mit Pfeilen oder im richtigen Moment
auch mit einem Schwert abwehren.

Habt ihr einen der Angriffe überstanden, so wird Sidon einlenken und ganz nahe
an den Titanen heranschwimmen. Ihr passiert dabei Wasserfälle an der Seite des
Titanen. **Zischt an einem Wasserfall hoch** und lasst euch in die Luft schleudern.
Holt in der Luft Pfeil und Bogen heraus, wodurch eine Zeitlupe aktiviert wird,
nehmt **Elektropfeile** zur Hand und zielt nun auf die rosafarbene leuchtende Kugel
vor euch. Trefft ihr sie, wird sie erlischen.

Dies müsst ihr nun bei allen vier Wasserfällen des Titanen wiederholen, damit
er freigegeben wird. Allerdings wehrt sich der Titan mit jedem Anlauf heftiger.
Bewahrt die Geduld, dann kommt ihr durch! Danach könnt ihr Vah Ruta endlich
betreten – viel Spaß im ersten echten Dungeon des Spiels.

Wenn ihr den Titan betreten habt, solltet ihr eure Ausrüstung prüfen. Habt ihr
Mahlzeiten dabei? Einen ganzen Stoß (normaler) Pfeile? Genug starke Waffen?
Wenn nicht, dann teleportiert euch nach Kakariko und geht einkaufen. Keine
Angst, ihr könnt euch problemlos wieder zurück zu Vah Ruta teleportieren.

Schaut euch im Dungeon um, Ihr werdet überall eine schwarze zähe Masse
entdecken. Die solltet ihr nicht berühren, da sie euch schadet, Schaut lieber,
ob ihr an der Masse ein Auge entdeckt. Schießt ihr es mit einem Pfeil ab, wird
die Masse verschwinden und den Weg freigeben.

Auf dem Stockwerk in dem ihr euch befindet, ist nicht viel los. Ihr findet ein
verschlossenes Tor vor, das ein wenig unter Wasser steht. Aktiviert das
Cryo-Modul und stemmt das verschlossene Gittertor mit einem Eisblock nach oben.
Dahinter ist ein Terminal das euch die **Karte des Vah Ruta Dungeons** vermittelt.
Ihr könnt die Karte mit der Minus-Taste einsehen. Interessant: Die Karte ist
dreidimensional und kann in ihrer Ansicht gedreht werden. In ihr verzeichnet
sind mehrere Punkte, die den Standort mehrerer **Kontrollsiegel** anzeigen. Ihr
müsst alle Kontrollsiegel aktivieren, um zum Boss zu gelangen.

Ebenfalls interessant: Vah Ruta hat die Form eines Elefanten, und ihr könnt
dessen Rüssel nach Belieben verstellen, sogar in mehreren Stufen. Dies werdet
ihr noch gut gebrauchen können. Aber eines nach dem anderen.

Schaut im Eingangssaal nach einer Kurbel mit einem Zahnrad. Die Kurbel könnt
ihr mit dem Magnetmodul drehen. Ein Terminal wird dadurch aus dem Wasser
hochgefahren. Aktiviert es, um das **erste Kontrollsiegel** zu lösen.

Nun verlasst den Raum durch das Tor, das gegenüber vom Eingang liegt. An der
Außenbalustrade trefft ihr auf mehr böse Masse. Vernichtet das Auge und stapft
nach oben in das nächste Stockwerk. Hier seht ihr zwei Zahnräder, in deren
Mitte diverse Dinge vorgehen. Im kleinere ist zum Beispiel ein Terminal, nur
verhindert das Wasser, dass Link es aktivieren kann. Wartet, bis der Terminal
in Bodennähe ist, dann stoppt einfach den Wasserzufluss, indem ihr an der
Fontäne links einen Eisbock erschafft. Das Zahnrad wird dadurch angehalten und
das Wasser gesenkt. Aktiviert dort das **zweite Kontrollsiegel**.

Wenn ihr den Eisblock beseitigt, läuft das Zahnrad wieder. Ihr könnt an einem
der Wasserschaufeln an dessen Rand weitere dunkle Masse beseitigen und eine
Schatzkiste erreichen, wenn ihr zeitig aufspringt. Nun wird es zeit, das
zweitem größere Zahnrad zu bewegen. Ruft dazu eure Karte auf (Minus-Knopf) und
bewegt den Rüssel des Elefanten auf die vierte Einraststelle von oben. Dadurch
spritzt der Rüssel Wasser ins Innere des Dungeons und treibt das Zahnrad an.

Achtet nun auf das Innere des Rades. Immer dann, wenn die Kugel nach unten
rutscht, öffnet sich das Gitter in der Mitte, das den Zugang zu einem Terminal
versperrt. Ihr müsst die Kugel zum richtigen Zeitpunkt mit dem Stasis-Modul
anhalten, damit sie länger in der Fassung verweilt als sonst. So könnt ihr in
die Mitte des Zahnrades treten (bzw. vom Steg aus schweben) und aktiviert
**Kontrollsiegel Nr.3**. Auch am großen Zahnrad klebt dunkle Masse, die eine
Schatzkiste begräbt. Die solltet ihr beseitigen und die Kiste plündern.

Da das Wasser des Rüssels in das Becken im unteren Stockwerk fließt, habt ihr
jetzt einen prima Zugang nach oben, Ihr müsst nur das Wasser aufwärts
schwimmen, was dank eurer Zora-Rüstung kein Problem darstellt. Oben angelangt,
geht ihr den Flur entlang. Fahrt den Rüssel des Elefanten über eure Karte weder
ganz herunter und lauft am Rüssel herunter. Veresst nicht, die dunkle Masse
durch einen Schuss auf das Auge zu beseitigen. So erreicht ihr eine weitere
Kiste. Nun fahrt den Rüssel zurück auf die höchste Position und geht in den
Gang zurück, von dem ihr gestartet seid.

Dreht euch wieder zum Rüssel und fahrt ihn über die Dungeon-Karte erneut ganz
nach unten. Nun muss Link an die Spitze des Rüssels gleiten. Habt ihr sie
erreicht, dreht ihr den Rüssel wieder nach oben (nicht auf die höchste
Einstellung, dritte oder vierte Position genügt) und balanciert auf die
Unterseite. Aktiviert dort das **vierte Kontrollsiegel**!

Schwebt nun auf den Kopf des Elefanten und vernichtet dort dunkle Masse, um
eine Schatzkiste freizulegen, von dort aus könnt ihr euch wieder ins Innere
fallen lassen, wo das **fünfte Kontrollsiegel** durch eine **Feuerwand** versperrt
wird. Einfache Sache: Dreht über die Karte erneut den Rüssel, sodass er sein
Wasser auf die Flammen spritzt (fünfte Einrastposition von oben). Aktiviert das
Siegel, hüpft eine Ebene nach unten zur letzten Schatzkiste und geht dann in
den Eingangssaal zurück. Dort führt ein Weg in einen Saal leicht nach unten zu
einem großen Terminal. Aktiviert es erst, wenn ihr sicher seid, genug
Mahlzeiten und Pfeile dabei zu haben – 20 sollten es schon sein. Ein paar
Eispfeile oder Feuerpfeile können nicht schaden. Notfalls teleportiert euch
erst in eine Stadt und geht einkaufen. Dann aktiviert das große Terminal und
macht euch für den Boss des Dungeons bereit.

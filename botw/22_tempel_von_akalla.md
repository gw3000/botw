
# Die Schreine von Akkala

In den Landstrichen von Akkala sind einige Schreine schwer zu finden oder schwer zu erreichen. Hier unsere Übersicht samt Lageplänen und Lösungswegen.

## Jizu-Kafui-Schrein

Unweit westlich vom Südstall Akkalas, direkt neben der Straßengabelung, befindet dch dieser Schrein. In seinem Inneren findet ihr zuerst ein Stachelbett vor, das ihr mithilfe der Bewegungssensoren eures Controllers verschieben könnt, wenn ihr das Terminal davor aktiviert. Verschiebt die Stacheln einfach durch Kippen des Controllers Ist der Weg für euch frei, dann springt hinüber und schreitet durch die Tür dahinter.

Im nächsten Raum seht ihr mehrere Laserstrahlen und einen Kristallschalter. Schlagt auf den Schalter, damit die Bodenplatten vor euch rotieren. Dies gibt euch eine Zeit lang Deckung, also rennt um die Bodenplatten herum zur Tür – zumindest bis sich die Platten wieder gesenkt haben. Ihr könnt den Kristallschalter aus der Ferne mit einem Pfeil treffen, um ihn erneut zu aktivieren.

Im dritten Raum stellt ihr euch an das Terminal, um ein weiteres Kippspiel zu bedienen. Dieses Mal sollt ihr der Kugeln in jeweilige Fassungen dirigieren, damit sie Schalter drücken. Ist nicht all zu schwer, nur etwas fummelig. Alle Schalter müssen gleichzeitig aktiviert werden. Ist das geschafft, öffnet sich die Tür zum Weisen, der euch mit einem Zeichen der Bewährung belohnt.

## Dahi-Shiino -Schrein

Dieser Schrein liegt östlich des Turms auf der Ostseite eines Sees, ist aber problemlos auffindbar. Die Aufgabe ist simpel: Besiegt einmal mehr einen Mini-Wächter. Sollte inzwischen kein Hindernis für euch sein. Holt euch im Anschluss ein Zeichen der Bewährung ab.

![Schreine der Akalla Gegend](pics/3564a41535f81a43f3a7b4798cb4bf3e.jpg)

## Ka-Muh-Schrein

Obwohl euer Detektor wie irre piept, könnt ihr den Schrein, der sich am Ende einer Inselkette ganz im Osten des Landstrichs befindet, nicht sehen. Kein Wunder, er ist unterirdisch versteckt. Arbeitet euch von der südlichsten Insel aus zur nördlichsten Insel namens Tinnela vor, indem ihr über Holzbrücken schreitet und warme Aufwinde mit eurem Parasegel ausnutzt. Inmitten der nördlichsten Insel der Inselkette liegt eine Platte auf dem Boden. Die könnt ihr ganz einfach bewegen, wenn ihr einen Oktorok-Ballon in die Hand nehmt und daran befestigt.

Solche Ballons erhaltet ihr von so ziemlich jedem getöteten Oktorok – das sind die nervigen Monster, die immer Steine spucken. Legt den Oktorok-Ballon auf die Platte. Der Ballon wird festkleben und nach oben schweben, wodurch die Platte einen unterirdischen gang freigibt. Dort findet ihr den Schrein.

Im Inneren des Ka-Muh-Schreins ist eine Waage zu sehen, zu deren linker Hand ein Fass steht. Schaut nun auf der rechten Seite des Raums nach ganz oben. Da steht ein großer Quader auf einem Holzvorsprung. Schießt einen Pfeil auf jedes der Seile, das die Plattform trägt, damit der Quader nach unten fällt. Nun müsst ihr den Quader nur noch per Magnetmodul aus großen Höhe auf die zweite Hand der Waage fallen lassen, während ihr selbst auf der anderen Seite steht. So werdet ihr hochgeschleudert und könnt mit dem Parasegel auf einer der oberen Plattformen schweben, Auf einer Seite ist eine Schatzkiste, auf der anderen der Weise, der euch ein Zeichen der Bewährung verschafft.

## Ritah-Zumo-Schrein

Auch dieser Schrein ist noch nicht sichtbar, wenn ihr seinen Standort erreicht. Er befindet sich in der Mitte einer Halbinsel-Spirale ganz im Osten am angrenzenden Meer. Dort ind er Mitte ist bislang nur eine Plattform mit einer kleinen Kugelfassung, Die zugehörige Kugel müsst ihr vom breiten Fuß der Spirale auflesen und zur Mitte bringen, Unterwegs bekommt ihr es mit vielen Gegnern zu tun. Legt die Kugel nach langer Reise in die Fassung der Plattform, damit der Schrein hochfährt. Im Inneren könnt ihr euch einfach das nächste Zeichen der Bewährung abholen.
[The Legend of Zelda: Breath of the Wild Bild 1 | Datum: 14.03.2017]

![Schreine der Akalla Gegend 2](pics/f627e4cf8252f019f1ebc4242ef12eb9.jpg)

## Kah-Tosa Schrein

Dieser Schrein befindet sich direkt neben dem Stall von Ost-Akkala (siehe unsere Karte).

Hier könnt ihr per Bewegungssensor Minigolfen, wenn ihr euch an den Terminal auf der rechten Seite stellt. Kippt ihr euren Controller nach rechts, dann holt der Schläger aus. Kippt ihn dann von rechts nach links, um die Kugel mit Schwung über den Steg zu schießen. Die Kugel soll in der Fassung ganz links landen. Schafft ihr das, so wird eine bewegliche Plattform erscheinen, die euch zum Weisen fährt. Ihr könnt noch eine Waffe abstauben, wenn ihr nicht das Zeichen abholt, sondern hinter dem Weisen ein weiteres Schlägelspiel bedient. Es ist ein wenig kniffliger, weil ihr die Kugel mit dem Schläger schräg anspielen müsst, um sie im Bogen zum Ziel zu schießen. Bedenkt dabei, dass ihr euch den Controller drehen könnt . Dann dreht sich auch die Ausrichtung des Schlägers.

## Toh-Karo-Schrein

Ganz im Nordosten des Akkala-Gebiets befindet sich eine riesige Festung namens Irrland, die einen Irrgarten beinhaltet. Ihr erreicht sie, indem ihr euch im Norden des Landstrichs auf eine der hohen Klippen stellt und den langen Weg per Parasegel hinüber gleitet.

Der Irrgarten ist gefährlich! Ihr solltet ihn nur betreten, wenn ihr mindestens drei oder vier antike Pfeile (besser mehr) oder zwei gute antike Nahkampfwaffen bei euch habt, denn schon am Ende der zentralen Halle wartet ein Wächter, um den ihr euch gleich kümmern solltet. Haut ihm einen antiken Pfeil ins Auge oder bekämpft ihn mit eurer antiken Nahkampfwaffe.

Nun bleibt es euch überlassen, ob ihr den Irrgarten plündern oder direkt zum Schrein gehen wollt. In den Ausläufern des Irrgarten sind nämlich einige Schatzkisten versteckt, die wertvolle Schätze beinhalten. In der Nähe des Schreins befindet sich zudem ein besonderer Rüstungsgegenstand, den ihr unbedingt mitnehmen solltet. Die Reise ist allerdings nicht ungefährlich.

Darum solltet ihr zuerst den Toh-Karo Schrein aufsuchen, damit ihr euch immer wieder hierher teleportieren könnt. Den kürzesten Weg durch den Irrgarten haben wir euch aufgezeichnet. Ihr werdet unterwegs Metallblockaden per Magnetmodul aus dem Weg schaffen müssen. Haltet zudem bei den aufwärts führenden Treppen nach einer Mulde Ausschau, aus der Dornen herausgucken. Diese Dornen könnt ihr mit einem Feuer- oder Bombenpfeil verbrennen um eine Leiter freizulegen. Folgt ihr der Leiter, erreicht ihr eine Schatzkiste mit einem starken Feuerschwert.
[The Legend of Zelda: Breath of the Wild Bild 1 | Datum: 14.03.2017]

Betretet den Schrein sofort, wenn ihr ihn erreicht, damit ihr euch jederzeit zurück teleportieren könnt. In dessen Inneren gibt es nichts zu tun, außer das Abholen eines Zeichens der Bewährung. Nun schaut euch das Loch an, das vor dem Schrein zwischen den zwei Treppen Wind nach oben bläst. Lasst euch hinabfallen und aktiviert erst kurz vor dem Boden euer Segel, um den Sturz abzufangen. Lauft nun den Gang entlang in eine große Halle, bis zum Rand gefüllt mit Wächtern, von denen die meisten inaktiv sind. Diese Wächter könnt und sollt ihr plündern, um antike Bauteile abzustauben. Haltet dabei immer einen antiken Pfeil im Anschlag, denn einige wenige Wächter sind noch aktiv.

Erst wenn ihr alle geplündert habt, die etwas hergeben (einige lassen sich nicht plündern, wendet ihr euch der Schatzkiste in der Mitte des Raums zu. Wenn ihr sie öffnet, erhaltet ihr das Diamant-Stirnband, ein sehr wertvolles Utensil, das den von Wächtern angerichteten Schaden reduziert. Nein, der Schaden ihrer Laser wird nicht absorbiert, nur verringert, und das sollte bei euch nun die Alarmglocken schrillen lassen, denn das Öffnen der Schatzkisten aktiviert einige der Wächter, die nun aus allen Kanonen auf euch feuern, Flüchtet in den Gang hinein, aus dem ihr gekommen seid und lasst euch an dessen Ende per Parasegel nach oben tragen. Nun könnt ihr die Insel verlassen

![Toh-Karo-Schrein](pics/1cd6c85f198df5eff01252cefa0a08c6.jpg)

## Zuna-Kai-Schrein

Ein Schrein ganz im Nordwesten des Akkala-Gebiets. Er gehört zu einer Nebenaufgabe, die euch von Jerrin im Institut von Akkala erteilt wird (Aufgabe „Linkes Auge des Totenkopfes“). Der Schrein sitzt auf einem extrem hohen Sockel innerhalb des Totenkopf-Sees, den ihr nicht hochklettern könnt. Darum könnt ihr ihn nur erreichen, wenn ihr nördlich des Sees auf die Berge steigt und einen sehr hohen Punkt sucht, um per Parasegel zu ihm zu schweben. Das Erreichen des Schreins ist Herausforderung genug, darum erwartet euch im Inneren kein weiteres Rätsel.

## Tsutsuna-Nima-Schrein

Für diesen Schrein muss eine Nebenaufgabe mit einem Drachengeist gelöst werden. Wir reichen die Lösung dieses Schreins bald nach.
# Das Vergessene Plateau

Link erwacht im Intro aus einem langen Schlaf und wird von einer lieblichen Stimme begrüßt. Völlig ahnungslos, warum er geweckt wurde, steht er auf und empfängt an einer Art Terminal den **Shiekah Stein**, einen magischen Helfer, der ihm in vielerlei Situationen nützlich sein wird. Nun seid ihr an der Reihe, Link zu steuern.

Lauft mit Link den Gang hinein und öffnet alle Kisten, die vor euch liegen, mit dem A-Knopf. Ihr findet darin die Grundausrüstung eurer Kleidung. Legt sie im Inventar an, indem ihr die Plus-Taste betätigt und im Inventar die Kleidungsstücke aussucht.

Nun lauft geradeaus zum großen Stein vor euch. An ihm kann Link hinaufklettern. Oben angekommen, entdeckt ihr saftig grüne Wiesen, die an einem sonnigen Tag über ein weites Land hinweg schauen. Dieses Land nennt sich Hyrule.

Erneut ist die liebliche Stimme zu hören. Sie ruft Link an einen Ort, der auf eurer Mini-Karte rechts unten auf dem Bildschirm verzeichnet wird. Dadurch wir die **erste Hauptaufgabe gestartet. „Die ferne Stimme“.**

Der Weg führt euch rechter Hand abwärts, immer in die Richtung, die die Markierung auf eurer Mini-Karte anzeigt. Unterwegs könnt ihr bereits einige Utensilien einsammeln. Zum Beispiel einen Zweig, der als Rute eine erste, primitive Waffe abgibt. Haltet die Augen offen, zum Beispiel nach Äpfeln auf Bäumen – ihr müsst nur ein kurzes Stück an einem Stamm hochklettern und könnt dann reife Äpfel mit dem A-Knopf aufsammeln. Auch Pilze und Eicheln liegen womöglich am Wegesrand. Sammelt sie auf, in dem ihr den A-Knopf drückt, wenn ihr vor ihnen steht.

Der Weg führt euch zu einem **alten Mann** in dunklen Gewändern, der sich an einer Feuerstelle wärmt. Sprecht ihn an, damit er euch erzählt, worum es in der Geschichte geht. Direkt danach könnt ihr den gebratenen Apfel, der nahe des Feuers liegt, aufsammeln – eure erste gekochte Mahlzeit. Hebt ihn euch auf, denn er kann eure Lebenskraft wieder auffrischen, wenn ihr ihn in Zeiten der Not verspeist.

Hinter dem Lagerfeuer des alten Mannes liegt ein **Fackelstock**. Sammelt ihn auf, in dem ihr daneben steht und den A-Knopf drückt. Der alte Mann wird euch darauf ansprechen. Antwortet nach Belieben. Nun könnt ihr zwischen zwei Waffen hin und her wechseln, nämlich zwischen der Fackel und der Rute. Geht ganz einfach: Drückt schlicht auf den Pfeiltasten des linken Joy-Cons nach rechts und haltet diesen Knopf fest, damit ein Menü erscheint. Nun blättert ihr mit dem rechten Analog-Stick nach links oder rechts, um durch die Waffen zu schalten.

Lauft nun weiter bergab, Kurz hinter dem Lager ist ein Baumstrumpf, in dem eine Holzfäller-Axt versenkt wurde. Nehmt sie mit – sie ist sehr wertvoll. Aber schaltet lieber vorerst auf eine andere Waffe (wie ihr das macht, seht ihr einen Absatz weiter oben), denn Waffen sind in diesem Spiel zerbrechlich und ihr braucht die Axt später noch.

Nun immer weiter bergab Richtung Ruinen, bis ihr dem ersten Gegner über den Weg lauft. Ein roter **Bokblin** steht da und ist angriffslustig. Haut ihm eins mit einer eurer Waffen auf die Rübe. Dazu drückt ihr mehrmals den Y-Knopf. Wollt ihr den Blick immer auf den Gegner gerichtet haben, dann haltet während des Angriffs den ZL-Knopf hinten am linken Joy-Con gedrückt. Das erleichtert den Angriff auch etwas, weil Link seinem Gegner folgt.

Ist der Bokblin besiegt, so verpufft er in einer Wolke und hinterlässt Gegenstände. Allem voran ein Bokblin-Horn und einen Bokblin-Hauer. Sammelt beides ein.

Die Markierung auf eure Mini-Karte in der rechten unteren Ecke des Bildschirms zeigt auf einen Ort nordöstlich von euch. Lauft also in diese Richtung und haut unterwegs Gegner zu Brei - zum Beispiel blaue Schleimklumpen. Sammelt ein, was von ihnen übrigbleibt und lauft weiter zum angezeigten Ort.

Ein weiterer Bokblin stellt sich euch in den Weg. Er ist mit Schwert und Schild bewaffnet, das den Kampf etwas schwerer macht, Wenn ihr euch auf seine Bewegungen konzentriert (ZL-Knopf festhalten) und eure Angriffe gut abstimmt, ist er kein Problem, Habt ihr ihn besiegt, dann hinterlässt er einen Schild und ein einfaches Schwert. Beide solltet ihr aufsammeln. Das Schild wird Link nun immer vor sich halten, wenn ihr euch im Kampf auf einen Gegner konzentriert, also wenn ihr den ZL-Knopf gedrückt haltet. So könnt ihr gewisse Wurfgeschosse oder Nahkampf-Attacken der Gegner blockieren.

Ihr seht nun die Reste eines Gebäudes, das in einem Hügel zu stecken scheint. In der Aussparung des Hügels ist wieder einer dieser **Terminal-Schalter**. Dieses Terminal könnt ihr mit dem Schiekah-Stein aktivieren. Tut ihr das, wird ein **riesiger Turm** aus dem Hügel herauswachsen. Wobei: Es geht nicht nur um einen Turm. In ganz Hyrule wachsen nun solche Türme aus dem Boden. Ihr könnt sie noch nicht erreichen, aber sie werden euch sehr nützlich sein, wenn ihr an ihnen vorbei kommt.

Von nun an könnt ihr mit der Minus-Taste auf eine Übersichtskarte schalten, die euch die Struktur des Landes Hyrule zeigt. Bisher ist nur das Plateau detailliert aufgezeigt. Alle anderen Länder erscheinen erst voll auf der Karte, wenn ihr den jeweils zugehörigen Shiekah-Turm aktiviert. Aber so weit seid ihr noch lange nicht. Eines nach dem anderen. Durch das Erscheinen der Türme habt ihr die Hauptaufgabe **„Die ferne Stimme“ abgeschlossen.**

Steigt nun den Turm hinab, in dem ihr euch durch die Öffnung im Boden fallen lasst. Ihr landet auf eine Balustrade. Arbeitet euch von Balustrade zu Balustrade herunter, damit ihr sicher am Boden ankommt. Unten am Boden entdeckt ihr gleich, wie der **alte Mann mit einer Art Segel zu euch hinunter gleitet.** Er wird euch weitere Auszüge der Geschichte Hyrules erzählen. Hört aufmerksam zu!

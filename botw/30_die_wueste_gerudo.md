
# Die Wüste Gerudo

Wenn ihr unserer Lösung gefolgt seid, habt ihr nun zwei Dungeons gemeistert und
dadurch zwei Sonderfertigkeiten erhalten. Ein Gebet, das eure Lebenskraft
auffrischt, wenn ihr zu sterben droht, sowie eine Goronen-Barriere, die einige
Angriffe aktiv abwehrt, sofern ihr ein Schild vor euch tragt (also beim
Gebrauch einer Einhand-Waffe den den ZL-Knopf drückt).

Damit seid ihr für das nächste Kapitel in der Gerudo-Wüste gut gewappnet.
Teleportiert euch zurück auf das Plateau, auf dem ihr das Spiel begonnen habt,
beziehungsweise direkt zum Tuomi-Soke-Schrein. Hier wollt ihr euch nicht lange
aufhalten, denn auf der Bergspitze ist es noch immer kalt.

Lauft ein Stück nach Süden, bis ihr zu einem hohen Teil der Randmauer des
Plateaus kommt und schaut nach Westen zu einem Berg. Rechts davon seht ihr den
Turm der Wüste. Das ist euer nächstes Ziel, allerdings erreicht ihr den Turm
geschickter, wenn ihr nicht direkt zu ihm segelt (ihr landet dann in einer
tiefen Schlucht), sondern erst den näher gelegenen Berg links daneben als
Zwischenstation ansteuert.

Segelt zum besagten Berg, steigt ihn empor und wendet euch dann nach Norden, um
Richtung Turm zu segeln. Bevor ihr diesen erreicht, werdet ihr noch ein paar
Felswände emporklettern, an denen ihr hölzerne Stege entdeckt.
![Gerudo](pics/dd5cdb83c66368611f56063b8e3a1a5f.jpg)

Der Turm der Wüste ist von einem kleinen Sumpf umgeben, in dem ihr sofort
versinkt, Versucht also gar nicht erst, hinüber zu schwimmen. Auch das Gleiten
von einem Berg aus klappt nicht immer, da die Windrichtung immer wieder
wechselt.

Schaut euch stattdessen um. Auf einer Seite es Sumpfes sind Metall-Kisten.
Schnappt sie euch mit dem Magnetmodul und baut euch damit eine kleine Brücke zu
den drei großen Felsblöcken inmitten des Sumpfes. Ein paar der Metallkisten
werden komplett versinken, aber das macht nichts, wenn ihr sie aufeinander
stapelt. So erreicht ihr zumindest einen der großen Felsblöcke, an dem ihr
hochklettert und über die anderen Blöcke zum Turm segelt. Klettert hinauf und
aktiviert den Turm, um die Übersichtskarte des Gebiets zu erhalten.

Vom Turm der Wüste aus segelt ihr nach Norden, damit ihr den Pfad erreicht, der
durch eine Schlucht inmitten der Felswände führt. Auf dem Weg angelangt,
schlagt ihr nach Westen ein, bis ihr den Stall der Schlucht erreicht. Der Stall
hilft euch leider nicht bei der Reise, denn ein Pferd zu verwenden wird euch
nichts nützen, schon wenige Meter weiter westwärts beginnt die Wüste, die euer
Ross aufgrund einer großen Stufe nicht erreichen kann.

Bevor ihr in die Wüste eilt, solltet ihr euch auf die Hitze vorbereiten, die
ihr nicht mit der Anti-Feuer-Rüstung abhalten könnt. Lauft ein paar mal in der
Schlucht auf und ab, bis ihr blaue Schmetterlinge (Frostflügler) erwischt. Mit
Geschick fangt ihr auch ein paar Frostlibellen. Ist euch die Suche zu
anstrengend, dann fragt mal Terri, den wandernden Händler mit der roten Nase,
der gelegentlich am Stall vorbeigeht. Er hat Frostflügler dabei. Kocht euch
daraus Kühlungs-Medizin (Rezept: Frostflügler oder Frostlibelle + Moblin Horn +
Moblin Hauer). Lauft anschließend zu Fuß nach Westen, bis ihr die Wüste
erreicht.

Schaut genau auf den Boden. Er ist zwar schwer auszumachen, aber da hebt sich
ein Weg vom hellen Sand ab. Folgt ihm, bis ihr eine Oase erreicht an der sich
der Wüstenbasar befindet.

Hier könnt ihr notfalls kurz einkaufen. Folgt dann dem Weg weiter nach Westen
(vergesst nicht, die Kühlungs-Medizin einzunehmen), bis ihr eine kleine Festung
erreicht. Dies ist die Gerudo-Stadt, die ihr nicht betreten könnt, da hier nur
Frauen hinein dürfen. Betretet lieber rechts neben dem Eingang den
Guko-Chise-Schrein, damit ihr euch immer wieder an diesen Ort teleportieren
könnt. Sprecht anschließend mit dem Mann, der vor dem Schein umherwandert und
einen Weg in die Gerudo-Stadt sucht. Er erzählt euch von einem Mann, der eine
Lösung für sein Problem gefunden hat. Dadurch startet die Hauptaufgabe „Männer
verboten“.

Hier vor der Gerudo-Stadt könnt ihr aktuell nichts ausrichten. Kehrt zurück zum
Wüstenbasar, der auf dem Weg lag. Sucht dort das Gasthaus und steigt bis ganz
oben auf das Dach, wo ihr eine Person namens Vilia trefft. Sie wird nach ihrer
Schönheit fragen. Sagt ihr, dass sie schön ist. Zur Belohnung macht Vilia euch
ein Angebot: 600 Rubine für ein Set mit Frauenkleidung. Diese Kleidung benötigt
ihr unbedingt! Verkauft norfalls ein paar Edelsteine bei einem Händler, um an
das Budget zu kommen.

Kauft die Kleidung und legt Hose, Hemd und Schleier des Gerudo-Sets an. Mit
dieser Verkleidung habt ihr nun Zutritt zur Gerudo-Stadt. Reist also wieder
westwärts und betretet die Stadt. Damit wird die Hauptaufgabe „Männer verboten“
abgeschlossen.

In der Gerudo-Stadt könnt ihr euch in Ruhe umsehen, solange ihr diese Kleidung
tragt. Hier gibt es einige Händler mit nützlichen Kochzutaten oder auch einem
Repertoire an Pfeilen. Schlagt ruhig zu. Sucht nun das große Hauptgebäude am
Ende der Stadt auf, wo die Herrscherin Riju auf ihrem Thron verweilt. Sprecht
mit ihr, um die Hauptaufgabe „Donnertitan Vah Naboris“ zu starten.

Leider werdet ihr nicht gleich zum Titanen gelassen. Link soll sich zuerst
durch eine andere Aufgabe bewähren, die sich um den Donnerhelm dreht. Dazu
müsst ihr das Kalzer-Tal aufsuchen, das nordöstlich der Stadt liegt. Das ist
ein ganz schön langer Weg, den ihr schneller bewältigen könnt, wenn ihr eine
Sandrobbe mietet. Sprecht im Westen der Stadt mit einer Frau namens Tekla, um
die Aufgabe fortzusetzen und sucht anschließend im Nordwesten der Stadt den
Sandrobbenverleih auf. 20 Rubine kostet der Spaß, und alle was ihr benötigt,
ist ein Schild, auf dem ihr reitet, während ihr von der Robbe gezogen werdet.
Es ist daher ratsam ein Einhänder-Schwert oder eine andere Einhand-Waffe
auszurüsten.

Lasst euch von der Sandrobbe nach Norden ziehen – schaut auf unsere Karte. Euer
Ziel liegt am Ende einer langgezogenen Schlucht. Eure Robbe wird euch schon
vorher verlassen, da der Boden felsiger wird.
![der Weg zur Yiga-Festung](pics/ce627f311a0ecbc2ebc3109b711a2d21.jpg)

Macht euch auf gelegentliche Angriffe plötzlich auftauchender Yiga gefasst.
Nehmt euch Pfeil-und Bogen zur Hilfe, denn sie sind flink, und lasst euch Zeit
bei der Bekämpfung. Eine Flucht hilft euch nicht. Wandert ansonsten weiter bis
zum Ende der Schlucht, an dem eine Art Altar steht. Dahinter sehr ihr mehrere
Wandteppiche, die rundherum am Felsen herunterhängen.



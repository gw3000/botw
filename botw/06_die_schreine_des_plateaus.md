# Die vier Schreine des Plateaus

Der alte Mann erzählt euch, wie eine böse Macht, genannt „Verheerung Ganon“ das Land verwüstete. Link würde gerne das Schloss aufsuchen, in dem die Verheerung Ganon wütet, doch das ist weit entfernt und das Plateau ist vorerst unüberwindbar, da es hoch über den anderen Landstrichen steht. Der alte Mann verspricht euch ein Parasegel als Belohnung, mit dem ihr in neue Landstiche hinabsegeln könnt, sofern ihr ihm Schätze aus einem **Schrein** bergt. Der Schrein ist nicht weit entfernt und an einem gelblichen Leuchten zu erkennen. Dadurch beginnt die **zweite Hauptaufgabe „Eine vergessene Welt“.**

Zwischen euch und dem Schrein liegt ein kleiner See. Lauft linker Hand um den See herum – so erreicht ihr den Schrein am schnellsten. Am Eingang des Schreins ist wieder so ein Terminal, der sich über den Shiekah-Stein aktivieren lässt. Tut das und stellt euch nun auf die runde plattform dahiter, damit Link in den Schrein hinabfahren kann.

## Der Mah-Ounu-Schrein

Dies ist der erste extrem vielen Schreinen in Hyrule. Jeder Schrein beinhaltet ein Rätsel, das ihr lösen sollt, um ein sogenanntes Zeichen der Bewährung zu bekommen. Bevor ihr zu den wirklich schweren Rätseln kommt, müsst ihr mit den Kräften umgehen lernen, die ihr braucht um Rätsel in zukunft zu lösen, In diesem Schrein erhaltet ihr die erste dieser Kräfte.

Stellt euch an den Terminal und aktiviert es mit dem Shiekah-Stein. Die Datenübertragung beginnt und schaltet in eurem Stein das **Magnetmodul** frei. Mithilfe dieses Moduls könnt ihr Gegenstände aus gewissen Metallen schweben lassen. Drückt dazu die L-Taste. Ein Fadenkreuz wird erscheinen. Haltet ihr das Fadenkreuz über einen Gegenstand, der sich über Magnetismus verschieben lässt, so leuchtet er auf. Dann braucht ihr nur noch den A-Knopf zu drücken, um ihn unter Kontrolle zu haben.

Probiert das gleich mal mit einer der beiden Bodenplatten in der Mitte des Raumes aus. Wenn ihr eine der Bodenplatten mit dem Magnetmodul erfasst und mithilfe des rechten Analogsticks hochhebt und wegschiebt, seht ihr einen Gang im Boden. Lasst euch da hineinfallen und lauft weiter. Etwas weiter kommt ihr an eine Stelle, in der eine Wand steht, Einer der Blöcke hebt sich durch seine Farbe hervor. Den könnt ihr mit dem Magnetmodul erfassen und herausziehen.

Ihr müsst an dem Mauerstück hochklettern (notfalls hochspringen über die X-Taste) und lauft dann weiter. Ein Nano-Wächter stellt sich euch in den Weg. Macht ihn platt und nehmt mit, was davon übrig bleibt – eine Antike Schraube. Sie ist sehr wertvoll, also mitnehmen!

Etwas weiter erreicht ihr einen Steg, der über einen Abgrund führt. Fallt nicht herunter, sonst kostet euch das ein Herzchen eurer Lebenskraft. Um über die Lücke zu kommen, müsst ihr euch mit dem Magnetmodul die kleine Plattform schnappen und sie mit Feingefühl über die Lücke legen. Lauft darüber zum Tor. Schaut dann nach links, da ist eine Kiste. Stellt euch dran und drückt den A-Knopf, um sie zu öffnen, dann erhaltet ihr einen Reisebogen. Sehr nützlich zum Verschießen von Pfeilen! Nun weiter durch das Tor. Auch die Flügeltür des Tores lässt sich über Magnetismus bewegen beziehungsweise öffnen. Kein Problem für euch, zieht die Flügeltür eifach mit dem Magnetmodul nach innen auf.

Glückwunsch, ihr wisst nun, wie man das Magnetmodul einsetzt. Stellt euch an den Zielpunkt des Schreins, wo ein Weiser namens Mah-Ounu auf euch wartet. Er überreicht euch ein wertvolles **„Zeichen der Bewährung“.** Nun habt ihr den ersten Schrein gelöst und werdet wieder an dessen Eingang gebracht. Wenn ihr euch den Schrein anschaut, werdet ihr feststellen, dass er nicht mehr gelblich leuchtet, sondern blau. Das zeigt, dass ihr ihn gelöst habt.

## Zurück auf den Turm

Der alte Mann wird vor dem Schrein wieder auftauchen. Entgegen seinem Versprechen überreicht er euch nicht das Parasegel, sondern möchte, dass ihr erst drei weitere Schreine besucht und deren Rätsel löst. Um zu sehen, wo diese sich befinden, sollt ihr auf den Turm zurückkehren. Da braucht ihr aber nicht hinzulaufen. Ihr könnt euch jederzeit **auf den Turm teleportieren,** wenn ihr die Karte öffnet (Minus-Knopf auf dem linken Joy-Con), den Cursor auf den Turm legt und den A-Knopf drückt, im folgenden Menü erscheint die Option zum Teleportieren.

Oben auf dem Turm verrät euch der alte Mann, dass ihr den Shiekah-Stein als Fernglas benutzen könnt, wenn ihr den R3-Knopf drückt (also den rechten Analogstick in das Joy-Con hineindrücken). Stellt euch auf die blaue Markierung und schaut euch mit einem Shiekah-Fernglas nach weiteren Schreinen um, die orange beziehungsweise gelblich leuchten. Markiert sie mit dem Shiekah-Stein, indem ihr das Fadenkreuz direkt darüber haltet und den A-Knopf drückt. Dann erscheint eine Lichtsäule darüber und der Ort ist auf euerer Karte markiert. Wenn ihr die drei Schreine nicht findet, dann schaut auf unsere Karte, wir haben euch die Orte der drei nächsten Schreine markiert.

[![](https://img.gameswelt.de/public/images/201703/2424e7bd5a30a57f6234ff3550b73ff3.jpg)](https://img.gameswelt.de/public/images/201703/c458ce1736181d5211088bde0fd7220e.jpg)

Die Reihenfolge, in der ihr die Schreine angeht ist euch überlassen, aber aus praktischen Gründen empfehlen wir, zuerst den **Ja-Bashif-Schrein** südöstlich vom Turm aufzusuchen. Den erreicht ihr problemlos.

ACHTUNG: Der Ja-Bashif-Schrein befindet sich am oberen Ende eines eingemäuerten Bereichs. Innerhalb dieser Mauern befinden sich Überreste uralter Kriegsmaschinen, sogenannte Wächter. Leider funktionieren einige davon noch, auch wenn sie sich nicht fortbewegen können, ihre Laserstrahlen sind verheerend! Ihr müsst euch immer wieder hinter Mauern verstecken, wenn sie feuern. Da wird es sehr schwer, über die letzte Mauer zu klettern, denn der Zugang zum Schrein ist versperrt.

**TIPP:** Ihr könnt den Ja-Bashif-Schrein viel bequemer erreichen, wenn ihr um das Gemäuer herum lauft und direkt vor dem Schrein rücklings über die Mauer klettert.

Aktiviert nun wieder den Schalter am terminal mit euren Shiekah-Stein und betretet den Schrein über den Aufzug.

## Ja -Bashif-Schrein

Wenn ihr in diesem Schrein an den Terminal geht und euren Shiekah-Stein einsetzt, erhaltet ihr das Modul für Bomben. Da ihr nun zwei Module habt, könnt ihr die Funktion wechseln, wenn ihr auf den Pfeilen des linken Joycons (das quasi-Steuerkreuz) nach oben drückt und das erwünschte Fertigkeiten-Modul mit dem rechten Analogstick auswählt. In diesem Schrein werdet ihr jedoch nur das Bomben-Modul benötigen.

Ihr habt dank des Moduls eine unendliche Anzahl Bomben, allerdings könnt ihr nicht ständig welche werfen. Es ist immer nur eine erlaubt, und wenn sie explodiert, müsst ihr ein paar Sekunden warten, bis die nächste Bombe greifbar wird. Dabei bleibt es euch überlassen, wann ihr eine Bombe sprengt. Mit dem L-Knopf nehmt ihr eine Bombe hervor, mit dem R-Knopf werft ihr sie und mit dem L-Knopf lasst ihr eine geworfene Bombe hochgehen, nebenbei: Ihr könnt im Modul-Menü auch eckige Bomben wählen. Wenn ihr sie absetzt, rollen sie nicht weg.

Alles was ihr nun tun müsst, ist die versperrenden Steinblöcke auf dem Weg zu sprengen, damit ihr durchkommt. Etwas weiter findet ihr linker Hand weitere Blöcke. Sprengt ihr sie weg, kommt eine Kiste zum Vorschein, aus der ihr ein großes Schwert entnehmen könnt - einen Reise-Zweihänder.

Euer Weg führt dann rechts entlang über die bewegliche Plattform. Hier müsst ihr ein wenig abschätzen, wann ihr die Bombe werft, damit ihr die vordere Wand abreißt. Ist aber keine Zauberei, nach spätestens zwei Würfen habt ihr den Kniff raus.

Die Geschichte mit dem Sprengen ist schnell erlernt, daher ist es auch kein Problem, zum Weisen Ja-Baschif vorzutreten, kassiert bei ihm ein weiteres Zeichen der Bewährung und lasst euch wieder zum Eingang des Schreins bringen.

## Vorbereitung auf die nächsten beiden Schreine

Für die nächsten beiden Schreine braucht ihr ein wenig Planung und Vorbereitung. Bewegt euch vom Schrein aus ein wenig nach Südosten Richtung Zitadelle. Ein Stück davor ist eine Schlucht samt einem Turm, auf dem ein Bokblin Wache hält.

Nun wird es knifflig. Geht so nah heran, dass der Bokblin euch entdeckt und anfängt, Pfeile auf euch zu schießen. Bewegt euch dabei damit er euch nicht trifft, sammelt aber flink die Pfeile ein, die er daneben schießt. Achtung, er ruft womöglich bald Verstärkung, dann solltet ihr euch aus dem Staub machen, sofern ihr euch nicht gleich mit dreien von der Sorte anlegen wollt. Das ist durchaus möglich, aber gefährlich.

Habt ihr zehn oder mehr Pfeile beisammen, dann geht nach Süden in Richtung Hylia-Berg. Wenn es westlich der Zitadelle wieder bergauf geht, könnt ihr in der Ferne einen schmalen Durchgang erkennen, der in eine verschneite Landschaft führt. Direkt vor dem Durchgang am Felsen wächst **Chili**. Nehmt davon alles mit und sucht an der Felswand in beiden Richtungen nach mehr Chili. Habt ihr einige **Chilischoten** eingesackt, dann verlasst diese Gegend (geht nicht in den Schnee, da ist es zu kalt für euch) und setzt euch ab an den nördlichen Teil des Totenflusses. Da ist ein Wäldchen.

In diesem kleinen Wald laufen vor allem vormittags Wildschweine herum. Ihr solltet welche Erlegen, um Fleisch zu sammeln. Passt nur auf, denn in einer bestimmten Schlucht kann ein **Steinriese** auftauchen – dem seid ihr noch nicht gewachsen. Lasst euch also auf keinen Kampf ein. Ihr wollt jagen, also schaut lieber nach Wildscheinen. Habt ihr welche entdeckt, dann drückt den linken Analogstick ein, um in den Schleich-Modus zu kommen - so kommt ihr leichter in die Nähe von wilden Tieren. Sie dürfen euch nur nicht sehen, sonst hauen sie ab.

Versucht nun ein Wildschwein zu erlegen, Zielt dabei möglichst auf den Kopf, denn dann kann unter Umständen ein einziger Pfeil ausreichen, um das Schwein zu erlegen. Ansonsten braucht ihr zwei, allerdings schreckt ein Treffer das Tier auf. Noch einmal treffen ist schwer, wenn sie herumrennen.

Weniges stilvoll, aber effektiver wäre eine Jagd mit Bomben. Da reicht dann auch eine einzige Bombe zum Erlegen. Erlegt mindestens drei Wildschweine, damit ihr drei Stück Wild im Inventar habt. Ein paar Früchte, Gräser und Pilze können ebenfalls nicht schaden. Habt ihr all das, dann verlasst den Wald und begebt euch in den Südosten, in die Nähe des Berges, auf dem der Wa-Modai-Schrein steht – also östlich der Zitadelle. In der Tiefebene vor dem Berg ist eine Jägdhütte samt einem Lagerfeuer, auf dem ein flacher Kochtopf steht.

Stellt euch direkt vor den Kochtopf und öffnet euer Inventar mit der Plus-Taste. Sucht euch ein Stück Wild aus drückt den A-Knopf und wählt die Option „in die Hand nehmen“. Nun sucht euch noch eine Chili-Schote und einen Apfel aus. Drückt ihr B, kommt ihr wieder zu Link vor dem, Kochtopf, und er hält alle ausgesuchten Zutaten in der Hand. Drückt ihr nun den A-Knopf, so wirft er die Zutaten in den Topf und lässt sie Schmoren. Herauskommen sollte ein Gericht, das euch eine Zeit lang vor Kälte schützt. Bereitet noch mehr solcher gerichte zu – immer mit dem scharfen Chili, denn das ist die Zutat für den Wärme-Effekt. Habt ihr genug scharfe Mahlzeiten, um euch insgesamt acht bis neun Minuten zu wärmen, dann seid ihr vorbereitet.

Lauft nun weiter nach Süden, nehmt die Holzfäller-Axt aus eurem Waffen. Inventar und hackt die Bäume vor der Schlucht um. Mindestens einer sollte so umfallen, dass dessen Stamm eine Brücke über die Schlucht schlägt. Lauft vorsichtig über den Stamm auf die andere Seite.

Kümmert euch um die Bokblins, die euch angreifen und schaut dann an der Felswand nach oben. Ihr solltet an einer Stelle mehrere Felsvorsprünge entdecken. Klettert nun an der Felswand hoch und pausieert auf jedem der Vorsprünge, damit sich eure Ausdauer regeneriert. Auf dem Weg könnt ihr gerne die Pilze an der Wand auflesen. Seid ihr heil oben angekommen,steht ihr direkt vor dem nächsten Schrein.

## Wa-Modai Schrein

Wenn ihr den Wa-Modai Schrein betreten habt, erhaltet ihr am Terminal das nächste Fertigkeiten-Modul: Das **Stasismodul.** Es ermöglicht euch, die Zeit für einen bestimmten Gegenstand einzufrieren. Zumindest für ein paar Sekunden. Das könnt ihr gleich ausprobieren, denn wegen des drehenden Zahnrades rotiert die Brücke, über die ihr schreiten sollt. Aktiviert also das Stasismodul per Druck auf den L-Knopf, nehmt das große Zahnrad links in das Fadenkreuz und haltet es mit dem A-Knopf in dem Moment an, wenn die Brücke waagerecht steht. Nun habt ihr einige Sekunden um über die Brücke zu gehen, bevor der Effekt erlischt.

Vor euch etwas weiter rechts ist eine Rampe. Dort rollt eine riesige Kugel herunter. Wollt ihr die Rampe hochgehen, müsst ihr die Kugel an der oberen Hälfte der Rampe zum Stehen bringen – natürlich mit dem Stasismodul. Alles nicht so schwer, nicht wahr? Am oberen Ende der Rampe findet ihr ein Reiseschild in der Kiste. Euer Weg führt aber links entlang zu einem Übergang an der eine Kugel ruhend in einer Mulde steht.

Benutzt das Stasismodul auf der Kugel und schlagt sie dann in Laufrichtung mit eurer Waffe. Im Bestfall mit dem Hammer, den ihr zuvor aus der Kiste holen konntet. Gegenstände, die in der Stase geschlagen werden, bewegen sich beim Stopp der Stase mit weit mehr Energie als normal. Darum wird sich auch die schwere Kugel bewegen, die Link normalerweise nicht vom Fleck bekommen hätte.

Nun geht zu Wa-Modai und holt euch das dritte Symbol ab. Ihr landet danach am Ausgang des Schreins. Mit dem Stasemodul könnt ihr nun die Felskugel neben dem Schrein bewegen. Einfach einfrieren und draufschlagen. In der Mulde ist eine Schatzkiste. Räumt sie aus.

Nun bleibt nur noch ein Schrein übrig. Den erreicht ihr, wenn ihr von diesem Schrein aus weiter nach Westen in die Kälte lauft. Bevor ihr im Schnee erfriert, solltet ihr euch allerdings eine der scharfen Mahlzeiten gönnen, die Link von innen wärmt. Sokann er der Kälte ohne Schaden widerstehen. Zumindest eine Zeit lang.

## Der Toumi-Soke-Schrein

Den letzten Schrein erreicht ihr, wenn ihr vom dritten Schein aus nach Westen geht und euch vom Fels aus vorsichtig hinunter in die Talschneise fallen lasst. Hier ist es bitterkalt, da kommt eure scharfe Mahlzeit gut zur Geltung. Haltet euch in der Schneise südlich, wo es bergauf geht, damit ihr schon bald wieder Richtung Westen am Felsen hochklettern könnt. Lauft dann immer weiter nach Westen. Ein längerer Marsch steht euch bevor, den ihr am besten bei Tag bestreitet.

Schon bald werdet ihr einen Hügel mit dem Schrein entdecken. Sollte euch der Kälteschutz eures scharfen Gerichts bis dahin ausgehen, dann legt nach, damit Link nicht erfriert. Haltet euch aber vom Wasser fern, denn das ist viel zu kalt für den Abenteurer. Erklimmt den Berg und betretet den Schrein.

Im Inneren des Schreins erhaltet ihr das letzte Fertigkeitenmodul – das Cryomodul, mit dem ihr Einblöcke aus Wasserflächen hervorzaubert. Diese schmelzen nicht. Ihr könnt sie als Brücke benutzen.

Zielt mit dem aktivierten Cryomodul auf das Wasser, um per Druck auf den A-Knopf einen Eisblock zu generieren, der euch weiterbringt – ihr müsst nur draufklettern. Lauft weiter zum verschlossenen Tor. Das bekommt ihr geöffnet, wenn ihr direkt unter dem Tor einen Eisblock im Wasser generiert. Der Eisblock drückt das Tor nach oben, sodass ihr durchtreten könnt. Hinter dem Nano-Wächter, den ihr plattmacht, ist eine ähnliche Aufgabe zu erledigen, nur braucht ihr dieses Mal zwei Eisblöcke – einen Block an jedem Ende, um die Plattform hoch zu drücken. Klettert hinauf und holt euch das letzte Zeichen der Bewährung.

Am Ausgang des Schreins wird sich der alte Mann noch einmal melden. Er verlangt, dass ihr euch an dem Punkt trefft, an dem sich gedachte Linien zwischen den Schreinen kreuzen. Einfache Sache, er will, dass ihr zur **Zitadelle der Zeit** kommt.

Betretet die Zitadelle un geht zu aller erst zur großen Göttinen-Statue am ende der Halle. Dort könnt ihr beten, worauf ihr die vier Zeichen der Bewährung, die ihr aus den Schreinen habt, eintauschen könnt. Entweder gegen einen Herzcontainer oder gegen einen Ausdauercontainer. Für den Anfang ist das Herz nützlicher, aber es bleibt euch überlassen.

Danach wird euch der alte Mann vom Dach des Gebäudes zurufen. Geht zu ihm, indem ihr an der Außenwand der Zitadelle eine Leiter hochklettert. Hoch oben im Turm wird er euch seine wahre Identität verraten. Der alte Mann ist in Wirklichkeit nur ein Geist. Der Geist des letzten Königs von Hyrule. Hört euch seine Geschichte an, dann wird er euch das **Parasegel überlassen**, mit dem ihr das Vergessene Plateau endlich verlassen könnt.

Der König wird euch in das nächste Gebiet Hyrules schicken und euch empfehlen, eine alte Bekannte namens Impa im Dorf Kakariko aufzusuchen.

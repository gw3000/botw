# Die Schreine der Ranelle-Gegend

Wir gehen die Schreine dieser Gegend in der Reihenfolge ab, in der ihr sie
antrefft, wenn ihr von Süden aus in die Ranelle-Gegend kommt. Den ersten findet
ihr gleich beim Überqueren der Grenze im Sumpfgebiet.

Bei den Zoras könnt ihr die ersten **permanent haltbaren Waffen** des Spiels
erwerben. Es geht hierbei um den Zora-Speer. Die Licht-Lanze und die
Zeremonienlanze. Diese drei Spezialwaffen findet ihr im Rahmen der Hauptaufgabe
(Licht-Lanze als Belohnung beim König) oder in und auf dem Weg zu Schreinen
(Zora-Speer, Zeremonienlanze). Diese Waffen können zwar durch Gebrauch brechen,
aber ihr könnt sie in der Schmiede bei den Zoras reparieren oder nachbauen
lassen. Kostet allerdings eine Kleinigkeit, unter anderem braucht ihr
Feuersteine.

Für die Reparatur der extra starken Lichtlanze müsst ihr zudem einen Diamanten
springen lassen. Aber aufgepasst: Ihr könnt diese Waffen nur reparieren lassen,
wenn sie kurz davor stehen, kaputt zu gehen – sie leuchten dann rot im
Waffenmenü. Ist eine der Waffen völlig hinüber, so wird euch in der Schmiede
lediglich ein Replikat der Waffe hergestellt. Das macht im Kampf keinen
Unterschied, nur ist es dann eben nicht mehr das Original.

## Data Kusu-Schrein

Gleich am Anfang des Schreins seht ihr ein Becken, in dem Kugeln schwimmen.
Nehmt euch per Magnetmodul die Schale am Boden des Beckens und holt damit eine
Kugel aus dem Becken heraus, Nun lasst die Kugel in das Gitter linker Hand
hineinfallen, damit es in die Fassung fällt.

Nun könnt ihr rechts entlang durchgehen zu einem weiteren Becken. Nehmt die
**große Schale** per Magnetmodul mit und fischt eine Kugel aus dem zweiten Becken.
Platziert die Kugel nun auf dem Dach des anderen Gatters auf der rechten Seite
– passt auf, dass sie nicht herunterrollt. Nun legt ihr die große Schale in das
Becken, und zwar so, dass der große Schalter am Boden des Beckens
heruntergedrückt wird. Dadurch wird das Wasser aus beiden Becken abgelassen.
Hinter jedem der Becken befindet sich ein Durchgang, den ihr über dessen Grund
erreicht, Hinter dem linken Becken ist eine Schatzkiste, hinter dem rechten
Becken der Ausgang, bzw. der Weg zum Weisen Data Kusu, der euch ein weiteres
Zeichen spendiert.

## Sem-Rat-Schrein

Dieser Schrein ist einfach zu lösen. Wenn ihr das Hebel-Rad linker Hand
anschubst und nur um eine Vierteldrehung bewegt, wird der Laser vor euch auf
seinem Stein ein mal rotieren. Kommt er wieder an seine Ursprungsposition, wird
das Wasser im Becken vor euch entweder eingelassen oder abgelassen, je nach
dem, was gerade Phase ist.

Der Laser braucht euch eigentlich nicht zu interessieren. Dreht bei vollem
Becken an dem Hebel-Rad und dann schwimmt in Windeseile auf die andere Seite
des Beckens, bevor das Wasser abgelassen wird. Geht ein Stück weiter. Nun
schnappt euch das schwere Fass und legt es auf den Schalter im Becken links von
euch. Dadurch öffnet sich das Gatter, das zum Weisen führt. Holt euch ein
weiteres Zeichen ab.

![Schreine der Ranell Gegend Teil 1](pics/1f83679d866da87faa5534177de6774c.jpg)

## Kaya Miwa-Schrein

Noch ein sehr einfacher Schrein. Ihr müsst hier immer nur Eisblöcke im Wasser
generieren, damit ihr vorwärts kommt. Ein Paar Mini-Wächter stehen im Weg, die
bekommt ihr aber mit Pfeilen relativ einfach beseitigt. Am letzten Stück fließt
regelmäßig ein Floß entlang. Theoretisch könnt ihr das ignorieren. Das Gatter
auf der linken Seite hebt ihr wie üblich, indem ihr einen Eisblock darunter
setzt. Arbeitet euch mit Eisblöcken zu den Plattformen ganz hinten vor und
schwebt von dort aus per Parasegel hinüber zum Weisen, der euch das Zeichen
gibt.

## Sao-Kohi-Schrein

Zu diesem Schrein könnt ihr direkt vom Ranelle-Turm aus schweben. Im Inneren
erwartet euch eine Kraftprobe gegen einen Mini-Wächter. Unterschätzt ihn nicht,
denn in seiner letzten Phase wird er einen sehr starken Laser aufladen, In
dieser Phase müsst ihr ihn umhauen, bevor er feuert. In der Zwischenzeit
versteckt euch hinter den Säulen und bleibt bei fehlender Deckung ständig in
Bewegung. Die Waffen, die er hinterlässt, sind stark und nützlich.

## Rukko-Ma-Schrein

Ein Schrein, umgeben von gefährlichen Stacheln. Um hineinzukommen, müsst ihr
euch den freien Weg durch die Stacheln suchen, der ein wenig wie ein rundes
Labyrinth aufgebaut ist. Innen erwartet euch eine interessante Aufgabe namens
„Die fünf Fackeln“. Feuerpfeile sind hier sehr praktisch.

Euer Ziel ist es, alle fünf Fackeln am Klotz in der Mitte des Beckens zum
Brennen zu bekommen. Ihr könnt mit Feuerpfeilen nachhelfen, allerdings müsst
ihr auch dafür sorgen, dass kein Feuer durch die Fontäne gelöscht wird. Links
vom Becken ist ein weiterer Stein mit Kristall-Armen. Haut ihr mit einem
Schwert auf die Arme, dann dreht sich auch der Klotz in der Mitte.

Geht wie folgt vor, damit es schnell geht: Zwei Fackeln sind am Anfang nah
beieinander. Schießt einen Feuerpfeil drauf, damit beide brennen. Schaut nun
auf den kleineren Klotz und seine Kristall-Arme. Zwei schauen nach oben bzw.
unten, nur einer zur Seite. Schlagt auf den Arm, der seitwärts gerichtet ist.
Die Fontäne schießt euch nun Wasser entgegen. Schießt einen weiteren Feuerpfeil
auf die letzte Fackel, schon sollten alle fünf brennen.

Nebenbei: Wenn ihr am anderen Ende des Saals die Ranken an der Wand in Brand
setzt, fallen Schatzkisten herunter, da die Bretter, auf denen sie stehen, aus
Holz sind.

![Schreine der Ranellgegend Teil 2](pics/ac497bf993f986a9e4b054d81724958f.jpg)

## Shi-Yota Schrein

Sprecht mit dem großen Vogel-Wesen, das am Strand auf einem Stein sitzt und
Akkordeon spielt. Das Wesen heißt Kashiwa und erzählt von einer Legende, bzw.
eine Aufgabe namens „Zähmer des Windes“. Die Aufgabe an sich ist simpel. Um den
Schrein hochzufahren, sollt ihr auf eine Plattform herabschweben und dafür die
Winde nutzen, die um die Steininsel vor euch wehen. Nur ist der Wind, der euch
tragen soll, noch nicht stark genug.

Schaut euch um. In der Nähe der zentralen Stein-Insel sind kleinere Inseln. Bei
manchen entdeckt ihr Geröll-Haufen. Sprengt sie mit Bomben, um den Wind
durchzulassen. Der wichtigste Geröllhaufen ist am schwersten zu erreichen. Er
steckt direkt an der Unterseite der zentralen Insel. Aber ihr könnt den Wind
zum Sprengen nutzen. Klettert auf die Steininsel nach ganz oben, direkt über
den Bogen, unter dem das Geröll klemmt. Stellt euch an den Rand und lasst eine
Bombe sachte herunterrollen, Der Wind wird sie unter den Bogen tragen. Sprengt
ihr zum rechten Zeitpunkt, dann knackt ihr das Geröll.

Ist der Wind stark genug, um euch zur Plattform zu tragen, dann seht ihr eine
kleine Zwischensequenz, in der die Plattform aufleuchtet. ACHTUNG: Ihr müsst
wirklich bis zum allerletzten Moment zur Plattform schweben!. Ihr müsst also
noch das Parasegel in der Hand haben, wenn ihr aufsetzt. Wenn nicht, wird der
Schrein nicht hochgefahren.

Im Schrein wartet keine weitere Aufgabe mehr. Ihr könnt euch einfach das
Zeichen abholen.

## Da Kikii-Schrein

Diesen Schrein könnt ihr erst lösen, wenn ihr das Vah Ruta-Dungeon geknackt
habt. Sprecht mit dem Kind auf dem zentralen Platz des Zora-Dorfes. Es erzählt
euch von einem Lied. Das Lied des Beckenfestes. Begebt euch anschließend auf
die Brücke im Westen des Zora-Dorfes, die auf die Klippen führt. Dort steht ein
Dorfbewohner, der euch von einer Lanze erzählt, die aus versehen von der Brücke
gefallen ist. Begebt euch nun direkt unter die Brücke generiert Eisschollen per
Cryo-Modul. Nun aktiviert euer Magnetmodul und drückt den L-Knopf, um es
einsatzbereit zu machen. So entdeckt ihr die Zeremonienlanze im Wasser. Ihr
müsst mithilfe der Eisschollen relativ nah heran, bevor ihr die Lanze per
Magnetmodul bergen könnt.

Diese Lanze ist sehr wichtig Schnappt sie euch und geht dann westlich vom Zora
Dorf zum Sela-Wasserfall. In dessen Teich ist eine Plattform. Zischt mithilfe
der Zora-Rüstung den Wasserfall hoch. Nun rüstet die gefundene Zeremonienlanze
aus und schwebt zur Plattform hinunter. Kurz bevor ihr aufsetzt, drückt ihr den
Angriffsknopf (Y-Knopf) um einen Sprunghieb mit der Lanze zu machen, Steckt ihr
in die Plattform, wird der Schrein aktiviert und fährt aus dem Boden hoch.

Im Schrein wartet keine weitere Aufgabe auf euch, also holt euch schlicht das
Zeichen vom Weisen.

## Neji-Yoma-Schrein

Der Neji-Yoma-Schrein liegt inmitten des Zora-Dorfes. Hier findet ihr eine
lange Schräge vor, auf der viele Kugeln nach unten rollen und in einem Abgrund
landen. Rennt die Schräge bis ganz nach oben und lasst euch nicht überrollen.
Ganz oben ist eine Kugel mit Leuchtmarkierungen. Ihr könnt sie mit dem
Stasemodul in der Zeit einfrieren und dann mit einer Waffe schlagen, damit sie
sich bewegt. Dirigiert sie auf diese Weise die Schräge hinunter. Passt nur auf,
dass sie nicht den Abgrund hinunter gleitet. Dort wo die Schräge wieder
horizontal wird, könnt ihr die Kugel nach vorne stoßen, damit sie in einer
Fassung landet. So öffnet ihr die Tür zum Weisen des Schreins, der euch ein
Zeichen spendiert.

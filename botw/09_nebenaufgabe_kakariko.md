# Nebenaufgaben in Kakariko

Hier sind alle Nebenaufgaben in Kakariko und auf dem Weg dorthin aufgelistet. Ihr müsst sie nicht machen, aber sie sind allesamt schnell erledigt und werden gut belohnt. Drei spezielle braucht ihr, um einen versteckten Schrein zu finden.

## Der Wildfang
Sprecht mit Maronus, der vor dem Stall der Zwillingsberge steht, Der Stall ist auf dem Weg nach Kakariko nicht zu übersehen, Er wettet, dass ihr es nicht schafft, ein Wildpferd in unter 2 Minuten Zeit zu fangen und zu ihm zu bringen. Wildpferde befinden sich hinter dem Stall auf der Wiese. Versucht, ein geflecktes Pferd zu fangen,die sind ruhiger und schneller zähmbar. Schleicht weuch dafür von hinten an und sitzt auf. Mit dem L-Knopf könnt ihr das Pferd tätscheln und beruhigen.

## Die einzig wahren Rasseln
Ebenfalls auf dem Weg nach Kakariko, wenn auch etwas näher am Dorf, wartet ein baumartiges Wesen am Wegesrand. Es heißt Maronus und hat seine geliebten Rasseln verloren. Die Rasseln findet ihr etwas weiter nördlich, Da ist ein Boblin-Lager. Beseitigt alle Bokblins und entnehmt die Rasseln aus der Schatzkiste. Wenn ihr sie Maronus wiederbriungt, wird er euch im Austausch gegen Krog-Samen **mehr Inventar-Plätze** verschaffen. Ihr werdet Maronus zukünftig öfter treffen, also sammelt weiter Krog Samen, auch nach diesem Treffen.

## große Räuberkönig
Sprecht Domidak beim Stall der Zwillingsberge an und händigt ihm 100 Rubine aus, um diese Nebenaufgabe zu starten. Zur gesuchten Höhle kommt jhr, wenn ihr vom Stall aus nach Süden über die Brücke lauftund euch stets weiter nach Südden begebt, bis an den untersten Zipfel dieses Bundeslandes. Dort liegt der Schatz hinter brüchigem Geröll in einer kleinen Höhle.

## Der brennende Pfeil
Sucht den Lebensmittelladen neben Impas Haus auf, sobald die Hauptmission“Impa. Die Weise“ abgeschlossen ist. Sprecht mit der Verkäuferin Cerola. Auf ihre bitte hin zündet ihr die vier Fackeln hinter der Göttinnenstatue an, die gegenüber von Impas Haus steht. Dazu braucht ihr feuerpfeile. Habt ihr keine, dann könnt ihr euch mit normalen Pfeilen behelfen, die ihr an einem Lagerfeuer anzündet.

## Entwischte Hühner
In der Nähe von Impas haus ist ein Hühnergehege. Ein mann namens Cado steht davor Er vermisst die meisten seiner zehn Hühner, die überall in der Stadt verstreut sind. Sammelt sie nacheinander ein und werft sie in das Gehege. Aufgepasst: Ein Huhn befindet sich auf dem Dach eines hauses, ein anders innerhalb eines eingezäunten Beetes mit empfindlichen Bäumen. Der Gärtner schimpft mit euch, wenn ihr das Beet betreten wollt. Wartet alsoab , bis er einen Spaziergang macht. Hat er sein beet nicht mehr im Blick,könnt ihr über den Zaun klettern. Den Standort aller Hühner findet ihr auf unserer Übersichtskarte.

![die enwtischten Hühner](pics/5bffa5e5c4e88b78cc55f84143ae5630.jpg?fileId=2229#mimetype=image%2Fjpeg&hasPreview=true)

## Kokos Küche
Tagsüber (wenn es nicht regner) steht der kleine Junge Koko am Kochtopf in der Nähe des Lebensmittelladens. Er möchte Koche, doch ihm fehlen Zutaten, die ihr schnell besorgen könnt, diese Nebenaufgabe besteht aus vier Unter-Aufgaben, bei der unterschiedliche Zutaten benötigt und Koko ausgehändigt werden, Dafür erhaltet ihr ein Exemplar von gericht, das er für seine Schwester kocht.

### Teil 1
Spurkarotte (findet ihr im Lebensmittelladen)

### Teil 2
Ziegenbutter (findet ihr im Lebensmittelladen)

### Teil 3
Ein Stück Wild (habt ihr keins im Inventar, dann erlegt ein Wildschein in der Nähe des Feenbrunnens nördlich des Dorfes

### Teil 4
Ausdauerhonig (ist schwerer zu finden, hängt zufllig an bestimmten Bäumen. Im schlimmsten Fall reitet schon mal Richtung Stadt hateno. Auf dem Weg ist auf jeden Fall ein Baum mit Honig.

## Ein Spielkamerad für Priko
Sprecht Priko in der Zeit zwischen 12 und 16 uhr an. Sie will mit euch Fangen spielen, Lasst sie rennen und fangt sie möglichst schnell wieder ein.

## Die Quelle der großen Fee
Nördlich des Dorfes ist ein Hügel mit einem Schrein, Lauft ihr am Schrein nach rechts in den Wald hinein, entdeckt ihr eine Feenquelle. Untersucht ihr die Quelle, so fragt eine große Hand nach 100 Rubinen zum Wiedererlangen der Feenkräfte. Gebt sie ihr, Die Fee wird die Quelle öffnen und euch sämtliche Rüstungsteile, die sich aufwerten lassen, verbessern.

## Die legendäre Feenquelle
Für diese Aufgabe müsst ihr zwei Bedingungen erfüllt haben: Erstens, ihr müsst nebenaufgabe „Dei Quelle der großen Fee“ gelöst haben. Die zweite Bedingung: Ihr müsst in der Stadt Hateno gewesen sein und dort im Institut eine Aufwertung eures Shiekah-Steins erhalten haben, welches den Stein in eine Art Kamera verwandelt. Sprecht nun mit Kanis im Nordosten Kakarikos, Er hätte gern ein Foto der Feenquelle. Geht also zur Quelle und fotografiert sie. Zeigt Kanis das Foto.

## Tanzende Lichter
Für diese Nebenaufgabe müsst ihr die Missionen „Entwischte Hühner“ und „Legendäre Feenquelle“ gelöst haben – für legendäre Feenquelle müsst ihr aber erst in Hateno gewesen sein, um euren Shiekah-Stein um ein Foto-Modul aufzuwerten. Schaut nun nach 22 Uhr im Süden Kakarikos nach einer Frau namens Himba, die in einem Haus auf ihrem Bett sitzt. Sie schwärmt von Glühwürmchen und möchte, dass ihr fünf Glühwürmchen zu ihr bringt.

Bei gutem Wetter findet ihr Glühwürmchen vor allem in der Nähe des Baches in Kakariko, aber auch auf dem Hügel der Feenquelle, wenn ihr Richtung Stadt schaut. Ihr müsst euch den Glühwürmchen sehr leise und langsam nähern, sonst fliegen sie weg, bevor ihr welche einsammeln könnt. Habt ihr fünf beisammen, dann bringt sie zu Himba ins Zelt und nehmt sie über das Inventar in die Hand (ähnlich wie beim Kochen). Lasst sie in ihrem Haus frei herumfliegen.

## Das gestohlene Juwel
Für diese Nebenaufgabe müsst ihr drei andere Aufgaben gelöst haben, die erst nach dem Besuch in Hateno samt Aufwertung eures Shiekah-Steins möglich ist, nämlich die Aufgaben „Entwischte Hühner“, „Legendäre Feenquelle“ und „Tanzende Lichter“. Begebt euch anschließend in Impas Haus, wo ihr feststellt, dass ihre geschätzte Kugel gestohlen wurde. Nach einer Zwischensequenz kommt ihr wieder in die Stadt. Beobachtet, wie eine der Wachen vor Impas Haus um etwa 22 Uhr ihren Posten verlässt und auf den Hügel nördlich der Stadt geht. Folgt ihm unauffällig, bis er eine Holzbrücke überquert und jemanden auffordert, sich zu zeigen. Dann geht zu ihm für eine weitere Zwischensequenz und einen Kampf mit einem unbekannten Samurai. Besiegt ihr ihn, erhaltet ihr die Kugel zurück und könnt sie vor Ort in eine Fassung im Boden stecken, die einen weiteren Schrein anhebt.
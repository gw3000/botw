
# Schleichen in der Yiga-Festung

Hier am Ende der Schlucht im Kalzer-Tal befindet sich der Eingang zur
Yiga-Festung. Um hinein zu gelangen, solltet ihr die Wandteppiche, die vom
Felsen herabhängen, in Brand setzen. Dahinter verbergen sich in der Regel
Schatzkisten, aber auch ein Durchgang, der in den Berg hinein führt.

Innerhalb der Yiga-Festung müsst ihr vorsichtig und leise sein, denn die Wachen
der Yiga-Krieger sind stark und in der Überzahl. Wenn ihr lautlos an den Wachen
vorbeikommt, erspart ihr euch heftige Kämpfe. Ein gutes Hilfsmittel dafür sind
Schwertbananen. Nicht umsonst hinterlassen sterbende Yiga-Krieger stets welche.
Sie stehen offenbar auf die gelbe Frucht.

Ihr könnt die Wachen dieser Festung mithilfe von geworfenen Schwertbananen in
eine Richtung locken und ablenken. WICHTIG: Lasst auf dem Weg hinein die Finger
von den Schatzkisten. Die könnt ihr auf dem Weg raus immer noch plündern, aber
auf dem Weg hinein setzt ihr euch durch Umwege unnötiger Gefahr aus.

Schon bei der ersten Wache könnt ihr eure Schleichkünste beweisen. Der Offizier
wandert um eine Säule herum. Haltet euch immer auf der gegenüberliegenden Seite
auf und bewegt euch ganz langsam, damit ihr keinen Krach macht. Geradezu führt
der Weg durch eine Tür.

Im folgenden Raum liegt viel Gerümpel herum, hinter dem ihr Schutz findet.
Lockt die Wache, die vor der Tür steht, durch eine Banane weg. Werft diese
Banane in den zentralen Gang des Raumes und schleicht euch dann über einen der
Außenwege zur Tür, während er die Banane untersucht. Alternativ könnt ihr Link
ganz kurz in das Sichtfeld der Wache stellen – dann erscheint ein Fragezeichen
über seinem Kopf und er läuft auf die Stelle zu , an der er Link gesehen haben
will. Das gibt euch genug Zeit, außen herum zu laufen. Bleibt dabei möglichst
leise.

Lauft nun weiter durch den Flur zu einer Leiter. Steigt auf und geht geradeaus,
in einen Raum voller Bananen. Nehmt ruhig welche mit. Kehrt dann um und lauft
ein kleines Stück zurück, bis ihr zu einer Öffnung in der Wand gelangt. Hier
könnt ihr in den nächsten Raum gleiten, ohne gesehen zu werden, da ihr höher
aufsetzt als das Blickfeld der Wachen reicht. Es ist ratsam, in einer ähnlichen
Höhe zu bleiben und den kürzesten Raum zum bewachten Ausgang an der Leiter zu
suchen. Klettert die Leiter hoch in die nächste Passage.

Wieder müsst ihr die Bewegung einer Wache abpassen. Lasst die Schatzkisten hier
in Ruhe und huscht schnell zur nächsten Leiter. Ihr landet weiter oben auf
Holzstegen. Folgt ihnen zur Ausgangstür, die von einem Yiga Wachposten
beschützt wird. Werft von oben eine Banane mitten in den Raum (in Sichtweite
der Wache), damit die Wache ihren Posten verlässt. Springt dann herunter und ab
durch die Tür.

Gleich erreicht ihr den Boss. Doch bevor ihr euch ihm stellt, könnt ihr völlig
ohne Gefahr ein paar Schatzkisten plündern, die mitunter euren Vorrat
auffrischen, Nehmt das Magnetmodul zur Hand und hebt vergrabene Kisten aus dem
Sand. Habt ihr alle, richtet das Magnetmodul auf die Metalltür, die in einer
der Wände eingelassen ist und zieht sie auf. Dahinter kommt ihr ins Freie, wo
der Boss der Yiga-Bande auf euch wartet.

## Koga – Anführer der Yiga

Nun müsst ihr euch dem Anführer der Yiga-Bande stellen. Nach einem einleitenden
Gespräch geht der Kampf los. Koga ist ein kleiner Feigling, der Link meist auf
Entfernung hält und mit Steinen um sich wirft. Wenn sie erst einmal fliegen,
könnt ihr gegen diese Steine nichts ausrichten, egal, welche Gegenmaßnahme ihr
ergreift. Da hilft nur rennen und ausweichen.

Allerdings könnt ihr schon vor seiner Attacke zum Konter ausholen. Wenn Koga
telepathisch einen Stein anhebt, dann wartet auf den Moment, wenn der Stein
über seinem Kopf schwebt und schießt einen Pfeil direkt auf Kogas Körper. Er
wird die Kontrolle verlieren, wodurch der Stein auf seinem Kopf landet und
mächtig Schaden anrichtet. Der pummelige Boss purzelt daraufhin bewusstlos zu
Boden. Theoretisch wäre dies euer Zeichen für einen Nahkampf-Angriff.
Allerdings bewirkt ihr im Nahkampf so gut wie keinen Schaden, darum spart euch
das und wartet lieber auf die nächste Steinwurf-Runde, in der Koga auf die
gleiche Strategie hereinfällt.
![Koga, Anführer der Yiga](pics/cdd18f6385ee279666c6e3f2cd82a173.jpg)

Koga leitet nach Verlust des ersten Drittels Lebenskraft eine weitere
Kampfphase ein, in der er über dem Loch in der Mitte des Kampfgebiets schwebt.
Grundsätzlich ändert sich nichts an eurer Strategie, nur müsst ihr das Timing
nun anders ansetzen, da zwei Steine nun um ihn herum rotieren, bevor er sie
wirft. Ihr müsst geduldig bleiben und warten, bis einer der Steine direkt über
seinem Kopf schwebt, bevor ihr einen Pfeil abfeuert.

In der letzten Kampfphase kurz vor Schluss greift Koga auf Stachelkugeln
zurück, die er ziemlich schnell schleudert. Haltet das Magnetmodul bereit und
erfasst damit die Kugel, sobald sie fliegt. Haut sie Koga um die Ohren.

Habt ihr den Boss besiegt, wird er euch drohen: Seine Niederlage wird gerächt!
Das könnt ihr wörtlich nehmen, denn von nun an werden ständig Yiga-Krieger aus
heiterem Himmel auftauchen, die euch das Fell über die Ohren ziehen wollen.

Geht nun zurück in die Yiga-Festung. Alle Räume sind leer, ihr könnt also nach
Herzenslust nach Schätzen suchen und alles plündern. Kehrt anschließend nach
Gerudo-Stadt zurück und berichtet der Anführerin. Sie wartet nicht im
Thronsaal, sondern ein Stockwerk darüber in ihren Gemächern.


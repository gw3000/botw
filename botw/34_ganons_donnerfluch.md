
# Ganons Donnerfluch

Dieser Boss gehört zu den gefährlichsten im ganzen Spiel, weil er ungemein
schnell ist. Seine langsame Attacke besteht aus einer Handvoll Blitzkugeln, die
gemächlich auf euch zu schweben. Ihnen auszuweichen ist nicht schwer. Richtig
übel wird es erst, wenn Ganons Donnerfluch sich in mehreren blitzschnellen
Aktionen auf Link zubewegt und noch schneller zuhaut. Er kündigt diese Aktion
zwar an, aber darauf zu reagieren erfordert äußerst gutes Reaktionsvermögen.
Ihr habt zwei Möglichkeiten. Einerseits könnt ihr eine Einhand-Waffe ausrüsten,
mit der ihr ebenso schnell reagieren könnt und ihn gerade in dem Moment, wenn
er vor eure Nase zischt, erwischt.

Ein mal getroffen, könnt ihr gleich eine Weile auf ihn los knüppeln. Das klappt
aber wirklich nur mit kurzen Waffen, mit denen ihr nicht lange ausholen müsst.
Mit großen Zweihängern trefft ihr wesentlich seltener, weil Link zu lange
braucht, bis er zuschlägt.

In Variante zwei verlasst ihr euch auf euer Schild. Auch hier dürft ihr also
keine Zweihänder-Waffe tragen. Haltet euer Schild stets vor euch (ZL-Knopf) und
wartet ab. Genau in dem Moment, wenn Ganons Donnerfluch zuschlägt, vollzieht
ihr mit dem Sprungknopf einen Rückwärtssalto, der eine Zeitlupen-Sequenz
einleitet, in der ihr zum Konter ansetzen könnt. Auch hier nicht zögern: Ein
mal erwischt müsst ihr kontinuierlich weiter draufhauen. Rechnet damit, eine
oder zwei eurer Waffen zu verlieren.
![Ganons Donnerfluch](pics/a4678e03cc41b881d461bf468160a698.jpg)

Verliert Ganons Donnerfluch die Hälfte seiner Kraft, wird er in eine zweite
Kampfphase wechseln, in der er noch schneller und noch gemeiner wird. Er wird
elektrische geladene Stäbe generieren, die um Link herum einschlagen. Ihr müsst
den Stäben entwischen und habt derweil keine Chance, Ganons Donnerfluch zu
verletzen, da er sich schützt. Eine gute Fluchtposition ist auf dem Steg über
euch gegeben.

Wenn ihr euch auf dem Steg befindet, aktiviert euer Magnetmodul und schnappt
euch einen der Stäbe, die als letztes herabgefallen sind. Bewegt den Stab in
die unmittelbare Umgebung des Bosses, damit der nächste Blitz ihn selbst
erwischt. Ist das der Fall, habt ihr nur sehr kurze Zeit, um zu ihm zu eilen
und mit einer Nahkampfwaffe die Leviten zu lesen. Prügelt ihn grün und blau!

Ganons Donnerfluch wendet dann kurzzeitig seine alte Taktik an, in der er noch
schneller zu euch zischt als zuvor und sogar seine Waffe elektrisch auflädt,
mit der er euch entwaffnen oder sofort töten kann. Rennt in diesem Fall besser
einfach davon. Die Taktik zur Verteidigung bleibt ansonsten dieselbe, bis er
erneut Elektro-Stäbe auflädt. In dem Fall flüchtet wieder auf den Steg in der
Mitte und folgt der zweiten Taktik.

Gefährlich: Unter großer Verzweiflung wird Ganons Donnerfluch einen Laserstrahl
auf euch anvisieren. Auch hier habt ihr zwei Möglichkeiten. Erstens: Ausweichen
und hoffen, dass ihr schnell genug entkommt. Zweitens einen perfekten Block
ansetzen, der den Strahl auf ihn zurückfeuert. Dazu haltet ihr euer Schild vor
euch (ZL-Knopf halten) und drückt gerade in dem Moment, wenn er feuert, den
A-Knopf zum Abwehren. Der zurückgeschleuderte Strahl tut ihm mächtig weh.
Verpasst ihr jedoch den rechten Moment, kostet euch das jede Menge Herzen.

Nebenbei: Mit dieser Technik könnt ihr auch die gefährlichen antiken Wächter
bezwingen. An denen könnt ihr notfalls diese Technik üben.


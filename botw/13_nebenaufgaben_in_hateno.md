# Nebenaufgaben in Hateno

In der Stadt Hateno und in Stadtnähe sind wieder einige Nebenaufgaben verteilt, die ihr nicht zwingend lösen müsst. Aber ihr könnt euch kleine Belohnungen und die Gunst der Bewohner verdienen.

## Ein Ausdauernder Verehrer

Neben dem Gasthaus von Hateno steht ein Mann namens Mansa. Er ist geknickt, weil er nicht weiß, was seiner Angebeteten gefällt. Es geht um Primella, die Rezeptionistin des Gasthauses. Er fragt Link, ob er sie nicht einfach fragen könnte, was sie toll findet. Stapft also in das Gashaus und stellt euch seitlich neben die Empfangstheke, damit ihr mit der Dame ein Privatgespräch führt. Fragt sie nach ihrer Vorliebe. Sie wird die Frage befremdlich finden und mit etwas absurdem antworten – sie wäre gerne von 100 Heuschrecken umgeben. Bringt diese Antwort zurück zum Verehrer. Mansa brauchtHilfe beim Sammeln der Schrecken, gibt sich aber schon mit 10 Ausdauerschrecken als Start zufireden, Ausdauerschrecken findet ihr quasi überall, wenn ihr Gräser mit einem Schwert der einer anderen scharfen Klinge mäht.

## Schabernack mit Schafen

Auf dem Bauernhof nahe des Instituts steht Koypin. Durch ein Gespräch erfahrt ihr von einer Bedrohung ihrer Schafe durch Monster im Südosten am Hateno-Strand. Macht die Monster paltt und erstattet ihr Bericht.

## Der andere Forscher

Wenn ihr die Hauptaufgabe im Hateno -Institut erledigt habt, (siehe vorherige Seiten), dann sprecht erneut mit Purah, Sie schickt euch ins Akkala -Institut, weit im Nordosten Hyrules. Brecht aber nicht gleich auf, Sprecht erst mit dem anderen Forscher im Institut. Legt dann all eure Rüstungsteile ab sprecht nochmal mit Robelo. Nehmt die Fackel an der Wand und geht nach draußen, um das blauer Feuer abzuholen und es zum Institut zu bringen, Dort entzündet ihr die Fackel vor dem Haus mit blauem Feuer. Sprecht danach wieder mit Robelo.

## Verborgene Kräfte

Durch das Upgrade des Shiekah-Steins im Hateno-Institut habt ihr ein Foto-Modul erhalten. Danach könnt ihr mit Purah sprechen, um eure Module aufwerten zu lassen. Dafür müsst ihr antike Bauteile wie etwa antike Federn und Zahnräder investieren, die ihr bei kaputten Wächtern auf der Oberwelt einsameln konnten, wenn ihr sie untersucht habt. Manchmal bekommt ihr sie auch in Schreinen spendiert, wenn ihr kleinere Wächter-Roboter vernichtet.

## Fotografisches Gedächtnis

Wennihr den Shiekah-Sensoor Plus (+) bekommen habt, mit dem ihr nicht nur Schrene ausfindig machen könnt, dann sprecht mit Symin im Institut. Er bittet euch den Glutling Pilz an der Außenwand des Intituts zu fotografieren, Geht also raus aus dem Haus auf die Rückseite des gebäudes und fotografiert den Pilz, Geht nun zurück und zeigt ihm das Foto. Danach will er , dass ihr mit dieser methode gleich drei Pilze dieser Art findet. Im nahe gelegenen Exüpa-Wald findet ihr viele davon. Bringt sie ihm.

Dunkle Geschäfte

Auf der Straße hatenos findet ihr ein Kind namens Tibo. Nach einem Gespräch führt es euch an eine seltsam aussehende Statue am Rand der Stadt. Sprecht ihr mit der Statue, so wir sie euch eines Herzens berauben, ihr bekommt es gleich wieder, wenn ihr noch einmal mit der Statue sprecht. Von nun an könnt ihr an dieser Stelle Herzen gegen Ausdauer tauschen, oder den Tausch rückgängig machen, das kostet aber eine Menge Geld!

## Der Waffennarr

Ein Kind in Hateno heißt Nebbo. Sprecht mit dem Kind. Es möchte erfahren, wie ein Reiseschwert aussieht. Es gibt diverse Wege ein Reiseschwert zu finden, allerdings beginnt hier eine ganze Serie von Folge-Aufgaben mit anderen Waffen, die das Balg gerne sehen möchte. AN sich aber keine schwere Aufgabe.

## Der Schatz des Helden

Ein Mann namens Kishwah befindet sich an der Nordbucht von Hateno. Er gibt euch einen kryptisch formulierten Hinweis, der auf die Steine der Bucht gemünzt ist, welche aus dem Wasser herausschauen. Betrachtet sie einfach als Uhrzeit-Anzeige, bzw. als Ziffernblatt einer Uhr. Am Fünf-Uhr-Felsen befindet sich eine Schatzksite, die ihr plündert, um die Aufgabe zu lösen.

## Dein eigenes Traumhaus

Dies ist eine langfristige Quest, aber eine die sich lohnt, weil ihr dadurch ein festes Lager erwerbt., In Hateno werkelt jemand an der Außenseite eines Hauses. Er verrät euch vom Abriss des Hauses vor ihm und rät euch, mit dem Besitzer zu sprechen, wenn ihr es haben wollt. Sucht nun einem Person namens Landa, die sich südlich des Myama-Gana-Schreins aufhält. Er verkauft euch das Haus für satte 3000 Rubine und 30 Holzbündel. Wenn es euch gehört, könnt ihr es ausstatten und als Lager für alles Erdenkliche nutzen.

## Auf Expansionskurs

Sprecht erneut mit Landa, dem Ex-Hausbesitzer, und dann mit Dumsda, nachdem ihr das Haus in der Aufgabe „dein eigenes traumhaus“ erworben habt. Dumsda zieht weg, Ihr könnt ihn in Zukunft auf einer kleinen Insel au dem Akkala-See antreffen, wo ihr ihm beim Holz sammeln helft.

# Die Schreine von Kakriko

## Taro-Nihi Schrein

Dieser Schtein hat nur eine Prüfung. Schaut zuerst nach rechts, da ist eine Kiste, die einen starken Schild enthält. Schnappt euch den Schild und rüstet ihn aus, Werft notfalls einen schwächeren Schild über das Inventar weg.

Nun stellt euch der Robo-Spinne und folgt einfach den Anweisungen, idie euch die basis des Kampfsystems näherbringen. Habt ihr die Prüfung bestanden, dann plündert noch die Kiste, bevor ihr zum Weisen Taro Nihi geht und euch das Zeichen abholt.

![Schreine in der Umgebung von Kakriko](pics/9c60e9308766861770d02058e8df1f05.jpg)

## Rhana-Roki-Schrein

Wichtig: habt ihr denTaro-Nihi-Schrein gelöst, dann verfolgt die kommende Hauptaufgabe und geht der Geschichte weiter nach, in dem ihr nach Hateno reist. Der folgende Schrein ist erst dann auffindbar, wenn euer Shiekah-Stein in Hateno aufgewertet wurde. Wir platzieren die Beschreibung dennoch in dieses Kapitel, weil es durch die Örtlichkeit besser hierher passt.

Ihr habt bereits drei Zeichen in dieser Gegend eingesackt. Na dann wäre es praktisch, wenn ihr den vieten und letzten Schrein dieses gegend auch noch ausfindig macht. Um den nächsten Schrein zu finden, müsst ihr allerdings erst drei Nebenaufgaben im Dorf Kakariko erledigen. Die Aufgaben heißen

    - Entwischte Hühner
    - Tanzende Lichter
    - die legendäre Feenquelle

Für Details zu diesen Aufgaben, schaut bitte in das zugehörige Unterkapitel am Ende dieses Kapitels. Beachtet, dass ihr für „die legendäre Feenquelle erst die Fotofunktion des Shiekah:Steins benötigt. Die bekommt ihr in der Stadt Hateno.

Habt ihr diese Nebenaufgaben geschafft, dann besucht Impa. Ihre Dienerin wird verzweifelt nach einer heiligen Kugel suchen, die zuvor neben Impa lagerte und gestohlen wurde. Dies startet die Nebenaufgabe Das gestohlene Juwel.

Schaut euch die Zwischensequenz an und verlasst dann Impas Haus. Eine der Wachen vor dem Haus heißt Dorian. Er wird gegen 22 Uhr aufbrechen um etwas zu suchen. Schleicht hinter ihm her, ohne entdeckt zuwerden. Er wird an der großen Feenquelle hinter dem Taro-Niri:Schrein auf dem Berg neben Kakariko vorbeigehen und über eine Zweigbrücke schreiten. Dort fordert er jemanden, den man nicht sehen kann, heraus, sich zu zeigen. Nahert euch danach Dorian, damit eine weitere Zwischensequenz startet. In dieser wird eine Art Samurai erscheinen. Er ist der Dieb! Haut ihn zu Brei!

Habt ihr den Unbekannten Krieger besiegt, erhaltet ihr das Kugel-Juwel, das ihr in die Mulde des orangenen Sockels im Wald einsetzt. Daraufhin wird ein Schrien aus dem Boden wachsen. In diesem Schreib gibt es keine besondere Aufgabe. Dass ihr es geschafft habt, ihn zu finden, ist offenbar Herausforderung genug für ein weiteres Zeichen. Vergsst nicht das Großschwert aus der Kiste mitzunehmen.

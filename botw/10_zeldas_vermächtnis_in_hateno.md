# Zeldas Vermächtnis in Hateno

Die uralte Zofe Zeldas schickt euch mit der **Hauptaufgabe „Zeldas Vermächtnis“** in die östlich gelegene Stadt **Hateno.** Ohne ein Pferd wäre das eine sehr lange und unangenehme Reise, aber da ihr längst einen der wilden Gäule gezähmt habt, wird es nur halb so wild.

Reitet von Kakariko aus zurück nach Süden. Solltet ihr die Nebenaufgabe „die einzig wahren Rasseln“ noch nicht erfüllt haben, dann tut es jetzt. Der große Maronus steht spätestens jetzt am Wegesrand und trauert seinen **Rasseln** hinterher, die ihr in einem nahe gelegenen Bokblin-Lager findet. Bringt ihr sie ihm wieder, wird er euer Inventar im Tausch gegen **Krog-Samen** vergrößern.

Überall in Hyrule wurden **900 Krog-Samen** versteckt, die ihr über diverse Mini-Spiele verdienen könnt. Klingt nach viel, ist für euer Inventar aber gerade mal genug, denn mit jeder Erweiterung für eine Inventar-Kategorie steigt der Krog-Samen-Preis. Zur reinen Erweiterung der Taschen werden allerdings nur rund 400 benötigt. Haltet überall Ausschau nach **Krog-Samen-Puzzles**. Zum Beispiel kleine Stein-Strukturen, die ihr vervollständigen müsst. Oder Sonnenblumen, die in einer gewissen Reihenfolge auftauchen und verschwinden, wenn ihr sie pflücken wollt.

Die Krog-Wesen, welche die Samen hüten, verstecken sich manchmal auch einfach nur und müssen schlicht ausfindig gemacht werden. Solltet ihr irgendwo in der Natur einen Kreis aus Blumen oder Steinen entdecken, dann werdet aufmerksam – schaut nach einem Stein oder Geröll, das ihr in die Mitte des Kreises schubsen könnt. Ebenfalls verdächtig sind Ballons in der Luft. Schießt sie ab, um einen Krog zu finden, der euch einen Krog-Samen spendiert. Karten der Krog-Samen Fundstellen findet ihr demnächst in dieser Komplettlösung.

Um nach Hateno zu kommen, biegt ihr an der Gabelung südlich der Kakariko-Brücke nach Osten ab. Folgt der Straße und achtet auf die vielen verrosteten Wächter am Wegesrand. Wenn ihr an sie herantretet, könnt ihr wertvolle Wächter-Teile einsacken, zum Beispiel antike Schrauben. Die könnt ihr später gebrauchen, um euren Shiekah-Stein zu verbessern, aber auch zu relativ guten Preisen verkaufen, wenn mal Not am Mann ist. Übertreibt es aber nicht mit dem Verkauf, ihr braucht diese **antiken Schrauben** und **Zahnräder** noch. Außerdem ist Vorsicht geboten, denn einige Wächter funktionieren noch. Ihr Laser verbrennt Link sofort.

Sobald ihr den Eingang zur Festung von Hateno durchquert habt und irgendwann die Nacht hereinbricht, sollte euch die erste Nachricht von Zelda erreichen, die den Blutmond betrifft. Steigt der Blutmond auf, sind Ganons Diener besonders stark und zahlreich. Gerade am Anfang, wenn ihr noch schwach ausgerüstet seid, kann es schlauer sein, an solchen Nächten am Lagerfeuer zu bleiben und die Nacht zu überspringen.

Seid auch vor gewissen Passanten gewarnt, die euch in eine Falle locken. Etwa eine weinende Person. Manchmal ist das nur ein falscher Vorwand für einen Angriff. Ihr werdet die Bekanntschaft mit den getarnten Kriegern noch oft genug machen.

Reitet nun weiter nach Osten. Sobald die Straße sich nach Süden neigt und an eine Gabelung kommt, solltet ihr den rechten Weg nehmen. Er führt euch zum **Hateno-Turm.** Besteigt den Turm unbedingt und holt euch an dessen Spitze die Übersichtskarte des Gebiets, damit ihr euch nicht weiter verirrt. Der Turm lässt sich nur von der Ostseite bequem besteigen, weil er mit Ranken und Stacheln übersät ist. Außerdem liegen die Ruhepunkte weit auseinander. Alternativ könnt ihr die Ranken abbrennen.

Es schadet nicht, wenn ihr für den Aufstieg eine Mahlzeit dabei habt, die eure Ausdauer wieder auffüllt (Achtung, diese Mahlzeit nicht vorher essen, sondern erst, wenn eure Ausdauer kurz vor dem Versiegen ist).

Vom Hateno-Turm aus reitet ihr ostwärts in die Stadt Hateno. Schaut euch ruhig hier um. Am Ende der Stadt gibt es zum Beispiel einen nützlichen **Bekleidungsladen**, der stabile Rüstungen für euch bereithält. Gegen Bares, versteht sich.

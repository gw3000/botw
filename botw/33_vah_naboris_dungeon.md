
# Vah Naboris-Dungeon

Wie in jedem anderen Dungeon gilt auch hier: Achtet auf eine gute Versorgung!
Gehen euch die Ressourcen aus, dann teleportiert euch in eine Stadt. Ihr kommt
problemlos wieder zurück. Ganz wichtig für den Boss: Besorgt euch mindestens
zwei, besser drei Einhänder-Waffen aus Metall (keine instabilen Holzknüppel)
und zwei stabile Schilde. Solltet ihr nur Bumerangs als Einhänder-Waffen
auftreiben können, sind vielleicht sogar vier angebracht, denn sie zerbrechen
schnell.

Vah-Naboris ist der komplizierteste Titan des Spiels. Er besteht aus drei
zylindrischen Körpersegmenten, die ihr mithilfe der Dungeon-Karte jeweils auf
vier Positionen drehen könnt, sobald ihr die Karte gefunden habt. Darum eines
nach dem anderen.

Lauft vom Startpunkt aus geradeaus und dann die Rampe hinauf, die euch in das
Innere führt. Dunkle Materie steht euch im Weg, aber ihr wisst sicherlich
schon, wie ihr sie beseitigt: Schießt einfach einen Pfeil auf das Auge, das an
der Materie klebt.

Im zentralen Raum des Titanen lauft ihr zum gegenüberliegen Ende des Zylinders.
Eine Rampe führt euch zum zentralen Terminal, welches die Dungeon-Karte
freigibt. Wenn ihr einen Blick auf die Karte werft, entdeckt ihr die drei
Zylinder-Segmente, die ihr bewegen könnt. Auf welcher Dreh-Position sich die
drei Teile jeweils befinden, ist gut anhand des Streifens festzustellen, der
die Stromverbindung darstellt.

Nun rotiert den vorderen Ring (der in der Nähe des Kopfes des Kamelartigen
Titanen) dreimal, damit er sich um 270 Grad dreht (also eine
Drei-Viertel-Drehung vollzieht). Ist die Rotation vollzogen, dann schaut mit
Link in Richtung des ersten Rings und stellt euch auf die vertikale Leiste, die
nun auf der rechten Seite zu sehen ist. Nun öffnet ihr wieder die Karte und
dreht sie noch einmal um 90 Grad, damit sie die volle Drehung vollzieht. Link
steht dann auf der horizontal ausgerichteten leiste und hat den Terminal für
den ersten Kontrollpunkt direkt vor der Nase. Davor ist noch eine Lücke, in der
ihr dunkle Materie erspäht. Springt auf die andere Seite, aktiviert das
Terminal und schießt dann das Auge in der Lücke ab, um die dunkle Materie zu
beseitigen.

Dort, wo ihr nun steht, steht ihr gut. Öffnet die Karte und rotiert das
mittlere Segment um ein Viertel, also um 90 Grad. Nun seht ihr unter euch eine
Truhe, zu der ihr hinabgleiten könnt, Öffnet die Kiste. Nun ist ein wenig
Balance gefragt. Rotiert das mittlere Segment noch einmal um 90 Grad und
versucht, Link auf der Oberseite zu halten, damit er nicht abrutscht. Lauf ein
Stück weiter in die gleiche Richtung und lasst euch hinunter zur zweiten Truhe
fallen. Ihr könnt sie erst erreichen und plündern, wenn ihr das mittlere
Segment noch einmal um 90 Grad rotieren lasst.

Lauft mit Link in den hinteren teil des Titanen ins Freie. Dort hängt eine
Schatztruhe an einem Seil. Ihr dürft das Seil nicht zerschneiden, sonst fällt
sie in die Wüste und wird unerreichbar. Stattdessen müsst ihr drei Manöver sehr
schnell hintereinander vollziehen. Erstens: friert die Truhe mit dem
Stasismodul in der Zeit ein. Zweitens: schnappt euch einen Pfeil und zerschießt
das Seil. Drittens: Zieht die Truhe mit dem Magnetmodul an euch heran, wenn die
Wirkung der Stasis nachlässt. Nun könnt ihr die Kiste plündern.

Bleibt nun auf der Stelle stehen, an der ihr die Schatztruhe vor dem Fall in
die Wüste bewahrt habt. Öffnet die Dungeon-Karte und rotiert alle drei
Zylindersegmente so, dass die grüne Stromleitung im Inneren des Titanen eine
Reihe an der Decke bildet. So läuft der Strom und treibt das vordere Ende des
Titanen an. Wie ihr nun seht, rotieren die Ausgänge des vorderen Segments, die
Richtung Kopf des Titanen schauen.

Stellt euch in eine der Ausgänge des rotierenden Teils hinein und lasst euch
ganz nach oben zu einem Durchgang tragen. Schreitet in diesen Durchgang, sodass
ihr den Hals des Kamels seht. Dort unten befindet sich ein bewegliches
Drehkreuz. Es lässt sich in zwei Richtungen rotieren. Dreht das Kreuz erst in
die eine Richtung, bis rechts oder Links ein Verteiler-Knoten erscheint und
oben einrastet. Dreht ihr zu weit, senkt er sich wieder, also dreht nur so
weit, bis der Verteiler oben angelangt ist, Danach dreht ihr das Kreuz in die
andere Richtung, bis ein zweiter Verteiler-Knoten auf der anderen Seite
einrastet. Läuft der Strom nun auf beiden Seiten durch, wird ein Aufzug in
Bewegung gesetzt, der am Hals des Titanen auf und ab fährt. Fahrt nun ganz nach
oben und aktiviert dort das Terminal für den zweiten Kontrollpunkt.

Schwebt nun zurück in den Gang, der euch hergeführt habt und wartet, bis die
rotierende Scheibe einen Ausgang in den zentralen Raum des Dungeons freigibt.
Nun schaut genau hin: Mit der Vorderteil rotiert auch ein sehr langer Steg.
Lasst euch auf die lange Seite des Stegs fallen, wenn sie auf der linken Seite
nach oben rotiert und springt dann gleich weiter auf die Plattform dahinter,
die sich weit oben im Titanen befindet. Lauft hier oben bis an das andere Ende
des Titanen. Schaut auf die Seitenwand, da seht ihr drei Metallschienen. Mit
eurem Magnetmodul könnt ihr die eisernen, per Rohr verbundenen Verteiler-Knoten
verschieben. Der oberste Knoten muss nach hinten, wo der Strom aus der Mitte
des Titanen herkommt. Die zweite Verteiler-Kette muss an das andere Ende des
Ganges geschoben werden, damit der Strom durchläuft. Ist der Kontakt
hergestellt, bewegt sich der Schwanz des Kamel-Titanen.

Unpraktisch: Der Schwanz ist auf der richtigen Position, aber ihr steht noch
vorne. Unterbrecht den Stromkreis, indem ihr die Karte öffnet und das mittlere
Segment des Titanen rotieren lasst. Geht zum Ende des Kamelschwanzes und dreht
das mittlere Segment per Dungeon-Karte wieder in die richtige Postion, sodass
der Strom wieder läuft und der Schwanz des Titanen sich hebt. Ihr könnt nun auf
die obere Seite des Titanen schreiten. Beseitigt die dunkle Materie vor euch
durch einen Schuss auf das Auge, das ihr erst sehr, wenn ihr näher heran
schreitet.

In der Kuppel ist noch mehr dunkle Materie. Kümmert euch darum und klettert
dann die Leiter am Rand der Kuppel hinauf. Bewegt das Drehkreuz bis der Strom
fließt und ein Aufzug erscheint, der zwischen den beiden Kuppeln des Titanen
hin und her fährt. So erreicht ihr die zweite Kuppel – quasi die Höcker des
Kamel-Titanen, was gleich noch von Nutzen sein wird.

Fahrt in der ersten Kuppel mit der Aufzug-Plattform nach unten und schaut an
die Wände, Ihr findet auf einer Seite eine Schatztruhe und auf der anderen
Seite eine Metallkugel vor. Schnappt euch die Kugel mit eurem Magnetmodul.
Tragt die Kugel nach ganz unten vor einen Raum der sich zwischen beiden Höckern
befindet.

Nun fahrt wieder nach oben, schreitet über die Plattform in der Mitte zwischen
den Kuppeln und sucht die zweite Kuppel auf – also den zweiten Höcker. Dort
bekämpft ihr zuerst einen Nano-Wächter und dann ein weiteres Auge, um das
Terminal für den dritten Kontrollpunkt von dunkler Materie zu befreien.
Aktiviert den dritten Kontrollpunkt und stellt euch dann auf den Aufzug, der im
Höcker auf und ab fährt. Fahrt ganz nach oben. Schnappt euch die zweite
Metallkugel und fahrt ganz nach unten. Beseitigt den Schleim und nutzt dann
eurer Magnetmodul, um die Kugel über der Tür mit dem Verteiler-Knoten zu
verbinden. Durch den Stromfluss öffnet sich die Tür.

Wenn ihr durchgeht, erreicht ihr den Mittelraum, in dem ein Nano-Wächter
wartet. Macht ihn platt. Nun nehmt ihr die Kugel und setzt sie in eine der
beiden Fassungen ein. Holt euch die Kugel, die ihr vorhin abgelegt hattet und
legt sie in die zweite Fassung. Läuft von beiden Seiten der Strom, so öffnet
sich die Barriere vor euch und der Terminal für den vierten Kontrollpunkt kann
aktiviert werden.

Kehrt in den hinteren Höcker zurück und lauft ins Freie an das hintere Ende des
Titanen. Nun müsst ihr einen schwierigen Sprung wagen. Bei dem ihr in die Mitte
des Titanen direkt zwischen seine oberen Beingelenke auf der rechten Seite
gleitet. Da ist eine Plattform, über die ihr einen Raum am rechten hinteren
Bein erreicht. Solltet ihr euch nicht trauen oder mehrere Fehlversuche
hinlegen, dann kehrt stattdessen in den Hauptraum zurück und dreht das mittlere
Segment des Titanen , sodass ein Loch in der rechten Außenwand frei wird. Über
dieses Loch erreicht ihr die Plattform ebenfalls.

Im Hinterbein angelangt, könnt ihr den Terminal für den fünften Kontrollpunkt
schon sehen, aber nicht aktivieren, weil er auf der falschen Seite steht. Ihr
müsst euch in den Raum stellen, in dem sich der Terminal befindet und dann über
die Dungeonkarte das hintere Segment des Titanen ein Mal um 90 Grad drehen, um
den Terminal aufzurichten. Aktiviert Kontrollpunkt Nummer fünf und dreht das
hintere Segment des Titanen drei mal, damit ihr den Raum wieder verlassen
könnt.

Kehrt nun in die zentrale Halle des Dungeons zurück. Wenn ihr weitere
Schatzkisten freilegen wollt, könnt ihr die Teile des Titanen noch ein paar mal
rotieren, um in den kreisrunden Ausschnitten Ausgänge freizulegen, Einer davon
muss noch per Bombe gesprengt werden. Ansonsten dreht ihr das mittlere Segment
so, dass das Steuerungsterminal des Titanen aufrecht steht und ihr es bedienen
könnt. Daraufhin begegnet ihr dem Boss.



# Vah Rudania Dungeon

Wie im vorherigen großen Dungeon solltet ihr gut vorbereitet sein, bevor ihr euch um dessen Lösung kümmert. Sobald ihr Vah Rudania betreten habt, ist es kein Problem mehr, zurückzukehren – ihr könnt euch hierher teleportieren. Seid ihr also schlecht versorgt, dann teleportiert euch in eines der Dörfer und deckt euch ein – mit Pfeilen, Waffen und vor allem mit Nahrung für den Bosskampf.

Gleich hinter dem Startpunkt ist eine Truhe, die eine antike Schraube beinhaltet. Plündert sie und kehrt um. Im inneren der großen Echse ist es zur Zeit stockfinster. Ihr müsst euch also mit dem wenigen Licht, das euch zur Verfügung steht, voran tasten. Zum Glück liegen Fackeln herum und Feuerquellen stehen bereit. Spätestens dann, wenn ihr euch um die ersten Augen gekümmert habt, die dunkle Materie in der Echse verteilen.

Schaut euch genau nach den Augen um und zerstört sie mit einem gezielten Pfeilschuss. Schaut auch an die Decke, da ist ein weiteres Auge. Spätestens jetzt solltet ihr einen Stock oder eine Fackel bereit haben, mit dem ihr das Feuer tragen könnt, um euch zu orientieren. Nehmt das Feuer mit zur Schale vor der versperrten Tür. Ist die Schale entzündet, öffnet sie sich.

Im folgenden Raum müsst ihr kurz ein Schwert zur Verteidigung ziehen. Schaut euch nun nach weiteren Augen um und erledigt sie alle mit Pfeilen, Ein paar Pfeile dürftet ihr über umliegende Kisten zurückbekommen. Nun bringt ihr das Feuer zur nächsten Tür an die Fackel und kassiert im folgenden Raum die Dungeon-Karte. Zum Glück könnt ihr nun endlich alles erkennen. Auch weil Licht in die Räume fällt.

Werft einen Blick auf die Karte. Ihr seht die Dungeon-Struktur in Form einer Echse. Wie im letzten Dungeon sind hier fünf Kontrollpunkte zu aktivieren, bevor ihr zum Boss könnt. Zudem ermöglicht die Karte abermals das Verändern des Dungeons. Dieses mal könnt ihr die komplette Echse auf die Seite drehen.

Stapft zurück in die Eingangshalle. In der Nähe des Haupteingangs ist ein gigantisches Tor. Dessen Türen könnt ihr mit eurem Magnetmodul öffnen, sobald ihr den Querbalken mit einem Holzpfeil (oder Bombenpfeil) vernichtet habt. Dahinter liegt das erste der fünf Kontrollsiegel. Aktiviert dazu den Terminal hinter dem Giganto-Tor. Brutzelt ihr dazu die Ranken an der Wand ab, fällt eine Schatzkiste herunter, die ein antikes Zahnrad spendiert.

Schaut nun in die umgekehrte Richtung in der nähe des Eingangs. Da ist eine Tür mit einem Loch, Schaut ihr durch das Loch, erkennt ihr einen Fackelhalter. Nehmt einen Holzpfeil, spannt ihn und geht damit zu einer Fackel, um die Spitzes eures Pfeils anzuzünden, Nun schießt den brennenden Pfeil durch das Loch der Tür, um die Fackel anzuzünden. So kommt ihr durch.

Nun schaut auf einer Seite nach Gestrüpp an der Decke. Entzündet es mit einem brennenden Pfeil, damit ein großer Quader herabfällt. Diesen Quader könnt ihr mit dem Magnetmodul erfassen und in die Feuerwand auf der linken Seite stellen. Nun dreht ihr das komplette Dungon, indem ihr die Karte öffnet und den rosafarbenen Pfeil auf die Seite der Karte legt. Dank der neuen Ausrichtung liegen die Feuerwerfer auf der Seite und ihr könnt neben dem Quader durchgehen. Um dahinter das zweite Kontrollsiegel zu aktivieren, müsst ihr die Karte noch einmal bemühen und die Echse wieder zurück in ihre Ursprungsposition drehen.

![Va Rudania](pics/cba6c97dc630dadbaff343cb1f19820e.jpg)

Geht zurück in den Hauptsaal, nehmt eure Fackel zur Hand, entzündet sie und lauft die große Schräge hoch. Ihr gelangt in den Außenbereich, lasst euch zur nächsten Fackel herunterfallen. Dreht das Dungeon erneut auf die Seite und entzündet die Fackel. Dadurch wird eine Kugel befreit, die in einer Schiene rollt, aber nicht ins Innere kann. Kippt die Echse nun erneut per Karte. Die Kugel wird im Inneren der Echse ganz hinunter rollen Lauft ihr nicht gleich nach. Schaut erst an der Seite der Schiene nach dem Terminal, das für das dritte Kontrollsiegel steht. Segelt dort hin und aktiviert das Terminal. Nun schaut nach dem Ziel der Kugel.

Dort wo die Kugel angekommen ist, zieht ihr per Magnetmodul den kleinen Metallquader nach oben, damit die Kugel nachrutschen kann. Ein Tor öffnet sich, hinter dem ihr Kontrollsiegel Nr. 4 aktiviert.

Kippt die Echse über die Karte erneut und lauft Richtung Eingang. Wenn ihr unter dem langen Balken zur Außenseite steht, dann kippt nochmal die Echse. Ihr steht nun am Zugang zur langen Rampe. Besteigt die Rampe und segelt zur Fackel herunter. Ballert einen Pfeil auf das Auge, geht ein Stück weiter und schießt auf das zweite Auge, das sich hinter der dunklen Materie verbarg. Dadurch wird eine Schatzkiste frei, Lauft hin und öffnet sie. Stellt euch nun direkt an die Wand und kippt das Dungeon abermals in die normale Ausrichtung. Nun ist das Terminal für das fünfte Kontrollsiegel direkt vor eurer Nase.

Nun müsst ihr nur noch zur zentralen Steuereinheit. Aktiviert ihr sie, könnt ihr euch dem Boss stellen.
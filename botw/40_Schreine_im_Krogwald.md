
# Schreine im Krog-Wald

Das Master Schwert mag nicht die stärkste aller Waffen sein, denn in der üblichen Umgebung Hyrules liegt sein Stärke-Wert gerade mal bei 30 Zählern. Erst bei Ankunft im Hyrule-Schloss wird dieser Wert auf 60 ansteigen. Dennoch hat es einen gewaltigen Vorteil gegenüber anderen Waffen: Es kann nicht brechen, zumindest nicht permanent. Das Master Schwert wird nach einigen Schlägen seine Kraft verlieren und somit auch seine Einsatzfähigkeit. Doch schon nach einigen Minuten Regeneration ist es wieder intakt.

Noch kommt ihr nicht dazu, die Stärke des berühmten Schwertes auszutesten. Hier im Krog Wald könnt solltet ihr zuerst den vier Herausforderungen der Krogs nachgehen, die jeweils mit einem Schrein zu tun haben. Sprecht mit dem Krog Papystus um die Herausforderung anzunehmen. Da der Wald weiterhin vernebelt ist, könnt ihr nur offiziellen Wegen folgen, die vom Sockel des Master-Schwerts aus nach Nordwesten, Südwesten und Nordosten führen. Der vierte Schrein liegt ebenfalls im Nordosten, ist aber über einen kleinen Umweg zu erreichen.

## Kiyo-Uh-Schrein

In unmittelbarer Nähe des Schwertsockels (nur leicht Nordöstlich davon in einer Art Seitengasse mit hellem Licht) liegt der Kiyo Uh Schrein. Er beinhaltet ein kleines Rätsel namens „Es liegt in den Sternen“. Eigentlich ganz einfach. Lauft die Treppe hinauf und schaut auf die Sternenkarte an der Wand vor euch. Dort sind diverse Sternenkonstellationen aufgemalt. Welche Art Konstellationen seht ihr direkt an der Wand davor, da ist von jedem Typ eines groß als Beispiel aufgezeichnet.

Eure Aufgabe besteht im Zählen der Anzahl der Sterne in jeder Konstellation und dem Zusammenfassen der Typen. Die Frage die ihr beantworten sollt lautet somit, wie viele Sternbilder bestehen aus drei Sternen, aus fünf Sternen und so weiter. Die Anzahl tragt ihr der Kugeln ein, die links und rechts neben der Treppe in nummerierte Fassungen passen. Die Nummern für jede Mulde erkennt ihr anhand der Leuchten an der Wand. Beispiel: Es gibt fünf Konstellationen, die aus drei Sternen bestehen. Das Symbol für die Drei-Sterne Konstellation ist ganz links an der Wand. also geht es um die Reihe ganz links außen. Nehmt eine der Kugeln und setzt sie in die Reihe ganz links außen in die Fassung neben den fünf Leuchten. Nun schaut wieder auf die Sternbilder. Drei der Konstellationen bestehen aus fünf Sternen. Also legt eine Kugel in der passenden Reihe in die dritte Mulde.

Hier die Lösung für alle Reihen.
Links außen: 5
Links innen: 3
Rechts innen: 1
Rechts außen: 2

Habt ihr die Kugeln korrekt eingesetzt, öffnet sich die Tür und ihr könnt zum Weisen. Die Tür bleibt nun permanent offen, ihr könnt also die Kugeln noch einmal umsetzen, wenn ihr an die Schatzkiste im zweiten Raum heran wollt. Dort ist eine andere Sternenkarte.

Die Lösung für die Schatztruhe lautet.
Links außen: 4
Links innen: 2

Rechts innen: 2
Rechts außen: 1

## Da-Chokahi-Schrein

Dieser Schrein befindet sich im Nordwesten des Waldes. Nur ein Pfad führt in diese Richtung und schon an dessen Anfang dürfte euch ein Krog namens Papistus auffallen. Sprecht mit ihm. Er bittet euch, ein Auge auf seinen Kumpel Tazius zu legen, der sich ebenfalls zum Da-Chokahi-Schrein aufgemacht hat, die Prüfung durch den Wald aber alleine bestehen will. Ihr sollt seinem Kumpel also lautlos folgen und dabei nicht entdeckt werden.

Ihr findet den kleinen Krog schon wenige Meter weiter und werdet feststellen, dass er ein nervöser kleiner Fratz ist, der schon bei der geringsten Störung innehält, gegebenenfalls sogar ein Stück zurück geht und auf keinen Fall wissentlich beschützt werden will. Haltet also guten Abstand, aber verliert ihn nicht aus den Augen. Das ist nicht so leicht wie es sich anhört, denn farblich verschmilzt er gerne mit den hohen Gräsern. Tipp: Schaltet notfalls euer Magnetmodul ein. Die Umwelt färbt sich dann rosa und alle Objekte der näheren Umgebung fallen durch staken Kontrast auf. So verliert ihr den Knirps auch aus der Entfernung nicht.

Die ganze Sache ist allem voran eine Geduldsprobe, denn auf dem Weg zum Schrein begegnet ihr nur einem einzigen Feind – einem eher harmlosen Wolf. Aber auch diesen solltet ihr nicht zu früh erledigen, damit der kleine Krog-Wicht keinen Verdacht schöpft. Am Schrein angekommen, gibt es nichts weiter zu tun, als das Zeichen der Bewährung abzuholen.


## Khun Shidaji-Schrein

Um diesen Schrein zu finden, müsst ihr die Krog-Herausforderung begonnen haben und euch nach Südwesten wenden. Sprecht mit dem Krog Azukius, der euch anweist, immer jenen Bäumen zu folgen, die einen Eisen-Brocken im „Maul“ haben. Das leicht zu erkennen, wenn ihr das Magnetmodul zur Hand nehmt, denn die rosa Färbung der Umgebung erhöht die Kontraste.

Folgt ihr dem korrekten Pfad, so erreicht ihr eine Erhebung, auf der ein verrosteter Schild steht. Nehmt den Schild per Magnetmodul und hebt ihn in das „Maul“ des letzten Baumes. Setzt den Schild dort ab. Daraufhin erscheint eine Schatztruhe. Öffnet die Truhe schaut euch in der Nähe nach einem Krog-Fächer (ein großes Blatt) um – den braucht ihr gleich. Danach schnappt ihr die Truhe per Magnetmodul und setzt sie auf das Floß, das in der Nähe auf dem Wasser treibt. Stellt euch mit der Kiste auf das Floß und nehmt den Fächer zur Hand. Schlagt mit dem Fächer in die Richtung in die ihr den Wind generieren möchtet, der im Segel des Floßes landet und das Floß antreibt. So erreicht ihr auf dem Floß die Insel in der Mitte des Sees.

Nun legt die Truhe per Magnetmodul in das Maul des Baums auf der Insel, damit der Schrein erscheint. Im Inneren des Schreins holt ihr nur noch das Zeichen der Bewährung ab.

## Mahmu Rano-Schrein

Auch dieser Schrein ist Teil der Krog-Herausforderung, die ihr schon gestartet haben solltet. Der Schrein liegt im Nordosten. Sprecht mit dem Krog Damius auf dem Weg in diese Richtung. Er wird euch eine neues Ausrüstungsset geben und auffordern, nur dieses zu verwenden. Ihr braucht also ein wenig Platz im Inventar und dürft bis zum Erreichen des Schreins nicht auf eigene Waffenzurückgreifen.

Sobald ihr ihr seine Waffen habt, solltet ihr den Pfad entlang rennen. Ignoriert so viele Gegner wie möglich, aber vernichtet jene, an denen ihr definitiv nicht einfach so vorbeikommt, in Ruhe. Ihr erreicht eine Barriere mit Holzkisten. Die könnt ihr mit eurem Bombenmodul sprengen. Nun rennt weiter und ignoriert so viele der Gegner am Rand des Waldes, wie möglich. Wenn ihr den Sumpf erreicht, solltet ihr euch zuerst mithilfe von Pfeilen um die Oktoroks kümmern, danach legt ihr Eisblöcke mit dem Cryomodul in den Sumpf und arbeitet euch vor. Das letzte Stück zum Schrein ist ebenfalls von Feinden bewacht. Macht sie mit Pfeilen platt, bevor ihr zum Schrein hinübergleitet – nutzt die Aufwinde, um mehr Zeit zum Schweben zu bekommen.

Im Schrein selbst ist nichts weiter zu tun. Holt euch einfach das Zeichen der Bewährung ab.

Kito Wawai-Schrein. Ein weiterer Schrein befindet sich nördlich des Krog-Waldes. Ihr erreicht ihn gut, wenn ihr den Wald verlasst und am Rande des Todesberges entlang kraxelt, bis ihr ganz im Norden bei einer schwarzen Nebelwolke angekommen seid. Betretet sie von Süden aus.

Innerhalb dieses schwarzen Feldes ist es stockdunkel, aber ihr könnt euch behelfen, indem ihr Leuchtsteine verteilt und eine Feuer macht, an dem ihr eine Fackel oder einen Holzstab anzündet. Der Weg durch das Dunkel ist Anfangs verwirrend aber nicht sonderlich komplex. Ihr werdet Fackellampen erreichen, die ihr anzünden solltet – so orientiert ihr euch später leichter und verirrt euch weniger.

Inmitten des schwarzen Feldes entdeckt ihr eine Kugelfassung in einer orange leuchtenden Plattform. Nur: Wo ist die Kugel, die hier hineingehört? Antwort: Die Kugel baumelt am Gürtel eines blauen Hinox ganz in der Nähe (Richtung Norden). Der Hinox-Riese ist im Dunkeln kein ungefährlicher Gegner, aber gut zu besiegen, wenn ihr euch entsprechend vorbereitet. Immer nützlich sind Waffen mit Elektro-Funktion, also Pfeile, Speere und so weiter. Sie werden ihn lähmen.

Habt ihr keine explizite Elektro-Waffe, dann verlasst euch auf eure dritte Spezialfertigkeit vom Wüsten-Titanen. Ihr löst sie aus, wenn ihr eine Waffe beim Angriff aufladet und dann zuschlagt. Haltet einfach die Angriffstaste Y gedrückt, bis ihr ein akustisches Signal hört. Dann lasst los, damit Link entweder einen Wirbelangriff ausführt oder mit einem Speer mehrmals hintereinander zuschlägt. Zugleich wird Strom freigesetzt, der den Hinox lähmt. Ihr könnt eure Waffe übrigens in mehreren Stufen aufladen, die den Angriff noch verstärken. Inzwischen seid ihr aber dem Hinox ausgeliefert, also übertreibt es nicht.

Des Weiteren geht ihr nach der üblichen Hinox-Taktik vor: Schießt ihm Pfeile ins große Auge. Das ist nicht besonders schwer, denn sein Auge leuchtet auch im dunkeln. Trefft ihr ihn am Auge, wird er sich kurz zum Flennen hinsetzen – das ist euer Zeichen, noch härter mit Schwert, Lanze oder Axt drauf los zu knüppeln! Ist der Hinox besiegt, so hinterlässt er wertvollen Loot, mehrere Waffen und die Kugel, die ihr in die Fassung legt, um den Schrein aus dem Boden zu holen. Im Inneren holt ihr nur noch das Zeichen der Bewährung ab.
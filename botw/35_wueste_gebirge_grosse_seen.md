
# Wüste, Gebirge und große Fee

Drei Schreine habt ihr geschafft – wir nähern uns mit großen Schritten dem
möglichen Finale des Spiels, wobei noch viele Dinge bleiben, die ihr suchen und
erforschen könnt. Mitunter jede Menge Schreine und Nebenaufgaben. Bevor wir uns
den Schreinen und Nebenaufgaben der Gerudos widmen, ist es strategisch
sinnvoll, das komplette Gebiet der Gerudos zu erkunden. Dazu gehört neben der
Wüste auch das angrenzende Gebirge im Norden, das auf der Karte schlicht als
„Region Gerudo-Turm“ bezeichnet wird und als eigener Abschnitt verzeichnet ist.

Das Gebirge erreicht ihr ganz einfach. Lauft einfach von der Gerudo-Stadt aus
nach Norden und haltet euch östlich vom Eingang zum Kalzer Tal. Nach einer
relativ ausgiebigen Kletterpartie erreicht ihr den Gerudo-Turm und staunt dabei
nicht schlecht, denn dieser Turm ist unglaublich groß. Viel zu groß um einfach
hoch zu kraxeln.

Es gibt einen anderen Weg hinauf: Schaut mal um den Turm herum, da führt ein
Gebirgspfad kreisförmig um den Turm herum. Wenn ihr ihm folgt, werdet ihr
einige Male in Kämpfe verwickelt, bei denen ihr tunlichst nicht in die Schlucht
hinabstürzen solltet. Weit oben angelangt erreicht ihr mehrere Gerüste mit
Plattformen. Nutzt euer Magnetmodul, um Gewichte auf entsprechende Waagen zu
legen, die euch hochfahren. Seid ihr dann ganz oben auf der Spitze angekommen,
springt und gleitet Richtung Turm, damit ihr nur noch wenige Meter nach oben
klettern müsst.

Wenn ihr die Karte aktiviert, werdet ihr feststellen, dass diese Gegend nicht
einfach zu erforschen ist. In der zerklüfteten Bergregion geht es ständig auf
und ab. Was ihr zudem noch nicht wisst: In manchen Gegenden habt ihr keinerlei
Einsicht auf die Karte, weil ein Sandsturm euch die Orientierung vernebeln
wird.

Wir raten euch, zuerst die Schreine der Wüstenregion zu knacken und
anschließend hier in die Berge zu gehen. Durch die Zeichen der Bewährung, die
ihr in der Wüste verdient, dürfte mindestens ein weiteres Ausdauer-Upgrade
herausspringen. Ausdauer wird hier sehr nützlich sein, also gönnt es euch.

Außerdem solltet ihr euch ein wenig mehr als 1000 Rubine für die dritte große
Fee ansparen. Die findet ihr im äußersten Südwesten der Hyrule Karte, in der
untersten linken Ecke der Wüste, direkt unterhalb eines großen Skeletts. Schaut
auf unsere Karten auf der nachfolgenden Schrein-Seite. Die dritte große Fee
führt abermals Upgrades an euren Rüstungen aus, sofern ihr passende Materialien
dabei habt.

Nebenbei: jede der Feen kann jeweils alle Upgrades ausführen, ihr müsst nur die
anderen Feen gefunden haben. Auch die Reihenfolge spielt keine Rolle – habt ihr
drei Feen gefunden, kann jede von ihnen drei Upgrades ausführen. Insgesamt gibt
es vier Feen, allerdings ist der Preis für die vierte mit 10.000 Rubinen
gewaltig hoch. Die Reihenfolgen der ihr die Feen freischaltet, ist euch
überlassen. Die Steigerung des Preises hängt somit nicht von der Örtlichkeit
ab, sondern von der Anzahl bereits gefundener Feen. Die erste will immer 100
Rubine, die zweite 500, die dritte 1000 und die letzte 10-000 Rubine.

